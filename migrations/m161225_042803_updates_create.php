<?php

use app\components\extend\Migration;

class m161225_042803_updates_create extends Migration
{

    public $tableName = '{{%updates}}';
    public $tableIndex = 'update-id-index';

    public function up()
    {
        $this->createTable($this->tableName, [
            'update' => $this->string()->comment('update id')->defaultValue('x')->comment('Record unique id'),
        ]);
        $this->createIndex($this->tableIndex, $this->tableName, 'update', true);
        $this->addPrimaryKey('updates_pk', $this->tableName, ['update']);
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }

}
