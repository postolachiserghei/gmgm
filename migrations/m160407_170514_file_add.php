<?php

use app\models\File;
use app\components\extend\Migration;

class m160407_170514_file_add extends Migration
{

    public $tableName = '{{%file}}';
    public $destinationTableName = '{{%file_destination}}';
    public $destinationIndex = '{{%dest_file_name_index}}';
    public $tableIndex = 'file_name_index';

    public function up()
    {
        $this->createTable($this->tableName, [
            'scheme' => $this->char(20)->notNull()->comment('Scheme'),
            'host' => $this->char(150)->notNull()->comment('File host'),
            'path' => $this->text()->notNull()->comment('File path'),
            'name' => $this->char(50)->notNull()->comment('File name'),
            'title' => $this->text()->notNull()->comment('File title'),
            'extension' => $this->char(25)->notNull()->comment('File extension'),
            'size' => $this->bigInteger()->comment('File size'),
            'mime' => $this->char(150)->notNull()->comment('File mime type'),
            'status' => $this->smallInteger()->defaultValue(File::STATUS_UPLOADED)->notNull()->comment('File status'),
            'location' => $this->smallInteger()->defaultValue(File::LOCATION_LOCAL)->notNull()->comment('File location'),
            'owner' => $this->char(50)->defaultValue(0)->comment('File owner'),
        ]);
        $this->createIndex($this->tableIndex, $this->tableName, 'name');
        $this->addPrimaryKey('files_pk', $this->tableName, ['name']);
        $this->createTable($this->destinationTableName, [
            'file_name' => $this->char(50)->notNull()->comment('File name'),
            'destination' => $this->char(100)->notNull()->comment('File destination'),
        ]);
        $this->addPrimaryKey('file_destination_pk', $this->destinationTableName, ['file_name']);

        $this->createIndex($this->destinationIndex, $this->destinationTableName, 'file_name');
    }

    public function down()
    {
        $this->dropIndex($this->tableIndex, $this->tableName);
        $this->dropTable($this->tableName);
        $this->dropIndex($this->destinationIndex, $this->destinationTableName);
        $this->dropTable($this->destinationTableName);
    }

}
