<?php

use app\components\extend\Migration;

class m160417_082909_search_add extends Migration
{

    public $tableNameValues = '{{%search_values}}';
    public $tableName = '{{%search}}';

    public function up()
    {
        $isMysql = ($this->db->driverName === 'mysql');
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'link' => $this->text()->notNull()->comment('link'),
            'model' => $this->char(200)->notNull()->comment('Model'),
            'language_id' => $this->char(10)->comment('Result language'),
            'params' => $this->json()->comment('additional data'),
        ]);
        $this->createTable($this->tableNameValues, [
            'search_id' => $this->integer()->notNull()->comment('Search id'),
            'value' => $this->text()->notNull()->comment('Search values'),
            'attribute' => $this->char(100)->comment('Searched attribute'),
                ], ($isMysql ? 'ENGINE=MyISAM' : null));
        $this->addPrimaryKey('search_values_pk', $this->tableNameValues, ['search_id', 'attribute']);
        if ($isMysql) {
            $this->execute("ALTER TABLE $this->tableNameValues ADD FULLTEXT INDEX `search_full_text` (`value`)");
        }
    }

    public function down()
    {
        $this->dropTable($this->tableNameValues);
        $this->dropTable($this->tableName);
    }

}
