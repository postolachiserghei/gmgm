<?php

use app\components\extend\Migration;

class m170515_061241_transactions extends Migration
{

    public $tableName = '{{%transactions}}';
    public $tableNameIndex = 'transactions_user_index';
    public $tableNameItemIndex = 'transactions_item_index';

    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey()->comment('id'),
            'amount' => $this->money()->defaultValue(0)->comment('amount'),
            'currency' => $this->string(5)->comment('currency'),
            'payment_system' => $this->integer()->defaultValue(0)->comment('payment system'),
            'user_id' => $this->char(50)->notNull()->comment('buyer'),
            'item_id' => $this->char(200)->notNull()->comment('item id'),
            'item_model' => $this->text()->notNull()->comment('item class'),
            'quantity' => $this->integer()->defaultValue(1)->comment('item class'),
            'status' => $this->smallInteger()->defaultValue(0)->comment('status'),
        ]);
        $this->createIndex($this->tableNameIndex, $this->tableName, 'user_id');
        $this->createIndex($this->tableNameItemIndex, $this->tableName, 'item_id');
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }

}
