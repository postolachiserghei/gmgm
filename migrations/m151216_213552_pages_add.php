<?php

use yii\db\Schema;
use app\components\extend\Migration;

class m151216_213552_pages_add extends Migration
{

    public $tableName = '{{%pages}}';
    public $tableNameT = '{{%pages_t}}';
    public $fk = 'pages_pages_t_fk';

    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey()->comment('pk'),
            'status' => $this->smallInteger()->comment('Status'),
        ]);
        $this->createTable($this->tableNameT, [
            'page_id' => $this->integer()->notNull()->comment('Page id'),
            'title' => $this->char(100)->comment('Page title'),
            'content' => $this->text()->comment('Page content'),
            'data' => $this->json()->comment('additional data'),
            'language_id' => $this->char(10)->comment('Page language'),
        ]);
        $this->addForeignKey($this->fk, $this->tableNameT, 'page_id', $this->tableName, 'id', 'CASCADE', 'CASCADE');
        $this->addPrimaryKey('pages_t_pk', $this->tableNameT, ['page_id', 'language_id']);
    }

    public function down()
    {
        $this->dropForeignKey($this->fk, $this->tableNameT);
        $this->dropTable($this->tableNameT);
        $this->dropTable($this->tableName);
    }

}
