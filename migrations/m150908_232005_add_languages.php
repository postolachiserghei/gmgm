<?php

use yii\db\Schema;
use app\components\extend\Migration;

class m150908_232005_add_languages extends Migration
{

    public $tableName = '{{%languages}}';

    public function up()
    {
        $this->createTable($this->tableName, [
            'language_id' => $this->char(10)->notNull()->comment('Language id'),
            'language_name' => $this->char(100)->notNull()->comment('Language name'),
            'language_active' => $this->boolean()->notNull()->defaultValue(true)->comment('Language is active'),
            'language_is_default' => $this->boolean()->notNull()->defaultValue(false)->comment('Language is default'),
        ]);

        $this->addPrimaryKey('language_pk', $this->tableName, 'language_id');

        $this->insert($this->tableName, [
            'language_id' => 'en',
            'language_name' => 'English',
            'language_active' => '1',
            'language_is_default' => '1',
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ]);
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }

}
