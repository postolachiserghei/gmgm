<?php

use yii\db\Schema;
use app\components\extend\Migration;

class m150830_141602_translation_tables_add extends Migration
{

    public $tableNameSource = '{{%i18n_message_source}}';
    public $tableNameMessage = '{{%i18n_message}}';
    public $fk = 'FK_i18n_message_i18n_message_source';

    public function up()
    {

        $this->createTable($this->tableNameSource, [
            'id' => $this->primaryKey()->comment('pk'),
            'message' => $this->text()->comment('Source message'),
            'category' => $this->string(32)->comment('Message category'),
        ]);
        $this->createTable($this->tableNameMessage, [
            'id' => $this->integer(11)->comment('Message source'),
            'language' => $this->string(16)->comment('Language'),
            'translation' => $this->text()->comment('Translated message'),
            'is_new' => $this->smallInteger()->defaultValue(1)->comment('Message is new'),
        ]);
        $this->addPrimaryKey('message_pk', $this->tableNameMessage, ['id', 'language']);
        $this->addForeignKey($this->fk, $this->tableNameMessage, 'id', $this->tableNameSource, 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey($this->fk, $this->tableNameMessage);
        $this->dropTable($this->tableNameMessage);
        $this->dropTable($this->tableNameSource);
    }

}
