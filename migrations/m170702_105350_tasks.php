<?php

use app\components\extend\Migration;

class m170702_105350_tasks extends Migration
{

    public $tableName = '{{%tasks}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey()->comment('id'),
            'object' => $this->text()->comment('class object'),
            'method' => $this->char(250)->comment('oject method'),
            'params' => $this->json()->comment('method params'),
            'start_time' => $this->integer()->defaultValue(0)->comment('task start after time'),
            'priority' => $this->smallInteger()->defaultValue(1)->comment('task priority'),
            'status' => $this->smallInteger()->defaultValue(1)->comment('task status'),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }

}
