<?php

use yii\db\Schema;
use app\components\extend\Migration;

class m151207_161341_menu_add extends Migration
{

    public $tableName = '{{%menu}}';
    public $tableNameT = '{{%menu_t}}';
    public $fk = 'menu_t_menu_fk';

    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey()->comment('pk'),
            'type' => $this->smallInteger(4)->defaultValue(1)->comment('Menu type'),
            'parent' => $this->integer()->defaultValue(0)->comment('Menu parent'),
            'active' => $this->integer()->defaultValue(1)->comment('Menu is active'),
            'visible' => $this->smallInteger(1)->defaultValue(1)->comment('Menu visibility'),
            'icon' => $this->char(100)->comment('Menu icon'),
        ]);

        $this->createTable($this->tableNameT, [
            'menu_id' => $this->integer()->notNull()->comment('Menu pk'),
            'title' => $this->char(250)->defaultValue('')->comment('Menu item title'),
            'url' => $this->text()->notNull()->comment('Menu item url'),
            'language_id' => $this->char(15)->defaultValue('en')->comment('Menu item language'),
        ]);
        $this->addForeignKey($this->fk, $this->tableNameT, 'menu_id', $this->tableName, 'id', 'CASCADE', 'CASCADE');
        $this->addPrimaryKey('menu_t_pk', $this->tableNameT, ['menu_id', 'language_id']);
    }

    public function down()
    {
        $this->dropForeignKey($this->fk, $this->tableNameT);
        $this->dropTable($this->tableNameT);
        $this->dropTable($this->tableName);
    }

}
