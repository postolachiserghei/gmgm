<?php

use app\components\extend\Migration;

class m170615_203811_shopping_cart_add extends Migration
{

    public $tableName = '{{%shopping_cart}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey()->comment('id'),
            'status' => $this->smallInteger()->defaultValue(1)->comment('Status')
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }

}
