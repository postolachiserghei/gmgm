<?php

use yii\db\Schema;
use app\components\extend\Migration;
use app\models\User;
use app\components\helper\Helper;

class m130524_201442_user extends Migration
{

    public $tableName = '{{%user}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey()->comment('pk'),
            'username' => $this->char(255)->notNull()->comment('username'),
            'auth_key' => $this->char(32)->notNull()->comment('auth key'),
            'password_hash' => $this->char(255)->notNull()->comment('Password hash'),
            'password_reset_token' => $this->char(255)->comment('Password reset token'),
            'email' => $this->char(250)->comment('Email'),
            'role' => $this->smallInteger()->notNull()->defaultValue(10)->comment('Role'),
            'status' => $this->smallInteger()->notNull()->defaultValue(10)->comment('Status'),
            'avatar' => $this->char(50)->defaultExpression('null')->comment('Password reset token'),
                ], $tableOptions);
        $email = Helper::data()->getParam('adminEmail', 'tech@it-init.com');


//        admin user : 
//        login: admin 
//        password: 123qwe
        $this->insert($this->tableName, [
            'username' => 'admin',
            'auth_key' => 'issaMQsFw3RgS6go5bkZPmsAj3lo8_MI',
            'password_hash' => '$2y$13$.rfT16Jlq0ShfPAdIeU0wuojdjpFf4FEdtSBFVjbdcTB8G4RULtZ6',
            'password_reset_token' => 'NULL',
            'email' => $email,
            'role' => User::ROLE_ADMIN,
            'status' => User::STATUS_ACTIVE,
            'created_at' => date('U'),
            'updated_at' => date('U'),
        ]);
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }

}
