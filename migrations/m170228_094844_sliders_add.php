<?php

use app\components\extend\Migration;

class m170228_094844_sliders_add extends Migration
{

    public $tableName = '{{%slider}}';
    public $tableNameT = '{{%slider_t}}';
    public $tableNameTFk = 'slider_t_slider_fk';

    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey()->comment('id'),
            'type' => $this->smallInteger()->defaultValue(1)->comment('type'),
            'status' => $this->smallInteger()->defaultValue(1)->comment('status'),
        ]);
        $this->createTable($this->tableNameT, [
            'id' => $this->integer()->notNull()->comment('slider'),
            'title' => $this->char(250)->notNull()->comment('title'),
            'language_id' => $this->char(10)->notNull()->comment('language'),
        ]);

        $this->addPrimaryKey('slidder_t_pk', $this->tableNameT, ['id', 'language_id']);
        $this->addForeignKey($this->tableNameTFk, $this->tableNameT, 'id', $this->tableName, 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey($this->tableNameTFk, $this->tableNameT);
        $this->dropTable($this->tableName);
        $this->dropTable($this->tableNameT);
    }

}
