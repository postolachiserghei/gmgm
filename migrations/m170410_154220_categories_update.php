<?php

use app\components\extend\Migration;

class m170410_154220_categories_update extends Migration
{

    public $tableName = '{{%categories}}';
    public $serverTableName = '{{%servers}}';
    public $serverTableNameT = '{{%servers_t}}';
    public $serverGameRelTableName = '{{%game_servers}}';
    public $serverGameRelFK = 'game_server_rel_fk';
    public $serverServerRelFK = 'server_game_server_rel_fk';
    public $serverTFK = 'server_server_t_fk';
    public $categoryLotRelTableName = '{{%lot_type_category_rel}}';
    public $categoryLotRelFK = 'category_lot_type_fk';

    public function up()
    {
        $this->addColumn($this->tableName, 'show_in_top', $this->boolean()->defaultValue(false)->comment('show in top'));

        $this->createTable($this->categoryLotRelTableName, [
            'category_id' => $this->integer()->comment('game id'),
            'lot_type_id' => $this->integer()->comment('lot type id'),
        ]);
        $this->addForeignKey($this->categoryLotRelFK, $this->categoryLotRelTableName, 'category_id', $this->tableName, 'id', self::ON_D_U_CASCADE, self::ON_D_U_CASCADE);

        /**
         * servers
         */
        $this->createTable($this->serverTableName, [
            'id' => $this->primaryKey()->comment('server id'),
            'category_id' => $this->integer()->defaultValue(null)->comment('game id'),
            'url' => $this->text()->comment('url'),
        ]);
        $this->createTable($this->serverTableNameT, [
            'server_id' => $this->integer()->comment('server id'),
            'title' => $this->text()->comment('url'),
            'language_id' => $this->char(10)->notNull()->comment('language'),
        ]);
        $this->addPrimaryKey('server_t_pk', $this->serverTableNameT, ['server_id', 'language_id']);
        $this->addForeignKey($this->serverTFK, $this->serverTableNameT, 'server_id', $this->serverTableName, 'id', self::ON_D_U_CASCADE, self::ON_D_U_CASCADE);
        $this->createTable($this->serverGameRelTableName, [
            'category_id' => $this->integer()->comment('game id'),
            'server_id' => $this->integer()->comment('server id'),
        ]);
        $this->addForeignKey($this->serverGameRelFK, $this->serverGameRelTableName, 'category_id', $this->tableName, 'id', self::ON_D_U_CASCADE, self::ON_D_U_CASCADE);
        $this->addForeignKey($this->serverServerRelFK, $this->serverGameRelTableName, 'server_id', $this->serverTableName, 'id', self::ON_D_U_CASCADE, self::ON_D_U_CASCADE);
        $this->addPrimaryKey('server_game_rel_pk', $this->serverGameRelTableName, ['category_id', 'server_id']);
        /**
         * servers end
         */
    }

    public function down()
    {
        $this->dropColumn($this->tableName, 'show_in_top');
        $this->dropForeignKey($this->serverTFK, $this->serverTableNameT);
        $this->dropForeignKey($this->serverGameRelFK, $this->serverGameRelTableName);
        $this->dropForeignKey($this->serverServerRelFK, $this->serverGameRelTableName);
        $this->dropForeignKey($this->categoryLotRelFK, $this->categoryLotRelTableName);
        $this->dropTable($this->categoryLotRelTableName);
        $this->dropTable($this->serverGameRelTableName);
        $this->dropTable($this->serverTableName);
        $this->dropTable($this->serverTableNameT);
    }

}
