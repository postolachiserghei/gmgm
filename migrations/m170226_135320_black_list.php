<?php

use app\components\extend\Migration;

class m170226_135320_black_list extends Migration
{

    public $tableName = '{{%black_list}}';

    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey()->comment('id'),
            'value' => $this->string(20)->notNull()->comment('value'),
            'type' => $this->smallInteger()->notNull()->defaultValue(1)->comment('type'),
        ]);
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }

}
