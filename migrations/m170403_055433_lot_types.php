<?php

use app\components\extend\Migration;

class m170403_055433_lot_types extends Migration
{

    public $tableName = '{{%lot_types}}';
    public $tableNameT = '{{%lot_types_t}}';
    public $fk = 'lot_types_lot_types_t_fk';

    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey()->comment('pk'),
            'status' => $this->smallInteger()->comment('Status'),
        ]);
        $this->createTable($this->tableNameT, [
            'lot_id' => $this->integer()->notNull()->comment('Lot id'),
            'title' => $this->char(100)->comment('Lot title'),
            'language_id' => $this->char(10)->comment('Lot language'),
        ]);
        $this->addForeignKey($this->fk, $this->tableNameT, 'lot_id', $this->tableName, 'id', 'CASCADE', 'CASCADE');
        $this->addPrimaryKey('lot_types_t_pk', $this->tableNameT, ['lot_id', 'language_id']);
    }

    public function down()
    {
        $this->dropForeignKey($this->fk, $this->tableNameT);
        $this->dropTable($this->tableNameT);
        $this->dropTable($this->tableName);
    }

}
