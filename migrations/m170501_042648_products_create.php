<?php

use app\components\extend\Migration;

class m170501_042648_products_create extends Migration
{

    public $tableName = '{{%products}}';
    public $tableNameT = '{{%products_t}}';
    public $tableNameTFk = 'products_t_products_fk';
    public $tableNameCategoryTFk = 'category_products_fk';
    public $categoryTableName = '{{%categories}}';
    public $tableNameOwnerFk = 'products_user_fk';
    public $tableNameOwner = '{{%user}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey()->comment('id'),
            'category_id' => $this->integer()->defaultValue(1)->comment('category'),
            'type' => $this->integer()->comment('type'),
            'available' => $this->integer()->defaultValue(1)->comment('how many items are available'),
            'price' => $this->money()->notNull()->comment('price'),
            'owner_id' => $this->integer()->comment('owner'),
            'status' => $this->smallInteger()->defaultValue(1)->comment('status'),
        ]);
        $this->createTable($this->tableNameT, [
            'id' => $this->integer()->notNull()->comment('product'),
            'title' => $this->char(250)->notNull()->comment('title'),
            'description' => $this->text()->notNull()->comment('description'),
            'image' => $this->char(50)->notNull()->comment('image'),
            'language_id' => $this->char(10)->notNull()->comment('language'),
        ]);

        $this->addPrimaryKey('products_t_pk', $this->tableNameT, ['id', 'language_id']);
        $this->addForeignKey($this->tableNameTFk, $this->tableNameT, 'id', $this->tableName, 'id', self::ON_D_U_CASCADE, self::ON_D_U_CASCADE);
        $this->addForeignKey($this->tableNameOwnerFk, $this->tableName, 'owner_id', $this->tableNameOwner, 'id', self::ON_D_U_CASCADE, self::ON_D_U_CASCADE);
        $this->addForeignKey($this->tableNameCategoryTFk, $this->tableName, 'category_id', $this->categoryTableName, 'id', self::ON_D_U_CASCADE, self::ON_D_U_CASCADE);
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableNameOwnerFk, $this->tableName);
        $this->dropForeignKey($this->tableNameTFk, $this->tableNameT);
        $this->dropForeignKey($this->tableNameCategoryTFk, $this->tableName);
        $this->dropTable($this->tableName);
        $this->dropTable($this->tableNameT);
    }

}
