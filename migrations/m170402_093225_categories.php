<?php

use app\components\extend\Migration;

class m170402_093225_categories extends Migration
{

    public $tableName = '{{%categories}}';
    public $tableNameT = '{{%categories_t}}';
    public $fk = 'categories_t_categories_fk';

    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey()->comment('pk'),
            'type' => $this->smallInteger(4)->defaultValue(1)->comment('Category type'),
            'parent' => $this->integer()->defaultValue(0)->comment('Category parent'),
            'status' => $this->integer()->defaultValue(1)->comment('Category status'),
        ]);

        $this->createTable($this->tableNameT, [
            'category_id' => $this->integer()->notNull()->comment('Category pk'),
            'title' => $this->char(250)->defaultValue('')->comment('Category item title'),
            'language_id' => $this->char(15)->defaultValue('en')->comment('Category item language'),
        ]);
        $this->addForeignKey($this->fk, $this->tableNameT, 'category_id', $this->tableName, 'id', 'CASCADE', 'CASCADE');
        $this->addPrimaryKey('category_t_pk', $this->tableNameT, ['category_id', 'language_id']);
    }

    public function down()
    {
        $this->dropForeignKey($this->fk, $this->tableNameT);
        $this->dropTable($this->tableNameT);
        $this->dropTable($this->tableName);
    }

}
