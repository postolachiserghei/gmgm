<?php

use yii\db\Schema;
use app\components\extend\Migration;

class m160104_174111_settings_add extends Migration
{

    public $tableName = '{{%settings}}';

    public function up()
    {
        $this->createTable($this->tableName, [
            'key' => $this->char(250)->defaultValue('settings')->comment('Setting key'),
            'value' => $this->text()->comment('Setting key value'),
            'model' => $this->char(250)->defaultValue('settings')->comment('Model'),
        ]);

        $this->addPrimaryKey('settings_pk', $this->tableName, ['model', 'key']);
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }

}
