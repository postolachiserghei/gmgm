<?php

use app\components\extend\Migration;

class m170211_190618_recycle_bin extends Migration
{

    public $tableName = '{{%recycle_bin}}';

    public function up()
    {
        $this->createTable($this->tableName, [
            'model' => $this->char(255)->notNull()->comment('Related model'),
            'model_pk' => $this->char(255)->notNull()->comment('Related model primary key'),
        ]);
        $this->createIndex('recycle_bin_model_k', $this->tableName, 'model');
        $this->addPrimaryKey('rpk', $this->tableName, ['model', 'model_pk']);
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }

}
