<?php

use app\components\extend\Migration;

class m170319_104811_chat_create extends Migration
{

    public $tableNameChat = '{{%chat}}';
    public $tableNameChatMembers = '{{%chat_members}}';
    public $tableNameChatMessages = '{{%chat_messages}}';
    public $tableNameChatContacts = '{{%chat_contacts}}';
    public $tableNameUsers = '{{%user}}';
    public $chatMessFk = 'chat_message_fk';
    public $chatMemberFk = 'chat_member_fk';
    public $chatContactsUserFk = 'chat_user_fk';
    public $chatContactsContactFk = 'chat_user_contact_fk';
    public $mesMemberFk = 'chat_mess_member_fk';
    public $contactsPk = 'chat_contacts_pk';

    public function up()
    {
        $this->createTable($this->tableNameChat, [
            'id' => $this->bigPrimaryKey()->comment('id'),
            'subject' => $this->char(200)->comment('subject'),
        ]);
        $this->createTable($this->tableNameChatMembers, [
            'id' => $this->bigPrimaryKey()->comment('id'),
            'chat_id' => $this->bigInteger()->defaultExpression('null')->comment('chat'),
            'user_id' => $this->char(50)->notNull()->comment('user'),
            'name' => $this->char(200)->notNull()->comment('name'),
        ]);
        $this->createTable($this->tableNameChatMessages, [
            'id' => $this->bigPrimaryKey()->comment('id'),
            'chat_id' => $this->bigInteger()->notNull()->comment('chat'),
            'sender_id' => $this->bigInteger()->notNull()->comment('sender'),
            'message' => $this->text()->notNull()->comment('message'),
            'status' => $this->smallInteger()->defaultValue(1)->comment('status'),
        ]);
        $this->createTable($this->tableNameChatContacts, [
            'user_id' => $this->integer()->notNull()->comment('user_id'),
            'contact_id' => $this->integer()->notNull()->comment('contact user id'),
            'status' => $this->smallInteger()->defaultValue(1)->comment('status'),
        ]);

        $this->addPrimaryKey($this->contactsPk, $this->tableNameChatContacts, ['user_id', 'contact_id']);

        $this->addForeignKey($this->chatContactsUserFk, $this->tableNameChatContacts, 'user_id', $this->tableNameUsers, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey($this->chatContactsContactFk, $this->tableNameChatContacts, 'contact_id', $this->tableNameUsers, 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey($this->chatMemberFk, $this->tableNameChatMembers, 'chat_id', $this->tableNameChat, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey($this->mesMemberFk, $this->tableNameChatMessages, 'sender_id', $this->tableNameChatMembers, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey($this->chatMessFk, $this->tableNameChatMessages, 'chat_id', $this->tableNameChat, 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey($this->chatMessFk, $this->tableNameChatMessages);
        $this->dropForeignKey($this->chatContactsUserFk, $this->tableNameChatContacts);
        $this->dropForeignKey($this->chatContactsContactFk, $this->tableNameChatContacts);
        $this->dropForeignKey($this->mesMemberFk, $this->tableNameChatMessages);
        $this->dropForeignKey($this->chatMemberFk, $this->tableNameChatMembers);
        $this->dropTable($this->tableNameChat);
        $this->dropTable($this->tableNameChatMembers);
        $this->dropTable($this->tableNameChatMessages);
        $this->dropTable($this->tableNameChatContacts);
    }

}
