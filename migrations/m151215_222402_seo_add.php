<?php

use yii\db\Schema;
use app\components\extend\Migration;

class m151215_222402_seo_add extends Migration
{

    public $tableName = '{{%seo}}';
    public $tableNameT = '{{%seo_t}}';
    public $fk = 'seo_seo_t_fk';

    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey()->comment('pk'),
            'url' => $this->text()->comment('Url'),
        ]);
        $this->createTable($this->tableNameT, [
            'seo_id' => $this->integer()->notNull()->comment('Seo id'),
            'alias' => $this->text()->notNull()->comment('Seo alias'),
            'title' => $this->char(100)->comment('Seo title'),
            'h1' => $this->text()->comment('Seo h1'),
            'keywords' => $this->text()->comment('Seo keywords'),
            'description' => $this->text()->comment('Seo description'),
            'language_id' => $this->char(10)->comment('Seo language'),
        ]);
        $this->addForeignKey($this->fk, $this->tableNameT, 'seo_id', $this->tableName, 'id', 'CASCADE', 'CASCADE');
        $this->addPrimaryKey('seo_t_pk', $this->tableNameT, ['seo_id', 'language_id']);
    }

    public function down()
    {
        $this->dropForeignKey($this->fk, $this->tableNameT);
        $this->dropTable($this->tableNameT);
        $this->dropTable($this->tableName);
    }

}
