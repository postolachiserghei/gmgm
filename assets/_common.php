<?php

$plugins = [
    'js' => [
//        'public/plugins/redactor/redactor.js',
//        'public/plugins/redactor/plugins/source.js',
//        'public/plugins/redactor/plugins/fullscreen.js',
        'public/plugins/ckeditor/ckeditor.js',
        'public/plugins/ckeditor/samples/js/sample.js',
        'public/plugins/shape/shape.min.js',
        'public/plugins/md5.min.js',
        'public/plugins/bootbox.min.js',
        'public/plugins/bootstrap-notify.min.js',
        'public/plugins/swiper/swiper.min.js',
        'public/plugins/yii/yii.js',
    ],
    'css' => [
//        'public/plugins/redactor/redactor.css',
        'public/plugins/animate/animate.less',
        'public/plugins/shape/shape.min.css',
        'public/plugins/swiper/swiper.min.css',
    ]
];

$common = [
    'js' => array_merge($plugins['js'], [
        'public/js/keyEvents.js',
        'public/js/common.js',
        'public/js/languages.js',
        'public/js/chat.js',
    ]),
    'css' => array_merge($plugins['css'], [
        'public/css/common.less',
        'public/css/chat.less'
    ])
];

return $common;
