<?php

namespace app\assets;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FrontendAssets extends Asset
{

    public $css = [
        'public/css/frontend/default.css'
    ];
    public $js = [
        'public/js/frontend/default.js',
    ];

}
