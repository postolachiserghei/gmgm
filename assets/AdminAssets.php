<?php

namespace app\assets;

use app\components\extend\yii;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAssets extends Asset
{

    public $css = [
        'public/css/admin/default.less'
    ];
    public $js = [
        'public/js/common.js',
        'public/js/admin/default.js',
        'public/js/admin/updates.js'
    ];

}
