<?php

namespace app\assets;

use yii\web\AssetBundle;
use app\components\extend\yii;
use app\components\extend\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Asset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $moduleId;
    public $themeName;
    public $jsOptions = ['position' => View::POS_END];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        $common = require __DIR__ . '/_common.php';
        $this->js = array_merge($common['js'], $this->js);
        $this->css = array_merge($common['css'], $this->css);
        $this->publishOptions['forceCopy'] = true;
        if (is_a(Yii::$app, 'yii\web\Application')) {
            $this->moduleId = yii::$app->id;
        }
        if (is_a(yii::$app, 'yii\web\Application') && yii::$app->view->theme) {
            $this->themeName = yii::$app->view->theme->themeName;
        }
        $this->initDefaultJs();
        return parent::init();
    }

    /**
     * initialize javascript code
     */
    public function initDefaultJs()
    {
        if (!yii::$app->request->isConsoleRequest) {
            $host = \app\components\helper\Helper::settings()::getValue('tld', 'localhost');
            $tld = (substr($host, 0, strpos($host, ":")));
            $js = 'App.YII_ENV_DEV = ' . (YII_ENV_DEV ? 'true' : 'false') . ';';
            $js .= 'App.user.id = "' . (yii::$app->user->id) . '";';
            $js .= 'Socket.port = ' . (yii::$app->params['socketPort']) . ';';
            $js .= 'Socket.host = "ws://' . str_replace(':', '', ($tld != '' ? $tld : $host)) . '";';
            $js .= 'Socket.init(Socket.connection);';
            yii::$app->view->registerJs($js, View::POS_LOAD);
        }
    }

}
