<?php

/**
 * Configuration file for the "yii asset" console command.
 * ./yii asset assets/exec/minify_front_gmgm.php assets/min/frontend_gmgm.php
 */
// In the console environment, some path aliases may not exist. Please define these:
Yii::setAlias('@webroot', __DIR__ . '/../../web');
Yii::setAlias('@web', '/../../web');

return [
    // Adjust command/callback for JavaScript files compressing:
    'jsCompressor' => 'java -jar compiler.jar --js {from} --js_output_file {to}',
    // Adjust command/callback for CSS files compressing:
    'cssCompressor' => 'java -jar yuicompressor.jar --type css {from} -o {to}',
    // The list of asset bundles to compress:
    'bundles' => [
        'app\assets\GmgmarketAssets',
    ],
    // Asset bundle for compression output:
    'targets' => [
        'front_gmgm' => [
            'class' => 'yii\web\AssetBundle',
            'basePath' => '@webroot/public/gmgm/assets',
            'baseUrl' => '@web/public/gmgm/assets',
            'js' => 'all-gmgmarket-front.js',
            'css' => 'all-gmgmarket-front.css',
        ],
    ],
    // Asset manager configuration:
    'assetManager' => [
        'basePath' => '@webroot/public/gmgm/assets',
        'baseUrl' => '@web/public/gmgm/assets',
    ],
];
