<?php

namespace app\assets;

use app\components\extend\yii;

class GmgmarketAssets extends Asset
{

    public $js = [
        '/public/gmgm/js/owl.carousel.min.js',
        '/public/gmgm/js/main.js',
        '/public/js/shopping-cart.js',
    ];
    public $css = [
        '/public/gmgm/css/animate.css',
        '/public/gmgm/css/normalize.css',
        '/public/gmgm/css/owl.carousel.css',
        '/public/gmgm/css/owl.theme.css',
        '/public/gmgm/css/fonts.css',
        '/public/gmgm/css/icons.css',
        '/public/gmgm/css/style.css',
        '/public/gmgm/css/media.css',
        '/public/gmgm/css/custom.less',
    ];

}
