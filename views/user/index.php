<?php

use app\models\Menu;
use app\components\extend\yii;
use app\components\widgets\menu\MenuWidget;

/* @var $model app\models\User */

$this->title = yii::$app->l->t('profile', ['update' => false]);
$this->params['breadcrumbs'][] = yii::$app->l->t('profile');
?>