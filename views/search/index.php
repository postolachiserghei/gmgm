<?php

/* @var $this yii\web\View */

$this->title = yii::$app->l->t('search', ['update' => false]);
?>

<?= \app\components\widgets\search\SearchWidget::widget(['view' => 'results']); ?>

