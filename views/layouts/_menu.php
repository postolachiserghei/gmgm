<?php

use app\models\Menu;
use app\components\extend\Nav;
use app\components\extend\Html;
use app\components\extend\NavBar;
use app\components\widgets\chat\ChatWidget;

NavBar::begin([
    'brandLabel' => yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'main-top-nav navbar-inverse navbar-fixed-top',
    ],
]);
echo \app\components\widgets\search\SearchWidget::widget();
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right nav'],
    'items' => [
        ['label' => Html::tag('span', '', ['class' => 'count-chat-message text-success pa badge badge-success']), 'url' => '', 'linkOptions' => [
                'icon' => 'envelope',
                'data-pjax' => 0,
                'onclick' => 'Chat.drawConversations(null,true);return false;'
            ]
        ]
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right nav'],
    'items' => yii::$app->l->menuLanguages,
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right nav'],
    'items' => Menu::getMenuArray(Menu::TYPE_MAIN),
]);
NavBar::end();
echo ChatWidget::widget();
