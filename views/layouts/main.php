<?php

use app\assets\FrontendAssets;
use app\components\extend\Html;
use app\components\extend\Pjax;

/* @var $this \yii\web\View */
/* @var $content string */
FrontendAssets::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode(yii::$app->helper->data()->getParam('pageTitle', $this->title)) ?></title>
        <?php $this->head() ?>
    </head>
    <body data-notification-top-margin="71">
        <?php
        $this->beginBody();
        if (yii::$app->controller->isPjaxAction) {
            echo $this->render('_loading');
            Pjax::begin();
        }
        ?>
        <div class="wrap page-container">
            <?= $this->render('_menu') ?>
            <div class="container">
                <?= $this->render('_page_header') ?>
                <?= $content ?>
            </div>
        </div>
        <?php
        if (yii::$app->controller->isPjaxAction) {
            Pjax::end();
        }
        ?>
        <?= YII_ENV_PROD ? $this->render('_analytics') : '<div class="text-danger test-pjax-status"></div>'; ?>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage(); ?>