<?php

use yii\bootstrap\Modal;
use app\components\extend\Html;
use app\components\extend\ActiveForm;
use yii\captcha\Captcha;

$model = (new \app\models\forms\ContactForm());
?>

<?php
Modal::begin([
    'header' => Html::tag('h3', 'Hello'),
    'size' => Modal::SIZE_LARGE,
    'toggleButton' => [
        'tag' => 'div',
        'label' => 'Lorem ipsum',
        'class' => 'btn btn-default btn-block fs-2'
    ],
]);
?>

<div class="concept-info" onclick="yii.mes('sdsdfsdf')">
    <div class="row">
        <div class="col-sm-6 col-md-4 text-center">
            <div class="thumbnail">
                <div class="caption">
                    <i class="fa fa-code"></i>                     <hr>
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing.
                    </p>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 text-center">
            <div class="thumbnail">
                <div class="caption">
                    <i class="fa fa-tv"></i>                     <hr>
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing.
                    </p>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 text-center">
            <div class="thumbnail">
                <div class="caption">
                    <i class="fa fa-rocket"></i>                     <hr>
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing.
                    </p>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::end(); ?>