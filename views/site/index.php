<?php
/* @var $this yii\web\View */
/* @var $newStores[] app\models\User */
/* @var $newProducts[] app\models\Products */

$this->title = yii::$app->name;

use app\models\Slider;
use app\components\widgets\slider\SliderWidget;

$this->params['pageHeader'] = '';
?>


<?= $this->render('index/_games_catalog_mobile'); ?>
<?= $this->render('index/_top_games'); ?>
<?= $this->render('index/_how_to'); ?>
<section class="mp-store-wrap">
    <div class="container">
        <div class="row">
            <?= $this->render('index/_games_catalog'); ?>
            <div class="col-xs-12 col-sm-6 col-md-8 col-lg-9">
                <div class="mp-store-slider-wrap ">
                    <?= SliderWidget::widget(['type' => Slider::TYPE_ADS]) ?>
                    <?= SliderWidget::widget(['type' => Slider::TYPE_STORE]) ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mp-substore-wrap hidden-xs hidden-sm">
    <div class="container">
        <div class="row">
            <?= $this->render('index/_reviews'); ?>
            <?= $this->render('index/_new_products', ['newProducts' => $newProducts]); ?>
            <?= $this->render('index/_new_stores', ['newStores' => $newStores]); ?>
        </div>
    </div>
</section>