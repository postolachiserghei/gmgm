<?php

use app\components\extend\Url;
use app\components\extend\Html;
use app\components\extend\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model \app\models\forms\SignupForm */

$this->title = yii::$app->l->t('РЕГИСТРАЦИЯ');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="registration-form-wrap">
    <div class="reg-page-title">
        <h2>
            <?= Html::encode($this->title) ?>
        </h2>
    </div>
    <div class="reg-page-tab">
        <p>
            <?= yii::$app->l->t('Выберите кем вы хотите стать на сайте - покупателем или продавцом') ?>
        </p>
        <?php
        foreach ($model->availableRoles as $key => $role) {
            echo Html::a(yii::$app->l->t($role), Url::to(['signup', 'role' => $role]), [
                'data' => [
                    'rb' => $model->rbacRole
                ],
                'class' => 'reg-page-tab-item ' .
                ($model->rbacRole == $role ? 'active' : '')
            ]);
        }
        ?>

    </div>
    <div class="reg-page-form">
        <?php $form = ActiveForm::begin(['options' => ['class' => 'reg-page-form']]); ?>
        <?= $form->field($model, 'email'); ?>
        <?= $form->field($model, 'username'); ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'passwordRepeat')->passwordInput() ?>
        <div class="dark-inputs">
            <div class="no-required <?= $model->rbacRole == User::ROLE_SITE_STORE ? '' : 'hidden' ?>">
                <?php
                if ($model->rbacRole == User::ROLE_SITE_STORE) {
                    echo $form->field($model, User::ADDITIONAL_DATA_FIO);
                }
                ?>
            </div>
            <div class="no-required">
                <?= $form->field($model, User::ADDITIONAL_DATA_PHONE) ?>
            </div>
        </div>
        <?= Html::input('submit', null, yii::$app->l->t('зарегистрироваться')) ?>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="reg-page-terms">
        <p>
            <?=
            yii::$app->l->t('Нажимая на Зергистрироватсья Вы принимаете {rules} использования сайта и {privacy-policy}', [
                'rules' => Html::a(yii::$app->l->t('условия', ['lcf' => true]), Url::to(['/rules'])),
                'privacy-policy' => Html::a(yii::$app->l->t('политику конфиденциальности', ['lcf' => true]), Url::to(['/privacy-policy']))
            ])
            ?>
        </p>
    </div>
</div>