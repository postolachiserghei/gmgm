<?php

use app\components\extend\Html;
use app\components\extend\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

$this->title = yii::$app->l->t('сброс пароля', ['update' => false]);
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="registration-form-wrap">
    <div class="reg-page-title">
        <h2>
            <?= Html::encode($this->title) ?>
        </h2>
    </div>
    <?php
    $form = ActiveForm::begin([
                'id' => 'request-password-reset-form', 'options' => [
                    'class' => 'reg-page-form'
    ]]);
    ?>
    <?= $form->field($model, 'email') ?>
    <?= Html::input('submit', null, yii::$app->l->t('отправить')) ?>
    <?php ActiveForm::end(); ?>

</div>
