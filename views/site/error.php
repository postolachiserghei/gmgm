<?php

use app\components\extend\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>



<div class="site-error" style="min-height: 300px">
    <h2 class="text-muted text-center">
        <?= nl2br(Html::encode($message)) ?>
    </h2>

    <i class="fa fa-exclamation-circle" style="display: block;
       margin: 50px auto;
       font-size: 10em;
       text-align: center;
       color: #adadad!important;
       "></i>


    <p class="text-center">
        The above error occurred while the Web server was processing your request.
    </p>
    <p class="text-center">
        Please contact us if you think this is a server error. Thank you.
    </p>
</div>