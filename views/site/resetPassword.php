<?php

use app\components\extend\Html;
use app\components\extend\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\forms\ResetPasswordForm */

$this->title = yii::$app->l->t('Reset password');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="registration-form-wrap">
    <div class="reg-page-title">
        <h2>
            <?= Html::encode($this->title) ?>
        </h2>
    </div>
    <p><?= yii::$app->l->t('Please choose your new password') ?>:</p>
    <?php
    $form = ActiveForm::begin([
                'id' => 'reset-password-form',
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
                'options' => ['class' => 'reg-page-form']
    ]);
    ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= Html::input('submit', null, yii::$app->l->t('изменить пароль')) ?>
    <?php ActiveForm::end(); ?>
</div>
