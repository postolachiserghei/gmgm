<?php

use app\models\Menu;
use app\components\widgets\menu\MenuWidget;
?>
<section class="mp-how-to-wrap">
    <div class="container">
        <div class="row">
            <?= MenuWidget::widget(['type' => Menu::TYPE_HOW_TO]) ?>
        </div>
    </div>
</section>
