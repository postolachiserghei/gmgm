<?php

use app\components\extend\Url;
use app\components\extend\Html;

/* @var $this yii\web\View */
/* @var $newProducts[] app\models\Products */
?>

<div class="col-xs-12 col-sm-6 col-md-3">
    <div class="mp-substore-newlots-wrap">
        <div class="mp-orange-title">
            <?= Html::tag('h2', yii::$app->l->t('Новые лоты')) ?>
        </div>
        <?php foreach ($newProducts as $newProduct) : ?>
            <?= Html::beginTag('a', ['href' => Url::to(['/game/view', 'id' => $newProduct->category_id])]) ?>
            <div class="mp-substore-newlots-item">
                <?=
                $newProduct->renderImage([
                    'width' => '215',
                    'height' => '192',
                ])
                ?>
                <div class="mp-substore-newlots-info">
                    <div class="mp-substore-newlots-info-left">
                        <h3>
                            <?= $newProduct->title ?>
                        </h3>
                        <span>
                            <?= $newProduct->user->fullName; ?>
                        </span>
                    </div>
                    <div class="mp-substore-newlots-info-right">
                        <div class="price">
                            <?= $newProduct->getPrice(false, true) ?>
                            <span class="unit">
                                <?= $newProduct->getCurrencySign(); ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <?= Html::endTag('a') ?>
        <?php endforeach; ?>
    </div>
</div>