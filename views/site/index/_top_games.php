<?php

use app\models\Slider;
use app\components\widgets\games\GamesWidget;
use app\components\widgets\slider\SliderWidget;
?>


<section class="mp-top-news-wrap hidden-xs hidden-sm">
    <div class="container">
        <div class="row">
            <?= SliderWidget::widget(['type' => Slider::TYPE_MAIN]); ?>
            <?= GamesWidget::widget(['type' => GamesWidget::TYPE_TOP]); ?>
        </div>
    </div>
</section>
