
<div class="col-xs-12 col-sm-12 col-md-6">
    <div class="mp-substore-review-wrap">
        <div class="mp-orange-title">
            <h2>
                Последние отзывы
            </h2>
        </div>
        <div class="mp-substore-review-item">
            <div class="mp-substore-review-ava">
                <div class="user-avatar" style="background-image: url(/public/gmgm/img/user.png)"></div>
            </div>
            <div class="mp-substore-review-text">
                <div class="mp-substore-review-title">
                    <span>
                        Игра:
                    </span>
                    <a href="#">
                        Линейка
                    </a>
                    <span>
                        Куплено:
                    </span>
                    <strong>
                        Fuse Boost
                    </strong>
                </div>
                <div class="mp-substore-review-info">
                    <span>
                        Продавец:
                    </span>
                    <a href="#">
                        Nickman
                    </a>
                    <span class="saler-rates-wrap">
                        <i class="icn icn-star-full"></i>
                        <i class="icn icn-star-full"></i>
                        <i class="icn icn-star-full"></i>
                        <i class="icn icn-star-empty"></i>
                        <i class="icn icn-star-empty"></i>
                    </span>
                </div>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                </p>
            </div>
        </div>
        <div class="mp-substore-review-item">
            <div class="mp-substore-review-ava">
                <div class="user-avatar" style="background-image: url(/public/gmgm/img/user.png)"></div>
            </div>
            <div class="mp-substore-review-text">
                <div class="mp-substore-review-title">
                    <span>
                        Игра:
                    </span>
                    <a href="#">
                        Линейка
                    </a>
                    <span>
                        Куплено:
                    </span>
                    <strong>
                        Fuse Boost
                    </strong>
                </div>
                <div class="mp-substore-review-info">
                    <span>
                        Продавец:
                    </span>
                    <a href="#">
                        Nickman
                    </a>
                    <span class="saler-rates-wrap">
                        <i class="icn icn-star-full"></i>
                        <i class="icn icn-star-full"></i>
                        <i class="icn icn-star-full"></i>
                        <i class="icn icn-star-empty"></i>
                        <i class="icn icn-star-empty"></i>
                    </span>
                </div>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                </p>
            </div>
        </div>
        <div class="mp-substore-review-item">
            <div class="mp-substore-review-ava">
                <div class="user-avatar" style="background-image: url(/public/gmgm/img/user.png)"></div>
            </div>
            <div class="mp-substore-review-text">
                <div class="mp-substore-review-title">
                    <span>
                        Игра:
                    </span>
                    <a href="#">
                        Линейка
                    </a>
                    <span>
                        Куплено:
                    </span>
                    <strong>
                        Fuse Boost
                    </strong>
                </div>
                <div class="mp-substore-review-info">
                    <span>
                        Продавец:
                    </span>
                    <a href="#">
                        Nickman
                    </a>
                    <span class="saler-rates-wrap">
                        <i class="icn icn-star-full"></i>
                        <i class="icn icn-star-full"></i>
                        <i class="icn icn-star-full"></i>
                        <i class="icn icn-star-empty"></i>
                        <i class="icn icn-star-empty"></i>
                    </span>
                </div>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                </p>
            </div>
        </div>
        <div class="mp-substore-review-item">
            <div class="mp-substore-review-ava">
                <div class="user-avatar" style="background-image: url(/public/gmgm/img/user.png)"></div>
            </div>
            <div class="mp-substore-review-text">
                <div class="mp-substore-review-title">
                    <span>
                        Игра:
                    </span>
                    <a href="#">
                        Линейка
                    </a>
                    <span>
                        Куплено:
                    </span>
                    <strong>
                        Fuse Boost
                    </strong>
                </div>
                <div class="mp-substore-review-info">
                    <span>
                        Продавец:
                    </span>
                    <a href="#">
                        Nickman
                    </a>
                    <span class="saler-rates-wrap">
                        <i class="icn icn-star-full"></i>
                        <i class="icn icn-star-full"></i>
                        <i class="icn icn-star-full"></i>
                        <i class="icn icn-star-empty"></i>
                        <i class="icn icn-star-empty"></i>
                    </span>
                </div>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                </p>
            </div>
        </div>
    </div>
</div>
