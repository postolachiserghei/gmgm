<?php

use app\components\extend\Html;

/* @var $this yii\web\View */
/* @var $newStores[] app\models\User */
?>
<div class="col-xs-12 col-sm-6 col-md-3">
    <div class="mp-substore-newstore-wrap">
        <div class="mp-orange-title">
            <?= Html::tag('h2', yii::$app->l->t('Новые магазины')); ?>
        </div>

        <div class="mp-substore-newlots-item">
            <div class="mp-substore-newlots-img" style="background-image: url(/public/gmgm/img/tamp.jpeg)"></div>
            <div class="mp-substore-newlots-info">
                <div class="mp-substore-newlots-info-left">
                    <h3>
                        Fuse Boost
                    </h3>
                    <span>
                        Steam
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>