<?php

use app\components\extend\Url;
use app\components\extend\Html;
use app\components\extend\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = yii::$app->l->t('authorization', ['update' => false]);
?>

<div class="registration-form-wrap">
    <div class="reg-page-title">
        <h2>
            <?= Html::encode($this->title) ?>
        </h2>
    </div>

    <?php
    $form = ActiveForm::begin([
                'options' => ['class' => 'reg-page-form'],
                'id' => 'login-form',
                'enableClientValidation' => true,
                'enableAjaxValidation' => false
    ]);
    ?>
    <?=
    $form->field($model, 'email')->textInput([
        'placeholder' => yii::$app->l->t('email', ['update' => false]),
    ])->label('');
    ?>
    <?=
    $form->field($model, 'password')->passwordInput([
        'placeholder' => yii::$app->l->t('пароль', ['update' => false]),
    ])->label('');
    ?>

    <?php
//    echo $form->field($model, 'rememberMe')->checkbox([
//        'label' => $model->getAttributeLabel('rememberMe')
//    ]);
    ?>

    <?= Html::input('submit', null, yii::$app->l->t('вход')) ?>
    <?= Html::a(yii::$app->l->t('забыли пароль?'), Url::to(['/site/request-password-reset'])) ?>


    <?php ActiveForm::end(); ?>
</div>