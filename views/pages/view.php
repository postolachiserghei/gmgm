<?php

use app\components\extend\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = $model->title;

$this->params['breadcrumbs'][] = yii::$app->helper->data()->getParam('h1', $model->title);
$this->params['pageHeader'] = Html::tag('h1', yii::$app->helper->data()->getParam('h1', $model->title));
?>

<?= $model->content ?>

