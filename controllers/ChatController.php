<?php

namespace app\controllers;

use yii\helpers\Json;
use app\components\extend\yii;
use app\components\FrontendController;
use app\components\extend\ActiveDataProvider;

class ChatController extends FrontendController
{

    public function init()
    {
        $init = parent::init();
        $this->addAllowedActions([
            'contacts'
        ]);
    }

    public function actionContacts()
    {
        $user = yii::$app->helper->user()->identity();
        $q = $user->getContacts();
        $contacts = new ActiveDataProvider([
            'query' => $q,
            'pagination' => [
                'pageSize' => 100
            ]
        ]);

        return Json::encode([
                    'dataProvider' => $contacts,
                    'models' => $contacts->getModels(),
                    'user_id' => $user->id,
                        ], true);
    }

}
