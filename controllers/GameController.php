<?php

namespace app\controllers;

use app\models\Categories;
use app\components\extend\yii;
use yii\web\NotFoundHttpException;
use app\components\FrontendController;
use app\models\search\CategoriesSearch;

class GameController extends FrontendController
{

    /**
     * @inheritdoc
     */
    public function init()
    {
        $init = parent::init();
        $this->addAllowedActions([
            'view',
            'index'
        ]);
        return $init;
    }

    /**
     * view game by id
     * @param type $id
     * @return type
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }

    /**
     * all games
     * @return type
     */
    public function actionIndex()
    {
        $searchModel = new CategoriesSearch();
        $searchModel->type = Categories::TYPE_GAME;
        $dataProvider = $searchModel->search(yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Categories::find()->where([
            'id' => (int) $id,
            'status' => Categories::STATUS_ACTIVE,
            'type' => Categories::TYPE_GAME
        ])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
