<?php

namespace app\controllers;

use app\models\Transactions;
use app\components\extend\yii;
use yii\web\NotFoundHttpException;
use app\components\FrontendController;
use app\models\search\TransactionsSearch;
use Codeception\Exception\ConfigurationException;

class PaymentController extends FrontendController
{

    public function init()
    {
        $init = parent::init();
        yii::$app->request->enableCsrfValidation = false;
        $this->addAllowedActions(['index', 'process', 'cancel', 'result']);
        return $init;
    }

    /**
     *
     * @return type
     * @throws \Codeception\Exception\ConfigurationException
     */
    public function actionIndex()
    {
        $post = yii::$app->request->post('DynamicModel') ? yii::$app->request->post('DynamicModel') : yii::$app->request->post();
        $transaction = Transactions::add($post);
        if (!$transaction->isNewRecord) {
            if (!$transaction || !$model = $transaction->getModel()) {
                throw new ConfigurationException('wrong configuration');
            }
            $transaction->goToPay();
            if (yii::$app->request->isPost) {
                $this->refresh();
            }
        }
        $search = new TransactionsSearch();
        return $this->render('index', [
            'dataProvider' => $search->searchMy(yii::$app->request->get()),
            'transaction' => $transaction,
        ]);
    }

    /**
     * get transaction id from request
     * @return type
     */
    public function getTransactionId()
    {
        $request = $_REQUEST;
        if (!empty($request['orderId'])) {
            return $request['orderId'];
        } elseif (!empty($request['transaction'])) {
            return $request['transaction'];
        }
    }

    /**
     *
     * @return type
     * @throws \Codeception\Exception\ConfigurationException
     * @throws NotFoundHttpException
     */
    public function actionProcess()
    {
        $transactionId = $this->getTransactionId();
        $log = yii::$app->helper->log()->setDetails(true);
        if (!$transactionId) {
            $log->error(yii::$app->l->t('error while processing payment, no transaction ID given!'));
            throw new ConfigurationException(yii::$app->l->t('wrong data sent'));
        }
        if ($t = Transactions::findMy()->where(['id' => (int) $transactionId, 'status' => Transactions::STATUS_NEW])->one()) {
            /* @var $t \app\models\behaviors\PaymentBehavior */
            try {
                if ($result = $t->processPayment()) {
                    return $result === true ? $this->redirect(['result', 'transaction' => $transactionId]) : $result;
                } else {
                    $log->error(yii::$app->l->t('error while processing payment, process failed!'));
                    throw new ConfigurationException(yii::$app->l->t('wrong data sent'));
                }
            } catch (\Exception $exc) {
                $log->info($exc);
            }
        }
    }

    /**
     *
     * @param type $transaction
     */
    public function actionResult($transaction = null)
    {
        if (!$t = Transactions::findMy()->where(['id' => (int) $transaction])->one()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        /* @var $t Transactions */
        if ($t->status <= $t::STATUS_NEW) {
            return $this->redirect(['/payment/index']);
        }
        return $this->render('result', [
            'transaction' => $t,
            'model' => $t->model,
        ]);
    }

    /**
     *
     * @param type $transaction
     */
    public function actionCancel($transaction = null)
    {
        if ($t = Transactions::findMy()->andWhere(['id' => (int) $transaction])->one()) {
            /* @var $t \app\models\behaviors\payment\PaymentBase */
            $t->cancelPayment();
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $this->redirect(['/payment/index']);
    }

}
