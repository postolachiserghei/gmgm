<?php

namespace app\controllers;

use app\models\File;
use app\models\User;
use app\models\Products;
use app\components\extend\yii;
use app\models\forms\LoginForm;
use app\models\forms\SignupForm;
use app\models\forms\ContactForm;
use yii\web\BadRequestHttpException;
use app\components\FrontendController;
use app\models\forms\ResetPasswordForm;
use app\models\forms\PasswordResetRequestForm;

class SiteController extends FrontendController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        if (yii::$app->user->can('user-login-as', ['module' => 'admin'])) {
            $this->addAllowedActions(['auth-user-by-id']);
        }
        return parent::behaviors();
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'error' => [
                'class' => '\app\components\extend\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
//                'backColor' => '0x333333',
                'foreColor' => '736146',
                'transparent' => true,
                'fixedVerifyCode' => YII_ENV_DEV ? '1' : null,
            ],
        ]);
    }

    public function actionIndex()
    {
        $newStores = User::find()->limit(3)->where(['status' => User::STATUS_ACTIVE, 'role' => User::ROLE_SITE_STORE])->all();
        $newProducts = Products::find()->limit(3)->where(['status' => Products::STATUS_ACTIVE])->all();
        return $this->render('index', [
            'newStores' => $newStores,
            'newProducts' => $newProducts,
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (!yii::$app->user->returnUrl || yii::$app->user->returnUrl == '/') {
                return $this->redirect(['/user/index']);
            }
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->contact(Yii::$app->params['adminEmail'])) {
                $this->setMessage('success');
                return $this->refresh();
            }
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     *
     * @param type $role
     * @return type
     */
    public function actionSignup($role = null)
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            $this->ajaxValidation($model);
            if ($user = $model->signup()) {
                if (yii::$app->getUser()->login($user)) {
                    $js = 'yii.alert("' . yii::$app->l->t('вы успешно зарегистрировались под ником {username}', ['username' => $user->username]) . '");';
                    $this->setMessage('js', $js);
                    return $this->goHome();
                }
            }
        }

        if ($role) {
            $model->rbacRole = $role;
        }
        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(yii::$app->request->post())) {
            $this->ajaxValidation($model);
            if ($model->validate()) {
                if ($model->sendEmail('/site/reset-password')) {
                    $this->setMessage('success', yii::$app->l->t('check your email for further instructions.'));
                    return $this->goHome();
                } else {
                    $this->setMessage('error', yii::$app->l->t('sorry, we are unable to reset password for email provided.'));
                }
            }
            return $this->refresh();
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->resetPassword()) {
                $this->setMessage('success', yii::$app->l->t('new password was saved.'));
            } else {
                $this->setMessage('error');
            }
            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionDownload($file)
    {
        $model = File::find()->where(['name' => $file, 'status' => File::STATUS_UPLOADED])->one();
        /* @var $model File */
        if ($model) {
            $model->download();
        } else {
            throw new \yii\web\NotFoundHttpException(yii::$app->l->t('file not found'));
        }
    }

    public function actionAuthUserById($id)
    {
        yii::$app->user->login(User::find()->where('id=:id', ['id' => (int) $id])->one());
        return $this->redirect(yii::$app->homeUrl);
    }

}
