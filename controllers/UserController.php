<?php

namespace app\controllers;

use app\models\User;
use app\models\Products;
use app\components\extend\yii;
use yii\web\NotFoundHttpException;
use app\components\FrontendController;

class UserController extends FrontendController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $this->addAllowedActions(['view']);
        return parent::behaviors();
    }

    public function actionIndex($tab = 'profile', $update = null)
    {
        $model = $this->findModel(yii::$app->user->id);
        $model->setScenario($model::SCENARIO_PROFILE);
        /* @var $model User */
        $lot = $update == null ? new Products : Products::find()->where(['id' => (int) $update, 'owner_id' => $model->id])->one();
        $lot->owner_id = $model->id;
        if ($this->saveProfile($model) === true) {
            return $this->refresh();
        }
        if ($this->savePaymentSystem($model->paymentStstem) === true) {
            return $this->refresh();
        }
        if ($this->saveLot($lot) === true) {
            return $this->refresh();
        }
        return $this->render('index', [
            'model' => $model,
            'lot' => $lot,
            'tab' => $tab
        ]);
    }

    /**
     * save profile changes
     * @param \app\models\search\ProductsSearch $model
     * @return boolean
     */
    public function savePaymentSystem($model)
    {
        if ($post = yii::$app->request->post($model->shortClassName)) {
            $model->attributes = $post;
            $this->ajaxValidation($model);
            if ($model->saveMultiple()) {
                $this->setMessage('success');
                return true;
            } else {
                $p = $model->getErrors(null, true);
                $this->setMessage('error', 'errors: ' . $p);
                return true;
            }
        }
    }

    /**
     * save profile changes
     * @param \app\models\search\ProductsSearch $model
     * @return boolean
     */
    public function saveLot($model)
    {
        if ($post = yii::$app->request->post($model->shortClassName)) {
            $model->attributes = $post;
            $this->ajaxValidation($model);
            if ($model->isNewRecord) {
                $model->status = $model::STATUS_ACTIVE_FALSE;
                $this->setMessage('js', 'yii.alert("' . yii::$app->l->t('record will be published after moderation', ['update' => false]) . '")');
            }
            if ($model->save()) {
                $this->setMessage('success');
                return true;
            } else {
                $p = $model->getErrors(null, true);
                $this->setMessage('error', 'errors: ' . $p);
                return true;
            }
        }
    }

    /**
     * save profile changes
     * @param User $model
     * @return boolean
     */
    public function saveProfile($model)
    {
        if ($post = yii::$app->request->post($model->shortClassName)) {
            $model->attributes = $post;
            $this->ajaxValidation($model);
            if ($model->save()) {
                $this->setMessage('success');
                return true;
            } else {
                $p = $model->getErrors(null, true);
                $this->setMessage('error', 'errors: ' . $p);
                return true;
            }
        }
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::find()->where(['id' => (int) $id, 'status' => User::STATUS_ACTIVE])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(yii::$app->l->t('The requested page does not exist.'));
        }
    }

}
