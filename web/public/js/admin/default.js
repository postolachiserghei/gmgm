App.undoDeleted = function ($el)
{
    if ($el.hasClass('disabled')) {
        return;
    }
    App.loading();
    $el.addClass('disabled');
    $.ajax({
        type: 'GET',
        url: $el.attr('href'),
        async: true,
        dataType: 'json',
        error: function (xhr)
        {
            App.loading(true);
            $el.button('reset');
            yii.mes(xhr.responseText, 'error');
        }
    }).done(function ($data)
    {
        App.refresh();
    });
};

/**
 * 
 * @returns {defaultSettings}
 */
Settings = new function ()
{
    /**
     * 
     * @returns {Settings.defaultSettings#Update}
     */
    this.Update = new function ()
    {
        /**
         * restore default settings buttons function /views/settings/update
         * @param {type} $el
         * @returns {undefined}
         */
        this.restoreDefaultSettings = function ($el)
        {
            var $data = $el.data();
            yii.confirm($data.confirmMessage, function ()
            {
                $.ajax({
                    async: true,
                    type: 'post',
                    url: $el.data('url'),
                    data: {type: 'restoreDefaults'},
                    error: function (xhr)
                    {
                        yii.mes(xhr.responseText, 'error');
                    }
                }).done(function ($r)
                {
                    var $response = JSON.parse($r);
                    if ($response.result == 'success') {
                        App.refresh();
                    }
                });
            });
        };
    };
};


/**
 * black list
 * @returns {defaultBlackList}
 */
BlackList = new function ()
{
    this.Form = new function ()
    {
        this.valueSelector = '#blacklist-value';
        this.maskContainerSelector = '.mask-container';
        this.maskContainerActiveSelector = this.maskContainerSelector + '.mask-{type}';

        this.onValueChange = function ($input)
        {
            $(this.valueSelector).val($input.val());
            console.log($input.val());
        };

        /**
         * 
         * @param {type} $el
         * @returns {undefined}
         */
        this.onTypeChange = function ($el)
        {
            $(this.maskContainerSelector).addClass('hidden');
            var $type = $('input:checked', $el).val();
            var $activeContainer = $(this.maskContainerActiveSelector.replace('{type}', $type));
            $activeContainer.removeClass('hidden');
            $(this.valueSelector).val($('input', $activeContainer).val()).trigger('change');
        };
    };
};