/**
 * 
 * @returns {updatesUpdates}
 */
Updates = new function ()
{
    /**
     * updates config
     */
    this.conf = {
        progressModal: '#js-update-progress-modal',
        conditionSelector: 'input.js-input-condition',
        currentCounter: 0,
        totalToProcess: 0
    };

    /**
     * prepare data for updates to be generated
     * @param {type} $bt
     * @returns {undefined}
     */
    this.applyUpdates = function ($bt)
    {
        var $items = App.colectCheckedValues($bt.data('checkbox-selector'));
        if ($items.length === 0) {
            return;
        }
        Updates.conf.currentCounter = 0;
        Updates.conf.totalToProcess = $items.length;
        yii.confirm($bt.data('confirm-message'), function ()
        {
            Updates.applyRequest($bt, $items, 0, $items[0]);
        });
    };

    /**
     * apply request
     * @param {type} $bt
     * @param {type} $items
     * @param {type} $k
     * @param {type} $v
     * @returns {undefined}
     */
    this.applyRequest = function ($bt, $items, $k, $v)
    {
        var $row = $('tr[data-key="' + $k + '"]');
        $.ajax({
            type: 'POST',
            url: $bt.data('url'),
            async: true,
            dataType: 'json',
            data: {data: $v, type: 'applyUpdates'},
            error: function (xhr)
            {
                Updates.inProgress = false;
                yii.mes(xhr.responseText, 'error');
            }
        }).done(function ($data)
        {
            Updates.conf.currentCounter++;
            Updates.inProgress = false;
            var $message = $('.item-label', $row).html() + ' : ' + (Updates.conf.currentCounter) + '/' + ($items.length) + ' <br/>';
            var $next = $k + 1;
            Updates.calculateProgress(Updates.conf.currentCounter, Updates.conf.totalToProcess, $message, function ()
            {
                App.refresh();
            });
            if ($items[$next] != undefined) {
                Updates.applyRequest($bt, $items, $next, $items[$next]);
            }
        });
    };

    /**
     * prepare data for updates to be generated
     * @param {type} $bt
     * @returns {undefined}
     */
    this.generatePrepare = function ($bt)
    {
        var $items = App.colectCheckedValues($bt.data('checkbox-selector'));
        if ($items.length === 0) {
            return;
        }
        Updates.conf.currentCounter = 0;
        Updates.conf.totalToProcess = $items.length;
        yii.confirm($bt.data('confirm-message'), function ()
        {
            $.each($items, function ($k, $v)
            {
                var $row = $('tr[data-key="' + $v + '"]');
                $items[$v] = {tb: $k, condition: $(Updates.conf.conditionSelector, $row).val()};
            });
            $.ajax({
                type: 'POST',
                url: $bt.data('url'),
                async: true,
                dataType: 'json',
                data: {data: $items, type: 'generatePrepare'},
                error: function (xhr)
                {
                    yii.mes(xhr.responseText, 'error');
                }
            }).done(function ($data)
            {
                if ($data.response == 'success') {
                    Updates.saveUpdateItems($bt.data('url'), $data.result, $items);
                }
            });
        });
    };

    /**
     * save updates
     * @param {string} $url
     * @param {object} $operations
     * @param {object} $items
     * @returns {undefined}
     */
    this.saveUpdateItems = function ($url, $operations, $items)
    {
        var $order = 0;
        $.each($operations, function ($key, $operation)
        {
            $.ajax({
                type: 'POST',
                url: $url,
                async: true,
                dataType: 'json',
                data: {data: $key, order: $order, condition: $items[$key]['condition'], type: 'saveUpdateItems'},
                error: function (xhr)
                {
                    Updates.inProgress = false;
                    yii.mes(xhr.responseText, 'error');
                }
            }).done(function ($data)
            {
                Updates.conf.currentCounter++;
                Updates.inProgress = false;
                var $message = $operation.label + ' : ' + (Updates.conf.currentCounter) + '/' + ($items.length) + ' <br/>';
                Updates.calculateProgress(Updates.conf.currentCounter, Updates.conf.totalToProcess, $message, function ()
                {
                    App.refresh();
                });
            });
            $order++;
        });
    };

    /**
     * calculate progress by passing current operation, total number of operations and operation
     * @param {integer} $currentOperation
     * @param {integer} $totalnumberOfOperations
     * @param {message} $message
     * @returns {undefined}
     */
    this.calculateProgress = function ($currentOperation, $totalnumberOfOperations, $message, $callback)
    {
        if (!$(Updates.conf.progressModal).hasClass('in')) {
            $(Updates.conf.progressModal).modal('show');
        }
        var $percentage = (100 / $totalnumberOfOperations * ($totalnumberOfOperations - ($totalnumberOfOperations - $currentOperation)));
        $('.progress-bar', Updates.conf.progressModal).css('width', $percentage + '%');
        $('.current-operation', Updates.conf.progressModal).html($message);
        if (Updates.conf.currentCounter === Updates.conf.totalToProcess) {
            setTimeout(function ()
            {
                $(Updates.conf.progressModal).modal('hide');
                if (typeof $callback === 'function') {
                    $callback();
                }
            }, 2000);
        }

    };
};