/**
 * 
 * @returns {keyEventsKeyPress}
 */
Keys = new function ()
{
    this.selector = null;
    this.pressedKeys = [];
    this.keys = {
        'ctrl': [17, 91, 93],
        'cmd': [91, 93],
        'left': [37],
        'up': [38],
        'right': [39],
        'down': [40],
        'enter': [13],
        'shift': [16],
        'alt': [18],
        'a': [65],
        's': [83],
        'c': [67],
        'f': [70],
        'g': [71],
        'l': [76],
        'r': [82],
        't': [84],
        'q': [81],
        'h': [72],
        '1': [49],
        '2': [51],
        '3': [52],
        '4': [53],
        '5': [54]
    };

    this.element = null;

    /**
     * 
     * @param {string} $selector
     * @param {object} $comboEvents 
     * {
     *  'ctrl+shift+l': function (){
     *      yii.mes('this combinatin triggered');  
     *  },
     *  'ctrl+up': function (){
     *      //do my code when "ctrl+up" is pressed
     *  }
     *}
     * @returns {undefined}
     */
    this.events = function ($selector, $comboEvents)
    {
        $this = this;
        $this.selector = $selector;
        if ($($comboEvents).length > 0) {
            $.each($comboEvents, function ($combo, $callback)
            {
                var $k = $this.selector == $selector ? $this : (Object.create(Keys));
                $k.bind($selector, $combo, $callback);
            });
        }
    };

    /**
     * 
     * @param {string} $selector
     * @param {string} $combo : ctrl, cmd, lef, up, right, down, enter, shift, alt, f
     * @param {type} $callback
     * @returns {undefined}
     */
    this.bind = function ($selector, $combo, $callback)
    {
        var $el = $($selector);
        if ($el.length === 0) {
            return;
        }
        var $comboExists = $el.data('key-combo');
        if ($comboExists && $comboExists.indexOf($combo) !== -1) {
            return;
        }
        $el.data('key-combo', ($comboExists !== undefined ? $comboExists + ',' + $combo : $combo));
        $el.on($combo, function ()
        {
            return typeof $callback == 'function' ? $callback($(this)) : null;
        });
        if (!$el.hasClass('js-key-trigger')) {
            $el.addClass('js-key-trigger');
            this.init($el);
        }
    };

    /**
     * 
     * @param {type} $el
     * @returns {undefined}
     */
    this.init = function ($el)
    {
        var $this = this;
        $el.keydown(function (e)
        {
            if ($this.pressedKeys.indexOf(e.which) === -1) {
                $this.pressedKeys.push(e.which);
            }
            $this.checkTriggers($(this), e);
            App.YII_ENV_DEV ? console.log('pressed keys') : '';
            App.YII_ENV_DEV ? console.log($this.pressedKeys) : '';
        });
        $el.keyup(function (e)
        {
            $this.pressedKeys = [];
        });
    };

    /**
     * 
     * @param {type} $el
     * @param {type} $event
     * @returns {undefined}
     */
    this.checkTriggers = function ($el, $event)
    {
        var $this = this;
        if (!$el) {
            return;
        }
        var $t = $el.data('key-combo');
        var $triggers = $t.indexOf(',') !== -1 ? $t.split(',') : [$t];
        if ($triggers.length === 0) {
            return;
        }
        $.each($triggers, function ($i, $c)
        {
            var $combinations = $c.indexOf('+') !== -1 ? $c.split('+') : [$c];
            var $matches = '';
            if ($combinations.length > 0) {
                $.each($combinations, function ($key, $combo)
                {
                    if (!$this.keys[$combo]) {
                        return;
                    }
                    var $c = $this.keys[$combo];
                    $.each($this.pressedKeys, function ($k, $pressedKey)
                    {
                        $.inArray($pressedKey, $c) !== -1 ? $matches += ($matches === '' ? $combo : '+' + $combo) : null;
                    });
                });
            }
            if ($matches === $c) {
                $event.preventDefault();
                $el.trigger($c);
            }
        });
    };

    /**
     * register default key combinations
     * @returns {undefined}
     */
    this.registerEvents = function ()
    {
        this.events('document', {
            'ctrl+shift+a': function ()
            {
                return App.redirect('/admin', true);
            },
            'ctrl+shift+l': function ()
            {
                var $l = $('a[href$="login"]');
                if ($l.length > 0) {
                    return $l.trigger('click');
                }
            },
            'ctrl+shift+q': function ()
            {
                var $l = $('a[href$="logout"]');
                if ($l.length > 0) {
                    return $l.trigger('click');
                }
            },
            'ctrl+shift+h': function ()
            {
                return App.redirect('/', true);
            },
            'enter+s': function ()
            {
                return yii.mes('ctrl+s is disabled :) ');
            },
            'shift+ctrl+1': function ()
            {
                $('input[type=text],textarea').first().focus();
            }
        });
    };

};