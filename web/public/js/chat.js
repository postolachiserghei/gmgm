/**
 *
 * @returns {jsChat}
 */
Chat = new function ()
{
    this.cahtIsActive = false;
    this.title = '';
    this.conversationId = function ()
    {
        var $id = $(Chat.selectors.conversationItem + '.active').data('key');
        App.YII_ENV_DEV ? yii.mes('conversation id is: ' + $id) : '';
        return $id;
    };

    /**
     * default pages
     */
    this.p = {
        conversations: 1,
        contacts: 1,
        messages: 1
    };



    /**
     * actions
     */
    this.selectors = {
        countMessages: '.count-chat-message',
        chatModal: '#js-chat-modal',
        mainContainer: '.js-chat-container',
        contactsContainer: '.js-chat-contacts',
        contactItem: '.js-chat-contact-item',
        conversationsContainer: '.js-chat-conversations',
        conversationItem: '.js-chat-conversation-item',
        conversationItemCountNew: '.js-chat-conversation-item-count-new',
        messagesContainer: '.js-chat-conversation-messages',
        messageItem: '.js-chat-message-item',
        textarea: '.js-chat-textarea',
        sendBt: '.send-bt'
    };

    /**
     * actions
     */
    this.actions = {
        contactsList: {name: 'get-contacts', params: {}},
        conversationsList: {name: 'get-conversations', params: {}},
        messageSend: {name: 'send-message', params: {}},
        messageList: {name: 'get-messages', params: {}},
        count: {name: 'get-new-messages', params: {}}
    };

    /**
     * @param {type} $container
     * @returns {jsChat.getDirectionId.$key|Boolean}
     */
    this.getDirectionId = function ($container, $direction)
    {
        var $highest = 0;
        var $lowest = 0;
        if ($container.length > 0) {
            var $ids = $container.map(function ()
            {
                return parseInt($(this).data('key'), 10);
            }).get();

            $highest = Math.max.apply(Math, $ids);
            $lowest = Math.min.apply(Math, $ids);
        }


        return $direction == '+' ? $highest : $lowest;
    };

    /**
     * do request
     * @param {Chat.actions}} $action
     * @param {function|boolean} $callback
     * @returns {undefined}
     */
    this.request = function ($action, $callback)
    {
        $request = $.ajax({
            type: 'POST',
            url: $(Chat.selectors.mainContainer).data('url') + (($action.params && $action.params.page != undefined) ? '?page=' + $action.params.page : ''),
            async: true,
            dataType: 'json',
            data: {'chat-request': {
                    'action': $action.name,
                    'params': ($action.params ? $action.params : '---')
                }
            },
            error: function (xhr)
            {
                yii.mes(xhr.responseText, 'error');
            }
        }).done(function ($data)
        {
            if (typeof $callback === 'function') {
                $callback($data);
            }
            return $data;
        });

        return $request.responseJSON;
    };

    /**
     * @returns {undefined}
     */
    this.getCountNewMessages = function ($el, $direction)
    {
        return Chat.request(Chat.actions.count, function ($data)
        {
            var $d = $data.data;
            var $c = ($d.total != undefined && parseInt($d.total, 10) > 0) ? $d.total : '';
            var $counter = $(Chat.selectors.countMessages);
            $counter.html($c);
            $c !== '' ? $counter.removeClass('hidden') : $counter.addClass('hidden');
            $(Chat.selectors.countMessages).html($c);
            $.each($d.chats, function ($chat, $count)
            {
                var $badge = $(Chat.selectors.conversationItem + '[data-key="' + $chat + '"] ' + Chat.selectors.conversationItemCountNew);
                $badge.html($count);
                $count > 0 ? $badge.removeClass('hidden') : $badge.addClass('hidden');
            });
        });
    };
    /**
     * @returns {undefined}
     */
    this.drawContacts = function ($el, $direction)
    {
        $direction = ($direction != '-' && $direction != '+') ? '+' : $direction;
        var $container = $(Chat.selectors.contactsContainer);
        Chat.actions.contactsList.params.page = Chat.p.contacts;
        Chat.actions.contactsList.params.direction = $direction;
        Chat.actions.contactsList.params.directionId = Chat.getDirectionId($(Chat.selectors.contactItem), $direction);
        Chat.request(Chat.actions.contactsList, function ($data)
        {
            if ($data) {
                Chat.p.contacts++;
                if ($direction == '-') {
                    $container.prepend($data.data);
                } else {
                    $container.append($data.data);
                    setTimeout(function ()
                    {
                        App.scrollTo(true, $container, 100);
                    }, 400);
                }
            }
        });
    };

    /**
     * @returns {undefined}
     */
    this.drawConversations = function ($direction, $refresh)
    {
        Chat.setTitle();
        $direction = ($direction != '-' && $direction != '+') ? '+' : $direction;
        $(Chat.selectors.chatModal).modal('show');
        var $container = $(Chat.selectors.conversationsContainer);
        Chat.actions.conversationsList.params.page = Chat.p.conversations;
        Chat.actions.conversationsList.params.direction = $direction;
        Chat.actions.conversationsList.params.directionId = ($refresh ? 0 : Chat.getDirectionId($(Chat.selectors.conversationItem), $direction));
        Chat.request(Chat.actions.conversationsList, function ($data)
        {
            if ($data && $data != 'null') {
                if (!$refresh) {
                    Chat.p.conversations++;
                    if ($direction == '-') {
                        $container.prepend($data.data);
                    } else {
                        $container.append($data.data);
                        App.scrollTo(true, $container);
                    }
                } else {
                    $container.html($data.data);
                    App.scrollTo(true, $container);
                }
                setTimeout(function ()
                {
                    $(Chat.selectors.conversationItem + (($(Chat.selectors.conversationItem + '.active').length == 0) ? ':first' : '.active'))
                            .trigger('click');
                }, 300);
            }
            Chat.getCountNewMessages();
        });
    };

    /**
     * @returns {undefined}
     */
    this.drawMessages = function ($el, $direction)
    {
        $(this.selectors.conversationItem).removeClass('active');
        $el.addClass('active');
        var $container = $(Chat.selectors.messagesContainer);
        if (Chat.conversationId != $el.data('key')) {
            $container.html('');
        }
        Chat.title = $el.text();
        $direction = ($direction != '-' && $direction != '+') ? '+' : $direction;
        Chat.actions.messageList.params.chat_id = $el.data('key');
        Chat.conversationId = Chat.actions.messageList.params.chat_id;
        if (!Chat.p.messages[Chat.actions.messageList.params.chat_id]) {
            Chat.p.messages[Chat.actions.messageList.params.chat_id] = 0;
        }
        Chat.actions.messageList.params.page = Chat.p.messages[Chat.actions.messageList.params.chat_id];
        Chat.actions.messageList.params.direction = $direction;
        Chat.actions.messageList.params.directionId = Chat.getDirectionId($(Chat.selectors.messageItem + '[data-chat="' + $el.data('key') + '"]'), $direction);
        Chat.request(Chat.actions.messageList, function ($data)
        {
            if ($data) {
                Chat.p.messages[Chat.actions.messageList.params.chat_id]++;
                if ($direction == '-') {
                    $container.prepend($data.data);
                } else {
                    $container.append($data.data);
                    setTimeout(function ()
                    {
                        App.scrollTo(true, $container);
                    }, 500);
                }
                Chat.setTitle();
                Chat.getCountNewMessages();
            }
        });
    };

    /**
     *
     * @param {type} $title
     * @returns {undefined}
     */
    this.setTitle = function ($title)
    {
        $(Chat.selectors.chatModal + ' .modal-header .text-uppercase.txt-center').html(($title ? $.trim($title) : $.trim(Chat.title)));
    };

    this.receiveSockets = function ($data)
    {
        App.YII_ENV_DEV ? yii.mes('receive chat message:') : '';
        App.YII_ENV_DEV ? yii.mes($data) : '';
        if (Chat.chatIsActive) {
            var $el = $(Chat.selectors.conversationItem + '[data-key="' + $data.data.chat_id + '"]' + '.active');
            $el.length > 0 ? Chat.drawMessages($el) : Chat.drawConversations();
        } else {
            Chat.getCountNewMessages();
        }
    };

    /**
     * @returns {undefined}
     */
    this.sendMessages = function ($el)
    {
        App.scrollTo(true, Chat.selectors.messagesContainer);
        Chat.actions.messageSend.params.chat_id = Chat.conversationId;
        Chat.actions.messageSend.params.message = $.trim($el.val());
        if (Chat.actions.messageSend.params.message === '') {
            App.YII_ENV_DEV ? yii.mes('message is empty!') : '';
            return;
        }
        Chat.request(Chat.actions.messageSend, function ($data)
        {
            if ($data) {
                Chat.drawMessages($(Chat.selectors.conversationItem + '.active'));
                Socket.send({
                    'type': 'chat',
                    'data': {'chat_id': Chat.conversationId}
                }).to($data.receivers);
                App.YII_ENV_DEV ? yii.mes('send chat message to receivers:' + JSON.stringify($data.receivers)) : '';
                $el.val('');
            } else {
                App.YII_ENV_DEV ? yii.mes($data) : '';
            }
        });
    };


    $(this.selectors.messagesContainer).on('scroll', function ()
    {
        var pos = $(this).scrollTop();
        if (pos == 0 && $(Chat.selectors.messageItem).length > 10) {
            Chat.drawMessages($(Chat.selectors.conversationItem + '.active'), '-');
        }
    });

    /**
     * s
     */
    Keys.bind('body', 'alt+shift+c', function ($el)
    {
        Chat.drawConversations(null, true);
    });
};

$(function ()
{
    setTimeout(function ()
    {
        $(Chat.selectors.chatModal).on('hidden.bs.modal', function ()
        {
            Chat.chatIsActive = false;
        });
        $(Chat.selectors.chatModal).on('shown.bs.modal', function ()
        {
            Chat.chatIsActive = true;
        });
        Chat.getCountNewMessages();
    }, 1000);
});
