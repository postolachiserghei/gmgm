
/**
 * languages
 * @returns {languagesLanguages}
 */
Languages = new function ()
{

    /**
     * edit translation
     * @param {type} $bt
     * @returns {undefined}
     */
    this.editTranslation = function ($bt)
    {
        yii.prompt($bt.data('message'), function ($new)
        {
            $.ajax({
                type: 'POST',
                url: $bt.data('url'),
                async: true,
                dataType: 'json',
                data: {source: $bt.data('source'), category: $bt.data('category'), language: $bt.data('language'), new : $new},
                error: function (xhr)
                {
                    yii.mes(xhr.responseText, 'error');
                },
            }).done(function ($data)
            {
                yii.mes($data.message, $data.response);
                if ($data.response === 'success') {
                    $bt.html($new);
                    $bt.data('t', $new)
                }
            });
        }, $bt.data('t'));
    };
};