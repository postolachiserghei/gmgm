/**
 * common js functions
 * @type App.commonCommon
 */
var App = new function ()
{
    this.tmp = {};
    this.isPjax;
    this.user = {};
    this.YII_ENV_DEV;

    this.init = function ()
    {
        App.loading(true);
        Keys.registerEvents();
        $('.test-pjax-status').text('.').addClass('text-success');
    };

    /**
     * scroll to element
     * @param {type} $toEl
     * @param {type} $el
     * @param {type} $speed
     * @returns {undefined}
     */
    this.scrollTo = function ($toEl, $el, $speed)
    {
        setTimeout(function ()
        {
            if ($el === undefined) {
                $el = 'html,body';
            }
            $el = (typeof $el === 'object' ? $el : $($el));
            if ($toEl !== true) {
                $toEl = (typeof $toEl === 'object' ? $toEl : $($toEl));
            }
            if ($el.length === 0 || ($toEl !== true && $toEl.length === 0)) {
                return false;
            }
            return $el.animate({
                scrollTop: ($toEl === true ? $el.prop('scrollHeight') : $toEl.offset().top)
            }, ($speed !== undefined ? $speed : 500));
        }, 100);
    };

    /**
     * return values of checked ccheckboxes
     * @param {JQuerySelector} $selector
     * @returns {Array|colectCheckedValues.$ar}
     */
    this.colectCheckedValues = function ($selector)
    {
        var $ar = [];
        $($selector).each(function ()
        {
            if ($(this).is(':checked')) {
                $ar.push($(this).val());
            }
        });
        return $ar;
    };

    /**
     * @param {string} $gridViewCssClass
     * @param {string} $url
     * @param {string} $confirm
     * @param {array} $ids [1,2,3...]
     * @returns {Boolean}
     */
    this.deleteRecordsFromArray = function ($gridViewCssClass, $url, $confirm, $ids, $callback, $el)
    {
        $ids = !$ids ? this.colectCheckedValues($gridViewCssClass) : $ids;
        if (!$ids || $ids.length === 0)
            return false;
        if ($el.hasClass('disabled')) {
            return;
        }
        yii.confirm($confirm.replace(' ?', ' (' + $ids.length + ')' + ' ?'), function ()
        {
            $el.data('loading-text', '...');
            App.loading();
            $.ajax({
                async: true,
                url: $url,
                type: 'post',
                data: {ids: $ids},
                error: function (xhr)
                {
                    App.loading(true);
                    yii.mes(xhr.responseText, 'error');
                }
            }).done(function ($data)
            {
                $el.button('reset');
                var $dr = JSON.parse($data);
                if ($dr['type'] == 'success') {
                    if (typeof $callback == 'function') {
                        return $callback();
                    } else {
                        App.refresh();
                    }
                }
            });
        });
    };


    /**
     * check if string contains string (no case sensitive)
     * @param {string} $text
     * @param {string} $needle
     * @returns {Boolean}
     */
    this.stringContains = function ($text, $needle)
    {
        var $t = $text.toLowerCase();
        return $t.indexOf($.trim($needle.toLowerCase())) >= 0 ? true : false;
    };

    /**
     * redirects to url
     * @param {string} $url
     * @returns {undefined}
     */
    this.redirect = function ($url, $forceReload)
    {
        if (!$forceReload && App.isPjax) {
            App.loading();
            return this.pjaxRedirect($url);
        } else {
            return location.href = $url;
        }

    };

    /**
     * refresh page
     * @returns {undefined}
     */
    this.refresh = function ()
    {
        if (App.isPjax) {
            return this.pjaxRedirect(null);
        } else {
            return location.reload();
        }
    };

    this.pjaxData = {};
    /**
     * redirect pjax
     * @param {string} $url
     * @returns {undefined}
     */
    this.pjaxRedirect = function ($url)
    {
        if (App.pjaxData.id != undefined) {
            var $data = App.pjaxData;
            $data['url'] = $url;
            $.pjax($data);
        }
    };
    /**
     * init pjax page if it is set
     * @returns {undefined}
     */
    this.initPjaxPage = function ()
    {
        if ($('.pjax-app').length > 0) {
            App.isPjax = true;
            this.pjaxData['id'] = $('.pjax-app').attr('id');
            this.pjaxData['container'] = '#' + this.pjaxData['id'];
            $.each($('.pjax-app').data(), function ($k, $v)
            {
                App.pjaxData[$k] = $v;
            });
            var $pjaxContainer = $(this.pjaxData['container']);
            $pjaxContainer.on('pjax:start', function ($data, $status, $xhr, $options)
            {
                App.loading();
            });
            $pjaxContainer.on('pjax:success', function ($data, $status, $xhr, $options)
            {
                App.init();
            });
            $pjaxContainer.on('pjax:error', function ($xhr, $textStatus, $error, $options)
            {
                if ($textStatus.data != undefined && $textStatus.responseText != undefined) {
                    yii.mes($textStatus.data + '<br/>' + $textStatus.responseText, 'error');
                } else {
                    if ($textStatus.responseText) {
                        yii.mes($textStatus.responseText, 'error', 100);
                    }
                    console.log('pjax error:');
                    console.log($options);
                    console.log($xhr);
                    console.log($textStatus);
                    console.log($error);
                    console.log('pjax error end.F');
                }
                Socket.registerUser();
                App.loading(true);
            });
        }
    };

    /**
     * init loading
     * @param {boolean} $reset
     * @returns {undefined}
     */
    this.loading = function ($reset)
    {
        if ($reset) {
            $('.js-pjax-loading-progress,.pjax-loading').addClass('hidden');
        } else {
            $('.js-pjax-loading-progress,.pjax-loading').removeClass('hidden');
        }
    };
};

/**
 * sockets
 * @returns {commonSocket}
 */
Socket = new function ()
{
    this.data = {};
    this.connection;
    this.port = null;//is changed by app Assets
    this.host = null;//is changed by app Assets
    /**
     * initialize sockets
     * @param {type} $connection
     * @returns {unresolved}
     */
    this.init = function ($connection)
    {
        if ($connection && $connection.readyState === $connection.OPEN) {
            return $connection;
        }
        try {
            this.connection = new WebSocket(Socket.host + ':' + Socket.port);
        } catch (e) {
            console.log(e);
            return;
        }

        this.connection.onclose = function ($e)
        {

        };
        this.connection.onopen = function ($e)
        {
            setTimeout(function ()
            {
                Socket.registerUser();
            }, 1000);
        };
        this.connection.onmessage = function ($e)
        {
            var $data = JSON.parse($e.data);
            if ($data.type != undefined) {
                switch ($data.type) {
                    case 'chat':
                        Chat.receiveSockets($data);
                        break;
                    default:
                        break;
                }
            }
        };
    };

    /**
     * register user connection
     * @returns {undefined}
     */
    this.registerUser = function ()
    {
        if (!Socket) {
            App.YII_ENV_DEV ? yii.mes('no socket') : '';
            return;
        }
        Socket.send({
            u: App.user.id,
            dest: 'registeruser'
        }).to(App.user.id);
    };

    /**
     * prepare to send data to users
     * @param {type} $data
     * @returns {Socket}
     */
    this.send = function ($data, $afterSend)
    {
        if (typeof $data !== 'object') {
            this.data = {'data': $data};
        } else {
            this.data = $data;
        }
        return this;
    };

    /**
     * send to all users
     * @returns {undefined}
     */
    this.toAll = function ()
    {
        var $data = this.data;
        $data.to = 'all';
        return this.connection.send(JSON.stringify($data));
    };

    /**
     * send to certain user by id ex: to(1) || to([1,2,3...])
     * @param {mixed} $ids
     * @returns {undefined}
     */
    this.to = function ($ids)
    {
        var $data = this.data;
        $data.to = $ids;
        if (!this.connection) {
            App.YII_ENV_DEV ? yii.mes('no connection') : '';
            return;
        }
        return this.connection.send(JSON.stringify($data));
    };
};
App.initPjaxPage();
$(function ()
{
    $('a[href="#"]').on('click', function ($event)
    {
        $event.preventDefault();
    });
    setInterval(function ()
    {
        Socket.init(Socket.connection);
    }, 3000);
    App.init();
});


/* TODO #PS: notification */
function notifyMe()
{
    // Let's check if the browser supports notifications
    if (!("Notification" in window)) {
        alert("This browser does not support desktop notification");
    }

    // Let's check whether notification permissions have already been granted
    else if (Notification.permission === "granted") {
        // If it's okay let's create a notification
        var notification = new Notification("Hi there!", {
            tag: "ache-mail",
            body: "Привет, высылаю материалы по проекту...",
            icon: "http://habrastorage.org/storage2/cf9/50b/e87/cf950be87f8c34e63c07217a009f1d17.jpg"
        });
    }

    // Otherwise, we need to ask the user for permission
    else if (Notification.permission !== "denied") {
        Notification.requestPermission(function (permission)
        {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
                var notification = new Notification("Hi there!");
            }
        });
    }
    yii.mes('asdhaksdj');
    // At last, if the user has denied notifications, and you
    // want to be respectful there is no need to bother them any more.
}

