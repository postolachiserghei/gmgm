/**
 *
 * @returns {jsChat}
 */
ShoppingCart = new function ()
{

    this.el;

    /**
     * set element
     * @param {type} $el
     * @return {ShoppingCart}
     */
    this.setEl = function ($el)
    {
        this.el = $el;
        return this;
    };

    /**
     * actions
     */
    this.selectors = {
        modal: '#shopping-cart-modal',
        container: '.js-shopping-cart-container'
    };

    /**
     * actions
     */
    this.actions = {
        list: {name: 'get-list', params: {}},
        add: {name: 'add-to-cart', params: {}},
        remove: {name: 'remove-item', params: {}},
        changeQuantity: {name: 'change-quantity', params: {}},
        submitPayment: {name: 'submit-payment', params: {}},
    };

    /**
     *
     * @param {type} $id
     * @param {type} $params
     * @return {undefined}
     */
    this.addActionParam = function ($id, $params)
    {
        if (typeof this.actions.add.params[$id] === 'object') {
            $.extend(this.actions.add.params[$id], $params);
        } else {
            this.actions.add.params[$id] = $params;
        }
    };

    /**
     *
     * @param {type} $state
     * @return {undefined}
     */
    this.loading = function ($state)
    {
        var $that = this;
        if (typeof $that.el === 'object') {
            if ($state === false) {
                $that.el.button('reset');
            } else {
                $that.el.button('loading');
            }
        }
    };

    /**
     * do request
     * @param {ShoppingCart.actions}} $action
     * @param {function|boolean} $callback
     * @returns {undefined}
     */
    this.request = function ($action, $callback)
    {
        var $that = this;
        $request = $.ajax({
            type: 'POST',
            url: $(ShoppingCart.selectors.modal).data('url'),
            async: true,
            dataType: 'json',
            data: {'shopping-cart-request': {
                    'action': $action.name,
                    'params': ($action.params ? $action.params : '---')
                }
            },
            error: function (xhr)
            {
                $that.loading(false);
                yii.mes(xhr.responseText, 'error', 99999);
            }
        }).done(function ($data)
        {
            if (typeof $callback === 'function') {
                $callback($data);
            }
            return $data;
        });

        return $request.responseJSON;
    };

    /**
     *
     * @return {undefined}
     */
    this.closeModal = function ()
    {
        $(this.selectors.modal).modal('hide');
    };
    /**
     *
     * @return {undefined}
     */
    this.showModal = function ()
    {
        $(this.selectors.modal).modal('show');
    };

    /**
     *
     * @return {undefined}
     */
    this.open = function ()
    {
        var $that = this;
        $that.request($that.actions.list, function ($data)
        {
            if ($.trim($data) !== '') {
                $that.showModal();
                $($that.selectors.container).html($data);
                initQuantityControl();
            } else {
                $that.closeModal();
            }
        });
    };

    /**
     *
     * @param {integer} $id
     * @param {string} $item
     * @return {undefined}
     */
    this.add = function ($id, $item)
    {
        var $that = this;
        $that.loading();
        $that.addActionParam($id, {item: $item});
        $that.request($that.actions.add, function ($data)
        {
            setTimeout(function ()
            {
                $that.loading(false);
                $that.open();
            }, 100);
        });
    };

    /**
     *
     * @param {integer} $id
     * @param {string} $item
     * @return {undefined}
     */
    this.remove = function ($id)
    {
        var $that = this;
        yii.confirm($that.el.data('confirm-message'), function ()
        {
            delete $that.actions.add.params[$id];
            $that.actions.remove.params['id'] = $id;
            $that.request($that.actions.remove, function ($data)
            {
                setTimeout(function ()
                {
                    $that.open();
                }, 100);
            });
        });
    };

    /**
     *
     * @param {integer} $id
     * @param {integer} $quantity
     * @return {undefined}
     */
    this.changeQuantitty = function ($id, $quantity, $immediate)
    {
        var $that = this;
        $that.addActionParam($id, {quantity: $that.el.val()});
        $that.actions.changeQuantity.params['id'] = $id;
        $that.actions.changeQuantity.params['quantity'] = $quantity;
        if ($immediate) {
            if (!$that.el.hasClass('disabled')) {
                $that.el.addClass('disabled');
                $that.request($that.actions.changeQuantity, function ()
                {
                    $that.el.removeClass('disabled');
                    $that.open();
                });
            }
        }
    };

    /**
     *
     * @param {type} $el
     * @return {undefined}
     */
    this.setPaymentSystem = function ($el)
    {
        var $that = this;
        if ($that.el.is(':checked')) {
            $that.actions.submitPayment.params['system'] = $that.el.data('system');
        }
    };

    /**
     *
     * @return {undefined}
     */
    this.submitPayment = function ($bt)
    {
        var $that = this;
        $that.loading();
        $that.request($that.actions.submitPayment, function ($response)
        {
            setTimeout(function ()
            {
                $that.loading(false);
                var $result = $response['data'];
                if ($result.redirect != undefined) {
                    window.location.href = $result.redirect;
                } else {
                    if ($result.message != undefined && $result.status != undefined) {
                        yii.mes($result.message, $result.status);
                    }
                }
            }, 100);
        });
    };

    /**
     * s
     */
    Keys.bind('body', 'alt+shift+s', function ($el)
    {
        ShoppingCart.open();
    });
};
