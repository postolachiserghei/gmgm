function initializeMainJs()
{
    $('.mobile-hamburger-toggle').click(function ()
    {
        $(this).toggleClass('open');
        $('.mobile-hamburger-menu').toggleClass('open');
    });

    $('.header-search-wrap-showmore').click(function ()
    {
        wrap = $(this).closest('.header-search-wrap');
        wrap.find('.header-search-subbox').toggleClass('open');
    });

    $('.mobile-gamelist-toggle').click(function ()
    {
        $(this).toggleClass('open');
        $(this).closest('.mp-store-gamecat-wrap').find('.mobile-gamelist-body').slideToggle();
    });

    $('body').on('click', '[role=clear-form]', function (e)
    {
        e.preventDefault();
        e.stopPropagation();
        $(this).closest('form').find('input').each(function ()
        {
            $(this).val('');
        })
    })

    $('body').on('click', '.gamepage-lots-info-showmore', function (e)
    {
        e.preventDefault();
        console.log($(this).closest('tr'));
        $(this).closest('tr').next().toggle();
        $(this).closest('tr').toggleClass('open');
    });
    initQuantityControl();
    $('.no-required').each(function ()
    {
        var reqBox = '<div class="form-group-reqbox" data-toggle="tooltip" data-placement="right" title="Не обезательно для заполнения">?</div>';
        $(this).append(reqBox);
    });

    $('[data-toggle=tooltip]').tooltip();
}

$(window).ready(function ()
{
    initializeMainJs();
});

$('*.pjax-app').on('pjax:success', function ()
{
    initializeMainJs();
});

function initQuantityControl()
{
    $('.gamepage-lots-setvalue-wrap').each(function ()
    {
        if ($(this).hasClass('inited')) {
            return;
        }
        $(this).addClass('inited');
        var $container = $(this);
        var $el = $('.gamepage-lots-setvalue', $container);
        var $input = $container.find('input');
        var $oldVal = $input.val();
        $input.on('change', function ()
        {
            if ($container.data('max') < $input.val() || !$.isNumeric($input.val())) {
                $input.val($oldVal);
            }
        });

        $el.click(function ()
        {
            if ($input.hasClass('disabled')) {
                return;
            }
            if ($(this).hasClass('__minus')) {
                if ($input.val() == 1) {
                    return false;
                }
                $input.val(Number($input.val()) - 1);
            } else if ($(this).hasClass('__plus')) {
                if ($container.data('max') <= $($input).val()) {
                    return false
                }
                $input.val(Number($input.val()) + 1);
            }
            $('input[name="quantity"]', $('#payment-form-item-' + $container.data('item'))).val($input.val());
            $input.trigger('change');
        });
    });
}

