<?php

$localFile = __DIR__ . '/index-local.php';
if (is_file($localFile)) {
    return require($localFile);
}
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// comment out the following two lines when deployed to production
//defined('YII_DEBUG') or define('YII_DEBUG', true);
//defined('YII_ENV') or define('YII_ENV', 'dev');
// uncomment out the following line when deployed to production
defined('YII_ENV_PROD') or define('YII_ENV_PROD', true);
require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../components/extend/WebApplication.php');
$config = require(__DIR__ . '/../config/web.php');
ob_start();
(new \app\components\extend\WebApplication($config))->run();
$html = ob_get_clean();
$search = array(
    '/\>[^\S ]+/s', // strip whitespaces after tags, except space
    '/[^\S ]+\</s', // strip whitespaces before tags, except space
    '/(\s)+/s', // shorten multiple whitespace sequences
    '/<!--(.|\s)*?-->/' // Remove HTML comments
);
$replace = array(
    '>',
    '<',
    '\\1',
    ''
);
echo preg_replace($search, $replace, $html);