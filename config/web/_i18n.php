<?php

$tablePrefix = (isset($db) && is_array($db) && array_key_exists('tablePrefix', $db) ? $db['tablePrefix'] : '');

return [
    'translations' => [
        'app' => [
            'on missingTranslation' => array('app\models\Languages', 'insertMissingTranslationIntoDb'),
            'class' => 'yii\i18n\DbMessageSource',
            'enableCaching' => true,
            'cachingDuration' => 15,
            'sourceLanguage' => 'undefined',
            'sourceMessageTable' => $tablePrefix . 'i18n_message_source',
            'messageTable' => $tablePrefix . 'i18n_message'
        ],
    ],
];
