<?php

return [
    'enablePrettyUrl' => true,
    'enableStrictParsing' => false,
    'showScriptName' => false,
    'class' => 'app\components\extend\UrlManager',
    'rules' => [
        '/' => 'site/index',
        //default,
        'admin' => 'admin/',
        'admin/<action:\w+>' => 'admin/default/<action>',
        'admin/<controller:\w+>/<action:\w+>' => 'admin/<controller>/<action>',
        '<controller>/<action>' => '<controller>/<action>',
        //lang
        '<language:\w+>/<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
        '<language:\w+>/admin' => 'admin/',
        '<language:\w+>/admin/<action:\w+>' => 'admin/default/<action>',
        '<language:\w+>/admin/<controller:\w+>/<action:\w+>' => 'admin/<controller>/<action>',
        '<language:\w+>/<controller>/<action>' => '<controller>/<action>',
        '<language:[\w]{2,2}>' => 'site/index',
//        '<language:\w+>/?' => 'site/index',
    ]
];
