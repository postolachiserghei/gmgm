<?php
return [
    'class' => 'yii\db\Connection',
//    'dsn' => 'pgsql:host=localhost;dbname=DB_NAME',
    'dsn' => 'mysql:host=localhost;port=3006;dbname=DB_NAME',
    'username' => 'DB_USER',
    'password' => 'DB_PASSWORD',
    'charset' => 'utf8',
    'tablePrefix' => 'APP_NAME_',
];
