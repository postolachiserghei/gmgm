<?php

yii::setAlias('@tests', dirname(__DIR__) . '/tests');

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');
$mailer = $params['mailer'];

return [
    'name' => 'CONSOLE',
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'response' => [
            'class' => 'app\components\extend\ResponseConsole',
        ],
        'mailer' => $mailer,
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'l' => ['class' => 'app\models\Languages'],
        'helper' => ['class' => 'app\components\helper\Helper',],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'session' => [
            'class' => 'yii\web\Session'
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];
