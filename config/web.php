<?php

$params = require(__DIR__ . '/params.php');
$assetManager = require (__DIR__ . '/web/_assetManager.php');
$db = require(__DIR__ . '/db.php');
$i18n = require (__DIR__ . '/web/_i18n.php');
$urlManager = require (__DIR__ . '/web/_urlManager.php');
$log = require (__DIR__ . '/web/_log.php');
$mailer = $params['mailer'];


$config = [
    'basePath' => dirname(__DIR__),
    'name' => 'GMGMARKET',
    'id' => 'frontend',
    'language' => 'ru',
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Admin',
        ],
    ],
    'timeZone' => 'Europe/Chisinau',
    'components' => [
        'response' => [
            'class' => 'app\components\extend\ResponseWeb',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                if ($response->data !== null) {
                    $response->saveLog();
                }
            },
        ],
        'cache' => [
            'class' => 'yii\caching\MemCache',
            'useMemcached' => true,
//            'servers' => [
//                [
//                    'host' => '212.109.194.151',
//                    'port' => 11211,
//                    'persistent' => false
//                ],
//            ],
        ],
        'formatter' => [
            'dateFormat' => 'd.MM.yyyy',
            'timeFormat' => 'H:mm:ss',
            'datetimeFormat' => 'Y-M-d H:mm',
        ],
        'l' => ['class' => 'app\models\Languages',],
        'helper' => ['class' => 'app\components\helper\Helper',],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'class' => 'app\components\Users',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => ['errorAction' => 'site/error',],
        'mailer' => $mailer,
        'request' => [
            'class' => 'app\components\Request',
            'enableCsrfValidation' => true,
            'cookieValidationKey' => '9goCabMqPN4_GM-TBw1eaHrhJoJxUqL0',
        ],
        'assetManager' => $assetManager,
        'i18n' => $i18n,
        'urlManager' => $urlManager,
        'log' => $log,
        'db' => $db,
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['modules']['debug'] = [
        'class' => yii\debug\Module::className(),
        'allowedIPs' => ['*'],
    ];
    $config['bootstrap'][] = 'debug';
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = require (__DIR__ . '/web/_gii.php');
}

$webLocal = __DIR__ . '/web-local.php';
if (is_file($webLocal)) {
    require $webLocal;
}
return $config;
