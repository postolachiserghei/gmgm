<?php

$localFile = __DIR__ . '/params-local.php';
$localParams = is_file($localFile) ? require $localFile : [];

$params = [
    'frontendThemes' => [
        'default' => 'Default',
        'gmgm' => 'GmgMarket',
    ],
    'socketPort' => '8081',
    'adminEmail' => 'admin@it-init.com',
    'supportEmail' => 'gaftonsifon@yandex.com',
    'user.passwordResetTokenExpire' => 3600,
    'lessc-path' => '/usr/local/bin/lessc',
    'mailer' => [
        'class' => 'yii\swiftmailer\Mailer',
        'viewPath' => '@app/mail',
        'useFileTransport' => false,
    /* example for $localParams[mailer] */
//        'transport' => [
//            'class' => 'Swift_SmtpTransport',
//            'host' => 'smtp.gmail.com',
//            'username' => 'myaccountname@gmail.com',
//            'password' => 'mypassword',
//            'port' => '465',
//            'encryption' => 'ssl',
//                 "port" => "587",
//                "encryption" => "TLS",
//                'streamOptions' => [
//                    'ssl' => [
//                        'verify_peer' => false,
//                        'allow_self_signed' => true
//                    ],
//                ],
//            'plugins' => [
//                    [
//                    'class' => 'Swift_Plugins_LoggerPlugin',
//                    'constructArgs' => [new Swift_Plugins_Loggers_ArrayLogger()],
//                ],
//            ],
//        ],
    ]
];

return array_merge($params, (is_array($localParams) ? $localParams : []));
