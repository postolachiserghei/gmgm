<?php

namespace app\commands;

use app\models\File;
use app\models\Tasks;
use app\models\RecycleBin;
use yii\console\Controller;
use app\components\extend\yii;
use app\components\websockets\Pusher;
use app\components\extend\sockets\IoServer;
use app\components\extend\sockets\WsServer;
use app\components\extend\sockets\HttpServer;

/**
 * @property \app\components\helper\LogHelper $log log
 */
class CronController extends Controller
{

    public function getLog()
    {
        return yii::$app->helper->log()->setInitiator($this->className());
    }

    /**
     * process tasks action
     */
    public function actionTasks()
    {
        Tasks::process(true);
    }

    /**
     * base run action
     */
    public function actionFiles()
    {
        return File::syncTableFiles();
    }

    /**
     * clear recycle
     */
    public function actionRecycle()
    {
        RecycleBin::emptyRecycleBin();
    }

    /**
     * run sockets
     */
    public function actionSockets()
    {
        $p = yii::$app->helper->data()->getParam('webSocketPort', 8081);
        $pusher = new Pusher();
        $wsServer = new WsServer($pusher);
        $httpServer = new HttpServer($wsServer);
        $server = IoServer::factory($httpServer, $p);
        if (!$server) {
            return null;
        }
        yii::$app->helper->log()->info('websockets started');
        $server->run();
    }

}
