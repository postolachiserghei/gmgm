<?php

use app\components\extend\Html;
use app\components\extend\ActiveForm;
use app\components\widgets\uploader\UploaderWidget;

/* @var $this yii\web\View */
/* @var $model app\models\File */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="carousel-form">

    <?php $form = ActiveForm::begin(); ?>



    <div class="row">
        <div class="col-md-12">
            <?=
            UploaderWidget::widget([
                'name' => 'file',
                'ajax' => true,
                'files' => app\models\File::find()->all(),
                'template' => UploaderWidget::TEMPLATE_SINGLE
            ]);
            ?>
        </div>
    </div>

    <div class="clearfix"></div>
    <hr/>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? yii::$app->l->t('Create') : yii::$app->l->t('Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
