<?php

$menuActive = ($controller == 'menu');
$pagesActive = ($controller == 'pages');
$fileActive = ($controller == 'file');
$seoActive = ($controller == 'seo');
$sliderActive = ($controller == 'slider');
$canSlider = yii::$app->user->can('slider-index');
$canMenu = yii::$app->user->can('menu-index');
$canSeo = yii::$app->user->can('seo-index');
$canPages = yii::$app->user->can('pages-index');
$canFile = yii::$app->user->can('file-index');
$canCarousel = yii::$app->user->can('pages-carousel');
$canCategories = yii::$app->user->can('categories');
$categoriesActive = ($controller == 'categories');
$canLotTypes = yii::$app->user->can('lotTypes-index');
$LotTypesActive = ($controller == 'lot-types');
$canServers = yii::$app->user->can('servers-index');
$serversActive = ($controller == 'servers');
$canProducts = yii::$app->user->can('products-index');
$productsActive = ($controller == 'products');

return [
    'label' => yii::$app->l->t('content'),
    'linkOptions' => ['icon' => 'paperclip'],
    'visible' => ($canMenu || $canPages || $canSeo || $canCarousel || $canFile || $canCategories || $canLotTypes || $canServers || $canProducts),
    'active' => ($menuActive || $pagesActive || $seoActive || $fileActive || $categoriesActive || $LotTypesActive || $serversActive || $productsActive),
    'items' => [
        [
            'label' => yii::$app->l->t('categories'),
            'linkOptions' => ['icon' => 'cube'],
            'url' => ['/admin/categories/index'],
            'visible' => $canCategories,
            'active' => $categoriesActive
        ],
        [
            'label' => yii::$app->l->t('servers'),
            'linkOptions' => ['icon' => 'server'],
            'url' => ['/admin/servers/index'],
            'visible' => $canServers,
            'active' => $serversActive
        ],
        [
            'label' => yii::$app->l->t('products'),
            'linkOptions' => ['icon' => 'pie-chart'],
            'url' => ['/admin/products/index'],
            'visible' => $canProducts,
            'active' => $productsActive
        ],
        [
            'label' => yii::$app->l->t('lot types'),
            'linkOptions' => ['icon' => 'bookmark'],
            'url' => ['/admin/lottypes/index'],
            'visible' => $canLotTypes,
            'active' => $LotTypesActive
        ],
        [
            'label' => yii::$app->l->t('menu'),
            'linkOptions' => ['icon' => 'bars'],
            'url' => ['/admin/menu/index'],
            'visible' => $canMenu,
            'active' => $menuActive
        ],
        [
            'label' => yii::$app->l->t('slider'),
            'linkOptions' => ['icon' => 'image'],
            'url' => ['/admin/slider/index'],
            'visible' => $canSlider,
            'active' => $sliderActive
        ],
        [
            'label' => yii::$app->l->t('pages'),
            'linkOptions' => ['icon' => 'file-text-o'],
            'url' => ['/admin/pages/index'],
            'visible' => $canPages,
            'active' => $pagesActive
        ],
        [
            'label' => yii::$app->l->t('seo'),
            'linkOptions' => ['icon' => 'link'],
            'url' => ['/admin/seo/index'],
            'visible' => $canSeo,
            'active' => $seoActive
        ],
        [
            'label' => yii::$app->l->t('files'),
            'linkOptions' => ['icon' => 'file'],
            'url' => ['/admin/file/index'],
            'visible' => $canFile,
            'active' => $fileActive
        ],
    ]
];
