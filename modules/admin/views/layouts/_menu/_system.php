<?php

$canSettings = yii::$app->user->can('settings-update');
$settingsActive = ($controller == 'settings' && $action == 'update' && !yii::$app->request->get('m'));
$canUpdates = yii::$app->user->can('updates-apply');
$updatesActive = ($controller == 'updates' && $action == 'index');
$canTransactions = yii::$app->user->can('transactions-index');
$transactionsActive = ($controller == 'transactions' && $action == 'index');
return [
    'label' => yii::$app->l->t('system'),
    'linkOptions' => ['icon' => 'microchip'],
    'visible' => ($canSettings || $canUpdates || $canTransactions),
    'active' => ($settingsActive || $updatesActive || $transactionsActive),
    'items' => [
        [
            'label' => yii::$app->l->t('transactions'),
            'linkOptions' => ['icon' => 'money'],
            'url' => ['/admin/transactions/index'],
            'active' => $transactionsActive,
            'visible' => $canTransactions
        ],
        [
            'label' => yii::$app->l->t('settings'),
            'linkOptions' => ['icon' => 'cogs'],
            'url' => ['/admin/settings/update'],
            'active' => $settingsActive,
            'visible' => $canSettings
        ],
        [
            'label' => yii::$app->l->t('updates'),
            'linkOptions' => ['icon' => 'refresh'],
            'url' => ['/admin/updates/apply'],
            'active' => $updatesActive,
            'visible' => $canUpdates
        ],
    ],
];
