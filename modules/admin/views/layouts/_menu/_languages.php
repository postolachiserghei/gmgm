<?php

$languageActive = ($controller == 'languages');
$canLanguages = yii::$app->user->can('languages-index');
$canTranslate = yii::$app->user->can('translations-update');
$translationsActive = ($controller == 'translations');

return [
    'label' => yii::$app->l->t('languages'),
    'linkOptions' => ['icon' => 'globe'],
    'visible' => ($canLanguages || $canTranslate),
    'active' => ($languageActive || $translationsActive),
    'items' => [
        [
            'label' => yii::$app->l->t('language list'),
            'linkOptions' => ['icon' => 'globe'],
            'visible' => $canLanguages,
            'active' => $languageActive,
            'url' => ['/admin/languages/index'],
        ],
        [
            'label' => yii::$app->l->t('translate messages'),
            'linkOptions' => ['icon' => 'language'],
            'visible' => $canTranslate,
            'active' => $translationsActive,
            'url' => ['/admin/translations/update'],
        ],
    ]
];
