<?php

$userActive = (($controller == 'user' && $action != 'profile') || ($controller == 'settings' && yii::$app->request->get('m') == 'User'));
$rbacActive = $controller == 'rbac';
$blackListActive = $controller == 'black-list';
$canRbac = yii::$app->user->can('rbac-index');
$canUser = yii::$app->user->can('user-index');
$canBlackList = yii::$app->user->can('blacklist-index');

return [
    'label' => yii::$app->l->t('users'),
    'linkOptions' => ['icon' => 'users'],
    'active' => ($userActive || $rbacActive),
    'visible' => ($canUser || $canRbac || $blackListActive),
    'items' => [
        [
            'label' => yii::$app->l->t('users list'),
            'linkOptions' => ['icon' => 'users'],
            'url' => ['/admin/user/index'],
            'visible' => $canUser,
            'active' => $userActive
        ],
        [
            'label' => yii::$app->l->t('user roles'),
            'linkOptions' => ['icon' => 'lock'],
            'url' => ['/admin/rbac/index'],
            'visible' => $canRbac,
            'active' => $rbacActive
        ],
        [
            'label' => yii::$app->l->t('black list'),
            'linkOptions' => ['icon' => 'ban'],
            'url' => ['/admin/blacklist/index'],
            'visible' => $canBlackList,
            'active' => $blackListActive
        ]
    ]
];
