<?php

use app\components\extend\Nav;
use app\components\extend\Url;
use app\components\extend\Html;
use app\components\extend\NavBar;
use app\components\widgets\chat\ChatWidget;

$controller = yii::$app->controller->id;
$action = yii::$app->controller->action->id;


NavBar::begin([
    'brandLabel' => Html::tag('span', yii::$app->name, ['class' => 'text-info']),
    'brandUrl' => yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top main-top-nav',
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-left nav'],
    'items' => [
        require(__DIR__ . '/_menu/_user.php'),
        require(__DIR__ . '/_menu/_languages.php'),
        require(__DIR__ . '/_menu/_content.php'),
        require(__DIR__ . '/_menu/_system.php'),
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right nav'],
    'items' => [
        ['label' => yii::$app->l->t('site'), 'url' => yii::$app->helper->data()->getParam('tld', Url::to('/', true)), 'linkOptions' => [
                'target' => '_blank',
                'data' => [
                    'pjax' => 0
                ]
            ]],
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right nav'],
    'items' => yii::$app->l->menuLanguages,
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right nav'],
    'items' => [
        require(__DIR__ . '/_menu/_profile.php'),
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right nav'],
    'items' => [
        ['label' => Html::tag('span', '', ['class' => 'count-chat-message text-success pa badge badge-success']), 'url' => '', 'linkOptions' => [
                'icon' => 'envelope',
                'data-pjax' => 0,
                'onclick' => 'Chat.drawConversations(null,true);return false;'
            ],
            'visible' => (!yii::$app->user->isGuest)
        ]
    ],
]);
NavBar::end();
echo ChatWidget::widget();
