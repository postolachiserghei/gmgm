<?php

use app\assets\AdminAssets;
use app\components\extend\Html;
use app\components\extend\Pjax;
use app\components\extend\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */
AdminAssets::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode(yii::$app->helper->data()->getParam('pageTitle', $this->title)) ?></title>
        <?php $this->head() ?>
    </head>
    <body data-notification-top-margin="71">
        <?php
        echo $this->render('_loading');
        $this->beginBody();
        if (yii::$app->controller->isPjaxAction) {
            Pjax::begin();
        }
        ?>
        <div class="wrap">
            <?= $this->render('_menu') ?>
            <div class="container">
                <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]); ?>
            </div>
            <div class="container">
                <?= $this->render('_page_header') ?>
                <?= $content ?>
            </div>
        </div>
        <?php
        if (yii::$app->controller->isPjaxAction) {
            Pjax::end();
        }
        ?>
        <?php $this->endBody() ?>
        <?= YII_ENV_PROD ? '' : '<div class="text-danger test-pjax-status"></div>'; ?>
    </body>
</html>
<?php $this->endPage(); ?>