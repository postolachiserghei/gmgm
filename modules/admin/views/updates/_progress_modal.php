<?php

use yii\bootstrap\Modal;
?>

<?php
Modal::begin([
    'id' => 'js-update-progress-modal',
    'header' => yii::$app->l->t('progress'),
//    'closeButton' => false,
    'clientOptions' => [
        'backdrop' => 'static',
        'keyboard' => false
    ]
]);
?>
<div class="progress-info">
    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>
    </div>
    <div class="current-operation">...</div>
</div>
<?php Modal::end(); ?>