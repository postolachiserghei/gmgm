<?php

use app\components\extend\Url;
use app\components\extend\Nav;
use app\components\extend\Html;
use app\components\extend\GridView;

/* @var $model app\models\Updates */
/* @var $dataProvider yii\data\ArrayDataProvider */


$this->title = yii::$app->l->t('apply updates', ['update' => false]);
$this->params['breadcrumbs'][] = yii::$app->l->t('apply updates');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('apply updates'));
$this->params['menu'] = Nav::CrudActions($model, [
            [
                'label' => yii::$app->l->t('generate updates'),
                'url' => Url::to(['generate']),
                'visible' => yii::$app->user->can('updates-generate')
            ]
        ]);
?>

<div class="row">
    <div class="col-md-2">
        <?=
        Html::a(yii::$app->l->t('apply updates'), '#', [
            'class' => 'btn btn-block',
            'onClick' => 'Updates.applyUpdates($(this));return false;',
            'data' => [
                'checkbox-selector' => '.js-updates-list-checbox',
                'confirm-message' => yii::$app->l->t('apply selected updates', ['update' => false]),
                'url' => Url::to(['apply']),
            ]
        ])
        ?>
    </div>
    <div class="col-md-10">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items}{pager}',
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                GridView::checkboxColumn([
                    'class' => 'js-updates-list-checbox',
                    'disabled' => function($a, $key, $index) use ($model) {
                        return $model->getTableIsUpToDate('{{%' . $a['tb'] . '}}');
                    },
                    'value' => function($a, $key, $index) use ($model) {
                        return $a['file'];
                    },
                ]),
                [
                    'label' => yii::$app->l->t('compartment'),
                    'value' => function($a, $b, $c) {
                        return Html::tag('span', yii::$app->l->t($a['info']['label']), ['class' => 'item-label']);
                    }
                ],
                'totalCount',
                'created_at',
            ]
        ])
        ?>
    </div>

    <?= $this->render('_progress_modal', ['model' => $model]); ?>
</div>