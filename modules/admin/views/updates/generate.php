<?php

use app\components\extend\Url;
use app\components\extend\Nav;
use app\components\extend\Html;
use app\components\extend\GridView;

/* @var $model app\models\Updates */
/* @var $dataProvider yii\data\ArrayDataProvider */


$this->title = yii::$app->l->t('Generate Updates', ['update' => false]);
$this->params['breadcrumbs'][] = yii::$app->l->t('Generate updates');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('Generate Updates'));
$this->params['menu'] = Nav::CrudActions($model, [
            [
                'label' => yii::$app->l->t('apply updates'),
                'url' => Url::to(['apply']),
                'visible' => yii::$app->user->can('updates-apply')
            ]
        ]);
?>

<div class="row">
    <div class="col-md-2">
        <?=
        Html::a(yii::$app->l->t('Generate updates'), '#', [
            'class' => 'btn btn-block',
            'onClick' => 'Updates.generatePrepare($(this));return false;',
            'data' => [
                'checkbox-selector' => '.js-updates-list-checbox',
                'confirm-message' => yii::$app->l->t('generate updates for selected items', ['update' => false]),
                'url' => Url::to(['generate']),
            ]
        ]);
        ?>
    </div>
    <div class="col-md-10">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items}{pager}',
            'rowOptions' => function($a, $key, $index) use ($model) {
                $countNew = $model->getTableHasNewRecords($key);
                $cssClass = $countNew ? '' : 'disabled text-muted';
                return ['class' => $cssClass, 'data' => ['count-new' => $countNew]];
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                GridView::checkboxColumn([
                    'class' => 'js-updates-list-checbox',
                    'disabled' => function($a, $key, $index) use ($model) {
                        return !$model->getTableHasNewRecords($key);
                    }
                ]),
                [
                    'attribute' => 'label',
                    'value' => function($a, $key, $index) {
                        return yii::$app->l->t($a['label']);
                    }
                ],
//                [
//                    'attribute' => yii::$app->l->t('condition (for advanced users only)'),
//                    'value' => function($a, $key, $index) use ($model) {
//                        return Html::input('text', "condition[" . $model->getUpdatePrefix($key) . "]", null, [
//                                    'class' => 'form-control js-input-condition',
//                                    'disabled' => !$model->getTableHasNewRecords($key)
//                        ]);
//                    }
//                ],
                [
                    'label' => yii::$app->l->t('new records'),
                    'value' => function($a, $key, $index) use ($model) {
                        return $model->getCountNewItems($key);
                    }
                ],
            ]
        ])
        ?>
    </div>

    <?= $this->render('_progress_modal', ['model' => $model]); ?>
</div>