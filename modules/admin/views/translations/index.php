<?php

use app\components\extend\Html;
use app\components\extend\Nav;
use app\components\extend\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\I18nMessageSourceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\models\I18nMessageSource */
/* @var $languages array (all available languages if site is multilingual) */



$this->title = yii::$app->l->t('Manage I18n Message Source', ['update' => false]);
$this->params['breadcrumbs'][] = yii::$app->l->t('I18n Message Source');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('Manage I18n Message Source'));
$this->params['menu'] = Nav::CrudActions($model, null, 'all');
?>
<div class="i18n-message-source-index">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //GridView::checkboxColumn(),
            [
                'attribute' => 'id',
                'visible' => !YII_ENV_DEV
            ],
            [
                'attribute' => 'message',
                'contentOptions' => ['style' => 'text-align: left!important;'],
                'value' => function($model) {
                    return $model->getTranslationsForEachLanguage();
                }
            ],
            [
                'attribute' => 'category',
                'value' => function($model, $pk) {
                    return yii::$app->l->t($model->category);
                },
                'filter' => $model->getCategories(),
                'filterInputOptions' => GridView::defaultOptionsForFilterDropdown()
            ],
        ],
    ]);
    ?>


</div>
