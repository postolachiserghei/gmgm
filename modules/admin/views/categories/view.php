<?php

use app\models\File;
use app\components\extend\Nav;
use app\components\extend\Html;
use app\components\extend\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */


$this->title = yii::$app->l->t('view', ['update' => false]);
$this->params['breadcrumbs'][] = ['label' => yii::$app->l->t('Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = yii::$app->l->t('view');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('View Categories'));
$this->params['menu'] = Nav::CrudActions($model);
?>
<div class="Categories-view">

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => DetailView::DefaultAttributes($model, [
            'type' => [
                'attribute' => 'type',
                'value' => $model->getTypeLabels($model->type)
            ],
            'labels' => [
                [
                    'label' => yii::$app->l->t('translations'),
                    'format' => 'raw',
                    'value' => $model->getTButtons(),
                    'visible' => yii::$app->l->multi
                ]
            ],
            'parent' => [
                'attribute' => 'parent',
                'value' => $model->getParentName(),
            ],
            $model::ADDITIONAL_DATA_IMAGE => [
                'format' => 'html',
                'attribute' => $model::ADDITIONAL_DATA_IMAGE,
                'value' => $model->renderImage(['size' => File::SIZE_MD]),
            ],
            $model::ADDITIONAL_DATA_DESCRIPTION => [
                'format' => 'html',
                'attribute' => $model::ADDITIONAL_DATA_DESCRIPTION,
            ],
            'status' => [
                'attribute' => 'status',
                'value' => $model->getStatusLabels($model->status),
            ],
            'url' => [
                'format' => 'raw',
                'attribute' => 'url',
                'value' => $model->getLink(),
            ]
                ], ['order']),
    ])
    ?>

</div>
