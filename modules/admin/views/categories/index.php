<?php

use \app\components\extend\Html;
use \app\components\extend\Nav;
use \app\components\extend\Url;
use \app\modules\admin\components\widgets\categories\CategoriesTreeWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CategoriesSearch */
/* @var $dataProvider app\components\extend\ActiveDataProvider */



$this->title = yii::$app->l->t('Manage Categories', ['update' => false]);
$this->params['breadcrumbs'][] = yii::$app->l->t('Categories');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('Manage Categories'));
$this->params['menu'] = Nav::CrudActions($model, [], ['delete_all'], true);
?>
<div class="row">
    <div class="col-lg-12">
        <?= CategoriesTreeWidget::widget(['type' => $type]) ?>
    </div>
</div>
