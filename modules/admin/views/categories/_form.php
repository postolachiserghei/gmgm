<?php

use app\components\extend\Url;
use app\components\extend\Html;
use app\components\extend\ActiveForm;
use app\components\widgets\redactor\RedactorWidget;
use app\components\widgets\uploader\UploaderWidget;
use app\modules\admin\components\widgets\categories\CategoriesTreeWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$css = '.jstree-anchor{   
        height: 0!important;
        visibility: hidden!important;
    }';
yii::$app->view->registerCss($css);
?>

<div class="Categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= Html::tag('div', $model->getTButtons(), ['class' => 'text-right']); ?>

    <div class="row">
        <div class="col-lg-8">
            <?=
            $form->field($model, 'type')->dropDownList($model->getTypeLabels(false, false), [
                'class' => 'form-control ' . (count($model->getTypeLabels()) > 1 ?: 'hidden'),
                'onchange' => 'CategoriesTreeWidget.reloadJsTreeByType($(this),$(this).data());return false;',
                'data' => [
                    'url' => Url::to([yii::$app->controller->action->id, 'id' => $model->primaryKey]),
                    'id' => $model->primaryKey,
                ]
            ]);
            ?>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model->t, $model::ADDITIONAL_DATA_IMAGE)->widget(UploaderWidget::className()); ?>
                </div>
                <div class="col-md-8">
                    <?= $form->field($model, 'title')->textInput(); ?>
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model->seo, 'alias')->textInput() ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model->seo, 'title')->textInput() ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model->seo, 'keywords')->textarea(['rows' => 5]) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model->seo, 'description')->textarea(['rows' => 5]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <?= $form->field($model, $model::ADDITIONAL_DATA_DESCRIPTION)->widget(RedactorWidget::className()); ?>
        </div>
        <div class="col-lg-4">
            <?=
            $form->field($model, 'parent')->hiddenInput([
                'value' => 0
            ]);
            ?>
            <?= CategoriesTreeWidget::widget(['view' => 'form', 'type' => $model->type, 'model' => $model]) ?>
        </div>
    </div>
    <?= $form->field($model, 'status')->radioList($model->getStatusLabels()); ?>
    <hr/>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? yii::$app->l->t('Create') : yii::$app->l->t('Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
