<?php

use app\components\extend\Html;
use app\components\extend\Nav;
use app\components\extend\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LotTypesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\models\LotTypes */



$this->title = yii::$app->l->t('Manage Lot Types', ['update' => false]);
$this->params['breadcrumbs'][] = yii::$app->l->t('Lot Types');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('Manage Lot Types'));
$this->params['menu'] = Nav::CrudActions($model);
?>
<div class="lot-types-index"> 
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>



    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            GridView::checkboxColumn(),
            'title',
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->getStatusLabels($model->status);
                },
                'filterInputOptions' => GridView::defaultOptionsForFilterDropdown(),
                'filter' => $model->getStatusLabels()
            ],
            [
                'class' => 'app\components\extend\ActionColumn',
                'template' => Html::tag('div', '{view} ' . (yii::$app->l->multi ? '{TButtons}' : '{update}') . '  {delete}', ['class' => 'grid-view-actions']),
                'buttons' => GridView::DefaultActions($model, [
                    'TButtons' => function($url, $model) {
                        return $model->getTButtons();
                    }
                ]),
            ],
        ],
    ]);
    ?>


</div>
