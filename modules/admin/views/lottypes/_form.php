<?php

use app\components\extend\Html;
use app\components\extend\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LotTypes */
/* @var $form ActiveForm */
?>

<div class="lot-types-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= Html::tag('div', $model->getTButtons(), ['class' => 'text-right']); ?>
    <?= $form->field($model, 'title')->textInput() ?>
    <?= $form->field($model, 'status')->radioList($model->getStatusLabels()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? yii::$app->l->t('Create') : yii::$app->l->t('Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div> 

    <?php ActiveForm::end(); ?>

</div>
