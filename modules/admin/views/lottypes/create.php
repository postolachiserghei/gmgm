<?php
use app\components\extend\Html;
use app\components\extend\Nav;

/* @var $this yii\web\View */
/* @var $model app\models\LotTypes */
 

$this->title = yii::$app->l->t('create', ['update' => false]);
$this->params['breadcrumbs'][] = ['label' => yii::$app->l->t('Lot Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = yii::$app->l->t('create');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('Create Lot Types'));
$this->params['menu'] = Nav::CrudActions($model);
?>
<div class="lot-types-create"> 
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
