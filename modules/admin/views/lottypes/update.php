<?php
use app\components\extend\Html;
use app\components\extend\Nav;

/* @var $this yii\web\View */
/* @var $model app\models\LotTypes */


$this->title = yii::$app->l->t('update', ['update' => false]);
$this->params['breadcrumbs'][] = ['label' => yii::$app->l->t('Lot Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = yii::$app->l->t('update');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('Update Lot Types'));
$this->params['menu'] = Nav::CrudActions($model);
?>
<div class="lot-types-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
