<?php

use app\components\extend\Html;
use app\components\extend\Nav;
use app\components\extend\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\models\Slider */



$this->title = yii::$app->l->t('Manage Slider', ['update' => false]);
$this->params['breadcrumbs'][] = yii::$app->l->t('Slider');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('Manage Slider'));
$this->params['menu'] = Nav::CrudActions($model);
?>
<div class="slider-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]);   ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            GridView::checkboxColumn(),
            [
                'attribute' => $model::ADDITIONAL_DATA_IMAGE,
                'format' => 'html',
                'value' => function($model) {
                    return $model->renderImage(['width' => 75]);
                },
            ],
            't.title',
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->getStatusLabels($model->status);
                },
                'filterInputOptions' => GridView::defaultOptionsForFilterDropdown(),
                'filter' => $model->getStatusLabels()
            ],
            [
                'attribute' => 'type',
                'value' => function($model) {
                    return $model->getTypeLabels($model->type);
                },
                'filterInputOptions' => GridView::defaultOptionsForFilterDropdown(),
                'filter' => $model->getTypeLabels()
            ],
            // 'updated_at',
            // 'order',
            // 'is_deleted',
            [
                'class' => 'app\components\extend\ActionColumn',
                'template' => Html::tag('div', '{view} ' . (yii::$app->l->multi ? '{TButtons}' : '{update}') . '  {delete}', ['class' => 'grid-view-actions']),
                'buttons' => GridView::DefaultActions($model, [
                    'TButtons' => function($url, $model) {
                        return $model->getTButtons();
                    }
                ]),
            ],
        ],
    ]);
    ?>


</div>
