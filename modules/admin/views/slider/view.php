<?php

use app\components\extend\Html;
use app\components\extend\Nav;
use app\components\extend\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Slider */


$this->title = yii::$app->l->t('view', ['update' => false]);
$this->params['breadcrumbs'][] = ['label' => yii::$app->l->t('Slider'), 'url' => ['index']];
$this->params['breadcrumbs'][] = yii::$app->l->t('view');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('View Slider'));
$this->params['menu'] = Nav::CrudActions($model);
?>
<div class="slider-view"> 

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => DetailView::DefaultAttributes($model, [
            'labels' => [
                [
                    'label' => yii::$app->l->t('translations'),
                    'format' => 'raw',
                    'value' => $model->getTButtons(),
                    'visible' => yii::$app->l->multi
                ]
            ],
            $model::ADDITIONAL_DATA_IMAGE => [
                'attribute' => $model::ADDITIONAL_DATA_IMAGE,
                'format' => 'html',
                'value' => $model->renderImage()
            ],
            'type' => [
                'attribute' => 'type',
                'value' => $model->getTypeLabels($model->type)
            ],
            'status' => [
                'attribute' => 'status',
                'value' => $model->getStatusLabels($model->status)
            ]
        ]),
    ]);
    ?>

</div>
