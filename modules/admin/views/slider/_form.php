<?php

use app\models\Slider;
use app\components\extend\Html;
use app\components\extend\ActiveForm;
use app\components\widgets\uploader\UploaderWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Slider */
/* @var $form ActiveForm */
$js = "App.tmp.switchSliderTypes = function(el){
            input = $('input[type=radio]:checked',el);
            $('.type-container').addClass('hidden');
            $('.type-container.type-'+input.val()).removeClass('hidden');
    }";
$this->registerJs($js, $this::POS_LOAD)
?>

<div class="slider-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, $model::ADDITIONAL_DATA_IMAGE)->widget(UploaderWidget::className()); ?>
        </div>

        <div class="col-md-9">
            <?= Html::tag('div', $model->getTButtons(), ['class' => 'text-right']); ?>

            <?= $form->field($model, 'title')->textInput() ?>

            <?= $form->field($model, $model::ADDITIONAL_DATA_LINK)->textInput(); ?>

            <div class="type-container type-<?= Slider::TYPE_ADS ?> type-<?= Slider::TYPE_STORE ?> <?= ($model->type == Slider::TYPE_ADS || $model->type == Slider::TYPE_STORE ? '' : 'hidden') ?>">
                <?= $form->field($model, $model::ADDITIONAL_DATA_CONTENT)->textInput(); ?>
            </div>
            <div class="type-container type-<?= Slider::TYPE_ADS ?> <?= ($model->type == Slider::TYPE_ADS ? '' : 'hidden') ?>">
                <?= $form->field($model, $model::ADDITIONAL_DATA_PRICE)->textInput(); ?>
            </div>

            <?=
            $form->field($model, 'type')->radioList($model->getTypeLabels(), [
                'onchange' => "App.tmp.switchSliderTypes($(this))"
            ]);
            ?>

            <?= $form->field($model, 'status')->radioList($model->getStatusLabels()) ?>
        </div> 
    </div> 
    <hr/>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? yii::$app->l->t('Create') : yii::$app->l->t('Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div> 

    <?php ActiveForm::end(); ?>

</div>
