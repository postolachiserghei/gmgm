<?php

use app\components\extend\yii;
use app\components\extend\Html;
use app\components\extend\Nav;
use app\components\extend\GridView;
use app\models\behaviors\PaymentBehavior;
use app\components\extend\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TransactionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\models\Transactions */



$this->title = yii::$app->l->t('Manage Transactions', ['update' => false]);
$this->params['breadcrumbs'][] = yii::$app->l->t('Transactions');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('Manage Transactions'));
$this->params['menu'] = Nav::CrudActions($model, null, ['create','delete_all']);
?>
<div class="transactions-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>



    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            GridView::checkboxColumn(),
            'id',
            'amount',
            [
                'attribute' => 'currency',
                'filter' => $model->getCurrenciesLabels(null, true),
                'filterInputOptions' => GridView::defaultOptionsForFilterDropdown(),
                'value' => function($transaction) {
                    /* @var $transaction \app\models\Transactions */
                    return $transaction->getCurrenciesLabels($transaction->currency, true);
                }
            ],
            [
                'attribute' => 'payment_system',
                'filter' => ArrayHelper::map(PaymentBehavior::getAvailablePaymentSystems(), 'id', 'title'),
                'filterInputOptions' => GridView::defaultOptionsForFilterDropdown(),
                'value' => function($transaction) {
                    $model = $transaction->model;
                    /* @var $transaction \app\models\Transactions */
                    /* @var $model \app\models\behaviors\PaymentBehavior */
                    return $transaction->getPayment()->getPaymentSystemFromTransaction($transaction)->title;
                }
            ],
            [
                'attribute' => 'user_id',
                'value' => function($model) {
                    return $model->user ? $model->user->fullName : yii::$app->l->t('guest');
                }
            ],
            [
                'attribute' => 'status',
                'filter' => $model->getStatusLabels(),
                'filterInputOptions' => GridView::defaultOptionsForFilterDropdown(),
                'value' => function($model) {
                    return $model->getStatusLabels($model->status);
                }
            ],
            // 'status',
            // 'item_id',
            // 'item_model:ntext',
            // 'quantity',
            // 'additional_data',
            // 'created_at',
            // 'updated_at',
            // 'order',
            // 'is_deleted',
            [
                'class' => 'app\components\extend\ActionColumn',
                'template' => Html::tag('div', '{view}', ['class' => 'grid-view-actions']),
                'buttons' => GridView::DefaultActions($model),
            ],
        ],
    ]);
    ?>


</div>
