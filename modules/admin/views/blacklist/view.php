<?php

use app\components\extend\Html;
use app\components\extend\Nav;
use app\components\extend\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BlackList */


$this->title = yii::$app->l->t('view', ['update' => false]);
$this->params['breadcrumbs'][] = ['label' => yii::$app->l->t('Black List'), 'url' => ['index']];
$this->params['breadcrumbs'][] = yii::$app->l->t('view');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('View Black List'));
$this->params['menu'] = Nav::CrudActions($model);
?>
<div class="blacklist-view"> 

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => DetailView::DefaultAttributes($model, [
            'type' => [
                'attribute' => 'type',
                'value' => $model->getTypeLabels($model->type),
            ],
                ], ['additional_data']),
    ])
    ?>

</div>
