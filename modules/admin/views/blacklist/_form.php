<?php

use app\models\BlackList;
use yii\widgets\MaskedInput;
use app\components\extend\Html;
use app\components\extend\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BlackList */
/* @var $form ActiveForm */
?>

<div class="blacklist-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="mask-container mask-<?= BlackList::TYPE_IP ?> <?= $model->type != BlackList::TYPE_IP ? 'hidden' : '' ?>">
        <?=
        $form->field($model, BlackList::ADDITIONAL_DATA_IP)->widget(MaskedInput::className(), [
            'clientOptions' => [
                'alias' => 'ip',
//                'pattern' => '^([0-9]{1,3}\.){3}[0-9]{1,3}$'
                ],
            'options' => ['onKeyUp' => 'BlackList.Form.onValueChange($(this))', 'class' => 'form-control ',
                'placeholder' => $model->getAttributeLabel(BlackList::ADDITIONAL_DATA_IP)
            ]
        ])->label('')
        ?>
    </div>
    <div class="mask-container mask-<?= BlackList::TYPE_USER_ID ?> <?= $model->type != BlackList::TYPE_USER_ID ? 'hidden' : '' ?>">
        <?=
        $form->field($model, BlackList::ADDITIONAL_DATA_USER_ID)->widget(MaskedInput::className(), [
            'clientOptions' => ['alias' => 'id', 'mask' => '9999999999'],
            'options' => ['onKeyUp' => 'BlackList.Form.onValueChange($(this))', 'class' => 'form-control',
                'placeholder' => $model->getAttributeLabel(BlackList::ADDITIONAL_DATA_USER_ID)
            ]
        ])->label('')
        ?>
    </div>

    <?= $form->field($model, 'value')->hiddenInput(); ?>

    <?=
    $form->field($model, 'type')->radioList($model->getTypeLabels(), [
        'onChange' => "BlackList.Form.onTypeChange($(this))",
        'data' => [
            'type-ip' => BlackList::TYPE_IP,
            'type-id' => BlackList::TYPE_USER_ID,
        ]
    ])
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? yii::$app->l->t('Create') : yii::$app->l->t('Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div> 

    <?php ActiveForm::end(); ?>

</div>
