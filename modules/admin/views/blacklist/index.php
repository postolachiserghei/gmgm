<?php

use app\components\extend\Html;
use app\components\extend\Nav;
use app\components\extend\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BlackListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\models\BlackList */



$this->title = yii::$app->l->t('Manage Black List', ['update' => false]);
$this->params['breadcrumbs'][] = yii::$app->l->t('Black List');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('Manage Black List'));
$this->params['menu'] = Nav::CrudActions($model);
?>
<div class="blacklist-index"> 
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>



    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            GridView::checkboxColumn(),
            'value',
            [
                'attribute' => 'type',
                'value' => function($model) {
                    return $model->getTypeLabels($model->type);
                },
                'filter' => $model->getTypeLabels(),
            ],
            [
                'attribute' => 'created_at',
                'value' => function($model) {
                    return yii::$app->formatter->asDatetime($model->created_at);
                },
                'filter' => false,
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($model) {
                    return yii::$app->formatter->asDatetime($model->updated_at);
                },
                'filter' => false,
            ],
//            'additional_data',
            // 'order',
            // 'is_deleted',
            [
                'class' => 'app\components\extend\ActionColumn',
                'template' => Html::tag('div', '{view} {update} {delete}', ['class' => 'grid-view-actions']),
                'buttons' => GridView::DefaultActions($model),
            ],
        ],
    ]);
    ?>


</div>
