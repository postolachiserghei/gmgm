<?php

use app\components\extend\Html;
use app\components\extend\ActiveForm;
use app\components\widgets\uploader\UploaderWidget;
use app\components\widgets\redactor\RedactorWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form ActiveForm */
/* @var $categories \app\models\Categories */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= Html::tag('div', $model->getTButtons(), ['class' => 'text-right']); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'image')->widget(UploaderWidget::className()); ?>
            <?= $form->field($model, 'title')->textInput() ?>
            <?= $form->field($model, 'category_id')->dropDownList($categories) ?>
        </div>
        <div class="col-md-9">
            <?= $form->field($model, 'description')->widget(RedactorWidget::className()); ?>
        </div>
    </div>
    <hr/>
    <div class="row">
        <?=
        $form->field($model->seo, 'alias', [
            'template' => '<div class="col-lg-6">'
            . '<div class="input-group">
                                <span class="input-group-btn" style="font-size:1em">
                                  <button class="btn btn-default" type="button">/' . $model->seo->l . '</button>
                                </span>
                                {input}
                            </div><!-- /input-group -->'
            . '</div>' . '<div class="col-lg-6">
                            ' . $form->field($model->seo, 'title')->textInput() . '
                        </div>'
        ])->textInput([
            'value' => (!$model->seo->alias ? '/' : $model->seo->alias)
        ])
        ?>

        <div class="col-lg-6">
            <?= $form->field($model->seo, 'keywords')->textarea(['rows' => 5]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model->seo, 'description')->textarea(['rows' => 5]) ?>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, $model::ADDITIONAL_DATA_DELIVERY_IN_DAYS)->dropDownList($model->getSetting('delivery_time_list')) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'type')->dropDownList($model->getAvailableTypes()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'available')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4">
            <?=
            $form->field($model, 'price', [
                'template' => '<div class="input-group">
                                {input}
                                <span class="input-group-btn" style="font-size:1em">
                                  <button class="btn btn-default" type="button">' . $model->getCurrencySign() . '</button>
                                </span>
                            </div>{error}<!-- /input-group -->'
            ])->textInput([
                'class' => 'text-right form-control',
                'value' => (!$model->isNewRecord ? $model->getPrice(false, false) : 1)
            ])
            ?>
        </div>
    </div>
    <?= $form->field($model, 'status')->radioList($model->getStatusLabels()) ?>
    <hr/>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? yii::$app->l->t('Create') : yii::$app->l->t('Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
