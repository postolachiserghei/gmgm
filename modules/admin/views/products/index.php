<?php

use app\components\extend\Url;
use app\components\extend\Nav;
use app\components\extend\Html;
use app\components\extend\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\models\Products */



$this->title = yii::$app->l->t('Manage Products', ['update' => false]);
$this->params['breadcrumbs'][] = yii::$app->l->t('Products');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('Manage Products'));
$this->params['menu'] = Nav::CrudActions($model, [
    [
        'label' => Html::ico('check') . ' ' . Html::ico('long-arrow-right') . ' ' . Html::ico('check-square-o'),
        'url' => '#',
        'visible' => yii::$app->user->can('products-update'),
        'linkOptions' => [
            'onclick' => 'moderateSelectedItems($(this))',
            'title' => yii::$app->l->t('enable selected items', ['update' => false]),
            'data' => [
                'url' => Url::to(['update']),
                'operation' => $model::STATUS_ACTIVE,
                'not-selected-mess' => yii::$app->l->t('no items selected', ['update' => false]),
                'confirm-mess' => yii::$app->l->t('enable selected items', ['update' => false])
            ]
        ]
    ],
    [
        'label' => Html::ico('ban') . ' ' . Html::ico('long-arrow-right') . ' ' . Html::ico('check-square-o'),
        'url' => '#',
        'visible' => yii::$app->user->can('products-update'),
        'linkOptions' => [
            'onclick' => 'moderateSelectedItems($(this))',
            'title' => yii::$app->l->t('disable selected items', ['update' => false]),
            'data' => [
                'url' => Url::to(['update']),
                'operation' => $model::STATUS_ACTIVE_FALSE,
                'not-selected-mess' => yii::$app->l->t('no items selected', ['update' => false]),
                'confirm-mess' => yii::$app->l->t('disable selected items', ['update' => false])
            ]
        ]
    ]
]);

$js = 'function moderateSelectedItems($el)
    {
        var $ids = App.colectCheckedValues(".selectGridViewCheckBox");
        if ($ids.length === 0) {
            yii.mes($el.data("not-selected-mess"), "warning");
            return;
        }
        yii.confirm($el.data("confirm-mess")+" ?", function ()
        {
            $.ajax({
                type: "POST",
                url: $el.data("url"),
                async: true,
                dataType: "json",
                data: {operation: $el.data("operation"), ids: $ids, type: "moderateSelectedItems"},
                error: function (xhr)
                {
                    yii.mes(xhr.responseText, "error");
                },
            }).done(function ($data)
            {
                App.refresh();
            });
        });

    }';
$this->registerJs($js, $this::POS_END);
?>
<div class="products-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>



    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            GridView::checkboxColumn(),
            'title',
            [
                'attribute' => 'category_id',
                'value' => 'category.title',
                'filter' => $model->getAvailableCategories(),
                'filterInputOptions' => GridView::defaultOptionsForFilterDropdown(),
            ],
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->getStatusLabels($model->status);
                },
                'filter' => $model->getStatusLabels(),
                'filterInputOptions' => GridView::defaultOptionsForFilterDropdown(),
            ],
            [
                'attribute' => 'type',
                'value' => function($model) {
                    /* @var $model app\models\Products */
                    return $model->getLotType()->title;
                },
                'filter' => $model->getAvailableTypes(false),
                'filterInputOptions' => GridView::defaultOptionsForFilterDropdown(),
            ],
            [
                'attribute' => 'price',
                'value' => function($model) {
                    /* @var $model app\models\Products */
                    return $model->getPrice();
                },
            ],
            //'available',
            // 'status',
            // 'additional_data',
            // 'created_at',
            // 'updated_at',
            // 'order',
            // 'is_deleted',
            [
                'class' => 'app\components\extend\ActionColumn',
                'template' => Html::tag('div', '{view} ' . (yii::$app->l->multi ? '{TButtons}' : '{update}') . ' {delete}', ['class' => 'grid-view-actions']),
                'buttons' => GridView::DefaultActions($model, [
                    'TButtons' => function($url, $model) {
                        return $model->getTButtons();
                    }
                ]),
            ],
        ],
    ]);
    ?>


</div>