<?php

use app\components\extend\Html;
use app\components\extend\Nav;
use app\components\extend\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Products */


$this->title = yii::$app->l->t('view', ['update' => false]);
$this->params['breadcrumbs'][] = ['label' => yii::$app->l->t('Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = yii::$app->l->t('view');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('View Products'));
$this->params['menu'] = Nav::CrudActions($model);
?>
<div class="products-view">

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => DetailView::DefaultAttributes($model, [
            'labels' => [
                [
                    'label' => yii::$app->l->t('translations'),
                    'format' => 'raw',
                    'value' => $model->getTButtons(),
                    'visible' => yii::$app->l->multi
                ]
            ],
            'price' => [
                'attribute' => 'price',
                'format' => 'raw',
                'value' => $model->getPrice(),
            ],
            'category_id' => [
                'attribute' => 'category_id',
                'format' => 'raw',
                'value' => $model->category->title,
            ],
        ]),
    ])
    ?>

</div>
