<?php

use app\components\extend\Html;
use app\components\extend\Nav;

/* @var $this yii\web\View */
/* @var $model app\models\Servers */
/* @var $games[] array */


$this->title = yii::$app->l->t('update', ['update' => false]);
$this->params['breadcrumbs'][] = ['label' => yii::$app->l->t('Servers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = yii::$app->l->t('update');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('Update Servers'));
$this->params['menu'] = Nav::CrudActions($model);
?>
<div class="servers-update">
    <?=
    $this->render('_form', [
        'model' => $model,
        'games' => $games
    ])
    ?>
</div>
