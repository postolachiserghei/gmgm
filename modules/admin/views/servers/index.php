<?php

use app\components\extend\Html;
use app\components\extend\Nav;
use app\components\extend\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ServersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\models\Servers */
/* @var $games[] array */



$this->title = yii::$app->l->t('Manage Servers', ['update' => false]);
$this->params['breadcrumbs'][] = yii::$app->l->t('Servers');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('Manage Servers'));
$this->params['menu'] = Nav::CrudActions($model);
?>
<div class="servers-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>



    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            GridView::checkboxColumn(),
//            'id',
            'title',
            [
                'attribute' => 'category_id',
                'value' => 'game.title',
                'filter' => $games,
                'filterInputOptions' => GridView::defaultOptionsForFilterDropdown(),
            ],
//            'url:ntext',
//            'created_at',
//            'updated_at',
            // 'order',
            // 'is_deleted',
            [
                'class' => 'app\components\extend\ActionColumn',
                'template' => Html::tag('div', '{view} ' . (yii::$app->l->multi ? '{TButtons}' : '{update}') . ' {delete}', ['class' => 'grid-view-actions']),
                'buttons' => GridView::DefaultActions($model, [
                    'TButtons' => function($url, $model) {
                        return $model->getTButtons();
                    }
                ]),
            ],
        ],
    ]);
    ?>


</div>
