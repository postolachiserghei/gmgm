<?php

use app\models\Categories;
use app\components\extend\Html;
use app\modules\admin\components\widgets\categories\CategoriesTreeWidget;
use app\modules\admin\components\widgets\categories\CategoriesTreeWidgetAssets;
use app\components\extend\extensions\JsTree;

CategoriesTreeWidgetAssets::register($this);

$plugins = [
    "themes",
    "json_data",
    "checkbox",
    'ccrm',
    'ui',
    'types'
];
?>
<div class="categories-widget">
    <?php
    yii::$app->view->registerJs("$(function () {
            $('#jsTreeCategoriesWidgetForm').on('changed.jstree', function (\$e, \$data) {
                var \$id = \$data.instance.get_node(\$data.selected[0]).id;
                $('input[name=\"Categories[parent]\"]').val(\$id == 'root' ? 0 : \$id);
            });
    });", yii\web\View::POS_READY);
    ?>
    <?=
    JsTree::widget([
        'id' => 'jsTreeCategoriesWidgetForm',
        'jsOptions' => [
            'plugins' => $plugins,
            'checkbox' => [
                'three_state' => false,
                'check_callback' => true
            ],
            'types' => [
                "#" => [
                    "max_children" => 1
                ],
            ],
            'core' => [
                'multiple' => false,
                'data' => CategoriesTreeWidget::getTreeArray($type, $model),
                'themes' => [
                    'url' => '/public/plugins/proton/style.css',
                    'dots' => true,
                    'icons' => true,
                    'name' => 'proton',
                    'responsive' => true
                ]
            ],
        ]
    ]);
    ?>
</div>
