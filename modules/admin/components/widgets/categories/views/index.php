<?php

use \app\models\Categories;
use \app\components\extend\Html;
use \app\modules\admin\components\widgets\categories\CategoriesTreeWidget;
use \app\modules\admin\components\widgets\categories\CategoriesTreeWidgetAssets;
use \app\components\extend\extensions\JsTree;

CategoriesTreeWidgetAssets::register($this);

$plugins = [
    "themes",
    "json_data",
    'changed',
    'types'
];
if (yii::$app->user->can('categories-update')) {
    $plugins[] = 'dnd';
}
?>
<div class="categories-widget">
    <?=
    JsTree::widget([
        'id' => 'jsTreeCategoriesWidget',
        'jsOptions' => [
            'plugins' => $plugins,
            'checkbox' => [
                'three_state' => false,
                "keep_selected_style" => false,
            ],
            'types' => [
                "#" => [
                    "max_children" => 1
                ],
            ],
            'core' => [
                "check_callback" => true,
                'multiple' => false,
                'data' => CategoriesTreeWidget::getTreeArray($type, $model, true),
                'themes' => [
                    'url' => '/public/plugins/proton/style.css',
                    'dots' => true,
                    'icons' => true,
                    'name' => 'proton',
                    'responsive' => true
                ]
            ],
        ]
    ]);
    ?>
</div>
