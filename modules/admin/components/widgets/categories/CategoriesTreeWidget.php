<?php

namespace app\modules\admin\components\widgets\categories;

use app\models\File;
use yii\base\Widget;
use app\models\Categories;
use app\components\extend\yii;
use app\components\extend\Url;
use app\components\extend\Html;

class CategoriesTreeWidget extends Widget
{

    public $type;
    public $model;
    public $view = 'index';

    /**
     * @var array $parents
     */
    static $parents = [];

    public function run()
    {
        return $this->render($this->view, [
                    'model' => new Categories(),
                    'type' => $this->type,
                    'model' => $this->model
        ]);
    }

    /**
     *
     * @param type $type
     * @param type $model
     * @return array
     */
    public static function getTreeArray($type, $model = false, $control = false)
    {
        $items[] = static::getRootItem($model);
        $models = Categories::find()->where(['type' => ($type == null ? Categories::TYPE_MAIN : $type)])->orderBy(['parent' => SORT_ASC, 'order' => SORT_ASC])->all();
        if ($models) {
            foreach ($models as $m) {
                extract(static::getCategoriesManageButtons($m));
                $disabled = static::checkDisabled($m, $model);
                $link = Html::tag('span', $m->url, ['class' => 'a categories-url-link', 'icon' => 'link', 'onclick' => "window.open('" . $m->url . "','_blank');"]);
                $items[] = [
                    'id' => $m->primaryKey,
                    'text' => $m->renderImage([
                        'width' => 25,
                        'height' => 25,
                    ]) . '  ' . ($m->title ? $m->title : ($m->initTranslation(true)) . $m->title)
                    . ($model ? '' : $link . (!$control ? '' : ' - ' . static::warningDisabled($m) . ' &nbsp;&nbsp;' . $view . '&nbsp' . $update . '&nbsp;' . $delete)),
                    'class' => 'hidden',
                    'parent' => ($m->parent > 0 ? $m->parent : 'root'),
                    'li_attr' => [
                        'data-id' => $m->primaryKey,
                        'data-update-url' => Url::to(['update', 'id' => $m->primaryKey])
                    ],
                    'state' => [
                        'disabled' => $disabled, 'opened' => false, 'selected' => ($model && ($m->primaryKey === $model->parent))]
                ];
            }
        }
        return $items;
    }

    /**
     *
     * @param type $currentModel
     * @param type $comparedModel
     * @return boolean
     */
    public static function checkDisabled($currentModel, $comparedModel)
    {
        if (in_array($currentModel->parent, static::$parents, true)) {
            static::$parents[] = $currentModel->primaryKey;
            return true;
        }
        if ($comparedModel && ($currentModel->primaryKey === $comparedModel->primaryKey)) {
            static::$parents[] = $currentModel->primaryKey;
            return true;
        }
        return false;
    }

    public static function getRootItem($model)
    {
        return [
            'id' => 'root',
            'text' => yii::$app->l->t('Categories'),
            'icon' => 'glyphicon glyphicon-folder-open',
            'parent' => '#',
            'state' => [
                'disabled' => !$model,
                'opened' => true,
                'selected' => ($model && ($model->isNewRecord || $model->parent == 0))
            ]
        ];
    }

    public static function getCategoriesManageButtons($model)
    {

        $onClickUrl = "App.redirect($(this).data('url'));";

        $updateDefault = yii::$app->user->can('categories-update') ? (Html::ico('pencil', [
                    'onclick' => $onClickUrl,
                    'title' => yii::$app->l->t('view', ['update' => false]),
                    'class' => 'a',
                    'data' => [
                        'type' => 'follow',
                        'url' => Url::to(['update', 'id' => $model->primaryKey])
                    ]
                ])) : '';
        $update = static::checkTButtons($model, $updateDefault);
        $delete = yii::$app->user->can('categories-delete') ? (Html::ico('trash', [
                    'onclick' => "yii.confirm('" . yii::$app->l->t('delete') . " ?',function(){ App.redirect('" . Url::to(['delete', 'id' => $model->primaryKey]) . "'); });return false;",
                    'title' => yii::$app->l->t('delete', ['update' => false]),
                    'class' => 'a',
                    'data' => [
                        'type' => 'delete',
                        'id' => $model->primaryKey,
                    ]
                ])) : '';
        $view = yii::$app->user->can('categories-view') ? (Html::ico('eye', [
                    'onclick' => $onClickUrl,
                    'title' => yii::$app->l->t('view', ['update' => false]),
                    'class' => 'a',
                    'data' => [
                        'type' => 'follow',
                        'id' => $model->primaryKey,
                        'url' => Url::to(['view', 'id' => $model->primaryKey]),
                    ]
                ])) : '';
        return ['view' => $view, 'delete' => $delete, 'update' => $update];
    }

    public static function checkTButtons($model, $alternative)
    {
        return yii::$app->l->multi ? $model->getTButtons() : $alternative;
    }

    public static function warningDisabled($model)
    {
        return $model->status != Categories::STATUS_ACTIVE ? '&nbsp;' . Html::ico('exclamation-triangle text-danger', [
                    'title' => yii::$app->l->t('disabled', ['update' => false])
                ]) : '';
    }

}
