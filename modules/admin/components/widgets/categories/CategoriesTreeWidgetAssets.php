<?php

namespace app\modules\admin\components\widgets\categories;

use yii\web\AssetBundle;

class CategoriesTreeWidgetAssets extends AssetBundle
{

    public $sourcePath = '@app/modules/admin/components/widgets/categories/assets';
    public $js = [
        'js.js',
    ];
    public $css = [
        'style.css',
    ];
    public $publishOptions = [
        'forceCopy' => true,
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
