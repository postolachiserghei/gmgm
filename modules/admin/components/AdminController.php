<?php

namespace app\modules\admin\components;

use yii\helpers\Json;
use app\models\Settings;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\extend\yii;
use app\components\extend\Html;
use app\components\extend\Model;
use app\components\extend\Controller;

class AdminController extends Controller
{

    /**
     * @inheritdoc
     */
    public function init()
    {
        $init = parent::init();
        $theme = Settings::getValue('adminTheme', 'default');
        $this->themeName = (array_key_exists($theme, yii::$app->params['frontendThemes']) ? $theme : 'default');
        return $init;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => $this->allowActions,
                        'allow' => true,
                    ],
                    [
                        'actions' => ['error', 'resetPassword', 'requestPasswordReset', 'login', 'undo', 'captcha', 'ln', 'requestpasswordreset', 'resetpassword'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'profile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'controllers' => ['updates'],
                        'actions' => [
                            'index',
                        ],
                        'allow' => false // (yii::$app->request->get('view') == 'apply' && yii::$app->user->can('admin-updates-index')),
                    ],
                    [
                        'actions' => [$this->action->id],
                        'allow' => yii::$app->user->can(),
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    /**
     * @param Model $model
     * @return mixed
     */
    public function saveModel($model)
    {
        if ($post = yii::$app->request->post($model->shortClassName)) {
            $model->attributes = $post;
            $this->ajaxValidation($model);
            if ($model->save()) {
                $this->setMessage('success');
                $get = yii::$app->request->get();
                return yii::$app->user->can($this->id . '-view') ? $this->redirect(['view', 'id' => $model->primaryKey] + $get) : $this->refresh();
            } else {
                $this->setMessage('error', (YII_ENV_DEV ? $model->getErrors(null, true) : null));
                return $this->refresh();
            }
        }
    }

    /**
     * Deletes an existing Seo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id = false)
    {
        $this->deleteSelected();
        $model = $this->findModel($id);
        if ($model->delete()) {
            $this->setMessage('success', yii::$app->helper->str()->getDefaultMessage('success'));
            if (strpos(yii::$app->request->referrer, 'index?') !== false)
                return $this->redirect(yii::$app->request->referrer);
        } else {
            $this->setMessage('error');
            return $this->refresh();
        }
        return $this->redirect(['index']);
    }

    /**
     * delete selected items by ids
     */
    public function deleteSelected()
    {
        $ids = yii::$app->request->post('ids');
        if (yii::$app->request->isPost && is_array($ids)) {
            /* @var $model \app\components\extend\Model */
            $model = $this->findModel(@$ids[0]);
            $ar = ['deleted' => 0, 'not-deleted' => 0, 'type' => 'error', 'message' => yii::$app->helper->str()->getDefaultMessage('error')];
            $post = yii::$app->request->post();
            if (array_key_exists('ids', $post) && is_array($post['ids'])) {
                $models = $model::find()->where(['in', $model::primaryKey(), $post['ids']])->all();
                if ($models) {
                    foreach ($models as $m)
                        $m->delete() ? $ar['deleted'] += 1 : $ar['not-deleted'] += 1;
                    $ar['message'] = Html::tag('p', ($ar['deleted'] > 0 ? yii::$app->helper->str()->getDefaultMessage('success') : yii::$app->helper->str()->getDefaultMessage('error')));
                    if (count($post['ids']) > 1 && $ar['deleted'] < count($post['ids'])) {
                        $ar['message'] .= Html::tag('p', yii::$app->l->t('deleted: {deleted}', ['deleted' => $ar['deleted']]), ['class' => 'text-info']);
                        $ar['message'] .= Html::tag('p', yii::$app->l->t('faild to delete: {not-deleted}', ['not-deleted' => $ar['not-deleted']]), ['class' => 'text-danger']);
                    }
                    $ar['type'] = $ar['deleted'] === count($post['ids']) ? 'success' : 'warning';
                }
            }
            $this->setMessage($ar['type'], $ar['message']);
            die(Json::encode($ar));
        }
    }

}
