<?php

namespace app\modules\admin;

use app\components\extend\yii;
use app\components\extend\Url;

class Admin extends \yii\base\Module
{

    public $controllerNamespace = 'app\modules\admin\controllers';
    public $defaultController = 'default';
    public $layout = 'main';
    public $id = 'admin';
    public $name = 'INIT-CP';

    public function init()
    {
        parent::init();
//        Yii::$app->errorHandler->errorAction = $this->id . '/' . $this->defaultController . '/error';
        $this->setUserConf();
        yii::$app->name = strtoupper($this->name);
        Yii::$app->homeUrl = Url::to(['/' . $this->id . '/default/index']);
        if (!yii::$app->user->isGuest) {
            yii::$app->id = $this->id;
        }
    }

    /**
     * user configuration
     */
    public function setUserConf()
    {
        /**
         * user
         */
        Yii::$app->set('user', [
            'identityClass' => 'app\models\User',
            'class' => 'app\components\Users',
            'enableAutoLogin' => true,
            'loginUrl' => [$this->id . '/' . $this->defaultController . '/login'],
            'identityCookie' => ['name' => $this->id, 'httpOnly' => true],
            'idParam' => $this->id,
        ]);
    }

}
