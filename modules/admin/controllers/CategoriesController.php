<?php

namespace app\modules\admin\controllers;

use app\components\extend\yii;
use app\models\Categories;
use app\models\search\CategoriesSearch;
use app\modules\admin\components\AdminController;
use yii\web\NotFoundHttpException;
use app\components\extend\ActiveForm;
use yii\helpers\Json;
use app\components\extend\Html;

/**
 * CategoriesController implements the CRUD actions for Categories model.
 */
class CategoriesController extends AdminController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return parent::behaviors();
    }

    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex($type = null)
    {
        $searchModel = new CategoriesSearch();
        $searchModel->type = ($type ? $type : Categories::TYPE_MAIN);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new Categories();
        $model->load($searchModel->attributes);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => $searchModel->type,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Categories model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Categories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type = null)
    {

        $model = new Categories();
        $model->type = ($type ? $type : Categories::TYPE_MAIN);
        $model->status = $model::STATUS_ACTIVE;
        if ($order = $model::find()->orderBy(['order' => SORT_DESC])->one()) {
            $model->order = ($order->order + 1);
        }
        $this->getCategoriesItemsJson($model);
        if ($response = $this->saveModel($model)) {
            return $response;
        }
        return $this->render('create', [
            'model' => $model,
            'servers' => $model->availableServers,
            'parentType' => ($model->type == $model::TYPE_GAME ? $model::TYPE_MAIN : $model::TYPE_GAME),
        ]);
    }

    /**
     * Updates an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->updatePositionsAndParents();
        $model = $this->findModel($id);
        $this->getCategoriesItemsJson($model);
        if ($response = $this->saveModel($model)) {
            return $response;
        }
        return $this->render('update', [
            'model' => $model,
            'servers' => $model->availableServers,
            'parentType' => ($model->type == $model::TYPE_GAME ? $model::TYPE_MAIN : $model::TYPE_GAME),
        ]);
    }

    public function updatePositionsAndParents()
    {
        if (yii::$app->request->isAjax) {
            $post = yii::$app->request->post();
            $rightData = (isset($post['items'], $post['type']) && is_array($post['items']));
            if ($rightData && $post['type'] === 'updatePositionsAndParents') {
                $arr = ['type' => 'error'];
                foreach ($post['items'] as $k => $v) {
                    if (isset($v['order'], $v['parent']) && $m = $this->findModel((int) $k, false)) {
                        $m->parent = $v['parent'];
                        $m->order = $v['order'];
                        if ($m->save()) {
                            $arr['message'] = yii::$app->l->t('Operation succeeded');
                            $arr['type'] = 'success';
                        } else {
                            $arr['message'] = yii::$app->l->t('Operation failed');
                        }
                    }
                }
                die(Json::encode($arr));
            }
        }
    }

    /**
     * conver Categories object into json for jsTree widget
     * return Json
     */
    public function getCategoriesItemsJson($model)
    {
        $get = yii::$app->request->get();
        if (yii::$app->request->isAjax && isset($get['operation']) && $get['operation'] === 'treeArray') {
            $type = (isset($get['type']) ? (int) $get['type'] : Categories::TYPE_MAIN);
            $ar = \app\modules\admin\components\widgets\Categories\CategoriesTreeWidget::getTreeArray($type, $model);
            die(Json::encode($ar));
        }
    }

    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param boolean $throw (throw exception)
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $throw = true)
    {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        } else {
            if ($throw) {
                throw new NotFoundHttpException('The requested page does not exist.');
            } else {
                return null;
            }
        }
    }

}
