<?php

namespace app\modules\admin\controllers;

use yii\helpers\Json;
use app\models\I18nMessage;
use app\components\extend\yii;
use app\models\I18nMessageSource;
use yii\web\NotFoundHttpException;
use app\models\search\I18nMessageSourceSearch;
use app\modules\admin\components\AdminController;

/**
 * TranslationsController implements the CRUD actions for I18nMessageSource model.
 */
class TranslationsController extends AdminController
{

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->defaultAction = 'update';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return parent::behaviors();
    }

    /**
     * Lists all I18nMessageSource models.
     * @return mixed
     */
    public function actionUpdate()
    {
        $this->translationsUpdate();
        $searchModel = new I18nMessageSourceSearch();
        $dataProvider = $searchModel->search(yii::$app->request->queryParams);
        $model = new I18nMessageSource;
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
        ]);
    }

    /**
     * update translations 
     */
    public function translationsUpdate()
    {
        $post = yii::$app->request->post();
        if (!yii::$app->request->isAjax || !isset($post['category'])) {
            return null;
        }
        $ar = ['response' => 'error'];
        if (yii::$app->user->can('translations-update')) {
            YII_ENV_DEV ? $ar['post'] = $post : '';
            if (isset($post['source'], $post['category'], $post['language'], $post['new'])) {
                if ($model = I18nMessageSource::find()->where('message=:s AND category=:c', ['s' => $post['source'], 'c' => $post['category']])->one()) {
                    $t = I18nMessage::find()->where('id=:id AND language=:l', ['id' => $model->id, 'l' => $post['language']])->one();
                    if (!$t) {
                        $t = new I18nMessage;
                    }
                    $t->id = $model->id;
                    $t->language = $post['language'];
                    $t->translation = $post['new'];
                    if ($t->validate() && $t->save()) {
                        $ar['response'] = 'success';
                    }
                }
            }
        }
        /* TODO #PS: translates */
        $ar['message'] = yii::$app->helper->str()->getDefaultMessage($ar['response']);
        die(Json::encode($ar));
    }

    /**
     * Finds the I18nMessageSource model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return I18nMessageSource the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = I18nMessageSource::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
