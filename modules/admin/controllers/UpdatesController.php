<?php

namespace app\modules\admin\controllers;

use yii\helpers\Json;
use app\models\Updates;
use app\components\extend\yii;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;
use app\modules\admin\components\AdminController;

/**
 * UpdatesController implements the CRUD actions for Updates model.
 */
class UpdatesController extends AdminController
{

    /**
     * Lists all items available for update generation.
     * @return mixed
     */
    public function actionApply($rewrite = null)
    {
        $model = new Updates();
        $model->rewrite = $rewrite;
        $this->apply($model);
        $dataProvider = new ArrayDataProvider();
        $dataProvider->setModels($model->getUpdates());
        return $this->render('apply', [
                    'dataProvider' => $dataProvider,
                    'model' => $model,
        ]);
    }

    /**
     * apply updates from files
     * @param Updates $model
     * @return json | null
     */
    public function apply($model)
    {
        $post = yii::$app->request->post();
        if (!isset($post['data'], $post['type']) || !yii::$app->request->isAjax) {
            return null;
        }
        $ar = ['response' => 'error', 'result' => null, 'post' => (YII_ENV_DEV ? $post : null)];
        $type = $post['type'];
        $data = $post['data'];
        switch ($type) {
            case 'applyUpdates':
                $ar['result'] = $model->applyUpdates($data);
                $ar['response'] = 'success';
                break;
            default:
                break;
        }
        die(Json::encode($ar));
    }

    /**
     * Lists all items available for update generation.
     * @return mixed
     */
    public function actionGenerate()
    {
        $model = new Updates();
        $this->generate($model);
        $dataProvider = new ArrayDataProvider();
        $dataProvider->setModels($model->tables);
        return $this->render('generate', [
                    'dataProvider' => $dataProvider,
                    'model' => $model,
        ]);
    }

    /**
     * generates update files
     * @param Updates $model
     * @return json | null
     */
    public function generate($model)
    {
        $post = yii::$app->request->post();
        if (!isset($post['data'], $post['type']) || !yii::$app->request->isAjax) {
            return null;
        }
        $ar = ['response' => 'error', 'result' => null, 'post' => (YII_ENV_DEV ? $post : null)];
        $type = $post['type'];
        $data = $post['data'];
        switch ($type) {
            case 'generatePrepare':
                $ar['result'] = $model->selectThisTables($data);
                $ar['response'] = 'success';
                break;
            case 'saveUpdateItems':
                if (isset($post['order'])) {
                    $items = $model->getUpdateItems($data, (isset($post['condition']) ? $post['condition'] : null));
                    $ar['result'] = $model->saveUpdates($items, $post['order']);
                    $ar['response'] = 'success';
                } else {
                    return;
                }
                break;
            default:
                break;
        }
        die(Json::encode($ar));
    }

    /**
     * Finds the Updates model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Updates the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Updates::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
