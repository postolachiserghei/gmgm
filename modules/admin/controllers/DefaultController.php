<?php

namespace app\modules\admin\controllers;

use app\components\extend\yii;
use app\modules\admin\components\AdminController;
use app\models\forms\LoginForm;
use app\models\forms\PasswordResetRequestForm;
use app\models\forms\ResetPasswordForm;
use app\components\extend\Html;

class DefaultController extends AdminController
{

    public function actions()
    {
        return array_merge(parent::actions(), [
            'error' => [
                'class' => '\app\components\extend\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ]);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionRequestpasswordreset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(yii::$app->request->post())) {
            $this->ajaxValidation($model);
            if ($model->validate() && $model->sendEmail('admin/default/resetpassword')) {
                $this->setMessage('success');
                return $this->redirect(['/admin/default/login']);
            } else {
                $this->setMessage('error');
                return $this->refresh();
            }
        }

        return $this->render('requestPasswordResetToken', [
                    'model' => $model,
        ]);
    }

    public function actionResetpassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(yii::$app->request->post())) {
            if ($model->validate() && $model->resetPassword()) {
                $this->setMessage('success');
                return $this->goHome();
            } else {
                $this->setMessage('error');
            }
            return $this->refresh();
        }

        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

}
