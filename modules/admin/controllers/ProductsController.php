<?php

namespace app\modules\admin\controllers;

use yii\helpers\Json;
use app\models\Products;
use app\components\extend\yii;
use yii\web\NotFoundHttpException;
use app\models\search\ProductsSearch;
use app\modules\admin\components\AdminController;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends AdminController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return parent::behaviors();
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(yii::$app->request->queryParams);
        $model = new Products;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();
        if ($response = $this->saveModel($model)) {
            return $response;
        }
        return $this->render('create', [
            'categories' => $model->availableCategories,
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        $this->moderateSelectedItems();
        $model = $this->findModel($id);
        if ($response = $this->saveModel($model)) {
            return $response;
        }
        return $this->render('update', [
            'categories' => $model->availableCategories,
            'model' => $model,
        ]);
    }

    /**
     * moderate selected items
     */
    public function moderateSelectedItems()
    {
        $post = yii::$app->request->post();
        if (yii::$app->request->isAjax && @$post['type'] == 'moderateSelectedItems' && is_array(@$post['ids']) && @$post['operation'] !== null) {
            $updated = Products::updateAll(['status' => ($post['operation'])], ['in', 'id', $post['ids']]);
            $this->setMessage('success');
            echo Json::encode(['result' => 'success']);
            die();
        }
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
