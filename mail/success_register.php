<?php

use app\components\extend\Url;
use app\components\extend\Html;

/* @var $this yii\web\View */
/* @var $user app\models\User */
/* @var $password string */

$this->title = $subject;
?>


<?=
Html::tag('span', yii::$app->l->t('hello {username}', [
            'username' => Html::encode($user->fullName)
]));
?>

<p>
    <?=
    yii::$app->l->t('you have been successfuly registered on {app}', [
        'app' => Html::a(yii::$app->name, Url::to('/', true))
    ])
    ?>
</p>
<p>
    <b><?= yii::$app->l->t('details') ?>:</b>
<div>
    <?= yii::$app->l->t('login') ?>: <?= $user->email ?>
    <br/>
    <?= yii::$app->l->t('password') ?>: <?= $password ?>
</div>
</p>