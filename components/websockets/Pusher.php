<?php

namespace app\components\websockets;

use yii\helpers\Json;
use Ratchet\MessageComponentInterface;

/**
 * Description of Pusher
 *
 * @author Postolachi Serghei
 */
class Pusher implements MessageComponentInterface
{

    protected $users = [];

    /**
     * on open
     * @param \Ratchet\ConnectionInterface $conn
     */
    public function onOpen(\Ratchet\ConnectionInterface $conn)
    {
        
    }

    /**
     * on message
     * @param \Ratchet\ConnectionInterface $from
     * @param type $msg
     */
    public function onMessage(\Ratchet\ConnectionInterface $from, $msg)
    {
        $data = Json::decode($msg);
        if (!is_array($data)) {
            return;
        }
        if (array_key_exists('u', $data) && array_key_exists('dest', $data) && $data['dest'] == 'registeruser') {
            $this->users[( $data['u'] > 0 ? $data['u'] : uniqid())] = $from;
            return;
        }
        if (array_key_exists('to', $data)) {
            switch ($data['to']) {
                case 'all':
                    foreach ($this->users as $u) {
                        if ($u->resourceId !== $from->resourceId && $u) {
                            $u->send($this->normalizeMessage($data, $from));
                        }
                    }
                    break;
                default :
                    if (is_array($data['to'])) {
                        foreach ($data['to'] as $id) {
                            if (array_key_exists($id, $this->users) && $connection = $this->users[$id]) {
                                $connection->send($this->normalizeMessage($data, $connection, $from));
                            }
                        }
                    } else {
                        $id = $data['to'];
                        if (array_key_exists($id, $this->users) && $connection = $this->users[$id]) {
                            $connection->send($this->normalizeMessage($data, $connection, $from));
                        }
                    }
                    break;
            }
        }
    }

    /**
     * on close
     * @param \Ratchet\ConnectionInterface $conn
     */
    public function onClose(\Ratchet\ConnectionInterface $conn)
    {
        if ($user = array_search($conn, $this->users)) {
            unset($this->users[$user]);
        }
    }

    /**
     * on error
     * @param \Ratchet\ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(\Ratchet\ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }

    /**
     * normalize message
     * @param mixed $message
     * @param \Ratchet\ConnectionInterface $connection
     * @return type
     */
    public function normalizeMessage($data, $connection, $sender = null)
    {
        $message = [
            'receiver' => $connection->resourceId,
            'sender' => ($sender ? $sender->resourceId : $connection->resourceId),
        ];
        if (!is_array($data)) {
            $message['data'] = $data;
        } else {
            $message = array_merge($message, $data);
        }
        return Json::encode($message);
    }

}
