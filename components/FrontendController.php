<?php

namespace app\components;

use app\models\Seo;
use yii\filters\VerbFilter;
use app\components\extend\yii;
use yii\filters\AccessControl;
use app\components\extend\View;
use app\components\extend\Controller;
use app\components\widgets\shopping_cart\ShoppingCartWidget;

class FrontendController extends Controller
{

    public function init()
    {
        $init = parent::init();
        $theme = yii::$app->helper->settings()->getValue('frontendTheme', 'default');
        $this->themeName = (array_key_exists($theme, yii::$app->params['frontendThemes']) ? $theme : 'default');
        return $init;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => $this->allowActions,
                        'allow' => true,
                    ],
                    [
                        'controllers' => ['site'],
                        'actions' => [
                            'change-language',
                            'error',
                            'login',
                            'contact',
                            'about',
                            'index',
                            'signup',
                            'download',
                            'ln',
                            'reset-password',
                            'request-password-reset',
                            'translations-update',
                        ],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [$this->action->id],
                        'allow' => yii::$app->user->can(),
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), ShoppingCartWidget::actions());
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $ba = parent::beforeAction($action);
        $this->registerCustomScripts();
        return $ba;
    }

    /**
     * register custom scripts from admin settings
     */
    public function registerCustomScripts()
    {
        $headScript = yii::$app->helper->settings()->getValue('headScript', null);
        $bodyScript = yii::$app->helper->settings()->getValue('bodyScript', null);
        $ebs = array_flip(array_map('trim', explode(',', yii::$app->helper->settings()->getValue('excludeBodyScript', null))));
        $ehs = array_flip(array_map('trim', explode(',', yii::$app->helper->settings()->getValue('excludeHeadScript', null))));
        yii::$app->request->seo ? yii::$app->view->registerJs(yii::$app->request->seo->seo->getData(Seo::ADDITIONAL_DATA_HEAD_SCRIPT)) : '';
        yii::$app->request->seo ? yii::$app->view->registerJs(yii::$app->request->seo->seo->getData(Seo::ADDITIONAL_DATA_BODY_SCRIPT)) : '';
        $url = substr(yii::$app->request->url, 0, strpos(yii::$app->request->url, "?"));
        if ($url == '') {
            $url = '/';
        }
        if ($headScript && trim($headScript) != '' && !array_key_exists($url, $ehs)) {
            yii::$app->view->registerJs($headScript, View::POS_HEAD, 'headScript');
        }
        if ($bodyScript && trim($bodyScript) != '' && !array_key_exists($url, $ebs)) {
            yii::$app->view->registerJs($bodyScript, View::POS_END, 'bodyScript');
        }
    }

}
