<?php

namespace app\components;

use app\components\extend\yii;
use app\models\t\SeoT;
use app\models\Seo;
use yii\db\Expression;
use yii\web\NotFoundHttpException;
use app\models\Settings;

class Request extends \yii\web\Request
{

    public $web;
    public $backendUrl;
    public $seoAlias;
    public $allLanguages;
    public $seo;
    public $clearUri;

    /**
     * clear url
     * @param string $url
     * @param string $language
     * @return string
     */
    public function clearUrl($url = null, $language = null)
    {
        if (!$language) {
            $language = yii::$app->language;
        }
        $url = $url ? $url : $this->url;
        if (strpos($url, '?') !== false) {
            $url = substr($url, 0, strpos($url, "?"));
        }
        $u = trim(rawurldecode(mb_convert_encoding($url, "UTF-8", "auto")));
        $url = str_replace('/' . $language, '/', $u);
        $url = str_replace('//', '', $url);
        $url = str_replace('/' . $language . '/', '/', $u);
        return $url;
    }

    /**
     * check seo
     * @param type $route
     * @param type $get
     * @return type
     */
    public function checkSeo($route, $get)
    {
        $this->allLanguages = yii::$app->l->languages;
        $this->clearUri = $this->clearUrl($this->url);
        $query = SeoT::find();
        $query->select(Seo::tableName() . ".*," . SeoT::tableName() . '.*');
        $query->joinWith(['seo'])
        ->where('(alias=:a)', [
            'a' => $this->clearUri,
            'l_all' => implode(',', array_keys($this->allLanguages))
        ]);
        $query->andWhere([SeoT::tableName() . '.language_id' => yii::$app->language]);
        $query->orderBy([new Expression('FIND_IN_SET(language_id, :l_all)')]);
        $seoT = $query->one();
        if ($seoT) {
            $this->seo = $seoT;
            $seoURL = $seoT->seo->url;
            $get = $this->getParamsFromString($seoURL);
            $route = $this->seoAlias ? $this->seoAlias : $seoURL;
            $get = array_merge($get, $this->getParamsFromString(yii::$app->request->url), (yii::$app->l->multi ? ['l' => $seoT->language_id] : []));
            $_GET = $get;
        } else {
            if (!yii::$app->request->isAjax || yii::$app->request->isPjax) {
                $u = trim(rawurldecode($seoT ? $seoT->seo->url : $this->clearUri));
                $query = Seo::find();
                $query->joinWith(['seoT']);
                $query->where(['OR', ['url' => $u], ['alias' => $u]]);
                $seo = $query->one();
                $pass = $seo ? SeoT::find()->where(['seo_id' => $seo->primaryKey, 'language_id' => yii::$app->language])->count() > 0 : false;
                if ($pass && (!$seoT || ($seoT && ($seoT->seo->url != $seo->url)))) {
                    header("HTTP/1.1 301 Moved Permanently");
                    header('location: ' . (yii::$app->l->default == $seo->t->language_id ? '' : '/' . yii::$app->language) . $seo->alias);
                    exit();
                }
            }
        }
        return [$route, $get];
    }

    /**
     * parse string form get params
     * @param type $string
     * @return type
     */
    public function getParamsFromString($string)
    {
        $p = explode('?', $string);
        if (count($p) > 1 && array_key_exists(1, $p)) {
            $this->seoAlias = $p[0];
            parse_str($p[1], $params);
            return $params;
        }
        return [];
    }

    /**
     * @inheritdoc
     */
    public function resolve()
    {
        parent::resolve();
        $result = yii::$app->getUrlManager()->parseRequest($this);
        if ($result !== false) {
            list ($route, $params) = $result;
            if ($this->queryParams === null) {
                $_GET = $params + $_GET; // preserve numeric keys
            } else {
                $this->queryParams = $params + $this->queryParams;
            }
            return $this->checkSeo($route, $this->getQueryParams());
        } else {
            throw new NotFoundHttpException(yii::t('yii', 'Page not found.'));
        }
    }

    /**
     * @inheritdoc
     */
    public function getBaseUrl()
    {
        return str_replace($this->web, "", parent::getBaseUrl()) . $this->backendUrl;
    }

    /**
     * @inheritdoc
     */
    public function resolvePathInfo()
    {
        if ($this->getUrl() === $this->backendUrl) {
            return "";
        } else {
            return parent::resolvePathInfo();
        }
    }

    /**
     *
     * @param type $url
     * @param type $absolute
     * @return type
     */
    public function createUrl($url = [], $absolute = false)
    {
        if ($absolute)
            return yii::$app->urlManager->createAbsoluteUrl($url);
        return yii::$app->urlManager->createUrl($url);
    }

}
