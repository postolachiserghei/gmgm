<?php

namespace app\components\helper;

use app\models\User;
use app\components\extend\yii;

class UserHelper
{

    /**
     * return user \app\models\User if is logged in or new \app\models\User if not 
     * @return \app\models\User
     */
    public function identity()
    {
        $model = new User();
        $model->id = yii::$app->user->id;
        if (!yii::$app->user->isGuest) {
            $model->attributes = yii::$app->user->identity->attributes;
        } else {
            $model->username = yii::$app->l->t('guest') . '[' . $model->id . ']';
        }
        return $model;
    }

}
