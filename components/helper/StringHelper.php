<?php

namespace app\components\helper;

use app\components\extend\yii;
use yii\helpers\StringHelper as BaseStringHelper;

class StringHelper extends BaseStringHelper
{

    /**
     * will replace all tags: {attribute} with object attribute values
     * @param string $string
     * @param object/array $data
     * @param array $matchBetween default ['{','}']
     * @return string
     */
    public function replaceTagsWithDatatValues($string, $data, $matchBetween = ['{', '}'])
    {
        preg_match_all('#' . $matchBetween[0] . '(.*?)' . $matchBetween[1] . '#', $string, $matches);
        $params = [];
        if (count($matches) > 0) {
            if (array_key_exists(1, $matches) && is_array($matches[1])) {
                foreach ($matches[1] as $k)
                    $params[$matchBetween[0] . $k . $matchBetween[1]] = $k;
            }
        }
        foreach ($params as $k => $v) {
            try {
                $string = is_object($data) ?
                (@$data->{$v} !== null ? str_replace($k, $data->{$v}, $string) : $string) :
                (@$data[$v] !== null ? str_replace($k, $data[$v], $string) : $string);
            } catch (Exception $exc) {
                continue;
            }
        }
        return $string;
    }

    /**
     * get default messages by type ("success", "warning", "error")
     * @param string $type
     * @return string
     */
    public function getDefaultMessage($type)
    {
        $ar = [
            'success' => yii::$app->l->t('Operation succeeded'),
            'warning' => yii::$app->l->t('Operation failed'),
            'error' => yii::$app->l->t('Operation failed'),
        ];
        return @$ar[$type] ? $ar[$type] : $type;
    }

    /**
     * check if string contains substring
     * @param string $string
     * @param string $substring
     * @return boolean
     */
    public function strigContains($string, $substring)
    {
        if (mb_strpos($string, $substring) !== false) {
            return true;
        }
        return false;
    }

    /**
     * get string between some tags
     * @param string $string
     * @param string $start
     * @param string $end
     * @return string | null
     */
    public function getStringBetween($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = mb_strpos($string, $start);
        if ($ini == 0) {
            return null;
        }
        $ini += strlen($start);
        $len = mb_strpos($string, $end, $ini) - $ini;
        return mb_substr($string, $ini, $len);
    }

    /**
     * encode string into base64
     * @param type $str
     * @return type
     */
    public function base64Encode($str)
    {
        return strtr(base64_encode($str), '+/=', '-_,');
    }

    /**
     * decode string into base64
     * @param type $str
     * @return type
     */
    public function base64Decode($str)
    {
        return base64_decode(strtr($str, '-_,', '+/='));
    }

    /**
     *
     * @param string $string
     * @param string $key
     * @param type $type
     * @return type
     */
    public function encrypt($string, $key = 'my_encrypt_key_x', $type = null)
    {
        if (is_object($string) || is_array($string)) {
            $string = serialize($string);
        }
        return $this->base64Encode(utf8_encode(yii::$app->security->encryptByKey($string, $key)));
    }

    /**
     *
     * @param string $string
     * @param string $key
     * @param type $type
     * @return type
     */
    public function decrypt($string, $key = 'my_encrypt_key_x', $type = null)
    {
        $str = yii::$app->security->decryptByKey(utf8_decode($this->base64Decode($string)), $key);
        if ($unserialize = @unserialize($str)) {
            return $unserialize;
        }
        return $str;
    }

}
