<?php

namespace app\components\helper;

use app\components\extend\yii;

class Helper extends \yii\base\Component
{

    /**
     * data helper (get,set,unset -> sessions,kookies,params)
     * @param array $params
     * @return \app\components\helper\DataHelper
     */
    public static function data($params = [], $newInstance = false)
    {
        $className = '\app\components\helper\DataHelper';
        return self::getHelperObject($className, $params, $newInstance);
    }

    /**
     * email helper
     * @param array $params
     * @return \app\components\helper\EmailHelper
     */
    public static function email($params = [], $newInstance = false)
    {
        $className = '\app\components\helper\EmailHelper';
        return self::getHelperObject($className, $params, $newInstance);
    }

    /**
     * user helper
     * @param array $params
     * @return \app\components\helper\UserHelper
     */
    public static function user($params = [], $newInstance = false)
    {
        $className = '\app\components\helper\UserHelper';
        return self::getHelperObject($className, $params, $newInstance);
    }

    /**
     * string helper
     * @param array $params
     * @return \app\components\helper\StringHelper
     */
    public static function str($params = [], $newInstance = false)
    {
        $className = '\app\components\helper\StringHelper';
        return self::getHelperObject($className, $params, $newInstance);
    }

    /**
     * string helper
     * @param array $params
     * @return \app\components\helper\LogHelper
     */
    public static function log($params = [], $newInstance = true)
    {
        $className = '\app\components\helper\LogHelper';
        return self::getHelperObject($className, $params, $newInstance);
    }

    /**
     * file helper
     * @param array $params : default -> ['publicPath' => true, 'rootPath' => true]
     * @return \app\components\helper\FileHelper
     */
    public static function file($params = [], $newInstance = false)
    {
        $className = '\app\components\helper\FileHelper';
        return self::getHelperObject($className, $params, $newInstance);
    }

    /**
     *
     * @param array $params
     * @return \app\components\helper\ftp\FtpClient
     */
    public static function ftp($params = [], $newInstance = false)
    {
        $className = '\app\components\helper\ftp\FtpClient';
        return self::getHelperObject($className, $params, $newInstance);
    }

    /**
     *
     * @param array $params
     * @return \app\components\helper\SettingsHelper
     */
    public static function settings($params = [], $newInstance = false)
    {
        $className = '\app\components\helper\SettingsHelper';
        return self::getHelperObject($className, $params, $newInstance);
    }

    /**
     *
     * @param array $params
     * @return \app\components\helper\FormatterHelper
     */
    public static function formatter($params = [], $newInstance = false)
    {
        $className = '\app\components\helper\FormatterHelper';
        return self::getHelperObject($className, $params, $newInstance);
    }

    /**
     * get helper object
     * @param string $className
     * @param array $params
     * @param boolean $newInstance
     * @return \common\helpers\className
     */
    public static function getHelperObject($className, $params, $newInstance = false)
    {
        $indexClassname = 'helper-class-' . $className;
        if (!$newInstance && isset(yii::$app->params[$indexClassname])) {
            return yii::$app->params[$indexClassname];
        }
        $class = new $className($params);
        yii::$app->params[$indexClassname] = $class;
        return $class;
    }

}
