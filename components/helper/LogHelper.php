<?php

namespace app\components\helper;

use app\components\extend\yii;
use yii\bootstrap\Html;

class LogHelper extends \yii\base\Component
{

    public $email;

    const TYPE_INFO = 'info';
    const TYPE_SUCCESS = 'success';
    const TYPE_WARNING = 'warning';
    const TYPE_ERROR = 'error';

    public $details = false;
    public $initiator;

    /**
     * get log types labels
     * @param string|null $type
     * @return array|string
     */
    public function getTypeLabels($type = null)
    {
        $ar = [
            self::TYPE_INFO => yii::$app->l->t('info'),
            self::TYPE_SUCCESS => yii::$app->l->t('success'),
            self::TYPE_WARNING => yii::$app->l->t('warning'),
            self::TYPE_ERROR => yii::$app->l->t('error'),
        ];
        return $type !== null ? $ar[$type] : $ar;
    }

    public function init()
    {
        parent::init();
        if (!$this->email) {
            $this->setEmail(yii::$app->helper->data()->getParam('adminEmail', 'gaftonsifon@yandex.com'));
        }
        if (!$this->initiator) {
            $this->initiator = yii::$app->l->t('system', ['update' => 'false']);
        }
    }

    /**
     * set email
     * @param string $email
     */
    public function setEmail($email = null)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $data
     */
    public function info($data)
    {
        $this->add($data, self::TYPE_INFO);
    }

    /**
     * @param mixed $data
     */
    public function success($data)
    {
        $this->add($data, self::TYPE_SUCCESS);
    }

    /**
     * @param mixed $data
     */
    public function warning($data)
    {
        $this->add($data, self::TYPE_WARNING);
    }

    /**
     * @param mixed $data
     */
    public function error($data)
    {
        $this->add($data, self::TYPE_ERROR);
    }

    /**
     * @param mixed $data
     * @param string $type
     */
    public function add($data, $type = null, $details = null)
    {
        $this->save($data, $type, $details);
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setInitiator($name = 'app')
    {
        $this->initiator = $name;
        return $this;
    }

    /**
     * @param boolean $showDetails
     * @return $this
     */
    public function setDetails($showDetails = null)
    {
        $this->details = $showDetails;
        return $this;
    }

    protected function beforeRun()
    {
        $this->controller->enableCsrfValidation = false;
        return parent::beforeRun();
    }

    /**
     * log data
     * @param string $data
     * @param string $type
     */
    public function save($data, $type = null, $details = null)
    {
        if ($type === null) {
            $type = self::TYPE_INFO;
        }
        if ($details === null) {
            $details = $this->details;
        }
        if ($details === null && ($type == self::TYPE_ERROR || $type == self::TYPE_WARNING)) {
            $details = true;
        }
        $toSend = [];
        if ($details) {
            $toSend = [
                'details' => [
                    'action' => (yii::$app->module ? '/' . yii::$app->module->id : '' ) . '/' . yii::$app->controller->id . '/' . yii::$app->controller->action->id,
                    'request' => $_REQUEST,
                    'user_agent' => yii::$app->request->userAgent,
                    'user_ip' => yii::$app->request->userIP,
                    'headers' => yii::$app->request->headers,
                ]
            ];
        }
        if (is_object($data) || is_array($data)) {
            $toSend['data'] = $data;
            $toSend = '<pre>' . print_r($toSend, TRUE) . '</pre>';
        } else {
            if (count($toSend) > 0) {
                $toSend = $data . '<hr/><pre>' . print_r($toSend, TRUE) . '</pre>';
            } else {
                $toSend = $data;
            }
        }
        $toSend = Html::tag('div', $toSend);
        $emails = yii::$app->helper->settings()->getValue('receive_notifications_by_email', []);
        if ($emails && is_array($emails) && in_array($type, $emails)) {
            yii::$app->mailer->compose()
            ->setFrom($this->email)
            ->setTo($this->email)
            ->setSubject(yii::$app->name . ' - ' . $this->initiator . ' [' . $type . ']')
            ->setHtmlBody(Html::tag('h1', $type, ['style' => 'text-transform: uppercase;color:' . $this->getTypeColors($type) . ';']) . $toSend)
            ->send();
        }
    }

    /**
     *
     * @param string $type
     * @return array|string
     */
    public function getTypeColors($type = null)
    {
        $ar = [
            self::TYPE_INFO => '#74b0bb',
            self::TYPE_SUCCESS => '#56a045',
            self::TYPE_WARNING => 'orange',
            self::TYPE_ERROR => '#f75a5a',
        ];
        return $type === NULL ? $ar : @$ar[$type];
    }

}
