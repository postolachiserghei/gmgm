<?php

namespace app\components\extend;

use app\components\extend\yii;
use app\models\RecycleBin;
use app\components\extend\Html;
use yii\behaviors\TimestampBehavior;
use app\models\behaviors\JsonFields;
use app\components\extend\model\ModelQueries as MQ;

/**
 * @method mixed getData(string $jsonFieldDotdKey) json field value by key ex-> $model->getData('additional_data.my_key')
 */
class Model extends MQ
{

    const SCENARIO_UPLOAD_FILE = 'uploadFileScenario';

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        if (yii::$app->helper->str()->strigContains($name, 'additional_data[') || yii::$app->helper->str()->strigContains($name, 'additional_data.')) {
            return $this->getData($name);
        } else {
            return parent::__get($name);
        }
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value)
    {
        if (yii::$app->helper->str()->strigContains($name, 'additional_data[') || yii::$app->helper->str()->strigContains($name, 'additional_data.')) {
            return null;
        }
        return parent::__set($name, $value);
    }

    public $shortClassName;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->shortClassName) {
            $this->shortClassName = (new \ReflectionClass($this))->getShortName();
        }
        return parent::init();
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_UPLOAD_FILE => 'additional_data'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['additional_data'], 'safe'],
            [['updated_at', 'created_at', 'order'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'timeStampBehaviour' => [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated_at',
                'createdAtAttribute' => 'created_at',
            ],
            'jsonFields' => [
                'class' => JsonFields::className(),
                'fields' => [
                    'additional_data'
                ]
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'created_at' => yii::$app->l->t('created at'),
            'updated_at' => yii::$app->l->t('updated at'),
            'is_deleted' => yii::$app->l->t('is deleted'),
        ]);
    }

    /**
     * mod to get model errors as string as well
     * @param string $attribute
     * @param boolean $asString
     */
    public function getErrors($attribute = null, $asString = false)
    {
        $errors = parent::getErrors($attribute);
        if (!$asString)
            return $errors;
        $tmp = '';
        foreach ($errors as $k => $v) {
            $tmp .= Html::tag('div', ' - ' . $v[0]);
        }
        return $tmp;
    }

    /**
     * adds current language at the end of the attribute label (if record language is different from the current)
     * @param array $labels
     * @return array
     */
    public function LanguageNoteLabels($labels)
    {
        $ret = [];
        $language = '';
        if (yii::$app->l->multi) {
            $l = yii::$app->request->get('l');
            $language = ($l && $l != yii::$app->language) ? ' (' . yii::$app->l->languages[$l] . ')' : '';
        }
        foreach ($labels as $k => $v) {
            $ret[$k] = $v . $language;
        }
        return $ret;
    }

    public function fakeRule()
    {
        
    }

    /**
     * get model rule param value 
     * @param string $attribute
     * @param string $method
     * @param string $param
     * @param mixed $alternative
     * @return mixed
     */
    public function getRuleParam($attribute, $method, $param, $alternative = null)
    {
        foreach ($this->rules() as $k => $v) {
            if (($v[0] == $attribute || (is_array($v[0]) && in_array($attribute, $v[0]))) && $v[1] == $method) {
                if (array_key_exists($param, $v)) {
                    $result = $v[$param];
                }
                break;
            }
        }
        return isset($result) ? $result : $alternative;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $bs = parent::beforeSave($insert);
        if (!$this->order) {
            if ($o = $this->find()->orderBy(['order' => SORT_DESC])->one()) {
                $this->order = $o->order + 1;
            } else {
                $this->order = 1;
            }
        }
        return $bs;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        $as = parent::afterSave($insert, $changedAttributes);
        if ($this->is_deleted == self::DELETED_FALSE) {
            $this->restoreItemFromTrash();
        }
        return $as;
    }

    /**
     * @inheritdoc
     */
    public function delete()
    {
        if ($this->is_deleted === self::DELETED_FALSE) {
            $this->is_deleted = self::DELETED_TRUE;
            $rb = new RecycleBin();
            $rb->model = serialize($this->className());
            $rb->model_pk = serialize($this->primaryKey);
            $this->setScenario(self::SCENARIO_DELETE);
            if ($this->save()) {
                return $rb->save();
            } else {
                if (YII_ENV_DEV) {
                    echo '<pre>' . print_r($this->getErrors(), TRUE) . '</pre>';
                    die();
                }
            }
        } else {
            $this->is_deleted = self::DELETED_IRREVERSIBLE;
            if ($deleted = parent::delete()) {
                $this->restoreItemFromTrash();
                return $deleted;
            }
        }
    }

    /**
     * remove all items from trash
     */
    public function restoreItemFromTrash()
    {
        RecycleBin::deleteAll('model=:m AND model_pk=:pk', ['m' => serialize($this->className()), 'pk' => serialize($this->primaryKey)]);
    }

}
