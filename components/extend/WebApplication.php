<?php

namespace app\components\extend;

/**
 * Description of Application
 *
 * @author Postolachi Serghei
 *
 * @property \app\models\User $user
 * @property \app\components\Users $user
 * @property \app\components\extend\Controller $controller
 * @property \app\models\Languages $l Languages model
 * @property \app\components\helper\Helper $helper base Helper class
 * @property \app\components\Request $request base Helper class
 * @property UrlManager $urlManager base Helper class
 */
class WebApplication extends \yii\web\Application
{
    public function init()
    {
        return parent::init();
    }
}
