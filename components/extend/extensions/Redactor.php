<?php

namespace app\components\extend\extensions;

use app\components\extend\yii;
//use yii\redactor\widgets\Redactor as Imperavi;
use app\components\widgets\redactor\RedactorWidget;

class Redactor extends RedactorWidget
{

    /**
     * @inheritdoc
     */
    public function init()
    {
        $init = parent::init();
        foreach (self::getDefaultSettings() as $key => $value) {
            $this->{$key} = array_merge($value, $this->{$key});
        }
        return $init;
    }

    /**
     * 
     * @param array $customSettings
     * @param array $customOptions
     * @return array
     */
    public static function getDefaultSettings($customSettings = [], $customOptions = [])
    {
        $pasteBlockTags = [
            'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'table', 'tbody', 'thead', 'tfoot', 'th', 'tr', 'td', 'ul', 'ol', 'li', 'blockquote', 'pre',
            'div', 'span', 'i', 'block', 'p'
        ];
        $pasteInlineTags = ['strong', 'b', 'u', 'em', 'i', 'code', 'del', 'span', 'ins', 'samp', 'kbd', 'sup', 'sub', 'mark', 'var', 'cite', 'small'];
        $pasteTags = array_merge($pasteInlineTags, $pasteBlockTags);

        return [
            'clientOptions' => array_merge([
                'language' => (yii::$app->language != 'en' ? yii::$app->language : 'ru'),
                'clean' => false,
                'minHeight' => 350,
                'pasteInlineTags' => $pasteTags,
                'pasteBlockTags' => $pasteTags,
                'formatting' => $pasteTags,
                'plugins' => [
                    'source',
                    'fullscreen',
//                    'table',
//                    'alignment',
//                    'clips',
//                    'inlinestyle',
//                    'codemirror',
//                    'properties',
                ],
                    ], $customSettings),
            'options' => array_merge([
                'style' => 'display:none'
                    ], $customOptions)
        ];
    }

}
