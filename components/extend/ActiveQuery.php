<?php

namespace app\components\extend;

use yii\db\Expression;
use app\components\extend\yii;
use yii\db\ActiveQuery as BaseActiveQuery;

class ActiveQuery extends BaseActiveQuery
{

    /**
     * get sql query
     * @return string
     */
    public function printSql()
    {
        return $this->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql;
    }

    public function andWhereJson($condition, $params = array())
    {
        if (is_array($condition) && count($condition) > 0) {
            foreach ($condition as $k => $w) {
                $key = (is_integer($k) ? $w : $k);
                if (is_array($key)) {
                    continue;
                }
                $c = $this->normalizeJsonConstants($key);
                $condition[$c] = ($condition[$k]);
                if ($c != $key) {
                    unset($condition[$key]);
                }
            }
        } else {
            $condition = $this->normalizeJsonConstants($condition);
        }
        if ($this->where === null) {
            $this->where = $condition;
        } elseif (is_array($this->where) && isset($this->where[0]) && strcasecmp($this->where[0], 'and') === 0) {
            $this->where[] = $condition;
        } else {
            $this->where = ['and', $this->where, $condition];
        }
        $this->addParams($params);
        return $this;
    }

    /**
     * replace all constant [] with . for json search
     * @param string $constant
     * @param string $begin
     * @param string $end
     * @return string
     */
    public function normalizeJsonConstants($constant, $tableName = null, $begin = '->"$.', $end = '"')
    {
        if (yii::$app->helper->str()->strigContains($constant, '[')) {
            $constant = str_replace('.', '{table_separator}', $constant);
            $constant = str_replace('[', '.', $constant);
            $length = explode(']', $constant);
            $constant = implode($end, explode(']', $constant, count($length)));
            $constant = str_replace(']', '', $constant);
            $constant = implode($begin, explode('.', $constant, 2));
            $constant = str_replace('{table_separator}', '.', $constant);
            $constant = str_replace('".', '.', $constant);
            return '(' . ($tableName ? $tableName . '.' : '') . $constant . ')';
        }
        return $constant;
    }

}
