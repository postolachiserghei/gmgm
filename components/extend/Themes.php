<?php

namespace app\components\extend;

use app\components\extend\View;
use app\components\extend\yii;

class Themes extends \yii\base\Theme
{

    const THEMES_FOLDER = '/themes/';

    public $themeName;

    /**
     * set theme by name (@app/themes/$themeName/...)
     */
    public function setTheme()
    {
        yii::$app->set('view', [
            'class' => View::className(),
            'theme' => [
                'class' => self::className(),
                'themeName' => $this->themeName,
                'pathMap' => [
                    '@yii/debug' => '@app/vendor/yiisoft/yii2-debug',
                    '@app/views' => '@app' . self::THEMES_FOLDER . $this->themeName . '/' . yii::$app->id . '/views',
                    '@app/modules/' . yii::$app->id . '/views' => '@app' . Themes::THEMES_FOLDER . $this->themeName . '/' . yii::$app->id . '/views',
                ],
            ],
        ]);
    }

}
