<?php

namespace app\components\extend;

use yii\data\ActiveDataProvider as BaseActiveDataProvider;

/**
 * Description of ActiveDataProvider
 *
 * @author Postolachi Serghei
 */
class ActiveDataProvider extends BaseActiveDataProvider
{

    /**
     * @inheritdoc
     */
    public function setSort($value)
    {
        if (is_array($value) && array_key_exists('defaultOrder', $value)) {
            $value['defaultOrder'] = array_merge($value['defaultOrder'], ['order' => SORT_ASC]);
        } else {
            $value['defaultOrder'] = ['order' => SORT_ASC];
        }
        return parent::setSort($value);
    }

}
