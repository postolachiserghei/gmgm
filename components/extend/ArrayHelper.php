<?php

namespace app\components\extend;

use app\components\extend\yii;
use yii\helpers\ArrayHelper as BaseArrayHelper;

class ArrayHelper extends BaseArrayHelper
{

    /**
     *
     * @param array $array
     * @param string $key
     * @param mixed $default
     */
    public static function getValue($array, $key, $default = null)
    {
        return parent::getValue($array, $key, $default);
    }

    /**
     *
     * @param array $array
     * @param string $key
     * @param mixed $value
     */
    public static function setValue($array, $key, $value)
    {
        $pathParts = explode('.', $key);
        $current = &$array;
        foreach ($pathParts as $k) {
            $current = &$current[$k];
        }
        $current = $value;
        return $array;
    }

}
