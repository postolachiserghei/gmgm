<?php

namespace app\components\extend;

use yii\console\Response;
use app\components\extend\yii;

/**
 * @property \app\components\helper\LogHelper $log log
 */
class ResponseConsole extends Response
{

    /**
     * get log
     * @return \app\components\helper\LogHelper
     */
    public function getLog()
    {
        return yii::$app->helper->log()->setInitiator($this->className());
    }

    /**
     * save log
     */
    public function saveLog()
    {
        $noteIf = [
            '500'
        ];
        if (in_array($this->statusCode, $noteIf)) {
            $this->log->error($this->data);
        }
    }

}
