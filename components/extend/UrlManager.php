<?php

namespace app\components\extend;

use app\components\extend\yii;
use yii\web\UrlManager as BaseUrlManager;

/**
 * @property string $homeUrl home url
 */
class UrlManager extends BaseUrlManager
{

    /**
     * @inheritdoc
     */
    public function init()
    {
        $init = parent::init();
        yii::$app->l->setLanguageOptions($this);
        return $init;
    }

    /**
     * @inheritdoc
     */
    public function getBaseUrl()
    {
        $bu = parent::getBaseUrl();
        return $bu;
    }

    /**
     * @inheritdoc
     */
    public function setBaseUrl($value)
    {

        $bu = parent::setBaseUrl($value);
        return $bu;
    }

    /**
     * get home url
     * @return type
     */
    public function getHomeUrl()
    {
        return $this->baseUrl;
    }

}
