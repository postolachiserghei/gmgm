<?php

namespace app\components\extend;

use app\components\extend\yii;
use kartik\icons\Icon;
use yii\bootstrap\Html as BaseHtml;

class Html extends BaseHtml
{

    /**
     * @inheritdoc
     */
    public static function a($text, $url = null, $options = [])
    {
        if ($url !== null) {
            $options['href'] = Url::to($url);
        }
        return static::tag('a', $text, $options);
    }

    /**
     * @inheritdoc
     */
    public static function icons()
    {
        $i = new Icon();
        yii::$app->params['icon-framework'] = 'fa';
        $i->map(yii::$app->view);
        return $i;
    }

    /**
     * @param string $name
     * @param array $options
     * @return html
     */
    public static function ico($name, $options = [], $tag = 'i')
    {
        if (yii::$app->request->isConsoleRequest) {
            return null;
        }
        return static::icons()->show($name, $options, null, true, $tag);
    }

    /**
     * @inheritdoc
     */
    public static function tag($name, $content = '', $options = array())
    {
        return parent::tag($name, static::checkIcon($content, $options), $options);
    }

    /**
     *
     * @param array $item
     * @return string (returns html tag with icon class)
     */
    public static function checkIcon($content, $options)
    {
        $tmp = '';
        if (isset($options['icon'])) {
            $tmp .= Html::ico($options['icon'], (isset($options['iconOptions']) ? $options['iconOptions'] : []));
        }
        return $tmp . $content;
    }

    /**
     * @inheritdoc
     */
    public static function beginForm($action = '', $method = 'post', $options = array())
    {
        $defaultOptions = [
            'data' => [
                'pjax' => yii::$app->controller->isPjaxAction
            ]
        ];
        $options['data'] = @$options['data'] ? array_merge($defaultOptions['data'], $options['data']) : $defaultOptions['data'];
        return parent::beginForm($action, $method, $options);
    }

    public static function img($src, $options = array())
    {
        $imgOptions = $options;
        $bgSize = 'cover';
        if (isset($options['bg-size'])) {
            $bgSize = $options['bg-size'];
        }
        if ($src =='') {
            $src = 'url(/public/img/1no-image.png)';
        }
        $bg = 'display:inline-block;background-repeat:no-repeat;background-position: 0;background-size: ' . $bgSize . ';background-repeat: no-repeat;background-image : url(' . $src . ');';
        self::addCssStyle($options, $bg);
        self::addCssClass($options, 'img-bg');
        self::addCssStyle($imgOptions, 'visibility:hidden!important;max-width:100%;', true);
        return self::tag('div', parent::img($src, $imgOptions), $options);
    }

}
