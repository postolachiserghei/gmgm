<?php

namespace app\components\extend;

use app\components\extend\yii;
use app\components\extend\Html;
use app\components\extend\ArrayHelper;

class DetailView extends \yii\widgets\DetailView
{

    public $format;

    public function init()
    {
        parent::init();
        $this->format = yii::$app->l->liveEditT ? 'html' : 'text';
    }

    /**
     * @param Model $model
     * @param array $additionalItems
     * @param array/boolean $ignore
     * @return array
     */
    public static function DefaultAttributes($model, $additionalItems = [], $ignore = false)
    {
        $attrs = $model->attributeLabels();
        if (is_array($ignore)) {
            foreach ($ignore as $i) {
                if ($i && array_key_exists($i, $attrs)) {
                    unset($attrs[$i]);
                }
            }
        }
        $a = [];
        if (array_key_exists('labels', $additionalItems)) {
            foreach ($additionalItems['labels'] as $k => $v) {
                $a[] = $v;
            }
            unset($additionalItems['labels']);
        }
        foreach ($attrs as $k => $v) {
            switch ($k) {
                case ($k == 'updated_at' || $k == 'created_at'):
                    $value = yii::$app->formatter->asDatetime($model->{$k}, 'long');
                    break;
                case 'is_deleted':
                    $value = $model->{$k} == Model::DELETED_TRUE ? yii::$app->l->t('yes') : yii::$app->l->t('no');
                    break;
                default:
                    if ($model->hasProperty('t')) {
                        $value = ArrayHelper::getValue(ArrayHelper::merge($model->attributes, $model->t->attributes), $k);
                    } else {
                        $value = ArrayHelper::getValue($model->attributes, $k);
                    }
                    break;
            }
            if (is_array($value)) {
                $value = 'ARRAY';
            }
            !array_key_exists($k, $additionalItems) ? $a[] = ['label' => $model->getAttributeLabel($k), 'value' => $value] : $a[$k] = $additionalItems[$k];
        }


        return $a;
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $rows = [];
        $i = 0;
        foreach ($this->attributes as $attribute) {
            if (!array_key_exists('format', $attribute) || $attribute['format'] == 'text') {
                $attribute['format'] = $this->format;
            }
            $rows[] = $this->renderAttribute($attribute, $i++);
        }

        $options = $this->options;
        $tag = ArrayHelper::remove($options, 'tag', 'table');
        echo Html::tag($tag, implode("\n", $rows), $options);
    }

}
