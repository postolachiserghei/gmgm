<?php

namespace app\components\extend;

use app\components\extend\yii;
use app\models\Seo;

class Url extends \yii\helpers\Url
{

    /**
     * @inheritdoc
     */
    public static function to($route = '', $scheme = false)
    {
        $r = parent::to($route, $scheme);
        $url = self::checkSeoLink($r);
        return trim(rawurldecode(mb_convert_encoding($url, "UTF-8", "auto")));
    }

    /**
     * check if url is part of SEO
     * @param string $url
     * @param string $language
     * @param boolean $createUrl
     * @return string
     */
    public static function checkSeoLink($url, $language = null, $createUrl = true)
    {
        if ($language === null) {
            $language = yii::$app->language;
        }
        if ($language === false) {
            $language = yii::$app->language;
        }
        $seo = null;
        $q = Seo::find();
        $q->joinWith('seoT');
        $q->where('alias=:a OR url=:u');
        $q->addParams([
            'u' => str_replace('/' . $language . '/', '/', $url),
            'a' => yii::$app->request->clearUrl($url)
        ]);
        if ($language !== false) {
            $q->andWhere(['language_id' => $language]);
        }
        $seo = $q->one();
        return $seo ? ($createUrl ? yii::$app->urlManager->createUrl($seo->t->alias) : $seo->alias) : $url;
    }

}
