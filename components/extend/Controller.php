<?php

namespace app\components\extend;

use yii\helpers\Json;
use app\models\Settings;
use app\models\BlackList;
use app\models\I18nMessage;
use app\components\extend\yii;
use app\components\extend\Url;
use app\components\extend\View;
use app\models\I18nMessageSource;
use app\components\extend\Themes;
use yii\web\Controller as BaseController;
use app\components\widgets\chat\ChatWidget;

class Controller extends BaseController
{

    public $flash;
    public $themeName = 'default';
    public $messages;
    public $isPjaxAction;
    public $allowActions = ['captcha'];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->checkBlackList();
        $init = parent::init();
        yii::$app->helper->data()->setParam('adminEmail', yii::$app->helper->settings()->getSetting('adminEmail'));
        $this->isPjaxAction = Settings::getValue('usePjax' . $this->module->id);
        return $init;
    }

    public function checkBlackList()
    {
        $ip = yii::$app->request->userIP;
        $blackListModel = new BlackList();
        if ($blackListModel->ipIsInBlockRanges || $blocked = BlackList::find()->where('value=:ip OR value=:id', ['id' => yii::$app->user->id, 'ip' => $ip])->one()) {
            echo yii::$app->l->t('Access forbidden, that\'s mean that your ip or your user id was blocked by site administration');
            die();
        }
    }

    /**
     * in controller call this function inside "behaviors" method
     * @param array $actions
     * @return array
     */
    public function addAllowedActions($actions = [])
    {
        return $this->allowActions = array_merge($this->allowActions, $actions);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), ChatWidget::actions());
    }

    /**
     * set flash message
     * @param string $type
     * @param string $message
     */
    public function setMessage($type = 'info', $message = null)
    {
        if (!$message) {
            $message = yii::$app->helper->str()->getDefaultMessage($type);
        }
        yii::$app->session->setFlash($type, $message);
        return $this;
    }

    /**
     * get flash
     */
    public function getFlashMessages()
    {
        $messages = Yii::$app->session->getAllFlashes();
        foreach ($messages as $key => $m) {
            if ($key == 'js') {
                $message = $m;
                yii::$app->view->registerJs($message, View::POS_END);
            } else {
                $message = str_replace('"', '\"', $m);
                yii::$app->view->registerJs('yii.mes("' . $message . '","' . $key . '")', View::POS_END);
            }
        }
        yii::$app->params['flash'] = $this->flash;
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $ba = parent::beforeAction($action);
        (new Themes(['themeName' => $this->themeName,]))->setTheme();
        if ($seo = yii::$app->request->seo) {
            /* @var $seo \app\models\t\SeoT */
            $seo->setMetaData($this);
        }
        if ($this->isPjaxAction && $pjaxUrl = yii::$app->helper->data()->getSession('X-PJAX-URL', null)) {
            yii::$app->response->headers->set('X-PJAX-URL', $pjaxUrl);
            yii::$app->helper->data()->removeSession('X-PJAX-URL');
        }
        return $ba;
    }

    /**
     * @inheritdoc
     */
    public function render($view, $params = array())
    {
        $this->getFlashMessages();
        if (yii::$app->request->isAjax) {
            $aj = $this->renderAjax($view, $params);
            $r = $this->renderContent($aj);
        } else {
            $r = parent::render($view, $params);
        }
        return $r;
    }

    /**
     * @inheritdoc
     */
    public function redirect($url, $statusCode = 302)
    {
        $u = Url::to($url);
        if ($this->isPjaxAction) {
            yii::$app->helper->data()->setSession('X-PJAX-URL', $u);
            return yii::$app->response->redirect($u, $statusCode, false);
        } else {
            return parent::redirect($u, $statusCode, true);
        }
    }

    /**
     * @inheritdoc
     */
    public function goHome()
    {
        return $this->redirect(yii::$app->getHomeUrl());
    }

    /**
     * @inheritdoc
     */
    public function goBack($defaultUrl = null)
    {
        return $this->redirect(yii::$app->getUser()->getReturnUrl($defaultUrl));
    }

    /**
     * @inheritdoc
     */
    public function refresh($anchor = '')
    {
        return $this->redirect(yii::$app->getRequest()->getUrl() . $anchor);
    }

    /**
     * ajax validate model datagoog
     * @param object $model
     * @param array $attributes
     */
    public function ajaxValidation($model, $attributes = null)
    {
        if (yii::$app->request->isAjax && !yii::$app->request->isPjax) {
            die(Json::encode(ActiveForm::validate($model, $attributes)));
        }
    }

    /**
     * ajax validate model datagoog
     * @param array $models
     * @param array $attributes
     */
    public function ajaxValidationMultiple($models, $attributes = null)
    {
        if (yii::$app->request->isAjax && !yii::$app->request->isPjax) {
            die(Json::encode(ActiveForm::validateMultiple($models, $attributes)));
        }
    }

    /**
     *
     */
    public function actionTranslationsUpdate()
    {
        $ar = ['response' => 'error'];
        $post = yii::$app->request->post();
        if (yii::$app->user->can('translations-update')) {
            YII_ENV_DEV ? $ar['post'] = $post : '';
            if (isset($post['source'], $post['category'], $post['language'], $post['new'])) {
                if ($model = I18nMessageSource::find()->where('message=:s AND category=:c', ['s' => $post['source'], 'c' => $post['category']])->one()) {
                    $t = I18nMessage::find()->where('id=:id AND language=:l', ['id' => $model->id, 'l' => $post['language']])->one();
                    if (!$t) {
                        $t = new I18nMessage;
                    }
                    $t->id = $model->id;
                    $t->language = $post['language'];
                    $t->translation = $post['new'];
                    if ($t->validate() && $t->save()) {
                        $ar['response'] = 'success';
                    }
                }
            }
        }
        /* TODO #PS: translates */
        $ar['message'] = yii::$app->helper->str()->getDefaultMessage($ar['response']);
        die(Json::encode($ar));
    }

}
