<?php

namespace app\components\extend\model;

use app\components\extend\yii;
use yii\db\ActiveRecord as AR;
use app\components\extend\ActiveQuery;
use app\components\extend\ArrayHelper;

/**
 * Description of ModelQueries
 *
 * @author Postolachi Serghei
 */

/**
 * @property integer $is_deleted model record is deleted
 * @property integer $updated_at updated date
 * @property integer $created_at created date
 * @property integer $order position order
 * @property array $additional_data additional data
 */
class ModelQueries extends AR
{

    const DELETED_FALSE = 0;
    const DELETED_TRUE = 1;
    const DELETED_IRREVERSIBLE = 2;
    const EVENT_SETTINGS_ASIGN = 'asignSettingsForModel';
    const SCENARIO_DELETE = 'scenario-delete';

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DELETE] = ['is_deleted'];
        return $scenarios;
    }

    /**
     * 
     * @inheritdoc
     * @param integer $deleteState Model::DELETE_FALSE | Model::DELETED_TRUE
     * @return ActiveQuery
     */
    public static function find($deleteState = self::DELETED_FALSE)
    {
        $q = yii::createObject(ActiveQuery::className(), [get_called_class()]);
        $q->andOnCondition([self::tableName() . '.is_deleted' => $deleteState]);
        return $q;
    }

    /**
     * 
     * @inheritdoc
     * @param mixed $condition primary key value or a set of column values
     * @param integer $deleteState Model::DELETE_FALSE | Model::DELETED_TRUE
     * @return yii\db\ActiveQuery
     */
    public static function findOne($condition, $deleteState = self::DELETED_FALSE)
    {
        return self::findByCondition($condition, $deleteState)->one();
    }

    /**
     * Finds ActiveRecord instance(s) by the given condition.
     * This method is internally called by [[findOne()]] and [[findAll()]].
     * @param mixed $condition please refer to [[findOne()]] for the explanation of this parameter
     * @param integer $deleteState Model::DELETE_FALSE | Model::DELETED_TRUE
     * @return ActiveQueryInterface the newly created [[ActiveQueryInterface|ActiveQuery]] instance.
     * @throws InvalidConfigException if there is no primary key defined
     * @internal
     */
    protected static function findByCondition($condition, $deleteState = self::DELETED_FALSE)
    {
        $query = self::find($deleteState);

        if (!ArrayHelper::isAssociative($condition)) {
            // query by primary key
            $primaryKey = static::primaryKey();
            if (isset($primaryKey[0])) {
                $condition = [$primaryKey[0] => $condition];
            } else {
                throw new InvalidConfigException('"' . get_called_class() . '" must have a primary key.');
            }
        }
        return $query->andWhere($condition);
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        if (YII_ENV_DEV && $this->hasErrors()) {
            yii::$app->helper->data()->setParam('errors', array_merge(yii::$app->helper->data()->getParam('errors', []), [
                $this->className() => $this->getErrors()
            ]));
        }
        return parent::afterValidate();
    }

}
