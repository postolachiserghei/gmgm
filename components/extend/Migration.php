<?php

namespace app\components\extend;

use app\components\extend\Model;

/**
 * extends yii base Migration
 *
 * @author Postolachi Serghei
 */
class Migration extends \yii\db\Migration
{

    const ON_D_U_RESTRICT = 'RESTRICT';
    const ON_D_U_CASCADE = 'CASCADE';
    const ON_D_U_NO_ACTION = 'NO ACTION';
    const ON_D_U_SET_DEFAULT = 'SET DEFAULT';
    const ON_D_U_SET_NULL = 'SET NULL';

    /**
     * @inheritdoc
     */
    public function createTable($table, $columns, $options = null)
    {
        if (is_array($columns)) {
            $columns['additional_data'] = $this->json()->comment('Additional data');
            $columns['created_at'] = $this->integer()->defaultValue(0)->notNull()->comment('Date created');
            $columns['updated_at'] = $this->integer()->defaultValue(0)->notNull()->comment('Date updated');
            $columns['order'] = $this->integer()->defaultValue(0)->notNull()->comment('Order position');
            $columns['is_deleted'] = $this->smallInteger()->defaultValue(Model::DELETED_FALSE)->comment('Is deleted');
        }
        $ct = parent::createTable($table, $columns, $options);
        if (is_array($columns)) {
            $this->addCommentOnColumn($table, 'additional_data', 'additional data');
        }
        return $ct;
    }

    public function json()
    {
        return $this->getDb()->getSchema()->createColumnSchemaBuilder('JSON');
    }

}
