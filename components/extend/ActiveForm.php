<?php

namespace app\components\extend;

use app\components\extend\yii;
use app\components\extend\Html;
use yii\bootstrap\ActiveForm as BaseActiveForm;

class ActiveForm extends BaseActiveForm
{

    public $fieldClass = 'app\components\extend\ActiveField';
    public $labelInPlaceholder = true;

    /**
     * @inheritdoc
     */
    public static function begin($config = array())
    {
        return parent::begin(self::defaultConfig($config));
    }

    /**
     * default form configuration
     * @param array $config
     * @return array
     */
    public static function defaultConfig($config = [])
    {
        $defaultOptions = [
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'options' => [
                'enctype' => 'multipart/form-data',
                'data' => [
                    'pjax' => yii::$app->controller->isPjaxAction
                ]
            ]
        ];
        if (isset($config['options'])) {
            $config['options'] = array_merge($config['options'], $defaultOptions['options']);
        }
        return array_merge($defaultOptions, $config);
    }

    /**
     * @inheritdoc
     * @return ActiveField the created ActiveField object
     */
    public function field($model, $attribute, $options = [])
    {
        /* @var $model Model */
        $field = parent::field($model, $this->snormalizeAttributeName($attribute), $options);
        $label = $model->getAttributeLabel($attribute);
        if (!$this->labelInPlaceholder) {
            $field->label($label);
        } else {
            $field->label(false);
            $field->inputOptions['placeholder'] = $label;
        }
        return $field;
    }

    /**
     * normalize attribute name for input name (if name will be given as : <<myfield.data.info>> then output will be: <<myfield[data][info]>>)
     * @param string $attribute
     * @return string
     */
    public function snormalizeAttributeName($attribute)
    {
        if (strpos($attribute, '.')) {
            $pieces = explode('.', $attribute);
            $attribute = '';
            $i = 0;
            foreach ($pieces as $p) {
                $attribute .= ($i === 0 ? $p : '[' . $p . ']');
                $i++;
            }
        }
        return $attribute;
    }

}
