<?php

namespace app\components\extend\sockets;

use \RuntimeException;
use React\Socket\Server as BaseServer;
use React\Socket\ConnectionException;

/**
 * Description of Server
 *
 * @author Postolachi Serghei
 */
class Server extends BaseServer
{

    /**
     * @inheritdoc
     */
    public function __construct($loop, array $context = array())
    {
        parent::__construct($loop, $context);
        $this->loop = $loop;
        $this->context = $context;
    }

    /**
     * @inheritdoc
     */
    public function listen($port, $host = '127.0.0.1')
    {
        if (strpos($host, ':') !== false) {
            // enclose IPv6 addresses in square brackets before appending port
            $host = '[' . $host . ']';
        }

        $this->master = @stream_socket_server(
                        "tcp://$host:$port", $errno, $errstr, STREAM_SERVER_BIND | STREAM_SERVER_LISTEN, stream_context_create(array('socket' => $this->context))
        );
        if (false === $this->master) {
            return null;
            $message = "Could not bind to tcp://$host:$port: $errstr";
            throw new ConnectionException($message, $errno);
        }
        stream_set_blocking($this->master, 0);

        $that = $this;

        $this->loop->addReadStream($this->master, function ($master) use ($that) {
            $newSocket = @stream_socket_accept($master);
            if (false === $newSocket) {
                $that->emit('error', array(new \RuntimeException('Error accepting new connection')));

                return;
            }
            $that->handleConnection($newSocket);
        });
    }

}
