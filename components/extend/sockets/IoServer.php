<?php

namespace app\components\extend\sockets;

use app\components\extend\sockets\Server as Reactor;
use app\components\extend\sockets\Factory as LoopFactory;
use Ratchet\MessageComponentInterface;

/**
 * Description of IoServer
 *
 * @author Postolachi Serghei
 */
class IoServer extends \Ratchet\Server\IoServer
{

    /**
     * @inheritdoc
     */
    public static function factory(MessageComponentInterface $component, $port = 80, $address = '0.0.0.0')
    {
        $loop = LoopFactory::create();
        $socket = new Reactor($loop);
        $socket->listen($port, $address);
        return new static($component, $socket, $loop);
    }

}
