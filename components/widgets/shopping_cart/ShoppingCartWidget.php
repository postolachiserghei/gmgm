<?php

namespace app\components\widgets\shopping_cart;

use yii\base\Widget;
use app\components\extend\yii;
use app\components\widgets\shopping_cart\ShoppingCartWidgetAction;

class ShoppingCartWidget extends Widget
{

    const BASE_SOURCE_PATH = '@app/components/widgets/shopping_cart/';

    public $view = 'index';
    public $params = [];

    public function run()
    {
        $this->params = array_merge(['widget' => $this], $this->params);
        return $this->render(yii::$app->controller->themeName . '/' . $this->view, $this->params);
    }

    /**
     * chat actions
     * @return array
     */
    public static function actions()
    {
        return [
            'shopping-cart' => ShoppingCartWidgetAction::className()
        ];
    }

}
