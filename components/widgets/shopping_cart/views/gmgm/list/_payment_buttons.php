<?php

use app\components\extend\yii;
use app\components\extend\Html;
use app\components\extend\ActiveForm;
use app\models\behaviors\PaymentBehavior;

/* @var $this app\components\extend\View */
/* @var $model \app\models\ShoppingCart */
/* @var $transaction app\models\Transactions */
?>
<div class="payment-systems-list">
    <?php
    $checkBoxes = '';
    foreach ($model->getAvailablePaymentSystems() as $key => $psystem) :
        /* @var $psystem \app\models\behaviors\payment\PaymentBase */
        $dataModel = $psystem->getAuthModel();
        $paymentSystemLabel = $model->getPaymentSystemLabels($key);
        $checkBoxes .= Html::tag('label', '&nbsp;'.$psystem->title . '&nbsp;' . Html::radio('system', ($checkBoxes == ''), [
            'onclick' => "ShoppingCart.setEl($(this)).setPaymentSystem()",
            'data' => [
                'form-class' => '.payment-system-pay-form',
                'system' => $key
            ],
        ]));
        if (($checkBoxes == '')) {
            echo Html::tag('script', "ShoppingCart.actions.submitPayment.params['system'] = '$key';");
        }
    endforeach;
    echo Html::tag('div', yii::$app->l->t('Способ оплаты') . $checkBoxes, [
        'class' => 'payment-options'
    ]);
    ?>
</div>