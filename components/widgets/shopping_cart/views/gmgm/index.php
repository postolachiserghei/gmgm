<?php

use yii\bootstrap\Modal;
use app\components\extend\Url;
use app\components\extend\Html;

/* @var $this app\components\extend\View */
/* @var $widget \app\components\widgets\chat\ChatWidget */
?>

<?php

$modalId = 'shopping-cart-modal';
Modal::begin([
    'id' => $modalId,
    'size' => Modal::SIZE_LARGE,
    'header' => Html::tag('h1', yii::$app->l->t('купить'), ['class' => 'text-uppercase']),
    'options' => [
        'class' => 'js-shopping-cart-container',
        'data' => [
            'url' => Url::to(['shopping-cart'])
        ]
    ]
]);
?>

<?php

Modal::end();
$js = <<<JS
    ShoppingCart.selectors =  {
        modal:'#$modalId',
        container:'#$modalId .modal-body',
    };
JS;
$this->registerJs($js);
