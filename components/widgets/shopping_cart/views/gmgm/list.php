<?php

use app\components\extend\Html;
use app\components\extend\yii;

/* @var $this app\components\extend\View */
/* @var $shoppingCart \app\models\ShoppingCart */
/* @var $dataProvider \app\components\extend\ActiveDataProvider */
/* @var $items[] \app\components\extend\Model */

yii::$app->view->registerJs('initQuantityControl()', $this::POS_END);
$paymentButtons = '';
?>


<div class="row">
    <div class="col-md-12">
        <div class="gamepage-lots-tablebox" style="margin: 0;    width: 96%;">
            <table>
                <tbody>
                    <tr>
                        <th>
                            <?= yii::$app->l->t('Информация о лоте') ?>
                        </th>
                        <th>
                            <?= yii::$app->l->t('Доставка') ?>
                        </th>
                        <th>
                            <?= yii::$app->l->t('Количество') ?>
                        </th>
                        <th>
                            <?= yii::$app->l->t('Цена') ?>
                        </th>
                    </tr>
                    <?php
                    foreach ($items as $id => $item) {
                        /* @var $item \app\models\Products */
                        if (!$item) {
                            echo Html::tag('tr', Html::tag('td', yii::$app->l->t('item with id {id} not found', [
                                'id' => $id
                            ]), ['class' => 'text-center alert alert-danger', 'colspan' => 4]));
                            continue;
                        }
                        ?>
                        <tr>
                            <td>
                                <div class="gamepage-lots-info">
                                    <div class="gamepage-lots-info-title">
                                        <span>
                                            <?= $item->title; ?>
                                        </span>
                                    </div>
                                    <div class="gamepage-lots-info-box">
                                        <?=
                                        $item->renderImage([
                                            'class' => 'gamepage-lots-info-img'
                                        ]);
                                        ?>
                                        <div class="gamepage-lots-info-text">
                                            <span>
                                                <?= $item->category->title ?>
                                            </span>
                                            <br>
                                            <span>
                                                <?= $item->type ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="gamepage-lots-delivery-box">
                                    <?= yii::$app->helper->formatter()->asDuration($item->getData($item::ADDITIONAL_DATA_DELIVERY_IN_DAYS)); ?>
                                </div>
                            </td>
                            <td>
                                <div class="gamepage-lots-value-box">
                                    <div class="gamepage-lots-setvalue-wrap"  data-item='<?= $item->primaryKey ?>' data-max='<?= $item->getAvailable(); ?>'>
                                        <span class="gamepage-lots-setvalue __minus">-</span>
                                        <input onchange="ShoppingCart.setEl($(this)).changeQuantitty(<?= $item->primaryKey; ?>, $(this).val(), true);" type="text" value="<?= $shoppingCart->getItemQuantity($item->primaryKey) ?>">
                                        <span class="gamepage-lots-setvalue __plus">+</span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="gamepage-lots-price-box">
                                    <span class="gamepage-lots-price-value">
                                        <span><?= $shoppingCart->getItemPrice($item->primaryKey); ?></span>
                                    </span>
                                </div>
                            </td>
                            <td class="remove-item-container">
                                <?=
                                Html::a('×', null, [
                                    'class' => 'remove-shopping-cart-item-bt',
                                    'data' => [
                                        'confirm-message' => yii::$app->l->t('delete "{item}" from shopping cart', [
                                            'item' => $item->title
                                        ])
                                    ],
                                    'onclick' => "ShoppingCart.setEl($(this)).remove($item->primaryKey);return false;",
                                ]);
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    $paymentButtons .= $this->render('list/_payment_buttons', [
                        'model' => $shoppingCart,
                        'transaction' => $shoppingCart->transaction
                    ]);
                    ?>

                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right total-price">
        <?= Html::checkbox('secure-deal'); ?>
        <?= Html::a(yii::$app->l->t('использовать безопасную сделку'), null); ?>
        <br/>
        <span class="text">
            <?= yii::$app->l->t('итого к оплате') ?>:
        </span>
        <?= Html::a($shoppingCart->getPrice(), null); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <?= $paymentButtons; ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <?=
        Html::a(yii::$app->l->t('оплатить'), '#', [
            'class' => 'button __orange __small js-payment-submit-button',
            'onclick' => 'ShoppingCart.setEl($(this)).submitPayment();return false;',
        ])
        ?>
    </div>
</div>