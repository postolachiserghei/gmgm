<?php

namespace app\components\widgets\shopping_cart;

use yii\base\Action;
use yii\helpers\Json;
use app\models\ShoppingCart;
use app\models\Transactions;
use app\components\extend\yii;
use app\components\extend\ActiveDataProvider;

/**
 * Description of ChatWidgetAction
 *
 * @author ps
 *
 * @property ShoppingCart $cart Shopping cart model
 */
class ShoppingCartWidgetAction extends Action
{

    public $cart;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        yii::$app->controller->addAllowedActions([
            'shopping-cart'
        ]);
    }

    /**
     * initialize shopping cart model
     */
    public function initShoppingCart()
    {
        $cookieKey = 'shopping_cart_id';
        $cartId = yii::$app->helper->data()->getCookie($cookieKey, null);
        if (!$cartId) {
            $this->createShoppingCartInstance($cookieKey);
        } else {
            if (!$this->cart = ShoppingCart::find()->where(['status' => ShoppingCart::STATUS_NEW, 'id' => $cartId])->one()) {
                $this->createShoppingCartInstance($cookieKey);
            }
        }
    }

    /**
     * create shopping cart instance
     * @param string $cookieKey
     */
    public function createShoppingCartInstance($cookieKey)
    {
        $this->cart = new ShoppingCart();
        $this->cart->status = ShoppingCart::STATUS_NEW;
        if ($this->cart->save()) {
            yii::$app->helper->data()->setCookie($cookieKey, $this->cart->primaryKey);
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (yii::$app->request->isAjax && !yii::$app->request->isPjax) {
            return Json::encode($this->getOperation(yii::$app->request->post('shopping-cart-request')));
        }
    }

    /**
     * action distributor
     * @param array $post
     * @return array
     */
    public function getOperation($post)
    {
        if (!$post || !isset($post['action'])) {
            return YII_ENV_DEV ? ['post' => $post] : [];
        }
        $this->initShoppingCart();
        $params = isset($post['params']) ? $post['params'] : [];
        switch ($post['action']) {
            case 'get-list':
                return $this->getList($params);
            case 'add-to-cart':
                $this->clearOldTransactions();
                return $this->addToCart($params);
            case 'change-quantity':
                $this->clearOldTransactions();
                return $this->changeQuantity($params);
            case 'remove-item':
                $this->clearOldTransactions();
                return $this->removeItem($params);
            case 'submit-payment':
                $this->clearOldTransactions();
                return $this->submitPayment($params);
            default:
                return YII_ENV_DEV ? ['post' => $post] : [];
        }
    }

    /**
     *
     * @param type $params
     * @return type
     */
    public function getList($params = [])
    {
        $params['items'] = $this->cart->items;
        return $params['items'] ? $this->render('list', $params) : '';
    }

    /**
     * add item to cart
     * @param type $params
     * @return type
     */
    public function removeItem($params = [])
    {
        $response = $this->cart->removeItem(@$params['id']) ? $result = $this->cart->getErrors() : 'success';
        return [
            'data' => $response
        ];
    }

    /**
     * add item to cart
     * @param type $params
     * @return type
     */
    public function addToCart($params = [])
    {
        $response = $this->cart->add($params)->hasErrors() ? $result = $this->cart->getErrors() : 'success';
        return [
            'data' => $response
        ];
    }

    /**
     * add item to cart
     * @param type $params
     * @return type
     */
    public function changeQuantity($params = [])
    {
        $response = [
            'result' => ($this->cart->setItemQuantity($params['id'], $params['quantity']) ? 'success' : 'error'),
        ];
        return [
            'data' => $response
        ];
    }

    /**
     * clear old transaction items
     */
    public function clearOldTransactions()
    {
        $query = Transactions::find();
        $query->where(['status' => Transactions::STATUS_NEW, 'item_model' => $this->cart->className(), 'item_id' => $this->cart->id]);
        if ($oldItems = $query->all()) {
            foreach ($oldItems as $transaction) {
                $transaction->delete();
            }
        }
    }

    /**
     *
     * @param array $params
     * @return array
     */
    public function submitPayment($params = [])
    {
        $response = [];
        if (!yii::$app->user->isGuest) {
            $transaction = $this->cart->addTransaction([
                'system' => @$params['system']
            ]);
            if ($transaction->hasErrors()) {
                $response['errors'] = $transaction->getErrors();
            } else {
                $transaction->goToPay();
                $response['redirect'] = $transaction->getData($transaction::ADDITIONAL_DATA_URL . '_' . $transaction->payment_system);
            }
        } else {
            $response['message'] = yii::$app->l->t('you need to be authorized before proceed to payment!');
            $response['status'] = 'warning';
        }
        return [
            'data' => $response
        ];
    }

    /**
     *
     * @param string $view
     * @param array $params
     * @return type
     */
    public function render($view, $params = [])
    {
        $params['shoppingCart'] = $this->cart;
        return yii::$app->controller->renderPartial(ShoppingCartWidget::BASE_SOURCE_PATH . 'views/' . yii::$app->controller->themeName . '/' . $view, $params);
    }

}
