<?php

//

namespace app\components\widgets\redactor;

use app\components\extend\yii;
use yii\widgets\InputWidget;
use app\components\extend\Html;
use yii\helpers\Json;
use app\components\extend\Model;

class RedactorWidget extends InputWidget
{

    public $model;
    public $attribute;
    public $name;
    public $options = [];
    public $clientOptions = [];
    public $containerOptions = [];

    /**
     * @return html
     */
    public function run()
    {
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->id;
        }

        Html::addCssClass($this->options, 'hidden_');
        $this->options['style'] = '';
        if ($this->hasModel()) {
            if (!isset($this->options['placeholder'])) {
                $this->options['placeholder'] = $this->model->getAttributeLabel($this->attribute);
            }
            $input = Html::activeTextarea($this->model, $this->attribute, $this->options);
        } else {
            $input = Html::textarea($this->name, null, $this->options);
        }
        $this->registerJs();
        return Html::tag('div', $input, $this->containerOptions);
    }

    /**
     * register js
     */
    public function registerJs()
    {
        $js = "CKEDITOR.id = '" . $this->options['id'] . "';initCkeditorRedactor();";
        $this->view->registerJs($js, \app\components\extend\View::POS_END);
    }

    /**
     * @return boolean whether this widget is associated with a data model.
     */
    protected function hasModel()
    {
        return ($this->model instanceof Model && $this->attribute !== null);
    }

}

?>