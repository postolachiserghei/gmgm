<?php

use app\components\extend\Html;

/* @var $this app\components\extend\View */
/* @var $s \app\models\Slider */
/* @var $slider \app\models\Slider */
/* @var $dataProvider \app\components\extend\ActiveDataProvider */
/* @var $sWidget app\components\widgets\slider\SliderWidget */
?>

<?php
$items = '';
foreach ($dataProvider->getModels() as $s) {
    $title = Html::tag('h3', $s->title);
    $content = Html::tag('p', $s->getData($slider::ADDITIONAL_DATA_CONTENT));
    $infoLeft = Html::tag('div', $title . $content, ['class' => 'mp-store-slider-info-left']);

    $curency = Html::tag('span', ' $', ['class' => 'unit']);
    $price = Html::tag('div', $s->getData($slider::ADDITIONAL_DATA_PRICE) . $curency, ['class' => 'price']);
    $infoRight = Html::tag('div', $price, ['class' => 'mp-store-slider-info-right']);
    $info = Html::tag('div', $infoLeft . $infoRight, ['class' => 'mp-store-slider-info']);



    $file = $s->getFile($slider::ADDITIONAL_DATA_IMAGE);
    /* @var $file app\models\File */
    $imageUrl = $file->getUrl('', $file->getDefaultNoImageUrl());
    $link = Html::tag('div', Html::a(yii::$app->l->t('купить'), $s->getData($slider::ADDITIONAL_DATA_LINK)), ['class' => 'mp-store-slider-img-hover']);
    $image = Html::tag('div', $link, ['class' => 'mp-store-slider-img', 'style' => 'background-image:url(' . $imageUrl . ')']);
    $items .= Html::tag('div', $image . $info, [
        'class' => 'mp-store-slider-item',
    ]);
}
?>


<div class="mp-sect-title">
    <h2>
        <i class="icn __big icn-lots"></i>

        <?= yii::$app->l->t('Рекламные лоты'); ?>
    </h2>
    <a href="#">
        <?= yii::$app->l->t('Посмотреть все') ?>
    </a>
</div>
<div class="mp-store-slider-box mp-store-slider-storelist ads">
    <?= $items == '' ? yii::$app->l->t('нет результатов') : $items; ?>
</div>



<?php
if (isset($s)) {
    $js = "$('.ads.mp-store-slider-storelist').owlCarousel({
        navText: ['<i class=\"icn icn-arrow-left\"></i>', '<i class=\"icn icn-arrow-right\"></i>'],
        nav: true,
        loop: true,
        responsive: {
            0: {
                items: 2
            },
            992: {
                items: 3
            }
        }
    });";
    yii::$app->view->registerJs($js);
}
?>