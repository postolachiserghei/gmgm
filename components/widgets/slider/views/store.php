<?php

use app\components\extend\Html;

/* @var $this app\components\extend\View */
/* @var $slider \app\models\Slider */
/* @var $s \app\models\Slider */
/* @var $dataProvider \app\components\extend\ActiveDataProvider */
/* @var $sliderWidget app\components\widgets\slider\SliderWidget */
?>

<?php
$items = '';
foreach ($dataProvider->getModels() as $s) {
    $file = $s->getFile($slider::ADDITIONAL_DATA_IMAGE);
    /* @var $file app\models\File */
    $title = Html::tag('h3', $s->title);
    $content = Html::tag('p', $s->getData($slider::ADDITIONAL_DATA_CONTENT));
    $infoContent = Html::tag('div', $title . $content, ['class' => 'mp-store-slider-info-left']);
    $info = Html::tag('div', $infoContent, ['class' => 'mp-store-slider-info']);
    $imageUrl = $file->getUrl('',$file->getDefaultNoImageUrl());
    $image = Html::tag('div', '', ['class' => 'mp-store-slider-img', 'style' => 'background-image:url(' . $imageUrl . ')']);
    $items .= Html::tag('div', Html::a($image, $s->getData($slider::ADDITIONAL_DATA_LINK)) . $info, [
                'class' => 'mp-store-slider-item',
    ]);
}
?>



<div class="mp-sect-title">
    <h2>
        <i class="icn __big icn-store"></i>
        <?= yii::$app->l->t('магазин'); ?>
    </h2>
    <a href="#" onclick="yii.mes('to do');return false;">
        <?= yii::$app->l->t('Посмотреть все'); ?>
    </a>
</div>
<div class="mp-store-slider-box mp-store-slider-storelist">
    <?= $items == '' ? yii::$app->l->t('нет результатов') : $items; ?>
</div>


<?php
if (isset($s)) {
    $js = "$('.mp-store-slider-storelist').owlCarousel({
        navText: ['<i class=\"icn icn-arrow-left\"></i>', '<i class=\"icn icn-arrow-right\"></i>'],
        nav: true,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            420: {
                items: 2
            },
            992: {
                items: 3
            }
        }
    });";
    yii::$app->view->registerJs($js);
}
?>