<?php

use app\components\extend\View;
use app\components\extend\Html;

/* @var $this app\components\extend\View */
/* @var $slider \app\models\Slider */
/* @var $dataProvider \app\components\extend\ActiveDataProvider */
/* @var $sliderWidget app\components\widgets\slider\SliderWidget */
?>

<div class = "col-xs-12 col-md-7 col-md-push-5">
    <div class = "mp-top-news-slider-wrap" style="visibility: hidden;">

        <?php
        $items = '';
        foreach ($dataProvider->getModels() as $s) {
            $file = $s->getFile($slider::ADDITIONAL_DATA_IMAGE);
            /* @var $file app\models\File */
            $imageUrl = $file->getUrl('', $file->getDefaultNoImageUrl());
            $image = Html::tag('div', '', [
                'class' => 'mp-top-news-slider-item',
                'style' => 'background-image:url(' . $imageUrl . ')',
            ]);
            $items .= Html::a($image, $s->getData($slider::ADDITIONAL_DATA_LINK));
        }
        echo $items == '' ? yii::$app->l->t('нет результатов') : $items;
        ?>
    </div>
    <div class = "mp-top-news-slider-text">
        <p>
            <?= yii::$app->l->t('text for ' . $slider->getTypeLabels($sliderWidget->type)) ?>
        </p>
    </div>
</div>


<?php
if (isset($s)) {
    $autoPlayTimeout = ((int) $s->getSetting('slider_speed_' . $sliderWidget->type, 1000));
    $animationSpeed = ((int) $s->getSetting('slider_animation_speed_' . $sliderWidget->type, 1000));
    $js = " el = $('.mp-top-news-slider-wrap');
            el.owlCarousel({
                    items: 1,
                    autoplay: true,
                    animateOut: 'fadeOut',
                    nav: true,
                    navText: ['<i class=\"icn icn-arrow-left-big\"></i>', '<i class=\"icn icn-arrow-right-big\"></i>'],
                    autoplayTimeout: " . $autoPlayTimeout . ",
                    autoplaySpeed:" . $animationSpeed . ",
                    navSpeed:" . $animationSpeed . ",
                    smartSpeed:" . $animationSpeed . ",
                    fluidSpeed:" . $animationSpeed . ",
                    autoplaySpeed:" . $animationSpeed . ",
                    dotsSpeed:" . $animationSpeed . ",
                    dragEndSpeed:" . $animationSpeed . ",
                    paginationSpeed:" . $animationSpeed . ",
                    navigation: false,
                    loop: true
                });
            el.css('visibility','visible');
        ";
    yii::$app->view->registerJs($js, View::POS_READY);
}
?>