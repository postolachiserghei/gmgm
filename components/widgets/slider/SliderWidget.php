<?php

namespace app\components\widgets\slider;

use yii\base\Widget;
use app\models\Slider;
use app\models\search\SliderSearch;
use app\components\extend\ArrayHelper;
use app\components\extend\ActiveDataProvider;

class SliderWidget extends Widget
{

    public $type;
    public $params = [];

    /**
     * @return html
     */
    public function run()
    {
        $slider = new SliderSearch;
        $this->params['slider'] = $slider;
        $this->params['sliderWidget'] = $this;
        $q = $slider::findActive()->andWhere('type=:tp', ['tp' => $this->type]);
        $dataProvider = new ActiveDataProvider([
            'query' => $q
        ]);
        $params = ArrayHelper::merge(['dataProvider' => $dataProvider], $this->params);
        switch ($this->type) {
            case Slider::TYPE_MAIN:
                $m = $this->render('main', $params);
                break;
            case Slider::TYPE_STORE:
                $m = $this->render('store', $params);
                break;
            case Slider::TYPE_ADS:
                $m = $this->render('ads', $params);
                break;
            default:
                $m = null;
                break;
        }
        return $m;
    }

}
