<?php

use app\components\extend\Html;
use app\components\extend\Url;
use app\models\Search;

/* @var $model app\models\behaviors\JsonFields */
/* @var $model app\components\extend\Model */
/* @var $model Search */
?>


<?php

if ($m = $model->getSearchedModel()) {
    /* @var $m yii\db\ActiveRecord */
    /* @var $m app\models\behaviors\SearchBehavior */
    echo yii::$app->helper->str()->replaceTagsWithDatatValues($m->getSearchLayout(), ((array) $m + [
        'itemUrl' => Url::to($m->actionUrl)
    ]));
    echo Html::a(yii::$app->l->t('more'), Url::to($m->actionUrl));
    echo '<hr/>';
} else {
    echo yii::$app->l->t('No results');
}