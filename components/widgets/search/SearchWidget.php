<?php

namespace app\components\widgets\search;

use yii\base\Widget;
use app\models\Search;
use app\models\SearchValues;
use app\components\extend\yii;
use app\components\extend\Html;
use app\components\extend\ActiveDataProvider;

class SearchWidget extends Widget
{

    public $view = 'index';

    public function run()
    {
        $kw = mb_convert_encoding(yii::$app->request->get('Search'), 'UTF-8');
        if ($kw) {
            $query = Search::find();
            $query->joinWith('values');
            $query->select(Search::tableName() . '.*,MATCH (' . SearchValues::tableName() . '.value) AGAINST ("' . $kw . '" IN BOOLEAN MODE) as score');
            $query->where('MATCH (' . SearchValues::tableName() . '.value) AGAINST ("' . $kw . '" IN BOOLEAN MODE)');
            $query->orWhere(['like', SearchValues::tableName() . '.value', $kw]);
            $query->groupBy(['link']);

            $query->orderBy(['score' => SORT_DESC]);
            $dataProvider = new ActiveDataProvider([
                'query' => $query
            ]);
            yii::$app->view->params['pageHeader'] = Html::tag('h1', yii::$app->helper->data()->getParam('h1', yii::$app->l->t('search')) . ' "' . $kw . '"');
        }
        return $this->render($this->view, [
                    'post' => $kw,
                    'dataProvider' => isset($dataProvider) ? $dataProvider : null
        ]);
    }

}
