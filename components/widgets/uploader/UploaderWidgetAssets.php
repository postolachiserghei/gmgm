<?php

namespace app\components\widgets\uploader;

use yii\web\AssetBundle;
use app\components\widgets\uploader\UploaderWidget;

class UploaderWidgetAssets extends AssetBundle
{

    public $sourcePath = '@app/components/widgets/uploader/assets';
    public $js = [
    ];
    public $css = [
    ];
    public $publishOptions = [
        'forceCopy' => true,
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        $this->js[] = 'jquery.filer.js';
        $this->js[] = 'manage.js';
        return parent::init();
    }

}
