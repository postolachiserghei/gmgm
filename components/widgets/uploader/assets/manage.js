FilerPlugin = new function ()
{
    this.api = '';
    this.tmp = {};
    this.options = '';
    this.files = [];

    this.fiBeforeSendEvent = function ($container, $el, $l, $p, $o, $s, $id, $jqXHR, $settings)
    {
        return true;
    };

    this.fiRemoveEvent = function ($container, $el, $file, event)
    {
        if (event) {
            event.stopImmediatePropagation();
        }
        var $selector = $file.size + '-' + $file.name;
        var $item = $(this.options.templates._selectors.item + '[data-origin-file="' + $selector + '"]', $container);
        var $fileName = $item.data('file-name');
        var $index = $item.data('jfiler-index');

        if (!$fileName) {
            yii.mes('error', 'error');
            return false;
        }
        if (!$item.hasClass('hidden')) {
            var $m = $el.data('confirm-message') != undefined ? $el.data('confirm-message') : '?';
            var $this = this;
            yii.confirm($m, function ()
            {
                $item.addClass('hidden');
                $($this.options.templates._selectors.browse, $container).removeClass('hidden');
                $.ajax({
                    async: true,
                    url: $this.options.action,
                    type: 'post',
                    data: {fiRemoveFileAjax: $fileName},
                    error: function (xhr)
                    {
                        $item.removeClass('hidden');
                        yii.mes(xhr.responseText, 'error', 15);
                    }}).done(function ($r)
                {
                    try {
                        var $response = JSON.parse($r);
                        if ($response.response == 'success') {
                            $this.files.splice($.inArray($fileName, $this.files), 1);
                            if ($item.hasClass('just-uploaded')) {
                                $this.api.remove($index);
                            } else {
                                /* TODO #PS: filer remove trigger */
                            }
                            $item.remove();
                            $('input[type="hidden"]', $container).val($this.files.join(','));
                            if ($($this.options.templates._selectors.item, $container).length === 0) {
                                $($this.options.templates._selectors.browse, $container).removeClass('hidden');
                            }
                            return true;
                        }
                    } catch (e) {
                        return App.YII_ENV_DEV ? console.log(e) : true;
                    }
                });
            });
        }
        return true;
    };

    this.fiSuccessEvent = function ($container, $data, $textStatus, $jqXHR)
    {
        try {
            $data = JSON.parse($data);
            var $selector = $data.info.size + '-' + $data.info.name;
            var $el = $(this.options.templates._selectors.item + '[data-origin-file="' + $selector + '"]', $container);
            $el.data('file-name', $data.name);
            $el.addClass('just-uploaded');
            this.files.push($data.name);
            $('input[type="hidden"]', $container).val((this.options.limit == 1 ? $data.name : this.files.join(',')));
            $(this.options.templates._selectors.progressBar, $el).addClass('hidden');
            return true;
        } catch (e) {
            return App.YII_ENV_DEV ? console.log(e) : true;
        }

    };

    this.fiBeforeSelectEvent = function ($container, $files, $l, $p, $o, $s)
    {
        this.fiInputInfoIndicator($container);
    };

    this.fiOnEmptyEvent = function ($container, $p, $o, $s)
    {
        this.fiInputInfoIndicator($container);
        $(this.options.templates._selectors.browse, $container).removeClass('hidden');
    };

    this.fiOnSelectEvent = function ($container, $file)
    {
        if (this.options.limit == 1) {
            $(this.options.templates._selectors.browse, $container).addClass('hidden');
        }
        this.fiInitThumbOfFile($container, $file);
        this.fiInputInfoIndicator($container, $file);
        return true;
    };

    this.fiAfterShowEvent = function ($container, $l, $p, $o, $s)
    {
        return true;
    };

    this.fiInitThumbOfFile = function ($container, $file)
    {
        var $extension = $file.name.substr(($file.name.lastIndexOf('.') + 1));
        var $ico = 'file';
        var $color = '#888';
        switch ($extension) {
            case 'pdf':
                $ico = 'file-pdf-o';
                $color = '#C30C08';
                break;
            case 'txt':
                $ico = 'file-text';
                break;
            case 'doc':
            case 'pages':
            case 'docx':
                $color = 'rgb(20, 151, 175)';
                $ico = 'file-word-o';
                break;
            case 'wma':
            case 'mp3':
                $color = '#F4805C';
                $ico = 'file-audio-o';
                break;
            case 'mov':
            case 'wmv':
            case 'vob':
            case 'flv':
            case 'webm':
            case '3gp':
            case 'mp4':
            case 'mp4p':
            case 'mp4v':
            case 'avi':
            case 'mkv':
                $color = 'rgb(129, 93, 183)';
                $ico = 'file-video-o';
                break;
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'gif':
                $color = 'rgb(79, 175, 75)';
                $ico = 'file-image-o';
            case 'rar':
            case 'zip':
                $color = '#D09C36';
                $ico = 'file-archive-o';
                break;

        }
        var $item = $(this.options.templates._selectors.item + '[data-origin-file="' + $file.size + '-' + $file.name + '"]');
        $(this.options.templates._selectors.thumb + ' .f-file.f-file-ext-' + $extension, $item).addClass('fa fa-' + $ico).css('color', $color + '!important').html('');
    };

    this.fiInputInfoIndicator = function ($container, $file)
    {
        var $el = $(this.options.templates._selectors.progressBar, $container);
        if ($el.length > 0 && $file != undefined) {
            var $data = $el.data();
            var $counter = $(this.options.templates._selectors.item, $container).length;
            if ($counter === 1) {
                $el.val($file.name);
            } else {
                $el.val($data.text + ' : ' + $counter);
            }
            $el.data('counter', $counter);
        } else {
            $el.val('');
        }
    };
};