<?php

namespace app\components\widgets\uploader;

use app\components\extend\View;
use yii\base\Widget;
use app\models\File;
use yii\helpers\Json;
use yii\web\JsExpression;
use app\components\Request;
use app\components\extend\Url;
use app\components\extend\yii;
use app\components\extend\Html;
use app\components\extend\Model;
use yii\base\InvalidConfigException;

/**
 * @property Model $model Model
 * @property \app\models\behaviors\JsonFields $model json fields behavior
 */
class UploaderWidget extends Widget
{

    const TEMPLATE_SINGLE = 'single';
    const TEMPLATE_MULTIPLE = 'multiple';
    const TEMPLATE_COMPANY_LOGO = 'company_logo';
    const TEMPLATE_CHANGE_AVATAR = 'change_avatar';

    /**
     * default files
      $files = [
      File::find()->where(['id'=>1])->one(),
      File::find()->where(['id'=>2])->one(),
      ...,
      ];
      OR
      $files = File::find()->where(['id'=>2])->all();
     * @var array
     */
    public $fieldLabel;
    public $files;
    public $ajax = true;
    public $action;
    public $limit = 100; //max nr of files
    public $maxSize = 500; //max file size in mb
    public $extensions; //list of extensions allowed, ex: 'png,jpg,gif'
    public $showThumbs = true;
    public $model;
    public $template;
    public $attribute;
    public $name;
    public $items;
    public $options;
    public $pluginOptions;
    public $containerOptions = [];
    /* plugin events (!!!anonymous functions are available only in such way : (new function(){ .... })  !!!) */
    public $onRemove; /* variabiles:  $selector, $el, $id */
    public $onSelect; /* variabiles: $selector, $i */
    public $onBeforeSelect; /* variabiles : $selector, $files, $l, $p, $o, $s */
    public $onEmpty;  /* variabiles : $selector, $p, $o, $s */
    public $afterShow; /* variabiles: $selector, $l, $p, $o, $s */
    /* @var $onSuccess after file is uploaded (triggers only with ajax) */
    public $onSuccess; /* variabiles : $selector, $data, $textStatus, $jqXHR */
    /* @var $beforeSend before file is uploaded (triggers only with ajax) */
    public $onBeforeSend; /* variabiles :  $selector,$el, $l, $p, $o, $s, $id, $jqXHR, $settings */

    /**
     * @return html
     */
    public function run()
    {
        if (!$this->extensions) {
            $this->extensions = yii::$app->helper->file()->fileModel->getSetting('defaultExtensions');
        }
        $hasModel = $this->hasModel();
        if ($hasModel) {
            $this->fieldLabel = $this->model->getAttributeLabel($this->attribute);
            $this->limit = $this->model->getRuleParam($this->attribute, 'file', 'maxFiles', 1);
            if (!$this->template) {
                $this->template = $this->limit > 1 ? self::TEMPLATE_MULTIPLE : self::TEMPLATE_SINGLE;
            }
            $this->maxSize = yii::$app->helper->file()->bytesToSize($this->model->getRuleParam($this->attribute, 'file', 'maxSize', 5), 0, 0, false);
            $this->extensions = $this->model->getRuleParam($this->attribute, 'file', 'extensions');
            $this->id = Html::getInputId($this->model, $this->attribute);
            $name = Html::getInputName($this->model, $this->attribute);
            $files = $this->limit > 1 ? $this->model->getFiles($this->attribute) : $this->model->getFile($this->attribute);
            $this->files = is_array($files) ? $files : ((!($files) || $files->isNewRecord) ? null : [$files]);
            $this->name = $name;
        } else {
            $name = $this->name;
        }
        $this->containerOptions['id'] = $this->id . '-container';
        $this->containerOptions['class'] = 'uploader-widget-container ' . $this->template;
        $this->setDefaultAction();
        $this->validate();
        $this->defaultPluginOptions();
        $this->registerScript();
        Html::addCssClass($this->options, 'hidden uploader-input');
        $input = Html::fileInput($name, null, $this->options) . $this->items;
        if ($hasModel) {
            $input .= Html::hiddenInput($name, $this->model->getData($this->attribute), $this->options);
        }
        return Html::tag('div', $input, $this->containerOptions);
    }

    /**
     * check if main params are set
     * @throws InvalidConfigException
     */
    public function validate()
    {
        if ($this->name === null && !$this->hasModel()) {
            throw new InvalidConfigException("Either 'name', or 'model' and 'attribute' properties must be specified.");
        }
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->hasModel() ? Html::getInputId($this->model, $this->attribute) : $this->getId();
        }
    }

    /**
     * combine default plugin options with custom options
     * @return array
     */
    public function defaultPluginOptions()
    {
        $options = [
            'action' => $this->action, //nr of max files
            'limit' => $this->limit, //nr of max files
            'maxSize' => $this->maxSize, //max file size in mb
            'addMore' => ($this->limit > 1),
            'files' => null,
            'extensions' => $this->extensions ? explode(',', $this->extensions) : null, //extensions
            'showThumbs' => $this->showThumbs,
            'captions' => [
                'button' => yii::$app->l->t('Choose File' . ($this->limit > 1 ? 's' : '')) . '<br/>' . Html::tag('span', ($this->fieldLabel ? $this->fieldLabel . '<br/>' : '')
                        . '(' . str_replace(',', ', ', $this->extensions) . ')', [
                    'class' => 'extensions-list'
                ]),
                'feedback' => yii::$app->l->t('Choose files To Upload'),
                'feedback2' => yii::$app->l->t('files were chosen'),
                'drop' => yii::$app->l->t('Drop file here to Upload'),
                'removeConfirmation' => yii::$app->l->t('delete') . ' ?',
                'errors' => [
                    'filesLimit' => yii::t('yii', 'You can upload at most {limit, number} {limit, plural, one{file} other{files}}.', ['limit' => $this->limit]),
                    'filesType' => yii::t('yii', 'Only files with these extensions are allowed: {extensions}.', ['extensions' => $this->extensions]),
                    'filesSize' => yii::t('yii', 'The file "{file}" is too big. Its size cannot exceed {formattedLimit}.', [
                        'file' => '{{fi-name}}',
                        'formattedLimit' => '{{fi-maxSize}}MB'
                    ]),
                    'filesSizeAll' => yii::$app->l->t("Files you've choosed are too large! Please upload files up to {max-size} MB.", [
                        'max-size' => '{{fi-maxSize}}'
                    ])
                ]
            ],
            'dragDrop' => [
                'dragEnter' => null,
                'dragLeave' => null,
                'drop' => null,
            ],
        ];
        $options = array_merge($options, $this->prepareTemplate($options));
        $options['onRemove'] = $this->registerRemoveScript();
        $options['onSelect'] = $this->registerOnSelectScript();
        $options['beforeSelect'] = $this->registerBeforeSelectScript();
        $options['onEmpty'] = $this->registerOnEmptyScript();
        $options['afterShow'] = $this->registerAfterShowScript();

        if ($this->pluginOptions && is_array($this->pluginOptions) && count($this->pluginOptions) > 0) {
            $this->pluginOptions = array_merge($options, $this->pluginOptions);
        }
        $options['headers'][Request::CSRF_HEADER] = yii::$app->getRequest()->getCsrfToken();
        $options['params'][yii::$app->getRequest()->csrfParam] = yii::$app->getRequest()->getCsrfToken();

        $this->pluginOptions = $options;
    }

    /**
     * prepare template
     * @param array $options
     * @return type
     */
    public function prepareTemplate($options)
    {
        $this->pluginOptions['templates']['progressBar'] = Html::tag('div', '', ['class' => ' progress-bar progress-bar-info progress-bar-striped active']);
        $this->pluginOptions['templates']['_selectors'] = [
            'browse' => '.browse',
            'list' => '.uploader-file-list',
            'item' => '.uploader-file-item',
            'progressBar' => '.progress-bar',
            'thumb' => '.fi-thumb',
            'remove' => '.uploader-trash-action'
        ];
        $this->pluginOptions = array_merge($this->pluginOptions, $options);
        $options['templates'] = [
            'box' => $this->render('template/' . $this->template . '/_box', ['uploader' => $this]),
            'item' => $this->render('template/' . $this->template . '/_item', ['uploader' => $this]),
            'progressBar' => $this->pluginOptions['templates']['progressBar'],
            'removeConfirmation' => false,
            'itemAppendToEnd' => false,
            '_selectors' => $this->pluginOptions['templates']['_selectors']
        ];
        if ($this->ajax) {
            $options['uploadFile'] = [
                'url' => $this->action,
                'type' => 'POST',
                'enctype' => 'multipart/form-data',
                'beforeSend' => $this->registerBeforeSendScript(),
                'statusCode' => null,
                'onProgress' => null,
                'onComplete' => null,
                'error' => new JsExpression('function($jqXHR, $textStatus, $errorThrown){ console.log($textStatus); }'),
                'success' => $this->registerSuccessScript(),
            ];
        }
        if ($this->files) {
            $options['changeInput'] = $this->render('template/' . $this->template . '/_browse', ['uploader' => $this]);
            $this->items .= Html::tag('section', $this->getCurrentFiles(), ['class' => 'uploader-widget-container ' . $this->template]);
        } else {
            $options['changeInput'] = $this->render('template/' . $this->template . '/_browse', ['uploader' => $this]);
        }
        return $options;
    }

    /**
     *
     * @param string $name
     * @param boolean $asCssClass
     * @return string
     */
    public function getJsSelector($name, $asCssClass = false)
    {
        $selector = $this->pluginOptions['templates']['_selectors'][$name];
        return $asCssClass ? str_replace('.', '', $selector) : $selector;
    }

    /**
     * get current files
     */
    public function getCurrentFiles()
    {
        $tmp = '';
        $templatePath = 'template/' . $this->template . '/';
        foreach ($this->files as $k => $file) {
            /* @var $file File */
            $item = $this->render($templatePath . '_item', ['uploader' => $this]);
            $tmp .= yii::$app->helper->str()->replaceTagsWithDatatValues($item, [
                'fi-image' => $file->renderFile([], $file->getIsImage(explode(',', $this->extensions)[0])),
                'fi-name' => $file->name,
                'fi-size' => $file->size,
                'fi-title' => $file->title,
                'fi-id' => $file->name,
                'fi-progressBar' => '',
                    ], ['{{', '}}']);
        }
        return $this->render($templatePath . '_box', ['items' => $tmp, 'uploader' => $this]);
    }

    /**
     * default action for Ajax upload & remove file
     */
    public function setDefaultAction()
    {
        if (!$this->action) {
            $params = (['uploader-request' => 1] + yii::$app->request->getQueryParams());
            $this->action = Url::to('/' . yii::$app->request->pathInfo.'?'. http_build_query($params));
        }
    }

    /**
     * @return boolean whether this widget is associated with a data model.
     */
    protected function hasModel()
    {
        return ($this->model instanceof Model && $this->attribute !== null);
    }

    /**
     * catch and manage save/delete actions
     * @param array $params :
     * "name" => "file" / "model" => $myModel, "attribute" => "model_attribute",
     * can be set function => "beforeSave/afterSave" => function ($fileModel) {...} ])., "beforeDelete/afterDelete" => function ($fileName) {...} ]).
     */
    public static function manage($params = [])
    {
        $path = '/public/uploads/' . yii::$app->helper->settings()->getSetting('tld') . '/' . date('y') . '/' . date("W") . '/';
        $path = str_replace(':', '-', $path);
        $params['afterSave'] = function($fileName, $info) use ($params, $path) {
            $fileModel = File::saveFileInfo($fileName, $info, $path);
            $as = (array_key_exists('afterSave', $params)) ? $params['afterSave'] : null;
            if (is_callable($as)) {
                $as($fileModel, $info);
            }
            die(Json::encode(['name' => $fileModel->name, 'info' => $info]));
        };
        yii::$app->helper->file()->save($path, $params);
        $fileName = yii::$app->request->post('fiRemoveFileAjax');
        if ($fileName && trim($fileName) !== '') {
            $params['afterDelete'] = function($path) use ($params, $fileName) {
                $ad = (array_key_exists('afterDelete', $params)) ? $params['afterDelete'] : null;
                if (is_callable($ad)) {
                    $ad($fileName);
                }
                if (yii::$app->request->isAjax && !yii::$app->request->isPjax) {
                    die(Json::encode(['response' => 'success']));
                }
            };
            File::deleteFileByName($fileName, $params);
        }
    }

    /**
     * register plugin script
     */
    public function registerScript()
    {
        UploaderWidgetAssets::register(yii::$app->controller->view);
        $plugin = '$("#' . $this->id . '").filer(' . Json::encode($this->pluginOptions) . ');';
        $plugin .= 'FilerPlugin.options = ' . Json::encode($this->pluginOptions) . ';';
        $plugin .= 'FilerPlugin.api = $("#' . $this->id . '").prop("jFiler")';
        yii::$app->controller->view->registerJs($plugin, View::POS_END, $this->id);
    }

    public function registerSuccessScript()
    {
        $js = 'var $selector = $("#' . $this->containerOptions['id'] . '");';
        $js .= 'FilerPlugin.fiSuccessEvent($selector, $data, $textStatus, $jqXHR) ;' . $this->onSuccess . '; ';
        return new JsExpression('function($data, $textStatus, $jqXHR){ ' . $js . ' }');
    }

    public function registerBeforeSendScript()
    {
        $js = 'var $selector = $("#' . $this->containerOptions['id'] . '");';
        $js .= 'FilerPlugin.fiBeforeSendEvent($selector,$el, $l, $p, $o, $s, $id, $jqXHR, $settings) ;' . $this->onBeforeSend . '; ';
        return new JsExpression('function($el, $l, $p, $o, $s, $id, $jqXHR, $settings){' . $js . '}');
    }

    public function registerRemoveScript()
    {
        $js = 'var $selector = $("#' . $this->containerOptions['id'] . '");';
        $js .= 'FilerPlugin.fiRemoveEvent($selector, $el, $id);' . $this->onRemove . '; ';
        return new JsExpression('function($el,$id){ ' . $js . ' }');
    }

    public function registerBeforeSelectScript()
    {
        $js = 'var $selector = $("#' . $this->containerOptions['id'] . '");';
        $js .= 'FilerPlugin.fiBeforeSelectEvent($selector, $files, $l, $p, $o, $s); ' . $this->onBeforeSelect . ';';
        return new JsExpression('function($files, $l, $p, $o, $s){ ' . $js . ' return true;}');
    }

    public function registerOnEmptyScript()
    {
        $js = 'var $selector = $("#' . $this->containerOptions['id'] . '");';
        $js .= 'FilerPlugin.fiOnEmptyEvent($selector, $p, $o, $s) ;' . $this->onEmpty . '; ';
        return new JsExpression('function($p, $o, $s){ ' . $js . ' return true;}');
    }

    public function registerOnSelectScript()
    {
        $js = 'var $selector = $("#' . $this->containerOptions['id'] . '");';
        $js .= 'FilerPlugin.fiOnSelectEvent($selector, $i) ;' . $this->onSelect . ';';
        return new JsExpression('function($i){ ' . $js . ' }');
    }

    public function registerAfterShowScript()
    {
        $js = 'var $selector = $("#' . $this->containerOptions['id'] . '");';
        $js .= 'FilerPlugin.fiAfterShowEvent($selector, $l, $p, $o, $s) ;' . $this->afterShow . '; ';
        return new JsExpression('function($l, $p, $o, $s){ ' . $js . ' }');
    }

}
