<?php
/* @var $uploader app\components\widgets\uploader\UploaderWidget */
?>
<div class="<?= $uploader->getJsSelector('list', true) ?> row">
    <?= isset($items) ? $items : '' ?>
</div>
