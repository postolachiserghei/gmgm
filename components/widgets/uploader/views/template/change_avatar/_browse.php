<?php
/* @var $uploader app\components\widgets\uploader\UploaderWidget */
?>
<div title="<?= $uploader->fieldLabel ?>" class="<?= $uploader->getJsSelector('browse', true); ?> <?= ($uploader->files && $uploader->limit == 1) ? 'hidden' : '' ?>">
    <div class="bpv-infoform-avatar">
        <div class="user-avatar" style="background-image: url(/public/gmgm/img/user.png);"></div>
        <br>
        <div class="form-group">
            <input type="file">
            <label for="">
                <a href="#">
                    <?= yii::$app->l->t('Сменить аватарку') ?>
                </a>
            </label>
        </div>
    </div>
</div>