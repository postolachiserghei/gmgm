<?php
/* @var $uploader app\components\widgets\uploader\UploaderWidget */
?>
<div title="<?= $uploader->fieldLabel ?>" class="<?= $uploader->getJsSelector('browse', true); ?> <?= ($uploader->files && $uploader->limit == 1) ? 'hidden' : '' ?>">
    <?= $uploader->pluginOptions['captions']['button']; ?>
</div>