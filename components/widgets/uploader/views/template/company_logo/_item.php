<?php

use app\components\extend\Html;

/* @var $uploader \app\components\widgets\uploader\UploaderWidget */
/*
  <!--
  {{fi-size}}
  {{fi-name}}
  {{fi-name | limitTo: 25}}
  {{fi-size2}}
  {{fi-image}}
  {{fi-progressBar}}
  -->
 */
?>

<div class="<?= $uploader->getJsSelector('item', true) ?>  col-md-12" data-origin-file="{{fi-size}}-{{fi-name}}" data-file-name="{{fi-name}}">
    <div class="<?= $uploader->getJsSelector('thumb', true) ?>">
        <div class="row">
            <div class="col-md-push-2 col-md-8">
                {{fi-image}}
                <br/>
                {{fi-progressBar}}
            </div>
        </div>
        <div class="row">
            <div class="col-md-push-2 col-md-8 text-center">
                <?=
                Html::ico('trash', [
                    'class' => $uploader->getJsSelector('remove', true) . ' text-danger btn',
                    'data' => [
                        'confirm-message' => yii::$app->l->t('delete') . ' ?',
                        'index' => '{{fi-index}}',
                    ],
                    'onclick' => "FilerPlugin.fiRemoveEvent($('#" . $uploader->containerOptions['id'] . "'), $(this), {size:'{{fi-size}}', name:'{{fi-name}}'}, event);",
                ])
                ?>
            </div>
        </div>
    </div>
</div>
