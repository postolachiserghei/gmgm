<?php
/* @var $uploader app\components\widgets\uploader\UploaderWidget */
?>
<div title="<?= $uploader->fieldLabel ?>" class="<?= $uploader->getJsSelector('browse', true); ?> <?= ($uploader->files && $uploader->limit == 1) ? 'hidden' : '' ?>">
    <label for=""><span class="icn icn-logout"></span><?= yii::$app->l->t('Загрузить фото магазина'); ?></label>
</div>