<?php

namespace app\components\widgets\games;

use yii\base\Widget;
use app\models\LotTypes;
use app\models\Categories;
use app\components\extend\yii;
use app\components\extend\ActiveDataProvider;

/**
 * @property Categories $category Categories model
 */
class GamesWidget extends Widget
{

    /**
     * widget types
     */
    const TYPE_TOP = 'top';
    const TYPE_CATALOG = 'catalog';
    const TYPE_CATALOG_MOBILE = 'catalog_mobile';
    const TYPE_TOP_GAME_PAGE = 'top_game_page';
    const TYPE_MAIN_FILTER = 'main_filter';

    public $category;
    public $type;
    public $params = [];

    public function run()
    {
        $this->category = new Categories();
        if (!$this->type) {
            $this->type = self::TYPE_TOP;
        }
        $this->prepareWidgetData();
        $this->params = array_merge(['widget' => $this], $this->params);
        return $this->render(yii::$app->controller->themeName . '/' . $this->type, $this->params);
    }

    public function prepareWidgetData()
    {
        switch ($this->type) {
            case self::TYPE_MAIN_FILTER:
                $this->getFilterData();
                break;
            case self::TYPE_CATALOG:
            case self::TYPE_CATALOG_MOBILE:
                $this->getCatalogData();
                break;
            case self::TYPE_TOP:
                $this->getTopData();
                break;
            case self::TYPE_TOP_GAME_PAGE:
                $this->getTopData();
                break;
            default:
                break;
        }
    }

    /**
     * get filter data
     */
    public function getFilterData()
    {
        $q = $this->getGameCategory();
        $this->params['games'] = $q->all();
        $this->params['lotTypes'] = LotTypes::find()->where(['status' => LotTypes::STATUS_ACTIVE])->all();
    }

    /**
     * get top data
     */
    public function getTopData()
    {
        $q = $q = $this->getGameCategory();
        $q->andWhere('additional_data->"$.in_top"="1"');
        $dataProvider = new ActiveDataProvider([
            'query' => $q
        ]);
        $this->params['dataProvider'] = $dataProvider;
    }

    /**
     * get catalog data
     */
    public function getCatalogData()
    {
        $paramKey = 'GameWidget_catalog_dataDataProvider';
        if (!$dataProvider = yii::$app->helper->data()->getParam($paramKey)) {
            $q = $this->getGameCategory();
            $dataProvider = new ActiveDataProvider([
                'query' => $q,
                'pagination' => [
                    'pageSize' => ($this->category->getSetting('game_catalog_show_limit', 10))
                ],
                'sort' => ['defaultOrder' => ['order' => SORT_ASC]]
            ]);
            yii::$app->helper->data()->setParam($paramKey, $dataProvider);
        }
        $this->params['dataProvider'] = $dataProvider;
    }

    /**
     * get game category
     * @return \app\components\extend\ActiveQuery
     */
    public function getGameCategory()
    {
        $q = $this->category::find();
        $q->where(['type' => Categories::TYPE_GAME, 'status' => Categories::STATUS_ACTIVE]);
        return $q;
    }

}
