<?php

use app\components\extend\Url;
use app\components\extend\Html;

/* @var $this app\components\extend\View */
/* @var $model app\models\Categories */
/* @var $model app\models\behaviors\SeoBehavior */
/* @var $model app\models\behaviors\FileSaveBehavior */
/* @var $widget \app\components\widgets\games\GamesWidget */
/* @var $dataProvider app\components\extend\ActiveDataProvider */
?>

<?= Html::a($model->title, Url::to(['/game/view', 'id' => $model->primarykey])); ?>
