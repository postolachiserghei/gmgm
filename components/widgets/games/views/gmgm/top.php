<?php

use app\components\extend\Html;
use app\components\extend\ListView;

/* @var $this app\components\extend\View */
/* @var $widget \app\components\widgets\games\GamesWidget */
/* @var $dataProvider app\components\extend\ActiveDataProvider */
?>

<div class="hidden-xs hidden-sm col-md-5 col-md-pull-7 mp-sect-top-gamelist-wrap">
    <div class="mp-sect-title">
        <h2>
            <i class="icn __big icn-top"></i>
            <?= yii::$app->l->t('топ игры'); ?>
        </h2>
        <?= Html::a(yii::$app->l->t('Посмотреть все'), ['/game/index']) ?>
    </div>
    <div class="container-fluid">
        <div class="mp-top-news-gamelist">
            <?=
            ListView::widget([
                'layout' => '{items}{pager}',
                'dataProvider' => $dataProvider,
                'itemView' => $widget->type . '/_item',
                'viewParams' => $widget->params,
                'options' => [
                    'tag' => 'div',
                    'class' => 'mp-top-news-gamelist',
                ]
            ])
            ?>
        </div>
    </div>
</div>