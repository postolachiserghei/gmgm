<?php

use app\components\extend\Url;
use app\components\extend\Html;

/* @var $this app\components\extend\View */
/* @var $model app\models\Categories */
/* @var $model app\models\behaviors\FileSaveBehavior */
/* @var $widget \app\components\widgets\games\GamesWidget */
/* @var $dataProvider app\components\extend\ActiveDataProvider */
/* @var $file app\models\File */

$file = $model->getFile($model::ADDITIONAL_DATA_IMAGE);
?>

<?= Html::beginTag('a', ['href' => Url::to(['/game/view', 'id' => $model->primaryKey])]); ?>
<div class="mp-top-news-gamelist-item" style="background-image: url(<?= $file->getUrl('', $file->getDefaultNoImageUrl()) ?>)">

    <?=
    Html::tag('span', $model->title, [
        'class' => 'mp-top-news-gamelist-title'
    ]);
    ?>

</div>
<?= Html::endTag('a'); ?>