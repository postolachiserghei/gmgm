<?php

use app\components\extend\Url;
use app\components\extend\Html;
use app\components\extend\ListView;

/* @var $this app\components\extend\View */
/* @var $lotTypes \app\models\LotTypes */
/* @var $games app\models\Categories */
?>
<div class="col-xs-12 col-sm-8 col-md-6">
    <div class="header-search-wrap">
        <div class="header-search-mainbox">
            <div class="dropdown">
                <button class="dropdown-toggle" type="button" data-toggle="dropdown"><?= yii::$app->l->t('Игра'); ?>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <?php
                    foreach ($games as $game) {
                        $link = Html::a($game->title, '#');
                        echo Html::tag('li', $link);
                    }
                    ?>
                </ul>
            </div>
            <div class="dropdown">
                <button class="dropdown-toggle" type="button" data-toggle="dropdown"><?= yii::$app->l->t('Тип лота'); ?>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <?php
                    foreach ($lotTypes as $lotType) {
                        $link = Html::a($lotType->title, '#');
                        echo Html::tag('li', $link, [
                            'data' => [
                                'key' => $lotType->primaryKey
                            ]
                        ]);
                    }
                    ?>
                </ul>
            </div>
            <div class="form-group header-search-form">
                <input type="text" placeholder="<?= yii::$app->l->t('Поиск', ['update' => false]) ?>">
            </div>
            <button>
                <i class="icn icn-search"></i>
            </button>
        </div>
        <div class="header-search-subbox">
            <div class="form-inline">
                <div class="form-group">
                    <label for="price-from">
                        <?= yii::$app->l->t('сумма от') ?>
                    </label>
                    <input id="price-from" type="number">
                </div>
                <div class="form-group">
                    <label for="price-to">
                        <?= yii::$app->l->t('до') ?>
                    </label>
                    <input id="price-to" type="number">
                </div>
                <div class="form-group">
                    <input type="text" placeholder="<?= yii::$app->l->t('срок доставки', ['update' => false]) ?>" class="datepicker">
                </div>
                <div class="form-group">
                    <input type="text" placeholder="<?= yii::$app->l->t('Продавец', ['update' => false]) ?>">
                </div>
            </div>
            <div class="header-search-wrap-showmore">
                <span><?= yii::$app->l->t('Спрятать рассширеный фильтр') ?></span> <i class="icn icn-arrow-top"></i>
            </div>
        </div>
        <div class="header-search-wrap-showmore">
            <span><?= yii::$app->l->t('Расширенный поиск'); ?></span> <i class="icn icn-arrow-down"></i>
        </div>
    </div>

</div>