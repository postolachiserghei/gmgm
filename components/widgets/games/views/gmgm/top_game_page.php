<?php

use app\components\extend\Url;
use app\components\extend\Html;
use app\components\extend\ListView;

/* @var $this app\components\extend\View */
/* @var $widget \app\components\widgets\games\GamesWidget */
/* @var $dataProvider app\components\extend\ActiveDataProvider */
?>

<div class="hidden-xs hidden-sm gp-sect-top-gamelist-wrap">
    <div class="mp-top-news-gamelist __single">
        <div class="mp-sect-title">
            <h2>
                <i class="icn __big icn-top"></i>
                <?= yii::$app->l->t('топ игры'); ?>
            </h2>
        </div>
        <?=
        ListView::widget([
            'layout' => '{items}{pager}',
            'dataProvider' => $dataProvider,
            'itemView' => $widget::TYPE_TOP . '/_item',
            'viewParams' => $widget->params,
            'itemOptions' => function($model) {
                return [
                    'tag' => 'div',
                ];
            },
            'options' => [
                'tag' => 'div',
            ]
        ])
        ?>
        <div class="mp-top-news-gamelist-ctrl">
            <?= Html::a(yii::$app->l->t('посмотреть все'), Url::to(['/game/index']), ['class' => 'button __small']) ?>
        </div>
    </div>
</div>