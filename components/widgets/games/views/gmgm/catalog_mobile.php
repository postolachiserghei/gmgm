<?php

use app\components\extend\Url;
use app\components\extend\Html;
use app\components\extend\ListView;

/* @var $this app\components\extend\View */
/* @var $widget \app\components\widgets\games\GamesWidget */
/* @var $dataProvider app\components\extend\ActiveDataProvider */
?>

<section class="hidden-md hidden-lg">
    <div class="container">
        <div class="row">
            <div class="mp-store-gamecat-wrap">
                <div class="mp-sect-title mobile-gamelist-toggle">
                    <h2>
                        <i class="icn __big icn-catalog"></i>
                        <?= yii::$app->l->t('Каталог игр') ?>
                        <i class="icn icn-arrow-down"></i>
                    </h2>
                </div>
                <div class="mobile-gamelist-body shadow-elem">
                    <?=
                    ListView::widget([
                        'layout' => '{items}',
                        'dataProvider' => $dataProvider,
                        'itemView' => $widget->type . '/_item',
                        'viewParams' => $widget->params,
                        'itemOptions' => function($model) {
                            return [
                                'tag' => 'li',
                            ];
                        },
                        'options' => [
                            'tag' => 'ul',
                        ]
                    ])
                    ?>
                    <div class="mp-store-gamecat-ctrl">
                        <?= Html::a(yii::$app->l->t('Все игры'), Url::to(['/game/index']), ['class' => 'button']) ?>
                        <br>
                        <?= Html::a(yii::$app->l->t('Предложи свою игру'), Url::to(['/game/suggest']), ['class' => 'mp-store-gamecat-link']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>