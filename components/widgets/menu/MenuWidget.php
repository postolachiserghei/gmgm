<?php

namespace app\components\widgets\menu;

use yii;
use yii\base\Widget;
use app\models\Menu;
use app\components\extend\ArrayHelper;

class MenuWidget extends Widget
{

    const PAGE_DEFAULT = 'default';
    const PAGE_GAME = 'game';

    public $page;
    public $type;
    public $parent = 0;
    public $childs = null;
    public $params = [];

    /**
     * @return html
     */
    public function run()
    {
        $menuItems = Menu::getMenuArray($this->type, $this->parent, $this->childs, $this->params);
        $params = ArrayHelper::merge(['menuItems' => $menuItems], $this->params);
        $params['widget'] = $this;
        switch ($this->type) {
            case Menu::TYPE_MAIN:
                $m = $this->render('main', $params);
                break;
            case Menu::TYPE_ASIDE:
                $m = $this->render('aside', $params);
                break;
            case Menu::TYPE_HOW_TO:
                $m = $this->render('how_to', $params);
                break;
            case Menu::TYPE_FOOTER:
                $m = $this->render('footer_links', $params);
                break;
            case Menu::TYPE_FOOTER_LVL_2:
                $m = $this->render('footer_menu_lvl_2', $params);
                break;
            default:
                $m = null;
                break;
        }
        return $m;
    }

}
