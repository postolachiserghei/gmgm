<?php

use app\models\Menu;
use app\components\extend\Nav;
?>

<?=

Nav::widget([
    'options' => ['class' => 'footer-link-box'],
    'items' => $menuItems,
]);
?>