<?php

use app\components\extend\Html;

/* @var $menuItems array */
?>


<?php
$mainItems = '';
foreach ($menuItems as $key => $item) {
    $a = Html::a($item['label'], $item['url'], $item['linkOptions']);
    $li = Html::tag('li', $a, $item['options']);
    if (isset($item['items'])) {
        foreach ($item['items'] as $ckey => $cItem) {
            $ca = Html::a($cItem['label'], $cItem['url'], $cItem['linkOptions']);
            $li .= Html::tag('li', $ca, $cItem['options']);
        }
    }
    $ul = Html::tag('ul', $li);
    $mainItems .= Html::tag('div', $ul, ['class' => 'col-xs-12 col-sm-6 col-md-3']);
}
echo $mainItems;
