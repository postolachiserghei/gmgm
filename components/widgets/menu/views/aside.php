<?php

use app\components\extend\Html;

/* @var $menuItems array */
?>


<?php

$mainItems = '';
foreach ($menuItems as $key => $item) {
    $a = Html::a($item['label'], $item['url'], $item['linkOptions']);
    $li = Html::tag('li', $a);
    echo $li;
}
