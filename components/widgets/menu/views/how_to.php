<?php

use app\components\extend\Html;

/* @var $menuItems array */
/* @var $widget \app\components\widgets\menu\MenuWidget */
?>


<?php

$mainItems = '';
if ($widget->page == $widget::PAGE_GAME) {
    foreach ($menuItems as $key => $item) {
        $a = Html::a($item['label'], $item['url'], $item['linkOptions']);
        $inner = Html::tag('div', $a, ['class' => 'mp-how-to-inner']);
        echo Html::tag('div', $inner, ['class' => 'mp-how-to-item __horizontal']);
    }
} else {
    foreach ($menuItems as $key => $item) {
        $a = Html::a($item['label'], $item['url'], $item['linkOptions']);
        $li = Html::tag('li', $a, ['class' => 'mp-how-to-inner']);
        $ul = Html::tag('ul', $li, ['class' => 'mp-how-to-item']);
        $mainItems .= Html::tag('div', $ul, ['class' => 'col-xs-12 col-md-4']);
    }
    echo $mainItems;
}

