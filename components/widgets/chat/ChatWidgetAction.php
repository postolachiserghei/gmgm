<?php

namespace app\components\widgets\chat;

use app\models\Chat;
use yii\base\Action;
use app\models\User;
use yii\helpers\Json;
use app\models\ChatMembers;
use app\models\ChatMessages;
use app\components\extend\yii;
use app\components\extend\ArrayHelper;
use app\components\extend\ActiveDataProvider;

/**
 * Description of ChatWidgetAction
 *
 * @author ps
 */
class ChatWidgetAction extends Action
{

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        yii::$app->controller->addAllowedActions([
            'chat'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (yii::$app->request->isAjax && !yii::$app->request->isPjax) {
            return Json::encode($this->getOperation(yii::$app->request->post('chat-request')));
        }
    }

    /**
     * action distributor
     * @param array $post
     * @return array
     */
    public function getOperation($post)
    {
        if (!$post || !isset($post['action'])) {
            return YII_ENV_DEV ? ['post' => $post] : [];
        }
        $params = isset($post['params']) ? $post['params'] : [];
        switch ($post['action']) {
            case 'get-contacts':
                return $this->getContacts($params);
            case 'get-conversations':
                return $this->getConversations($params);
            case 'get-messages':
                return $this->getMessages($params);
            case 'send-message':
                return $this->sendMessage($params);
            case 'get-new-messages':
                return $this->getNewMessage($params);
            default:
                return YII_ENV_DEV ? ['post' => $post] : [];
        }
    }

    public function getNewMessage($param = [])
    {
        return [
            'data' => Chat::countNewMessages(),
        ];
    }

    /**
     * send message
     * @param array $params
     * @return array
     */
    public function sendMessage($params = [])
    {
        if (!isset($params['chat_id']) || !isset($params['message'])) {
            return null;
        }
        $chat = Chat::getMyChat((int) $params['chat_id']);
        $sent = $chat->sendMessage(yii::$app->user->id, $params['message']);
        return [
            'result' => 'success',
            'errors' => yii::$app->helper->data()->getParam('errors'),
            'data' => true,
            'receivers' => $sent,
        ];
    }

    /**
     * get user conversations
     * @param array $params
     * @return array
     */
    public function getConversations($params = [])
    {
        $model = new Chat;
        $q = $model::find();
        $q->orderBy(['updated_at' => SORT_DESC]);
        if (($params['directionId'] != 0)) {
            $q->andWhere($model::tableName() . '.id' . (($params['direction'] == '+') ? '>' : '<') . ':id', ['id' => (int) $params['directionId']]);
        }
        $dataProvider = $model->getMyChatList($q);
        if ($dataProvider->totalCount == 0 && $params['directionId'] == 0) {
            $model->addDefaultConversation();
            $dataProvider = $model->getMyChatList($q);
        }
        if ($dataProvider->totalCount == 0) {
            return;
        }
        return [
            'result' => 'success',
            'errors' => yii::$app->helper->data()->getParam('errors'),
            'data' => $this->render('conversations', ['dataProvider' => $dataProvider]),
            'totalCount' => $dataProvider->totalCount,
            'count' => $dataProvider->count,
        ];
    }

    /**
     * get user contacts
     * @param array $params
     * @return array
     */
    public function getContacts($params = [])
    {
        if (!isset($params['direction']) || !isset($params['directionId'])) {
            return null;
        }
        $contacts = yii::$app->helper->user()->identity()->getContacts()->all();
        $q = User::find(['in', 'id', ArrayHelper::map($contacts, 'contact_id', 'contact_id')]);
        $dataProvider = new ActiveDataProvider([
            'query' => $q,
            'sort' => ['defaultOrder' => ['updated_at' => SORT_DESC, 'order' => SORT_ASC]]
        ]);
        if (($params['directionId'] != 0)) {
            $q->andWhere(User::tableName() . '.id' . (($params['direction'] == '+') ? '>' : '<') . ':id', ['id' => (int) $params['directionId']]);
        }
        if ($dataProvider->totalCount == 0) {
            return;
        }
        return [
            'result' => 'success',
            'errors' => yii::$app->helper->data()->getParam('errors'),
            'data' => $this->render('contacts', ['dataProvider' => $dataProvider]),
            'totalCount' => $dataProvider->totalCount,
            'count' => $dataProvider->count,
        ];
    }

    /**
     * get user contacts
     * @param array $params
     * @return array
     */
    public function getMessages($params = [])
    {
        if (!isset($params['chat_id']) || !isset($params['direction']) || !isset($params['directionId'])) {
            return null;
        }
        $chat = new Chat();
        $chat->id = (int) $params['chat_id'];

        $q = ChatMessages::find()->joinWith(['chat' => function($q) use ($chat) {
                return $q->joinWith(['members' => function($q) use ($chat) {
                                return $q->where(['user_id' => yii::$app->user->id]);
                            }]);
            }]);
        $q->where([ChatMessages::tableName() . '.chat_id' => $chat->id]);
        if (($params['directionId'] != 0)) {
            $q->andWhere(ChatMessages::tableName() . '.id' . (($params['direction'] == '+') ? '>' : '<') . ':id', ['id' => (int) $params['directionId']]);
        }
        $q->orderBy([ChatMessages::tableName() . '.id' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $q,
            'pagination' => [
                'pageSize' => $this->getMessagePageSize($q, 20)
            ]
        ]);
        $models = $dataProvider->getModels();
        ArrayHelper::multisort($models, 'id', SORT_ASC);
        $dataProvider->setModels($models);
        if ($dataProvider->totalCount == 0) {
            return;
        }
        return [
            'result' => 'success',
            'errors' => yii::$app->helper->data()->getParam('errors'),
            'data' => $this->render('messages', ['dataProvider' => $dataProvider]),
            'totalCount' => $dataProvider->totalCount,
            'count' => $dataProvider->count,
        ];
    }

    /**
     * 
     * @param \app\components\extend\ActiveQuery $query
     * @param integer $default
     */
    public function getMessagePageSize($query, $default)
    {
        $q = ChatMessages::find();
        $q->create($query);
        $c = $q->andWhere([ChatMessages::tableName() . '.status' => ChatMessages::STATUS_NEW])->count();
        return $c > $default ? $c : $default;
    }

    /**
     * 
     * @param string $view
     * @param array $params
     * @return type
     */
    public function render($view, $params = [])
    {
        return yii::$app->controller->renderPartial(ChatWidget::BASE_SOURCE_PATH . 'views/' . yii::$app->controller->themeName . '/' . $view, $params);
    }

}
