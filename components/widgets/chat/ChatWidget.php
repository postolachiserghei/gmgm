<?php

namespace app\components\widgets\chat;

use yii\base\Widget;
use app\models\Chat;
use app\components\extend\yii;
use app\components\extend\ActiveDataProvider;

class ChatWidget extends Widget
{

    const BASE_SOURCE_PATH = '@app/components/widgets/chat/';

    public $view = 'index';
    public $params = [];

    public function run()
    {
        $chatModel = new Chat();
        $this->params = array_merge(['chatWidget' => $this, 'chatModel' => $chatModel,], $this->params);
        return $this->render(yii::$app->controller->themeName . '/' . $this->view, $this->params);
    }

    /**
     * chat actions
     * @return array
     */
    public static function actions()
    {
        return [
            'chat' => ChatWidgetAction::className()
        ];
    }

}
