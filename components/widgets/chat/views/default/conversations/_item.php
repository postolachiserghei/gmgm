<?php

use app\components\extend\Html;

/* @var $model \app\models\Chat */
/* @var $user \app\models\User */
$user = $model->getMembers()->where(['!=', 'user_id', $model->getUser()])->one();
?>



<?=
$user ? $user->user->renderAvatar([
    'class' => 'img-rounded',
    'width' => 75,
    'height' => 50
]) : '';
?>
&nbsp;
<?= $model->getTitle(); ?>

<?php
$new = $model->countNewMessages(null, $model->primaryKey)['total'];
echo Html::tag('span', ($new > 0 ? $new : ''), ['class' => 'pa badge badge-success js-chat-conversation-item-count-new', 'style' => 'right:10px;top:10px;']);
?>