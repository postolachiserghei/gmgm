<?php

use app\components\extend\ListView;

/* @var $dataProvider \app\components\extend\ActiveDataProvider */
?>

<?=

ListView::widget([
    'layout' => '{items}',
    'dataProvider' => $dataProvider,
    'itemView' => 'conversations/_item',
    'itemOptions' => function($model) {
        return [
            'tag' => 'a',
            'class' => 'list-group-item js-chat-conversation-item',
            'onclick' => 'Chat.drawMessages($(this));',
        ];
    },
    'options' => [
        'tag' => false,
    ]
]);
?>