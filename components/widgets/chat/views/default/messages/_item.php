<?php

use app\components\extend\Url;
use app\components\extend\Html;

/* @var $model app\models\ChatMessages */
/* @var $user app\models\User */

$user = $model->sender->getUser();
?>
<?php
$model->setAsRead();
if ($model->message === 'init') {
    echo Html::tag('p', yii::$app->l->t('conversation started'), ['class' => 'text-muted text-center']);
    echo '<hr/>';
    return;
}
?>

<div class="alert alert-<?= $model->sender->user_id != yii::$app->user->id ? 'default' : 'muted' ?>">
    <div class="row">
        <div class="col-md-12">
            <?=
            Html::tag('p', $user->fullName . Html::tag('span', yii::$app->formatter->asDatetime($model->created_at), ['class' => 'pull-right text-muted text-sm',]), [
                'class' => 'text-uppercase text-muted'
            ]);
            ?>
            <?= $model->message; ?>
        </div>
    </div>
</div>


