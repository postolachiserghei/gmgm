<?php

use yii\bootstrap\Modal;
use app\components\extend\Url;
use app\components\extend\Html;
use app\components\widgets\chat\ChatWidgetAssets;

/* @var $this app\components\extend\View */
/* @var $chatModel \app\models\Chat */
/* @var $chatWidget \app\components\widgets\chat\ChatWidget */
?>

<?php
Modal::begin([
    'id' => 'js-chat-modal',
    'size' => Modal::SIZE_LARGE,
    'header' => Html::tag('h3', yii::$app->l->t('chat') . ' - ' . $chatModel->getUser(), ['class' => 'text-uppercase txt-center']),
]);
?>

<div class="row js-chat-container" data-url="<?= Url::to(['chat']) ?>">
    <div class="col-md-4 col-sm-6 col-xs-6">
        <div class="js-chat-contacts list-group hidden"></div>
        <div class="js-chat-conversations list-group"></div>
    </div>
    <div class="col-md-8 col-sm-6 col-xs-6">
        <div class="js-chat-conversation-messages"></div>
    </div>
</div>
<form>
    <div class="row">
        <div class="col-md-12 pr">
            <textarea edi class="send form-control js-chat-textarea" placeholder="Press CTRL+ENTER or CMD+ENTER to submit message!"></textarea>
            <div class="send-bt btn btn-success text-uppercase"><?= yii::$app->l->t('ok') ?></div>
        </div>
    </div>
</form>


<?php Modal::end(); ?>




<?php
$js = "var css = {
        height: '400px',
        overflow: 'auto',
    };
    $(Chat.selectors.contactsContainer).css(css);
    $(Chat.selectors.conversationsContainer).css(css);
    $(Chat.selectors.messagesContainer).css(css);
    $(Chat.selectors.textarea).css({
        width: '90%',
        float: 'left',
        height: '60px',
        overflow: 'auto'
    });
    $(Chat.selectors.sendBt).css({
        width: '10%',
        float: 'right',
        left: '-1px',
        position: 'relative',
        height: '60px',
        padding: '20px'
    });";
$js .= "
    Keys.bind(Chat.selectors.textarea, 'ctrl+enter', function (el)
    {
        Chat.sendMessages(el);
    });
    $(Chat.selectors.sendBt).on('click', function ()
    {
        Chat.sendMessages($(Chat.selectors.textarea));
    });";
yii::$app->controller->view->registerJs($js);
?>