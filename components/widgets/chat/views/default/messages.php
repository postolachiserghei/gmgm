<?php

use app\components\extend\ListView;

/* @var $dataProvider \app\components\extend\ActiveDataProvider */
?>

<?=

ListView::widget([
    'layout' => '{items}',
    'dataProvider' => $dataProvider,
    'itemView' => 'messages/_item',
    'itemOptions' => function($model) {
        return [
            'data' => [
                'chat' => $model->chat_id
            ],
            'tag' => 'div',
            'class' => 'js-chat-message-item',
        ];
    },
    'options' => [
        'tag' => false,
    ]
]);
?>