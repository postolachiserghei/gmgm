<?php

use app\components\extend\ListView;

/* @var $dataProvider \app\components\extend\ActiveDataProvider */
?>

<?=

ListView::widget([
    'layout' => '{items}',
    'dataProvider' => $dataProvider,
    'itemView' => 'contacts/_item',
    'itemOptions' => function() {
        return [
            'tag' => 'a',
            'class' => 'list-group-item js-chat-contact-item'
        ];
    },
    'options' => [
        'tag' => false,
//        'class' => 'list-group-item',
    ]
]);
?>