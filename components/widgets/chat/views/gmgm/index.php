<?php

use yii\bootstrap\Modal;
use app\components\extend\Url;
use app\components\extend\Html;
use app\components\widgets\chat\ChatWidgetAssets;

/* @var $this app\components\extend\View */
/* @var $chatModel \app\models\Chat */
/* @var $chatWidget \app\components\widgets\chat\ChatWidget */

?>

<?php
Modal::begin([
    'id' => 'chat-box',
    'size' => Modal::SIZE_LARGE,
    'header' => Html::tag('h1', yii::$app->l->t('chat') . ' - ' . $chatModel->getUser(), ['class' => 'text-uppercase txt-center']),
    'options' => [
        'class' => 'js-chat-container',
        'data' => [
            'url' => Url::to(['chat'])
        ]
    ]
]);
?>

<div class="js-chat-contacts list-group hidden"></div>
<div class="js-chat-conversations list-group hidden"></div>
<div class="js-chat-conversation-messages chat-box-wrap"></div>


<div class="chat-send-wrap">
    <textarea></textarea>
</div>
<div class="chat-ctrl-wrap">
    <button onclick="Chat.sendMessages($(Chat.selectors.textarea));" class="button __orange __small">
        Отправить
    </button>
    <div class="<?= YII_ENV_DEV ? '' : 'hidden' ?> ">
        <a href="#" class="button __small">
            Отправить спецификацию
        </a>
        <a href="#" class="button __small">
            Завершить сделку
        </a>
    </div>
</div>


<?php Modal::end(); ?>


<?php
$js = "Chat.selectors = {
        countMessages: '.header-login-right .chat-counter.notif-count',
        chatModal: '#chat-box',
        mainContainer: '.js-chat-container',
        contactsContainer: '.js-chat-contacts',
        conversationsContainer: '.js-chat-conversations',
        messagesContainer: '.js-chat-conversation-messages',
        conversationItem: '.js-chat-conversation-item',
        messageItem: '.js-chat-message-item',
        contactItem: '.js-chat-contact-item',
        textarea: '.chat-send-wrap textarea',
        sendBt: '.chat-ctrl-wrap .button.__orange.__small'
    };";
$js .= "
    var css = {
        height: '400px',
        overflow: 'auto',
    };
    $(Chat.selectors.messagesContainer).css(css);
    ";

$js .= "
    Keys.bind(Chat.selectors.textarea, 'ctrl+enter', function (el)
    {
        Chat.sendMessages(el);
    });";
yii::$app->controller->view->registerJs($js, app\components\extend\View::POS_READY);
?>