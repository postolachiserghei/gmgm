<?php

use app\components\extend\Url;
use app\components\extend\Html;

/* @var $model app\models\ChatMessages */
/* @var $user app\models\User */

$user = $model->sender->getUser();
?>

<?php
$model->setAsRead();
if ($model->message === 'init') {
    echo Html::tag('p', yii::$app->l->t('conversation started'), ['class' => 'text-muted text-center']);
    echo '<hr/>';
    return;
}
?>

<div class="chat-mes-avatar">
    <div class="user-avatar" style="background-image: url(<?= $user->getFile('avatar')->getUrl(null, '/public/gmgm/img/user.png') ?>)"></div>
</div>
<div class="chat-mes-inner">
    <div class="chat-mes-username">
        <a href="#"><?= $user->fullName; ?></a>
    </div>
    <div class="chat-mes-text">
        <p>
            <?= $model->message; ?>
            <?= $model->setAsRead(); ?>
        </p>
        <span class="chat-mes-time">
            <?= yii::$app->formatter->asDatetime($model->created_at); ?>
        </span>
    </div>
</div>
