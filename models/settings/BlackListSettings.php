<?php

/**
 * Description of FileSettings
 *
 * @author postolachiserghei
 */

namespace app\models\settings;

use app\components\extend\yii;
use app\models\Settings;
use app\models\behaviors\SettingsBehavior;
use app\components\extend\Html;
use \app\components\extend\Model as BaseModel;

class BlackListSettings extends SettingsBehavior
{

    /**
     * @inheritdoc
     */
    public function events()
    {
        return[
            BaseModel::EVENT_SETTINGS_ASIGN => 'setSettingsForBehavior'
        ];
    }

    /**
     * set settings for owner model
     */
    public function setSettingsForBehavior()
    {
        $this->settings = [
            'block_ip_ranges' => [
                'before' => Html::tag('h3', yii::$app->l->t('Black list settings')),
                'label' => yii::$app->l->t('block users that match ip in ranges')
                . Html::tag('div', yii::$app->l->t('be very careful not to block yourself!')
                        . '<br/>' . yii::$app->l->t('your ip is: {ip}', ['ip' => yii::$app->request->userIP])
                        , ['class' => 'alert alert-danger']),
                'field' => Settings::FIELD_TEXTAREA,
                'value' => '',
                'options' => [
                    'placeholder' => '127.0.0.1, *1, 192.168.56.*'
                ],
                'containerOptions' => ['class' => 'col-md-10'],
            ],
        ];

        return $this->settings;
    }

}
