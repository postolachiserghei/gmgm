<?php

/**
 * Description of UserSettings
 *
 * @author postolachiserghei
 */

namespace app\models\settings;

use app\models\Settings;
use app\components\extend\yii;
use app\components\extend\Html;
use app\models\behaviors\SettingsBehavior;
use \app\components\extend\Model as BaseModel;

class TransactionSettings extends SettingsBehavior
{

    /**
     * @inheritdoc
     */
    public function events()
    {
        return[
            BaseModel::EVENT_SETTINGS_ASIGN => 'setSettingsForBehavior'
        ];
    }

    /**
     * set settings for owner model
     * @return type
     */
    public function setSettingsForBehavior()
    {
        $this->settings = [
            'bitcoin_merchant_id' => [
                'before' => Html::tag('h4', yii::$app->l->t('bitcoin settings')),
                'label' => yii::$app->l->t('merchant id'),
                'value' => '',
                'field' => Settings::FIELD_PASSWORD,
                'containerOptions' => ['class' => 'col-md-12'],
                'after' => '<div class="clearfix"></div><br/>'
            ],
            'bitcoin_api_id' => [
                'label' => yii::$app->l->t('api id'),
                'value' => '',
                'field' => Settings::FIELD_PASSWORD,
                'containerOptions' => ['class' => 'col-md-12'],
                'after' => '<div class="clearfix"></div><br/>'
            ],
            'bitcoin_private_key' => [
                'label' => yii::$app->l->t('private key'),
                'value' => '',
                'field' => Settings::FIELD_TEXTAREA,
                'containerOptions' => ['class' => 'col-md-12'],
                'after' => '<div class="clearfix"></div><br/>'
            ],
            'bitcoin_commission' => [
                'label' => yii::$app->l->t('commission in percentage'),
                'value' => 0,
                'field' => Settings::FIELD_TEXT,
                'containerOptions' => ['class' => 'col-md-12'],
                'after' => '<div class="clearfix"></div><br/>'
            ],
            'paypal_client_id' => [
                'before' => Html::tag('h4', yii::$app->l->t('paypal settings')),
                'label' => yii::$app->l->t('client id'),
                'value' => '',
                'field' => Settings::FIELD_PASSWORD,
                'containerOptions' => ['class' => 'col-md-12'],
                'after' => '<div class="clearfix"></div><br/>'
            ],
            'paypal_client_secret' => [
                'label' => yii::$app->l->t('client secret'),
                'value' => '',
                'field' => Settings::FIELD_PASSWORD,
                'containerOptions' => ['class' => 'col-md-12'],
                'after' => '<div class="clearfix"></div><br/>'
            ],
            'paypal_commission' => [
                'label' => yii::$app->l->t('commission in percentage'),
                'value' => 0,
                'field' => Settings::FIELD_TEXT,
                'containerOptions' => ['class' => 'col-md-12'],
                'after' => '<div class="clearfix"></div><br/>'
            ],
            'acct1.UserName' => [
                'label' => yii::$app->l->t('commission in percentage'),
                'value' => 0,
                'field' => Settings::FIELD_TEXT,
                'containerOptions' => ['class' => 'col-md-12'],
                'after' => '<div class="clearfix"></div><br/>'
            ],
            'systemCurrency' => [
                'label' => yii::$app->l->t('system currency'),
                'value' => $this->owner::CURRENCY_USD,
                'field' => Settings::FIELD_RADIO_LIST,
                'items' => $this->owner->getCurrenciesLabels(null, true),
                'containerOptions' => ['class' => 'col-md-12'],
            ],
        ];

        return $this->settings;
    }

}
