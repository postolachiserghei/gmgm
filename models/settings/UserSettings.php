<?php

/**
 * Description of UserSettings
 *
 * @author postolachiserghei
 */

namespace app\models\settings;

use app\models\Settings;
use app\components\extend\yii;
use app\components\extend\Html;
use app\models\behaviors\SettingsBehavior;
use \app\components\extend\Model as BaseModel;

class UserSettings extends SettingsBehavior
{

    /**
     * @inheritdoc
     */
    public function events()
    {
        return[
            BaseModel::EVENT_SETTINGS_ASIGN => 'setSettingsForBehavior'
        ];
    }

    /*
     * set settings for owner model
     */

    public function setSettingsForBehavior()
    {
        $this->settings = [
            'role_for_registration' => [
                'before' => Html::tag('h3', yii::$app->l->t('User settings')),
                'label' => yii::$app->l->t('default roles available for registration'),
                'value' => [],
                'field' => Settings::FIELD_CHECKBOX_LIST,
                'items' => yii::$app->helper->user()->identity()->getAvailableRoles(),
                'containerOptions' => ['class' => 'col-md-12'],
            ],
        ];

        return $this->settings;
    }

}
