<?php

/**
 * Description of UserSettings
 *
 * @author postolachiserghei
 */

namespace app\models\settings;

use app\models\Settings;
use app\components\extend\yii;
use app\components\extend\Html;
use app\models\behaviors\SettingsBehavior;
use \app\components\extend\Model as BaseModel;

class CategoriesSettings extends SettingsBehavior
{

    /**
     * @inheritdoc
     */
    public function events()
    {
        return[
            BaseModel::EVENT_SETTINGS_ASIGN => 'setSettingsForBehavior'
        ];
    }

    /*
     * set settings for owner model
     */

    public function setSettingsForBehavior()
    {
        $this->settings = [
            'game_catalog_show_limit' => [
                'before' => Html::tag('h3', yii::$app->l->t('categories settings')),
                'label' => yii::$app->l->t('how many items to show in catalog'),
                'value' => 10,
                'items' => [
                    10 => 10,
                    15 => 15,
                    20 => 20,
                ],
                'field' => Settings::FIELD_RADIO_LIST,
                'containerOptions' => ['class' => 'col-md-12'],
            ],
        ];

        return $this->settings;
    }

}
