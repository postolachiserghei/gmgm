<?php

/**
 * Description of BaseSettings
 *
 * @author postolachiserghei
 */

namespace app\models\settings;

use app\models\Settings;
use app\components\extend\yii;
use app\components\extend\Html;
use app\models\behaviors\SettingsBehavior;
use app\components\extend\Model as BaseModel;

class BaseSettings extends SettingsBehavior
{

    /**
     * @inheritdoc
     */
    public function events()
    {
        return[
            BaseModel::EVENT_SETTINGS_ASIGN => 'setSettingsForBehavior'
        ];
    }

    /*
     * set settings for owner model
     */

    public function setSettingsForBehavior()
    {
        $request = yii::$app->request;
        $log = yii::$app->helper->log();
        $this->settings = [
            'tld' => [
                'before' => Html::tag('h4', yii::$app->l->t('default site settings')),
                'label' => yii::$app->l->t('top level domain'),
                'field' => Settings::FIELD_TEXT,
                'value' => str_replace('http://', '', str_replace('https://', '', is_a(Yii::$app, 'yii\web\Application') ? $request->hostInfo : 'localhost'))
            ],
            'adminEmail' => [
                'label' => yii::$app->l->t('admin email'),
                'field' => Settings::FIELD_TEXT,
                'value' => yii::$app->helper->data()->getParam('adminEmail'),
                'after' => '<div class="clearfix"></div><br/>'
            ],
            'receive_notifications_by_email' => [
                'label' => yii::$app->l->t('send log to email'),
                'field' => Settings::FIELD_CHECKBOX_LIST,
                'value' => $log::TYPE_ERROR,
                'items' => $log->getTypeLabels(),
                'after' => '<div class="clearfix"></div><br/>'
            ],
            'usePjaxAdmin' => [
                'before' => Html::tag('h4', yii::$app->l->t('pjax settings')),
                'containerOptions' => ['class' => 'col-md-3'],
                'label' => yii::$app->l->t('use pjax for admin panel') . '&nbsp;',
                'field' => Settings::FIELD_CHECKBOX,
                'value' => 0
            ],
            'usePjaxFrontend' => [
                'label' => yii::$app->l->t('use pjax for frontend') . '&nbsp;',
                'field' => Settings::FIELD_CHECKBOX,
                'containerOptions' => ['class' => 'col-md-3'],
                'value' => 0,
                'after' => '<div class="clearfix"></div><br/>'
            ],
            'frontendTheme' => [
                'before' => Html::tag('h4', yii::$app->l->t('personalization settings')),
                'label' => yii::$app->l->t('frontend default theme') . '&nbsp;',
                'field' => Settings::FIELD_RADIO_LIST,
                'items' => yii::$app->params['frontendThemes'],
                'value' => 'gmgm',
                'containerOptions' => ['class' => 'col-md-3'],
                'value' => 'default',
                'after' => '<div class="clearfix"></div><br/>'
            ],
            'headScript' => [
                'before' => Html::tag('h4', yii::$app->l->t('include scripts')),
                'label' => yii::$app->l->t('include script into head section of the site') . '&nbsp;',
                'field' => Settings::FIELD_TEXTAREA,
                'containerOptions' => ['class' => 'col-md-6'],
                'value' => null
            ],
            'bodyScript' => [
                'label' => yii::$app->l->t('include script before body closing tag') . '&nbsp;',
                'field' => Settings::FIELD_TEXTAREA,
                'containerOptions' => ['class' => 'col-md-6'],
                'after' => '<div class="clearfix"></div><br/>',
                'value' => null
            ],
            'excludeHeadScript' => [
                'label' => yii::$app->l->t('exclude script from head where url contain') . '&nbsp;',
                'field' => Settings::FIELD_TEXTAREA,
                'options' => ['placeholder' => '/about, /ru/test/1, /, ...'],
                'containerOptions' => ['class' => 'col-md-6'],
                'value' => null,
            ],
            'excludeBodyScript' => [
                'label' => yii::$app->l->t('exclude script from body where url contain') . '&nbsp;',
                'field' => Settings::FIELD_TEXTAREA,
                'containerOptions' => ['class' => 'col-md-6'],
                'options' => ['placeholder' => '/about, /ru/test/1, /, ...'],
                'value' => null
            ],
        ];
        return $this->settings;
    }

}
