<?php

/**
 * Description of ProductsSettings
 *
 * @author postolachiserghei
 */

namespace app\models\settings;

use app\models\Settings;
use app\components\extend\yii;
use app\components\extend\Html;
use app\models\behaviors\SettingsBehavior;
use \app\components\extend\Model as BaseModel;

class ProductsSettings extends SettingsBehavior
{

    /**
     * @inheritdoc
     */
    public function events()
    {
        return[
            BaseModel::EVENT_SETTINGS_ASIGN => 'setSettingsForBehavior'
        ];
    }

    /*
     * set settings for owner model
     */

    public function setSettingsForBehavior()
    {
        $secondsInADay = (60 * 60 * 24);
        $secondsInADay2 = ($secondsInADay * 2);
        $secondsInADay3 = ($secondsInADay * 3);
        $this->settings = [
            'delivery_time_list' => [
                'before' => Html::tag('h3', yii::$app->l->t('Procts settings')),
                'label' => yii::$app->l->t('delivery time lists'),
                'value' => [
                    $secondsInADay => yii::$app->helper->formatter()->formatSeconds($secondsInADay),
                    $secondsInADay2 => yii::$app->helper->formatter()->formatSeconds($secondsInADay2),
                    $secondsInADay3 => yii::$app->helper->formatter()->formatSeconds($secondsInADay3),
                ],
                'field' => Settings::FIELD_DROPDOWN_MULTIPLE,
                'items' => [
                    $secondsInADay => yii::$app->helper->formatter()->formatSeconds($secondsInADay),
                    $secondsInADay2 => yii::$app->helper->formatter()->formatSeconds($secondsInADay2),
                    $secondsInADay3 => yii::$app->helper->formatter()->formatSeconds($secondsInADay3),
                ],
                'containerOptions' => ['class' => 'col-md-12'],
            ],
        ];

        return $this->settings;
    }

}
