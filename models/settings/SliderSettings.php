<?php

/**
 * Description of FileSettings
 *
 * @author postolachiserghei
 */

namespace app\models\settings;

use app\models\Settings;
use app\models\Slider;
use app\components\extend\yii;
use app\components\extend\Html;
use app\models\behaviors\SettingsBehavior;
use app\components\extend\Model as BaseModel;

class SliderSettings extends SettingsBehavior
{

    /**
     * @inheritdoc
     */
    public function events()
    {
        return[
            BaseModel::EVENT_SETTINGS_ASIGN => 'setSettingsForBehavior'
        ];
    }

    /**
     * set settings for owner model
     */
    public function setSettingsForBehavior()
    {
        $this->settings = [];
        $i = 0;
        $type = Slider::TYPE_MAIN;
        $label = Slider::getTypeLabels($type);
        $this->settings['slider_speed_' . $type]['before'] = Html::tag('h3', yii::$app->l->t('Slider list settings'));
        $this->settings['slider_speed_' . $type] = [
            'label' => yii::$app->l->t('slide speed for {type} (milliseconds)', [
                'type' => '"' . $label . '"'
            ]),
            'field' => Settings::FIELD_TEXT,
            'value' => '3000',
            'containerOptions' => ['class' => 'col-md-6'],
        ];
        $this->settings['slider_animation_speed_' . $type] = [
            'label' => yii::$app->l->t('slide animation speed for {type} (milliseconds)', [
                'type' => '"' . $label . '"'
            ]),
            'field' => Settings::FIELD_TEXT,
            'value' => '1000',
            'containerOptions' => ['class' => 'col-md-6'],
            'after' => '<div class="clearfix"></div><br/>'
        ];


        return $this->settings;
    }

}
