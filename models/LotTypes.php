<?php

namespace app\models;

use app\models\t\LotTypesT;
use app\components\extend\yii;
use app\models\behaviors\TranslateModel;

/**
 * This is the model class for table "{{%lot_types}}".
 *
 * @property integer $id
 * @property integer $status
 *
 * @property LotTypesT[] $lotTypesTs
 */
class LotTypes extends \app\components\extend\Model
{

    public $title;

    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 2;

    /**
     * @param integer/boolean $status
     * @param boolean $withLiveEdit (return translated labels wrapped in html tag if TRUE)
     * @return type
     */
    public function getStatusLabels($status = false, $withLiveEdit = true)
    {
        $ar = [
            self::STATUS_ACTIVE => yii::$app->l->t('active', ['update' => $withLiveEdit]),
            self::STATUS_DISABLED => yii::$app->l->t('disabled', ['update' => $withLiveEdit]),
        ];
        return $status !== false ? $ar[$status] : $ar;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return parent::behaviors() + [
            't' => [
                'class' => TranslateModel::className(),
                't' => new LotTypesT(),
                'fk' => 'lot_id',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%lot_types}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['title'], 'required', 'on' => ['create', 'update']],
            [['title'], 'safe'],
            [['status'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'title' => yii::$app->l->t('title'),
            'status' => yii::$app->l->t('Status'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLotTypesTs()
    {
        return $this->hasMany(LotTypesT::className(), ['lot_id' => 'id']);
    }

}
