<?php

namespace app\models;

use app\components\extend\yii;
use app\components\extend\ArrayHelper;
use app\components\extend\ActiveDataProvider;

/**
 * This is the model class for table "{{%products}}".
 */
class Products extends base\BaseProducts
{

    const ADDITIONAL_DATA_DELIVERY_IN_DAYS = 'additional_data[delivery_in_days]';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['title', 'type', 'category_id'], 'required'];
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $l = parent::attributeLabels();
        $l[self::ADDITIONAL_DATA_DELIVERY_IN_DAYS] = yii::$app->l->t('delivery in days');
        $l['price'] = yii::$app->l->t('price per unit');
        $l['category_id'] = yii::$app->l->t('game');
        return $l;
    }

    /**
     * get available categories (games)
     * @return type
     */
    public function getAvailableCategories()
    {
        $q = Categories::find();
        $q->andWhere(['type' => Categories::TYPE_GAME]);
        $result = ArrayHelper::map($q->all(), 'id', 'title');
        $result = ['' => yii::$app->l->t('select game')] + $result;
        return $result;
    }

    /**
     * get available types (LotTypes)
     * @return type
     */
    public function getAvailableTypes($prompt = true)
    {
        $q = LotTypes::find();
        $q->andWhere(['status' => LotTypes::STATUS_ACTIVE]);
        $result = ArrayHelper::map($q->all(), 'id', 'title');
        if ($prompt) {
            $result = ['' => yii::$app->l->t('select type')] + $result;
        }
        return $result;
    }

    /**
     *
     * @return ActiveDataProvider
     */
    public function getUserProducts($userid = null)
    {
        $q = self::find();
        if ($userid == null) {
            $userid = yii::$app->user->id;
        }
        $q->andWhere(['owner_id' => $userid]);
        return new ActiveDataProvider([
            'query' => $q
        ]);
    }

    /**
     * get lot type
     * @return \app\models\LotTypes
     */
    public function getLotType()
    {
        $model = LotTypes::findOne($this->type);
        if ($model) {
            return $model;
        }
        return new LotTypes;
    }

    /**
     * @param boolean $asString
     * @param boolean $formated
     * @return type
     */
    public function getPrice($asString = true, $formated = true)
    {
        $price = $formated ? yii::$app->formatter->asDecimal($this->price, 2) : ($this->price + 0);
        if ($asString !== true) {
            return $price;
        }
        return '<strong>' . yii::$app->formatter->asCurrency(($this->price + 0), $this->currencySign) . '</strong>';
    }

}
