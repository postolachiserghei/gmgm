<?php

namespace app\models;

use app\components\extend\yii;
use app\models\behaviors\PaymentBehavior;

/**
 * This is the model class for table "{{%shopping_cart}}".
 *
 * @property integer $id
 * @property string $additional_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order
 * @property integer $is_deleted
 * @property integer $status
 * @property Transactions $transaction
 */
class ShoppingCart extends \app\components\extend\Model
{

    const ADDITIONAL_DATA_TOTAL_PRICE = 'additional_data[total_price]';
    const ADDITIONAL_DATA_TOTAL_QUANTITY = 'additional_data[total_quantity]';
    const ADDITIONAL_DATA_ITEMS = 'additional_data[items]';
    const STATUS_NEW = 1;
    const STATUS_SUBMITTED = 2;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'payment' => [
                'class' => PaymentBehavior::className(),
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shopping_cart}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['status'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'id' => 'ID',
            'additional_data' => 'Additional Data',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'order' => 'Order',
            'is_deleted' => 'Is Deleted',
            'status' => 'Status',
        ]);
    }

    /**
     *
     * @param array $params
     */
    public function add($params)
    {
        $transaction = $this->transaction;
        if (!is_array($params)) {
            $this->addError('id', yii::$app->l->t('not array params given'));
        }
        foreach ($params as $id => $element) {
            if (!$item = @$element['item']) {
                continue;
            }
            $details = array_merge(yii::$app->helper->str()->decrypt($item), $params);
            if (!isset($details['model']) || !isset($details['attributes'])) {
                continue;
            }
            $quantity = ((int) @$params[$id]['quantity'] > 1 ? (int) @$params[$id]['quantity'] : 1);
            $itemModel = $this->getItem($id, $details);
            $pricePerUnit = $itemModel ? $itemModel->getPrice(false, false) : 0;
            $price = ($pricePerUnit * $quantity);
            $data = [
                'model' => $details['model'],
                'buyer' => yii::$app->user->id,
                'seller' => @$itemModel->owner_id,
                'system' => (@$params[$id]['system'] ? @$params[$id]['system'] : PaymentBehavior::PAYMENT_SYSTEM_PAYPAL),
                'quantity' => $quantity,
                'email' => yii::$app->helper->user()->identity()->email,
                'primaryKey' => $id,
                'price_per_unit' => $pricePerUnit,
                'price' => $price,
                'currency' => ($transaction->getSetting('systemCurrency', $transaction::CURRENCY_USD)),
            ];
            $info = $this->getData(self::ADDITIONAL_DATA_ITEMS . '.' . $id);
            $this->setData(self::ADDITIONAL_DATA_ITEMS . '.' . $id, ($info ? array_merge($info, $data) : $data));
            $this->refreshData();
            if (!$this->save()) {
                $this->addError('id', $this->getErrors());
            }
        }
        return $this;
    }

    /**
     *
     * @return Transactions
     */
    public function addTransaction($data = [])
    {
        $sysetem = @$data['system'] !== null ? $data['system'] : PaymentBehavior::PAYMENT_SYSTEM_PAYPAL;
        $data['class'] = self::className();
        $data['primaryKey'] = $this->primaryKey;
        $data['quantity'] = $this->getData(self::ADDITIONAL_DATA_TOTAL_QUANTITY);
        $data['amount'] = $this->getData(self::ADDITIONAL_DATA_TOTAL_PRICE);
        $data['system'] = $sysetem;
        $data['email'] = yii::$app->helper->user()->identity()->email;
        $data['userId'] = yii::$app->user->id;
        $data['item'] = yii::$app->helper->str()->encrypt($data);
        $data['shopping_cart_info'] = $this->getData(self::ADDITIONAL_DATA_ITEMS);
        return $this->transaction->add($data);
    }

    /**
     * get item by id
     * @return \app\components\extend\Model
     */
    public function getItem($id, $data = null)
    {
        if (!$info = $this->getData(self::ADDITIONAL_DATA_ITEMS . '.' . $id)) {
            $info = $data;
        }
        $modelClass = $info['model'];
        $model = $modelClass::find()->where(['id' => $id, 'status' => Products::STATUS_ACTIVE])->one();
        return $model;
    }

    /**
     *
     * @param integer $itemId
     */
    public function removeItem($itemId)
    {
        if ($items = $this->getData(self::ADDITIONAL_DATA_ITEMS)) {
            unset($items[$itemId]);
            $this->setData(self::ADDITIONAL_DATA_ITEMS, $items);
            $this->refreshData();
            return $this->save();
        }
    }

    /**
     * get items
     * @return array
     */
    public function getItems()
    {
        $models = [];
        if ($items = $this->getData(self::ADDITIONAL_DATA_ITEMS)) {
            foreach ($items as $id => $info) {
                $models[$id] = $this->getItem($id);
            }
        }
        return $models;
    }

    /**
     *
     * @param type $attribute
     * @param type $itemId
     * @return type
     */
    public function getItemAttribute($attribute, $itemId)
    {
        return self::ADDITIONAL_DATA_ITEMS . '.' . $itemId . '.' . $attribute;
    }

    /**
     *
     * @param type $itemId
     * @return type
     */
    public function getItemQuantity($itemId)
    {
        return $this->getData($this->getItemAttribute('quantity', $itemId));
    }

    /**
     *
     * @param type $itemId
     * @param type $quantity
     * @return type
     */
    public function setItemQuantity($itemId, $quantity)
    {
        $this->setData($this->getItemAttribute('quantity', $itemId), $quantity);
        $pricePerUnit = $this->getData($this->getItemAttribute('price_per_unit', $itemId));
        $this->setData($this->getItemAttribute('price', $itemId), ($pricePerUnit * $quantity));
        $this->refreshData();
        return $this->save();
    }

    /**
     *
     * @return int
     */
    public function refreshData()
    {
        $items = $this->getData(self::ADDITIONAL_DATA_ITEMS);
        if (!$items) {
            return;
        }
        $quantity = 0;
        $price = 0;
        foreach ($items as $id => $info) {
            $quantity += $info['quantity'];
            $price += $info['price'];
        }
        $this->setData(self::ADDITIONAL_DATA_TOTAL_QUANTITY, $quantity);
        $this->setData(self::ADDITIONAL_DATA_TOTAL_PRICE, $price);
    }

    /**
     * get item price
     * @param type $itemId
     * @param type $formated
     * @param type $asString
     * @return type
     */
    public function getItemPrice($itemId, $formated = true, $asString = true)
    {
        $p = $this->getData($this->getItemAttribute('price', $itemId));
        $model = $this->getItem($itemId);
        $price = $formated ? yii::$app->formatter->asDecimal($p, 2) : ($p + 0);
        if ($asString !== true) {
            return $price;
        }
        return yii::$app->formatter->asCurrency(($p + 0), ($model->currencySign));
    }

    /**
     * get total price
     * @param type $itemId
     * @return type
     */
    public function getPrice($formated = true, $asString = true)
    {
        return $this->getData(self::ADDITIONAL_DATA_TOTAL_PRICE);
    }

    /**
     * @param integer $status
     * @return boolean
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this->save();
    }

}
