<?php

namespace app\models;

use app\models\t\ServersT;
use app\components\extend\yii;

/**
 * This is the model class for table "{{%servers}}".
 *
 * @property integer $id
 * @property string $url
 * @property string $additional_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order
 * @property integer $is_deleted
 * @property integer $category_id
 *
 * @property ServersT[] $serversTs
 */
class Servers extends \app\components\extend\Model
{

    public $title;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            't' => [
                'class' => behaviors\TranslateModel::className(),
                't' => new ServersT(),
                'fk' => 'server_id',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%servers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['category_id'], 'required'],
            [['url'], 'url'],
            [['created_at', 'category_id', 'updated_at', 'order', 'is_deleted'], 'integer'],
            [['title'], 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'id' => yii::$app->l->t('server id'),
            'url' => yii::$app->l->t('url'),
            'title' => yii::$app->l->t('title'),
            'category_id' => yii::$app->l->t('game'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServersTs()
    {
        return $this->hasMany(ServersT::className(), ['server_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    /**
     * get available categories
     * @return Categories[]
     */
    public function getAvailableGames()
    {
        return Categories::find()->where(['type' => Categories::TYPE_GAME])->all();
    }

}
