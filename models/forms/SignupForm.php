<?php

namespace app\models\forms;

use app\models\User;
use app\components\extend\yii;
use app\components\extend\Model;
use app\components\helper\EmailHelper;

/**
 * Signup form
 */
class SignupForm extends \app\components\extend\Model
{

    public $username;
    public $email;
    public $password;
    public $passwordRepeat;
    public $rbacRole;
    public $availableRoles = [];
    public $additional_data;

    public function init()
    {
        $init = parent::init();
        $this->availableRoles = (new User())->getSetting('role_for_registration');
        if (count($this->availableRoles) > 0 && !$this->rbacRole) {
            $this->rbacRole = $this->availableRoles[0];
        }
        return $init;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => 'app\models\User'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['rbacRole', 'validateRole'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => 'app\models\User'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['passwordRepeat', 'required'],
            ['passwordRepeat', 'compare', 'compareAttribute' => 'password'],
            ['additional_data', 'safe'],
        ];
    }

    /**
     * check if right role is passed
     */
    public function validateRole()
    {
        if ($this->rbacRole && array_search($this->rbacRole, $this->availableRoles) === false) {
            $this->addError('rbacRole', yii::$app->l->t('wrong role selected'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => yii::$app->l->t('id'),
            'username' => yii::$app->l->t('username'),
            'password' => yii::$app->l->t('пароль'),
            'passwordRepeat' => yii::$app->l->t('повторить пароль'),
            'email' => yii::$app->l->t('email'),
            User::ADDITIONAL_DATA_FIO => yii::$app->l->t('fio'),
            User::ADDITIONAL_DATA_PHONE => yii::$app->l->t('Телефон'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->scenario = User::SCENARIO_SIGN_UP;
            $user->username = $this->username;
            $user->email = $this->email;
            $user->password = $this->password;
            $user->rbacRole = (is_array($this->rbacRole) ? $this->rbacRole : [$this->rbacRole]);
            $user->additional_data = $this->additional_data;
            if ($user->save()) {
                $subject = yii::$app->l->t('регистрация на сайте {app}', [
                    'app' => yii::$app->name
                ]);
                $render = ['view' => 'success_register', 'params' => [
                        'user' => $user,
                        'subject' => $subject,
                        'password' => $this->password,
                ]];
                $sent = EmailHelper::send($user->email, $subject, $render, [yii::$app->params['supportEmail'] => yii::$app->name]);
            }
            return $user;
        }
        return null;
    }

}
