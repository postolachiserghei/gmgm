<?php

namespace app\models;

use app\components\extend\yii;
use app\components\extend\ArrayHelper;
use app\components\extend\ActiveDataProvider;

/**
 * This is the model class for table "{{%chat}}".
 *
 * @property integer $id
 * @property string $subject
 * @property string $additional_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order
 * @property integer $is_deleted
 *
 * @property ChatMembers[] $members
 * @property ChatMessages[] $messages
 */
class Chat extends \app\components\extend\Model
{

    const INITIATOR_ID = 'initiator_id';
    const INITIATOR_NAME = 'initiator_name';
    const TITLE = 'title';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%chat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
            [['subject'], 'string', 'max' => 200],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'id' => yii::$app->l->t('id'),
            'subject' => yii::$app->l->t('subject'),
            'additional_data' => yii::$app->l->t('additional data'),
            'created_at' => yii::$app->l->t('Date created'),
            'updated_at' => yii::$app->l->t('Date updated'),
            'order' => yii::$app->l->t('Order position'),
            'is_deleted' => yii::$app->l->t('Is deleted'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMembers()
    {
        return $this->hasMany(ChatMembers::className(), ['chat_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(ChatMessages::className(), ['chat_id' => 'id']);
    }

    /**
     * 
     * @return ActiveDataProvider
     */
    public function getMyChatList($query = null)
    {
        $q = $query ? $query : $this->find();
        $q->joinWith(['members' => function($q) {
                $q->joinWith(['messages']);
                return $q->andWhere(['user_id' => yii::$app->user->id]);
            }]);
        return new ActiveDataProvider([
            'query' => $q,
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => ['defaultOrder' => ['updated_at' => SORT_DESC, 'order' => SORT_ASC]]
        ]);
    }

    /**
     * get user
     */
    public static function getUser()
    {
        return yii::$app->user->id;
    }

    /**
     * add default conversation
     */
    public function addDefaultConversation()
    {
        $supports = yii::$app->helper->user()->identity()->getSupportManagers()->andWhere('id!=:id', ['id' => yii::$app->user->id])->all();
        if (!$supports) {
            return;
        }
        $c = self::create('chat with admnidtration', yii::$app->user->id);
        if ($c && $c->joinMember(yii::$app->user->id)) {
            $message = $c->sendMessage(yii::$app->user->id, 'init');
            foreach ($supports as $support) {
                $c->joinMember($support->primaryKey);
            }
        }
    }

    /**
     * create new chat
     * @param string $subject
     * @param integer | string(50) $userId
     * @param array $data
     * @return Chat
     */
    public static function create($subject, $userId, $data = [])
    {
        $conversation = new Chat();
        $conversation->subject = $subject;
        $defaultData = [
            self::TITLE => $conversation->subject,
            self::INITIATOR_ID => yii::$app->user->id,
            self::INITIATOR_NAME => yii::$app->helper->user()->identity()->getFullName(),
        ];
        $conversation->additional_data = array_merge($defaultData, $data);
        if ($conversation->validate()) {
            $conversation->save();
        }
        return $conversation;
    }

    /**
     * @param type $userId
     * @param array $data
     */
    public function joinMember($userId, $data = [])
    {
        $member = new ChatMembers();
        $joined = $member::find()->where(['user_id' => $userId, 'chat_id' => $this->primaryKey])->one();
        if (!$joined) {
            $member->user_id = $userId;
            $member->chat_id = $this->primaryKey;
            $member->name = ($member->user->fullName ? $member->user->fullName : yii::$app->l->t('guest {id}', ['id' => yii::$app->user->id]));
            $member->additional_data = $data;
            if ($member->validate() && $member->save()) {
                return $member;
            }
        }
        return $joined ? $joined : $member;
    }

    /**
     * 
     * @param integer $id
     * @return Chat
     */
    public static function getMyChat($id)
    {
        return self::find()->joinWith(['members' => function($q) {
                        return $q->where(['user_id' => yii::$app->user->id]);
                    }])->where(['chat_id' => (int) $id])->one();
    }

    /**
     * add message
     * @param integer $memberId
     * @param string $message
     * @param array $data
     */
    public function sendMessage($memberId, $message, $data = [])
    {
        $member = ChatMembers::find()->where('user_id=:mid', ['mid' => $memberId])->one();
        if (!$member) {
            return;
        }
        $messages = new ChatMessages();
        $messages->chat_id = $this->primaryKey;
        $messages->sender_id = $member->primaryKey;
        $messages->message = strip_tags($message);
        $messages->additional_data = $data;
        if ($messages->validate()) {
            $saved = $messages->save();
        }

        $receivers = [];
        if ($receiverMembers = $this->getMembers()->select('user_id')->where('user_id!=:si', [
                    'si' => $memberId
                ])->all()) {
            foreach ($receiverMembers as $key => $r) {
                $receivers[] = $r->user_id;
            }
        }
        return $receivers;
    }

    /**
     * 
     * @param Userp[] $supportManagers
     */
    public function addToChat($supportManagers)
    {
        $managers = ArrayHelper::map($supportManagers, 'id', 'id');
        if (count($managers) == 0) {
            return;
        }
        $m = $this->getMembers()->where(['not in', 'user_id', $managers]);
        $membersIds = [];
        if ($m->count() == 1 && $m->one()->user_id = ChatMembers::TYPE_ADMIN) {
            foreach ($managers as $id) {
                if ($id == yii::$app->user->id) {
                    continue;
                }
                $m = $this->joinMember($id);
                $membersIds[] = $m->primaryKey;
            }
        }
        return $membersIds;
    }

    /**
     * 
     * @param type $userId
     * @return array [chats=>[chat_1=>9, chat_2=>10,...], total=>19]
     */
    public static function countNewMessages($userId = null, $chatId = null)
    {
        if (!$userId) {
            $userId = yii::$app->user->id;
        }
        $q = Chat::find();
        if ($chatId) {
            $q->andWhere([Chat::tableName() . '.id' => $chatId]);
        }
        $rAttr = ChatMessages::readAttribute($userId);
        $jAttr = $q->normalizeJsonConstants($rAttr, ChatMessages::tableName());

        $q->joinWith(['members']);
        $q->andWhere(ChatMembers::tableName() . '.user_id=:uid', [
            'uid' => $userId
        ]);

        $chats = $q->all();
        $newMessagesChats = [];
        $total = 0;
        if ($chats) {
            foreach ($chats as $chat) {
                $cq = ChatMessages::find()->where([ChatMessages::tableName() . '.chat_id' => $chat->primaryKey]);
                $cq->joinWith('sender');
                $cq->andWhere(ChatMembers::tableName() . '.user_id!=:sid', ['sid' => yii::$app->user->id]);
                $cq->andWhere([ChatMessages::tableName() . '.status' => ChatMessages::STATUS_NEW]);
//                $cq->andWhere($jAttr . ' NOT LIKE "%[' . $userId . ']%"');
                $count = $cq->count();
                $total = ($total + $count);
                $newMessagesChats[$chat->primaryKey] = $count;
            }
        }
        return [
            'chats' => count($newMessagesChats) > 0 ? $newMessagesChats : 0,
            'total' => ($total > 0 ? $total : 0)
        ];
    }

    /**
     * 
     * @return string
     */
    public function getTitle()
    {
        $initiatorId = $this->getData(self::INITIATOR_ID);
        if ($initiatorId == yii::$app->user->id) {
            return yii::$app->l->t($this->subject);
        }
        return $this->getData(self::INITIATOR_NAME);
    }

}
