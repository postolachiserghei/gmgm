<?php

namespace app\models;

use app\components\extend\yii;

/**
 * This is the model class for table "{{%black_list}}".
 *
 * @property string $id
 * @property integer $type
 * @property string $additional_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order
 * @property integer $is_deleted
 */
class BlackList extends \app\components\extend\Model
{

    const TYPE_IP = 1;
    const TYPE_USER_ID = 2;
    const ADDITIONAL_DATA_IP = 'additional_data[ip]';
    const ADDITIONAL_DATA_USER_ID = 'additional_data[user_id]';

    /**
     * @param integer $type
     * @param boolean $withLiveEdit (return translated labels wrapped in html tag if TRUE)
     * @return array | string
     */
    public function getTypeLabels($type = false, $withLiveEdit = true)
    {
        $ar = [
            static::TYPE_IP => yii::$app->l->t('block user by ip', ['update' => $withLiveEdit]),
            static::TYPE_USER_ID => yii::$app->l->t('block user by user id', ['update' => $withLiveEdit]),
        ];

        return $type === false ? $ar : $ar[$type];
    }

    public function init()
    {
        parent::init();
        if ($this->isNewRecord) {
            $this->type = self::TYPE_IP;
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%black_list}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return parent::behaviors() + [
            'settings' => [
                'class' => settings\BlackListSettings::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['value'], 'unique'],
            [['value', 'type'], 'required'],
            [['type', 'created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
            [['value'], 'validateIdIp'],
        ]);
    }

    /**
     * validate id or ip
     * @param type $attribute
     */
    public function validateIdIp($attribute)
    {
        $this->value = str_replace('_', '', $this->value);
        if ($this->type == self::TYPE_IP) {
            if (filter_var($this->value, FILTER_VALIDATE_IP) === false) {
                $this->addError($attribute, yii::$app->l->t("{ip} is not a valid IP address", ['ip' => $this->value]));
            }
        }
        if ($this->type == self::TYPE_USER_ID) {
            $user = User::findOne($this->value);
            if (!$user) {
                $this->addError($attribute, yii::$app->l->t('user with id {id} not found', ['id' => $this->value]));
            }
            if ($user && $user->role == User::ROLE_ADMIN) {
                $this->addError($attribute, yii::$app->l->t('you cannot block this user'));
            }
        }
        if (yii::$app->request->userIP == $this->value || $this->value == yii::$app->user->id) {
            $this->addError($attribute, yii::$app->l->t("you cannot block yourself"));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'value' => yii::$app->l->t('block user ip or user id'),
            'type' => yii::$app->l->t('type'),
            'additional_data' => yii::$app->l->t('additional data'),
            'created_at' => yii::$app->l->t('Date created'),
            'updated_at' => yii::$app->l->t('Date updated'),
            'order' => yii::$app->l->t('Order position'),
            self::ADDITIONAL_DATA_IP => yii::$app->l->t('user ip'),
            self::ADDITIONAL_DATA_USER_ID => yii::$app->l->t('user id'),
            'is_deleted' => yii::$app->l->t('Is deleted'),
        ]);
    }

    /**
     * check if ip is blocked
     * @param type $ip
     * @return boolean
     */
    public function getIpIsInBlockRanges($ip = null, $ranges = null)
    {
        $block = false;
        if (!$ranges) {
            $ranges = $this->getSetting('block_ip_ranges');
        }
        if (!$ranges || trim($ranges) === '') {
            $block = false;
        }
        $ips = explode(',', preg_replace('/\s+/', '', $ranges));
        if (!is_array($ips)) {
            $block = false;
        }
        if (!$ip) {
            $ip = $ip = yii::$app->request->userIP;
        }
        foreach ($ips as $filter) {
            if ($filter === '*' || $filter === $ip || (($pos = strpos($filter, '*')) !== false && !strncmp($ip, $filter, $pos))) {
                $block = true;
            }
        }

        return $block;
    }

}
