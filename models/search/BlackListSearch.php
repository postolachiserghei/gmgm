<?php

namespace app\models\search;

use app\components\extend\yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BlackList;

/**
 * BlackListSearch represents the model behind the search form about `app\models\BlackList`.
 */
class BlackListSearch extends BlackList
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id', 'value', 'additional_data'], 'safe'],
            [['type', 'created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BlackList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'value' => $this->value,
            'type' => $this->type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'order' => $this->order,
            'is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['like', 'id', $this->id])
                ->andFilterWhere(['like', 'value', $this->value])
                ->andFilterWhere(['like', 'additional_data', $this->additional_data]);

        return $dataProvider;
    }

}
