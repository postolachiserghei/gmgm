<?php

namespace app\models\search;

use app\components\extend\yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Slider;

/**
 * SliderSearch represents the model behind the search form about `app\models\Slider`.
 */
class SliderSearch extends Slider
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = array_merge(parent::rules(), [
            [['id', 'type', 'status', 'created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
            [['additional_data'], 'safe'],
        ]);
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Slider::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'order' => $this->order,
            'is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['like', 'additional_data', $this->additional_data]);

        return $dataProvider;
    }

    /**
     * 
     * @return yii\db\ActiveQuery
     */
    public static function findActive()
    {
        return Slider::find()->andWhere(['status' => self::STATUS_ACTIVE]);
    }

}
