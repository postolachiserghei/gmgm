<?php

namespace app\models\search;

use app\models\Transactions;
use app\components\extend\yii;
use app\components\extend\Model;
use app\components\extend\ActiveDataProvider;

/**
 * TransactionsSearch represents the model behind the search form about `app\models\Transactions`.
 */
class TransactionsSearch extends Transactions
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payment_system', 'quantity', 'status', 'created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
            [['amount'], 'number'],
            [['currency', 'user_id', 'item_id', 'item_model', 'additional_data'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transactions::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'amount' => $this->amount,
            'payment_system' => $this->payment_system,
            'quantity' => $this->quantity,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'order' => $this->order,
            'is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['like', 'currency', $this->currency])
        ->andFilterWhere(['like', 'user_id', $this->user_id])
        ->andFilterWhere(['like', 'item_id', $this->item_id])
        ->andFilterWhere(['like', 'item_model', $this->item_model])
        ->andFilterWhere(['like', 'additional_data', $this->additional_data]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchMy($params)
    {
        $query = Transactions::findMy();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'amount' => $this->amount,
            'payment_system' => $this->payment_system,
            'quantity' => $this->quantity,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'order' => $this->order,
            'is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['like', 'currency', $this->currency])
        ->andFilterWhere(['like', 'user_id', $this->user_id])
        ->andFilterWhere(['like', 'item_id', $this->item_id])
        ->andFilterWhere(['like', 'item_model', $this->item_model])
        ->andFilterWhere(['like', 'additional_data', $this->additional_data]);

        return $dataProvider;
    }

}
