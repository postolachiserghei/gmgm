<?php

namespace app\models\search;

use yii\base\Model;
use app\models\Servers;
use app\models\Categories;
use app\models\t\ServersT;
use yii\data\ActiveDataProvider;

/**
 * ServersSearch represents the model behind the search form about `app\models\Servers`.
 */
class ServersSearch extends Servers
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
            [['url', 'title', 'category_id', 'additional_data'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Servers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->sort->attributes['title'] = [
            'asc' => [ServersT::tableName() . '.title' => SORT_ASC],
            'desc' => [ServersT::tableName() . '.title' => SORT_DESC],
        ];

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith(['serversTs', 'game']);
        // grid filtering conditions
        $query->andFilterWhere([
            //ServersT::tableName() . '.title' => $this->title,
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'order' => $this->order,
            'is_deleted' => $this->is_deleted,
            'category_id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', 'url', $this->url])
        ->andFilterWhere(['like', ServersT::tableName() . '.title', $this->title]);

        return $dataProvider;
    }

}
