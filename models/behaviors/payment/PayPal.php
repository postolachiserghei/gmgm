<?php

/**
 * Description of FileSaveBehavior
 *
 * @author postolachiserghei
 */

namespace app\models\behaviors\payment;

use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use PayPal\Rest\ApiContext;
use PayPal\Api\RedirectUrls;
use app\components\extend\yii;
use app\components\extend\Url;
use app\components\extend\Html;
use PayPal\Auth\OAuthTokenCredential;
use app\models\behaviors\PaymentBehavior;
use app\models\behaviors\payment\paypal\Configuration;
/**
 * paypal adaptive
 * https://github.com/paypal/adaptiveaccounts-sdk-php
 */
use PayPal\Types\AA\AccountIdentifierType;
use PayPal\Service\AdaptiveAccountsService;
use PayPal\Types\AA\GetVerifiedStatusRequest;

/**
 * @property ApiContext $api PayPal api
 */
class PayPal extends PaymentBase
{

    public function init()
    {
        parent::init();
        $this->id = PaymentBehavior::PAYMENT_SYSTEM_PAYPAL;
        $this->icon = Html::ico('paypal');
    }

    public function auth()
    {
        $clientId = $this->transaction->getSetting('paypal_client_id');
        $clientSecret = $this->transaction->getSetting('paypal_client_secrets');
        $authTocken = new OAuthTokenCredential($clientId, $clientSecret);
        $this->api = new ApiContext($authTocken);
        return $this->api;
    }

    /**
     * process payment
     */
    public function processPayment()
    {
        $paymentId = @$_REQUEST['paymentId'];
        $payerId = @$_REQUEST['PayerID'];
        if (!$paymentId || $payerId) {
            $this->log->error(yii::$app->l->t('error while processing paypal payment'));
            return;
        }
        $transaction = $this->transaction;
        $this->auth();
        $payment = new Payment($this->api);
        $payment->setId($paymentId);
        $exec = new \PayPal\Api\PaymentExecution();
        $exec->setPayerId($payerId);
        try {
            $result = $payment->execute($exec, $this->api);
        } catch (Exception $e) {
            $this->log->error($e);
            die($e);
        }
        $transaction->status = $transaction::STATUS_PAID;
        if ($transaction->validate()) {
            $transaction->additional_data = array_merge($transaction->additional_data, ['paypal-result' => $result]);
            if (!$transaction->save()) {
                $this->triggerBasePaymentFail($transaction, $transaction->getErrors());
            } else {
                $this->triggerBasePaymentSuccess($transaction);
                return true;
            }
        }
    }

    public function goToPay()
    {
        $receivers = [
            1 => new Receiver
        ];
    }

    /**
     *
     * @return type
     */
    public function goToPayOld()
    {
        $this->auth();

        // Create new payer and method
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        // Set redirect urls
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(Url::to([
            '/payment/process',
            'transaction' => $this->transaction->id,
        ], true))
        ->setCancelUrl(Url::to(['/'], true));

        // Set payment amount
        $amount = new Amount();
        $amount->setCurrency($this->transaction->currency)
        ->setTotal($this->transaction->model->getPrice(false, false));

        // Set transaction object
        $transaction = new Transaction();
        $transaction->setAmount($amount)
        ->setDescription($this->transaction->model->getDescription());

        // Create the full payment object
        $payment = new Payment();
        $payment->setIntent('sale')
        ->setPayer($payer)
        ->setRedirectUrls($redirectUrls)
        ->setTransactions(array($transaction));

        // Create payment with valid API context
        try {
            $payment->create($this->api);
            // Get PayPal redirect URL and redirect user
            $approvalUrl = $payment->getApprovalLink();
            $transactions = $this->transaction;
            $transactions->setData($transactions::ADDITIONAL_DATA_URL . '_' . $this->id, $approvalUrl);
            $transactions->save();
            if (!yii::$app->request->isAjax) {
                header('location: ' . $approvalUrl);
                exit();
            }
            // REDIRECT USER TO $approvalUrl
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            echo $ex->getCode();
            echo $ex->getData();
            die($ex);
        } catch (Exception $ex) {
            $this->log->error($ex);
            die($ex);
        }
        return $this->api->getCredential();
    }

    /**
     * validate account
     * @param type $account
     * @return type
     */
    public function validateAccount($account)
    {
        $account = trim(strip_tags($account));
        $getVerifiedStatus = new GetVerifiedStatusRequest();
        $accountIdentifier = new AccountIdentifierType();
        $accountIdentifier->emailAddress = $account;
        $getVerifiedStatus->accountIdentifier = $accountIdentifier;
        $getVerifiedStatus->matchCriteria = 'NONE'; // NONE|NAME
        $config = [
            'mode' => (YII_ENV_PROD ? 'live' : 'sandbox'),
            'acct1.UserName' => 'postolachiserghei-facilitator@yandex.com',
            'acct1.Password' => 'AZND1q_HYluspF40OSh7blRddGM9LUlQRJMA49LDlMuwuGkERDZXFyuwmeTgHAhSusEtp8Helm7MVDr_',
            'acct1.Signature' => 'EP9bRAIK5Q4NLgQ7JudUxQFsZ59A2BeZiwDBKsVlu1_zin0fFSfBFIpKZKTW9HbnqXU5P0ciQhKRfmNg'
        ];
        $service = new AdaptiveAccountsService($config);
        try {
            $response = $service->GetVerifiedStatus($getVerifiedStatus);
        } catch (Exception $ex) {
            return;
        }
        $this->log->info([
            'action' => 'verification (validateAccount())',
            'response' => $response
        ]);
        if (strtoupper(@$response->accountStatus) == "VERIFIED") {
            return true;
        } else {
            return false;
        }
    }

    public function initPayment()
    {
        return false;
    }

    public function test($test, $q)
    {
        return $test;
    }

}
