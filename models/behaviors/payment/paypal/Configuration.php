<?php

/**
 * Description of FileSaveBehavior
 *
 * @author postolachiserghei
 */

namespace app\models\behaviors\payment\paypal;

/**
 * paypal adaptive configuration
 * https://github.com/paypal/adaptiveaccounts-sdk-php
 */

/**
 * @property ApiContext $api PayPal api
 */
class Configuration extends \yii\base\Component
{

    // For a full list of configuration parameters refer in wiki page (https://github.com/paypal/sdk-core-php/wiki/Configuring-the-SDK)
    public static function getConfig()
    {
        $config = [
            'mode' => (YII_ENV_PROD ? 'live' : 'sandbox'),
        // values: 'sandbox' for testing
        //		   'live' for production
        //         'tls' for testing if your server supports TLSv1.2
        // TLSv1.2 Check: Comment the above line, and switch the mode to tls as shown below
        // "mode" => "tls"
        // These values are defaulted in SDK. If you want to override default values, uncomment it and add your value.
        // "http.ConnectionTimeOut" => "5000",
        // "http.Retry" => "2",
        ];
        return $config;
    }

    // Creates a configuration array containing credentials and other required configuration parameters.
    public static function getAcctAndConfig()
    {
//        $config = array(
//            // Signature Credential
//            "acct1.UserName" => "jb-us-seller_api1.paypal.com",
//            "acct1.Password" => "WX4WTU3S8MY44S7F",
//            "acct1.Signature" => "AFcWxV21C7fd0v3bYYYRCpSSRl31A7yDhhsPUU2XhtMoZXsWHFxu-RWy",
//            "acct1.AppId" => "APP-80W284485P519543T"
//
//        // Sample Certificate Credential
//        // "acct1.UserName" => "certuser_biz_api1.paypal.com",
//        // "acct1.Password" => "D6JNKKULHN3G5B8A",
//        // Certificate path relative to config folder or absolute path in file system
//        // "acct1.CertPath" => "cert_key.pem",
//        // "acct1.AppId" => "APP-80W284485P519543T"
//        );


        $config = [
            'mode' => (YII_ENV_PROD ? 'live' : 'sandbox'),
            'acct1.UserName' => 'postolachiserghei-facilitator@yandex.com',
            'acct1.Password' => 'AZND1q_HYluspF40OSh7blRddGM9LUlQRJMA49LDlMuwuGkERDZXFyuwmeTgHAhSusEtp8Helm7MVDr_',
            'acct1.Signature' => 'EP9bRAIK5Q4NLgQ7JudUxQFsZ59A2BeZiwDBKsVlu1_zin0fFSfBFIpKZKTW9HbnqXU5P0ciQhKRfmNg'
        ];
        return array_merge($config, self::getConfig());
    }

}
