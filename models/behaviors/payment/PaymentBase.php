<?php

/**
 * Description of FileSaveBehavior
 *
 * @author postolachiserghei
 */

namespace app\models\behaviors\payment;

use app\components\extend\yii;
use app\components\extend\DynamicModel;

/**
 * @property \app\models\Transactions $transaction transaction model
 * @property \app\components\helper\LogHelper $log system log helper
 */
class PaymentBase extends \yii\base\Component
{

    public $id;
    public $api;
    public $icon;
    public $title;
    public $description;
    public $transaction;

    const ADDITIONAL_DATA_REQUEST_URL = 'additional_data[url]';

    /**
     * @param array $attributes
     * @return DynamicModel
     */
    public function getAuthModel($attributes = [])
    {
        $attrs = array_merge(['amount', 'transaction', 'quantity', 'item', 'key', 'system', 'email'], $attributes);
        $model = new DynamicModel($attrs);
        $model->addRule(['amount', 'key', 'item', 'system'], 'required');
        if (yii::$app->user->isGuest) {
            $model->addRule(['email'], 'required');
            $model->addRule(['email'], 'email');
        }
        return $model;
    }

    /**
     *
     * @return type
     */
    public function getLog()
    {
        return yii::$app->helper->log([
            'initiator' => yii::$app->l->t('{title} payment system', [
                'title' => $this->title,
                'update' => false
            ])
        ]);
    }

    /**
     *
     * @param type $transaction
     */
    public function cancelPayment($transaction)
    {
        /* @var $transaction \app\models\Transactions */
        if ($transaction->status === $transaction::STATUS_NEW) {
            yii::$app->controller->setMessage(($transaction->delete() ? 'success' : 'error'));
        } else {
            yii::$app->controller->setMessage('warning', yii::$app->l->t('transaction has begin and you cannot cancel the order. For more details please contact site administration!'));
        }
    }

    /**
     * validate account
     * @param string $account
     * @return boolean
     */
    public function validateAccount($account)
    {
        return true;
    }

    /**
     * @param \app\models\Transactions $transaction
     * @param type $details
     */
    public function triggerBasePaymentFail($transaction, $details = [])
    {
        $details['attributes'] = $transaction->attributes;
        $message = yii::$app->l->t('transaction Nr. {id} payment has failed', [
            'id' => $transaction->id
        ]);
        $data = is_array($details) ? array_merge($details, ['message' => $message]) : $message;
        $this->log->error($data);
    }

    /**
     * @param \app\models\Transactions $transaction
     * @param type $details
     */
    public function triggerBasePaymentSuccess($transaction, $details = [])
    {
        $shoppingCart = new \app\models\ShoppingCart;
        if ($transaction->item_model === $shoppingCart->className()) {
            if ($cart = $shoppingCart->findOne($transaction->item_id)) {
                $cart->setStatus($shoppingCart::STATUS_SUBMITTED);
            }
        }
        $details['attributes'] = $transaction->attributes;
        $message = yii::$app->l->t('transaction Nr. {id} payment has succeeded', [
            'id' => $transaction->id
        ]);
        $data = is_array($details) ? array_merge($details, ['message' => $message]) : $message;
        $this->log->success($data);
    }

}
