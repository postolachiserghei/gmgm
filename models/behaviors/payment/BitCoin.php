<?php

/**
 * Description of BitCoinBehavior
 *
 * @author postolachiserghei
 */

namespace app\models\behaviors\payment;

include_once('spectro_coin/constants.php');

use app\components\extend\yii;
use app\components\extend\Html;
use app\models\behaviors\PaymentBehavior;
use app\models\behaviors\payment\spectro_coin\SCMerchantClient\SCMerchantClient;
use app\models\behaviors\payment\spectro_coin\SCMerchantClient\data\OrderCallback;
use app\models\behaviors\payment\spectro_coin\SCMerchantClient\data\OrderStatusEnum;
use app\models\behaviors\payment\spectro_coin\SCMerchantClient\messages\CreateOrderRequest;

/**
 * @property Client $api Description
 */
class BitCoin extends PaymentBase
{

    const ADDITIONAL_DATA_REQUEST_INFO = 'additional_data[request_info]';

    public $merchant_id;
    public $api_id;
    public $SC_API_URL;
    public $ORDER_CALLBACK_URL;
    public $ORDER_SUCCESS_URL;
    public $ORDER_FAILURE_URL;

    public function init()
    {
        $p = parent::init();
        $this->icon = Html::ico('btc');
        $this->id = PaymentBehavior::PAYMENT_SYSTEM_BIT_COIN;
        return $p;
    }

    /**
     * config
     * @return $this
     */
    public function config()
    {
        if (!$this->merchant_id || $this->api_id) {
            $this->merchant_id = $this->transaction->getSetting('bitcoin_merchant_id');
            $this->api_id = $this->transaction->getSetting('bitcoin_api_id');
            $tld = yii::$app->helper->settings()->getSetting('tld');
            $domain = $_SERVER['REQUEST_SCHEME'] . '://' . $tld;
            $this->ORDER_CALLBACK_URL = $domain . '/payment/process';
            $this->ORDER_SUCCESS_URL = $domain . '/payment/result?transaction=' . $this->transaction->primaryKey;
            $this->ORDER_FAILURE_URL = $domain . '/payment/result?transaction=' . $this->transaction->primaryKey;
            $this->SC_API_URL = 'https://spectrocoin.com/api/merchant/1';
            $this->api = new SCMerchantClient($this->SC_API_URL, $this->merchant_id, $this->api_id);
            $this->api->setPrivateMerchantKey($this->transaction->getSetting('bitcoin_private_key'));
        }
        return $this;
    }

    public function goToPay()
    {
        $this->config();
        $transaction = $this->transaction;
        if ($oldUrl = $transaction->getData($transaction::ADDITIONAL_DATA_URL . '_' . $this->id)) {
            if (!yii::$app->request->isAjax) {
                header('location: ' . $oldUrl);
                die();
            }
            die();
        }
        $orderId = $transaction->primaryKey; // "Order005";
//        $payCurrency = 'BTC'; // Customer pay amount calculation currency
        $payCurrency = $transaction->currency; // Customer pay amount calculation currency
        $payAmount = $transaction->amount; // Customer pay amount in calculation currency
        $receiveCurrency = $transaction->currency; // Merchant receive amount calculation currency
        $receiveAmount = 1; //1; // Merchant receive amount in calculation currency
        $description = strip_tags(trim($transaction->model->description));
        $culture = yii::$app->language;

        $createOrderRequest = new CreateOrderRequest($orderId, $payCurrency, $payAmount, $receiveCurrency, $receiveAmount, $description, $culture, $this->ORDER_CALLBACK_URL, $this->ORDER_SUCCESS_URL, $this->ORDER_FAILURE_URL);
        $createOrderResponse = $this->api->createOrder($createOrderRequest);
        if ($createOrderResponse instanceof ApiError) {
            echo 'Error occurred. ' . $createOrderResponse->getCode() . ': ' . $createOrderResponse->getMessage();
        } else if ($createOrderResponse instanceof spectro_coin\SCMerchantClient\messages\CreateOrderResponse) {
            $url = $createOrderResponse->getRedirectUrl();
            /* @var $transaction \app\models\behaviors\JsonFields */
            $transaction->setData($transaction::ADDITIONAL_DATA_URL . '_' . $this->id, $url);
            //$transaction->setData(self::ADDITIONAL_DATA_REQUEST_INFO, (array) $createOrderResponse);
            $transaction->save();
            if (!yii::$app->request->isAjax) {
                header('location: ' . $url);
                die();
            }
        } else {
            $data = [
                'transaction' => $transaction->attributes,
                'response' => $createOrderResponse
            ];
            $this->log->error($data);
        }
    }

    /**
     * process payment
     */
    public function processPayment()
    {
        $this->config();
        $callback = $this->api->parseCreateOrderCallback($_REQUEST);
        if ($callback != null && $this->api->validateCreateOrderCallback($callback)) {
            switch ($callback->getStatus()) {
                case OrderStatusEnum::$Test:
                    $this->processTestCallback($callback);
                    break;
                case OrderStatusEnum::$New:
                    $this->processNewCallback($callback);
                    break;
                case OrderStatusEnum::$Pending:
                    $this->processPendingCallback($callback);
                    break;
                case OrderStatusEnum::$Expired:
                    $this->processExpiredCallback($callback);
                    break;
                case OrderStatusEnum::$Failed:
                    $this->processFailedCallback($callback);
                    break;
                case OrderStatusEnum::$Paid:
                    return $this->processPaidCallback($callback);
                    break;
                default:
                    echo 'Unknown order status: ' . $callback->getStatus();
                    break;
            }
            //$this->log->success('*ok*');
            echo '*ok*';
        } else {
            $this->log->error('Invalid callback!');
            echo 'Invalid callback!';
        }
    }

    public function processTestCallback(OrderCallback $callback)
    {
        // process
    }

    public function processNewCallback(OrderCallback $callback)
    {
        // process
    }

    public function processPendingCallback(OrderCallback $callback)
    {
        // process
    }

    public function processExpiredCallback(OrderCallback $callback)
    {
        // process
    }

    public function processFailedCallback(OrderCallback $callback)
    {
        // process
    }

    public function processPaidCallback(OrderCallback $callback)
    {
        $transaction = $this->transaction;
        $transaction->status = $transaction::STATUS_PAID;
        if ($transaction->validate()) {
            $transaction->setData('callback_request', $_REQUEST);
            if (!$transaction->save()) {
                $this->triggerBasePaymentFail($transaction);
            } else {
                $this->triggerBasePaymentSuccess($transaction);
                return '*ok*';
            }
        }
    }

    /**
     * validate account
     * @param type $account
     * @return type
     */
    public function validateAccount($account)
    {
        if (parent::validateAccount($account)) {
            return false;
        }
    }

}
