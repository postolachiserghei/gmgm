<?php

/**
 * Description of SeoBehavior
 *
 * @author postolachiserghei
 */

namespace app\models\behaviors;

use app\models\Seo;
use yii\base\Behavior;
use yii\db\BaseActiveRecord;
use app\components\extend\yii;

class SeoBehavior extends Behavior
{

    public $actionUrl;
    public $seo;
    public $url;
    public $alias;
    public $title;
    public $h1;
    public $keywords;
    public $description;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (!$this->owner || !$this->seo) {
            $this->seo = new Seo();
        }
    }

    /**
     * get action url
     * @return type
     */
    public function getActionUrl()
    {
        if (!$this->actionUrl) {
            $this->actionUrl = $this->owner->getActionUrl();
        }
        return $this->actionUrl;
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return[
            $this->owner::EVENT_AFTER_INSERT => 'afterSave',
            $this->owner::EVENT_AFTER_UPDATE => 'afterSave',
            $this->owner::EVENT_BEFORE_INSERT => 'beforeSave',
            $this->owner::EVENT_BEFORE_UPDATE => 'beforeSave',
            $this->owner::EVENT_AFTER_FIND => 'afterFind',
            $this->owner::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($event)
    {
        $post = yii::$app->request->post('Seo');
        if ($post) {
            $this->seo->attributes = $post;
            $this->actionUrl = yii::$app->helper->str()->replaceTagsWithDatatValues($this->getActionUrl(), $this->owner);
            $this->seo->url = $this->getActionUrl();
            if ($this->seo->validate()) {
                $this->seo->save();
            }
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->loadSeo();
    }

    public function loadSeo()
    {
        $this->actionUrl = yii::$app->helper->str()->replaceTagsWithDatatValues($this->getActionUrl(), $this->owner);
        $linkUrl = $this->getActionUrl();
        $seo = Seo::find()->joinWith('seoT')->where('url=:url', ['url' => $linkUrl])->one();
        $this->seo = $seo ? $seo : new Seo ();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave()
    {
        return true;
    }

    /**
     * url
     * @return type
     */
    public function getLinkUrl()
    {
        return yii::$app->helper->str()->replaceTagsWithDatatValues($this->getActionUrl(), $this->owner);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        $this->loadSeo();
        if ($this->seo) {
            $this->seo->delete();
        }
        return true;
    }

}
