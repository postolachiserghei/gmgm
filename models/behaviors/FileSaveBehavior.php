<?php

/**
 * Description of FileSaveBehavior
 *
 * @author postolachiserghei
 */

namespace app\models\behaviors;

use app\models\File;
use yii\db\BaseActiveRecord;
use app\components\extend\yii;
use app\components\widgets\uploader\UploaderWidget;

/**
 * @property \app\components\extend\Model $owner owner model
 */
class FileSaveBehavior extends \yii\base\Behavior
{

    public $fileAttributes;
    public $saveFilesBehaviorInitialized = false;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return[
            BaseActiveRecord::EVENT_INIT => 'saveSentFiles',
            BaseActiveRecord::EVENT_AFTER_UPDATE => 'attacheFiles',
            BaseActiveRecord::EVENT_AFTER_INSERT => 'attacheFiles',
            BaseActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    /**
     * @inheritdoc
     */
    public function saveSentFiles()
    {
        if ($this->owner->shortClassName == 'User') {
            $identityKey = 'identity-key-set';
            yii::$app->params[$identityKey] = (yii::$app->helper->data()->getParam($identityKey, 0) + 1);
            if (yii::$app->helper->data()->getParam($identityKey) == 1) {
                return;
            }
        }
        if (!yii::$app->request->isConsoleRequest && yii::$app->request->get('uploader-request')) {
            $this->saveFilesBehaviorInitialized = true;
            $ownerScenario = $this->owner->getScenario();
            $this->owner->setScenario($this->owner::SCENARIO_UPLOAD_FILE);
            $model = $this->owner;
            foreach ($this->fileAttributes as $attribute) {
                UploaderWidget::manage(['model' => $model, 'attribute' => $attribute]);
            }
            $this->owner->setScenario($ownerScenario);
        }
    }

    /**
     * @inheritdoc
     */
    public function attacheFiles()
    {
        foreach ($this->fileAttributes as $attribute) {
            if ($files = $this->getFiles($attribute)) {
                foreach ($files as $f) {
                    if ((int) $f->owner === 0 && (int) yii::$app->user->id > 0) {
                        $f->owner = (int) yii::$app->user->id;
                        $f->save();
                    }
                    $f->addDestination($this->owner->shortClassName);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        $this->deleteAttributeFiles();
    }

    /**
     * set deleted status for file record
     */
    public function deleteAttributeFiles()
    {
        foreach ($this->fileAttributes as $attribute) {
            $fileNames = explode(',', $this->owner->{$attribute});
            if (!(count($fileNames) > 0)) {
                continue;
            }
            foreach ($fileNames as $fileName) {
                if ($f = File::find()->where(['name' => $fileName])->one()) {
                    /* @var $f File */
                    $f->setDeleted($this->owner->shortClassName);
                }
            }
        }
    }

    /**
     * try to load file attached to current model attribute
     * @param string $attribute
     * @return File
     */
    public function getFile($attribute = null)
    {
        if ($attribute && $file = File::find()->where(['status' => File::STATUS_UPLOADED, 'name' => $this->owner->{$attribute}])->one()) {
            return $file;
        }
        return (new File());
    }

    /**
     * try to load file attached to current model attribute
     * @param string $attribute
     * @return File
     */
    public function getFiles($attribute = null)
    {
        $fileNames = $this->owner->getData($attribute);
        $q = File::find();
        $q->where(['in', 'name', explode(',', $fileNames)]);
        $q->andWhere(['status' => File::STATUS_UPLOADED]);
        if ($attribute && $file = $q->all()) {
            return $file;
        }
        return null;
    }

}
