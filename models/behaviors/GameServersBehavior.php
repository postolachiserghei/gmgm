<?php

/**
 * Description of CategoryServers
 *
 * @author postolachiserghei
 */

namespace app\models\behaviors;

use yii\base\Behavior;
use app\models\GameServers;
use app\components\extend\yii;
use app\components\extend\ArrayHelper;

/**
 * @property \app\models\Categories $owner
 */
class GameServersBehavior extends Behavior
{

    public $servers_list;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            $this->owner::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            $this->owner::EVENT_AFTER_INSERT => 'afterSave',
            $this->owner::EVENT_AFTER_UPDATE => 'afterSave',
            $this->owner::EVENT_AFTER_FIND => 'afterFind',
        ];
    }

    public function beforeValidate()
    {

    }

    /**
     * @inheritdoc
     */
    public function afterSave($event)
    {
        if ($this->owner->type != $this->owner::TYPE_GAME) {
            return true;
        }
        if (is_array($this->servers_list) && count($this->servers_list) > 0) {
            foreach ($this->servers_list as $id) {
                $model = new GameServers();
                $model->category_id = $this->owner->primaryKey;
                $model->server_id = $id;
                if (!$model->save()) {
                    $this->owner->addError('servers_list', $model->getErrors(null, true));
                }
            }
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        if ($this->owner->type != $this->owner::TYPE_GAME) {
            return true;
        }
        $q = GameServers::find();
        $q->andWhere(['category_id' => $this->owner->primaryKey]);
        $this->servers_list = ArrayHelper::map($q->all(), 'server_id', 'server_id');
    }

}
