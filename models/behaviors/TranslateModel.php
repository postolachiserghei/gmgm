<?php

namespace app\models\behaviors;

use app\models\File;
use yii\helpers\Json;
use yii\db\BaseActiveRecord;
use app\components\extend\Url;
use app\components\extend\yii;
use app\components\extend\Html;
use app\components\extend\Model;

/**
 * @property \app\components\extend\Model $t translated model
 * @property string $fk foreign key
 * @property string $l language
 * @property \app\components\extend\Model $owner
 */
class TranslateModel extends \yii\base\Behavior
{

    public $t;
    public $fk;
    public $l;
    public $filesToBeDeleted = [];

    public function init()
    {
        parent::init();
        $this->l = !yii::$app->request->isConsoleRequest ? (yii::$app->request->get('l') ? yii::$app->request->get('l') : yii::$app->language) : yii::$app->language;
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return[
            $this->owner::EVENT_AFTER_INSERT => 'afterSave',
            $this->owner::EVENT_BEFORE_VALIDATE => 'populateAttributes',
            $this->owner::EVENT_AFTER_UPDATE => 'afterSave',
            $this->owner::EVENT_AFTER_FIND => 'afterFind',
            $this->owner::EVENT_BEFORE_DELETE => 'prepareFilesForDelete',
            $this->owner::EVENT_AFTER_DELETE => 'deleteFiles',
        ];
    }

    /**
     * prepare files from all translations to be deleted
     */
    public function prepareFilesForDelete()
    {
        if ($this->owner->is_deleted !== Model::DELETED_IRREVERSIBLE) {
            return;
        }
        $translates = $this->getTranslates(true)->all();
        if ($translates && $this->owner->hasProperty('fileAttributes', true, true)) {
            foreach ($translates as $t) {
                /* @var $t FileSaveBehavior */
                foreach ($this->owner->fileAttributes as $fileAttribute) {
                    if ($files = explode(',', $t->{$fileAttribute})) {
                        $this->filesToBeDeleted = array_merge($this->filesToBeDeleted, (count($files) > 0 ? $files : []));
                    }
                }
            }
        }
    }

    /**
     * delete all files from all translations
     */
    public function deleteFiles()
    {
        if (count($this->filesToBeDeleted) === 0) {
            return;
        }
        $files = File::find()->where(['in', 'name', $this->filesToBeDeleted])->all();
        if ($files) {
            foreach ($files as $f) {
                /* @var $f Files */
                $f->delete();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave()
    {
        if ($this->owner->is_deleted == Model::DELETED_FALSE) {
            $this->saveTranslation();
        }
        return true;
    }

    /**
     * asign data to TModel from parent model
     */
    public function populateAttributes()
    {
        $this->t->setAttributes($this->owner->attributes, false);
        $this->t->language_id = $this->l;
        foreach ($this->t->attributes as $attr => $val) {
            if ($this->owner->hasProperty($attr) || $this->owner->hasAttribute($attr)) {
                $this->t->{$attr} = $this->owner->{$attr};
            }
        }
        if (!$this->t->validate()) {
            foreach ($this->t->errors as $k => $v) {
                $this->owner->addError($k, $v[0]);
            }
        }
    }

    /**
     * save TModel
     */
    public function saveTranslation()
    {
        $this->t->{$this->fk} = $this->owner->primaryKey;
        if ($this->t->validate()) {
            if ($this->t->is_deleted === null) {
                $this->t->is_deleted = Model::DELETED_FALSE;
            }
            $this->t->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->initTranslation();
    }

    /**
     *
     * @param boolean $anyTranslation
     */
    public function initTranslation($anyTranslation = false)
    {
        if ($model = $this->getTranslates($anyTranslation)->one()) {
            $this->t = $model;
            if ($this->t->isNewRecord) {
                return $model;
            }
            foreach ($this->t->attributes as $attr => $val) {
                if ($attr == 'order') {
                    continue;
                }
                if ($this->owner->hasProperty($attr)) {
                    $this->owner->{$attr} = $val;
                }
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslates($anyTranslation = false)
    {
        $condition = [$this->fk => $this->owner->primaryKey];
        if (!$anyTranslation)
            $condition['language_id'] = $this->l;
        $this->t->{$this->fk} = $this->owner->primaryKey;
        $q = $this->t->find();
        $q->where($condition);
        return $q;
    }

    /**
     * @param array $options
     * @return string
     */
    public function getTButtons($options = [])
    {
        if (!yii::$app->l->multi) {
            return '';
        }
        $tmp = '';
        Html::addCssClass($options, 'btn btn-xs ');
        $hasT = 0;
        foreach (yii::$app->l->languages as $k => $v) {
            $o = $options;
            $controller = yii::$app->controller->id;
            $action = yii::$app->controller->action->id;
            $get = ($action == 'index' || $hasT == 0) ? [] : yii::$app->request->get();
            if (!$this->owner->isNewRecord) {
                $hasT = $this->t->find()->where(['AND', [$this->fk => $this->owner->primaryKey, 'language_id' => $k]])->count();
                foreach ($this->owner->primaryKey() as $key) {
                    $get[$key] = $this->owner[$key];
                }
            }
            $hasT > 0 ? Html::addCssClass($o, 'btn-success') : Html::addCssClass($o, 'btn-warning');
            $get[0] = ($this->owner->isNewRecord) ? 'create' : (($action == 'view' && $hasT > 0) ? 'view' : 'update');
            if (!yii::$app->user->can($controller . '-' . $get[0])) {
                continue;
            }
            $get['l'] = $k;
            $url = Url::to($get + $_GET);
            if ($this->l == $k) {
                Html::addCssClass($o, 'active');
            }
            $ico = ($this->l == $k && ($action == 'create' || $action == 'update' || $action == 'view')) ? 'check' : ($hasT > 0 ? ( $action == 'view' ? 'eye' : 'pencil') : 'plus');
            $o['data'] = ['pjax' => yii::$app->controller->isPjaxAction];
            $o['title'] = $this->getButtonTtitle($ico, $v);
            $o['href'] = $url;
            $o['onclick'] = "App.redirect('$url')";
            $tmp .= Html::tag('a', strtoupper(($action == 'index' ? $k : $v)) . ' ' . Html::ico($ico), $o);
        }
        return Html::tag('div', $tmp, ['class' => 'btn-group model-t-buttons']);
    }

    /**
     *
     * @param string $ico
     * @param string $v
     * @return string
     */
    public function getButtonTtitle($ico, $v)
    {
        $title = '';
        switch ($ico) {
            case'eye':
                $title = yii::$app->l->t('view {item}', ['item' => '"' . $v . '"', 'lcf' => true, 'update' => false]);
                break;
            case'pencil':
                $title = yii::$app->l->t('update {item}', [
                    'item' => '"' . $v . '"',
                    'lcf' => true,
                    'update' => false,
                ]);
                break;
            case'plus':
                $title = yii::$app->l->t('add {item}', [
                    'item' => '"' . $v . '"',
                    'lcf' => true,
                    'update' => false,
                ]);
                break;
        }
        return $title;
    }

}
