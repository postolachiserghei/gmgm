<?php

/**
 * Description of FileSaveBehavior
 *
 * @author postolachiserghei
 */

namespace app\models\behaviors;

use app\models\Transactions;
use app\components\extend\yii;
use app\components\extend\Html;
use app\components\helper\Helper;
use app\models\behaviors\payment\PaymentBase;
use Codeception\Exception\ConfigurationException;

/**
 *
 * @property \app\components\extend\Model $owner Transaction model
 * @property \app\models\Transactions $transaction Transaction model
 * @property boolean $isPaid check if item was paid
 */
class PaymentBehavior extends \yii\base\Behavior
{

    public $transaction;
    public $itemIsPaid;

    const PAYMENT_SYSTEM_PAYPAL = 1;
    const PAYMENT_SYSTEM_BIT_COIN = 2;

    public function init()
    {
        $init = parent::init();
        if (!$this->transaction) {
            $this->transaction = new Transactions();
        }
        return $init;
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        $owner = $this->owner;
        /* @var $owner \app\components\extend\Model */
        return[
            $owner::EVENT_AFTER_FIND => 'getTransaction',
            $owner::EVENT_AFTER_INSERT => 'getTransaction',
            $owner::EVENT_AFTER_UPDATE => 'getTransaction',
        ];
    }

    public function getTransaction()
    {
        if (!$this->transaction = Transactions::find()->where(['item_id' => $this->owner->primaryKey, 'item_model' => $this->owner->className()])->one()) {
            $this->transaction = new Transactions();
        }
    }

    /**
     * get payment system labels
     * @param type $paymentSystem
     * @return type
     */
    public static function getPaymentSystemLabels($paymentSystem = null)
    {
        $ar = [
            self::PAYMENT_SYSTEM_PAYPAL => 'PayPal',
            self::PAYMENT_SYSTEM_BIT_COIN => 'BitCoin'
        ];
        return $paymentSystem !== null ? $ar[$paymentSystem] : $ar;
    }

    /**
     *
     * @param array $params
     * @param boolean $newInstance
     * @return payment\PayPal
     */
    public static function getPayPal($params = [], $newInstance = false)
    {
        if (!isset($params['title'])) {
            $params['title'] = self::getPaymentSystemLabels(self::PAYMENT_SYSTEM_PAYPAL);
        }
        return Helper::getHelperObject('\app\models\behaviors\payment\PayPal', $params, $newInstance);
    }

    /**
     *
     * @param array $params
     * @param boolean $newInstance
     * @return payment\BitCoin
     */
    public static function getBitCoin($params = [], $newInstance = false)
    {
        if (!isset($params['title'])) {
            $params['title'] = self::getPaymentSystemLabels(self::PAYMENT_SYSTEM_BIT_COIN);
        }
        return Helper::getHelperObject('\app\models\behaviors\payment\BitCoin', $params, $newInstance);
    }

    /**
     * get available payment system
     * @return array
     */
    public static function getAvailablePaymentSystems()
    {
        return [
            self::PAYMENT_SYSTEM_PAYPAL => self::getPayPal(),
            self::PAYMENT_SYSTEM_BIT_COIN => self::getBitCoin(),
        ];
    }

    /**
     *
     * @return type
     */
    public function getTitle()
    {
        $title = @$this->owner->title;
        if (!$title) {
            $title = $this->owner->shortClassName;
        }
        return $title;
    }

    /**
     * check if item is paid
     * @return type
     */
    public function getIsPaid()
    {
        if ($this->itemIsPaid !== null) {
            return $this->itemIsPaid;
        }
        if (yii::$app->request->isConsoleRequest) {
            return null;
        }
        $q = Transactions::find()->where([
            'user_id' => yii::$app->user->id,
            'item_id' => $this->owner->primaryKey,
            'item_model' => $this->owner->className()
        ]);
        $q->andWhere(['IN', 'status', [Transactions::STATUS_PAID, Transactions::STATUS_CLOSED, Transactions::STATUS_PENDING]]);
        $transaction = $q->one();
        if ($transaction) {
            $this->itemIsPaid = true;
            $this->transaction = $transaction;
            return true;
        }
        $this->itemIsPaid = false;
        return $this->itemIsPaid;
    }

    /**
     * @param string $label
     * @param array $options
     * @return type
     */
    public function getPaymentButton($label = null, $options = [])
    {
        /* TODO #PS: update payment button */
        if ($this->isPaid) {
            return yii::$app->l->t('{status}', [
                'status' => $this->transaction->getStatusLabels($this->transaction->status)
            ]);
        }
        $item = yii::$app->helper->str()->encrypt([
            'class' => $this->owner->className(),
            'primaryKey' => $this->owner->primaryKey
        ]);
        $f = Html::beginForm(['/payment/index'], 'post', ['id' => 'payment-form-item-' . $this->owner->primaryKey]);
        $f .= Html::input('hidden', 'quantity', 1);
        $f .= Html::input('hidden', 'item', $item);
        $f .= Html::submitButton(($label ? $label : yii::$app->l->t('pay')), $options);
        $f .= Html::endForm();
        return $f;
    }

    /**
     * process payment
     * @param Transactions $transaction
     */
    public function processPayment($transaction)
    {
        if ($this->isPaid) {
            return true;
        }
        /* @var $paymentSystem payment\PayPal */
        /* @var $paymentSystem payment\BitCoin */
        $paymentSystem = $this->getPaymentSystemFromTransaction($transaction);
        return $paymentSystem->processPayment();
    }

    /**
     * process payment
     * @param Transactions $transaction
     */
    public function goToPay($transaction)
    {
        if ($this->isPaid) {
            return true;
        }
        /* @var $paymentSystem payment\PayPal */
        /* @var $paymentSystem payment\BitCoin */
        $paymentSystem = $this->getPaymentSystemFromTransaction($transaction);
        return $paymentSystem->goToPay();
    }

    /**
     * process payment
     * @param Transactions $transaction
     */
    public function cancelPayment($transaction)
    {
        if ($this->isPaid) {
            return true;
        }
        /* @var $paymentSystem payment\PayPal */
        /* @var $paymentSystem payment\BitCoin */
        $paymentSystem = $this->getPaymentSystemFromTransaction($transaction);
        return $paymentSystem->cancelPayment($transaction);
    }

    /**
     *
     * @param Transactions $transaction
     * @throws ConfigurationException
     * @return PaymentBase
     */
    public function getPaymentSystemFromTransaction($transaction)
    {
        $systems = $this->getAvailablePaymentSystems();
        if (!$transaction->payment_system) {
            $transaction->payment_system = self::PAYMENT_SYSTEM_PAYPAL;
        }
        if (!array_key_exists($transaction->payment_system, $systems) || !($systems[$transaction->payment_system] instanceof PaymentBase)) {
            throw new ConfigurationException(yii::$app->l->t('wrong payment system'));
        }
        $paymentSystem = $systems[$transaction->payment_system];
        $paymentSystem->transaction = $transaction;
        return $paymentSystem;
    }

    /**
     *
     * @return type
     */
    public function getDescription()
    {
        if ($this->owner->hasAttribute('description')) {
            return $this->description;
        }
        return $this->owner->getTitle();
    }

    /**
     *
     * @return string
     */
    public function getEncodedData()
    {
        return yii::$app->helper->str()->encrypt([
            'model' => $this->owner->className(),
            'attributes' => $this->owner->attributes
        ]);
    }

}
