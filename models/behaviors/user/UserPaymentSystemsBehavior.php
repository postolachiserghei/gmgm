<?php

/**
 * Description of SearchBehavior
 *
 * @author postolachiserghei
 */

namespace app\models\behaviors\user;

use app\components\extend\yii;

/**
 * @property \app\models\User $owner User model
 * @property \app\models\Transactions $transactions Transactions model
 */
class UserPaymentSystemsBehavior extends \yii\base\Behavior
{

    const SCENARION_VALIDATE_UPS = 'user_payment_system';

    public $ups_attr_account = 'account';
    public $ups_attr_valid = 'valid';
    public $transaction;
    public $tmp = [];

    /**
     * @inheritdoc
     */
    public function events()
    {
        $user = $this->owner;
        return[
            $user::EVENT_AFTER_UPDATE => 'validatePaymentSystemAccount',
            $user::EVENT_AFTER_INSERT => 'validatePaymentSystemAccount',
        ];
    }

    /**
     * get available payment system
     * @return \app\models\behaviors\payment\PaymentBase[]
     */
    public function getPaymentSystems()
    {
        return $this->transactions->payment->getAvailablePaymentSystems();
    }

    /**
     * validate user payment accounts
     * @return boolean
     */
    public function validatePaymentSystemAccount()
    {
        $user = $this->owner;
        if ($user->getScenario() != self::SCENARION_VALIDATE_UPS) {
            foreach ($this->getPaymentSystems() as $id => $object) {
                $object->transaction = $this->transactions;
                $isValid = $this->getPSAttribuetValue($this->ups_attr_valid, $id);
                $account = $this->getPaymentSystemAccount($id);
                $oldAccount = $this->getPaymentSystemAccount($id, true);
                if ($account || ($account != $oldAccount) || !$isValid) {
                    $isValidated = $object->validateAccount($account);
                    $this->setPSAttribuetValue($isValidated, $this->ups_attr_valid, $id);
                    $user->scenario = self::SCENARION_VALIDATE_UPS;
                    $user->save();
                }
            }
        }

        return true;
    }

    /**
     * get payment system attribute
     * @param string $attribute
     * @param integer $paymentSystemId
     * @return mixed
     */
    public function getPSAttribuet($attribute, $paymentSystemId)
    {
        $user = $this->owner;
        return $user::ADDTIONAL_DATA_USER_PAYMENT_SYSTEM . "[$paymentSystemId-$attribute]";
    }

    /**
     * get payment system attribute value
     * @param string $attribute
     * @param integer $paymentSystemId
     * @param boolean $oldAttribute
     * @return mixed
     */
    public function getPSAttribuetValue($attribute, $paymentSystemId, $oldAttribute = false)
    {
        return $this->owner->getData($this->getPSAttribuet($attribute, $paymentSystemId), null, $oldAttribute);
    }

    /**
     * set payment system attribute value
     * @param mixed $value
     * @param string $attribute
     * @param integer $paymentSystemId
     * @return mixed
     */
    public function setPSAttribuetValue($value, $attribute, $paymentSystemId)
    {
        return $this->owner->setData($this->getPSAttribuet($attribute, $paymentSystemId), $value);
    }

    /**
     *
     * @param integer $paymentSystem
     * @param boolean $oldAttribute
     * @return string
     */
    public function getPaymentSystemAccount($paymentSystem, $oldAttribute = false)
    {
        return $this->getPSAttribuetValue($this->ups_attr_account, $paymentSystem, $oldAttribute);
    }

    /**
     * get users payment system data
     * @return array|null
     */
    public function getPaymentSystemData()
    {
        $user = $this->owner;
        return $user->getData($user::ADDTIONAL_DATA_USER_PAYMENT_SYSTEM);
    }

    /**
     *
     * @return \app\models\Transactions
     */
    public function getTransactions()
    {
        if ($this->transaction) {
            return $this->transaction;
        }
        $this->transaction = new \app\models\Transactions;
        return $this->transaction;
    }

}
