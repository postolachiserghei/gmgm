<?php

namespace app\models;

use app\components\extend\yii;

/**
 * This is the model class for table "{{%chat_members}}".
 *
 * @property integer $id
 * @property integer $chat_id
 * @property string $user_id
 * @property string $name
 * @property string $additional_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order
 * @property integer $is_deleted
 *
 * @property Chat $chat
 * @property ChatMessages[] $messages
 */
class ChatMembers extends \app\components\extend\Model
{

    const TYPE_BOT = 'system-bot';
    const TYPE_ADMIN = 'system-admin';
    const BOT_MESSAGE_TYPE_GREETENG = 1;
    const ADMIN_MESSAGE_TYPE_GREETENG = 2;

    /**
     * @param integer $type
     * @param boolean $withLiveEdit (return translated labels wrapped in html tag if TRUE)
     * @return array/string
     */
    public function getAdminMessage($type = false, $withLiveEdit = true)
    {
        $ar = [
            self::ADMIN_MESSAGE_TYPE_GREETENG => yii::$app->l->t('-----------------------------', [
                'update' => $withLiveEdit,
                'name' => yii::$app->helper->user()->identity()->fullName
            ]),
        ];
        return $type === false ? $ar : $ar[$type];
    }

    /**
     * @param integer $type
     * @param boolean $withLiveEdit (return translated labels wrapped in html tag if TRUE)
     * @return array/string
     */
    public function getBotMessage($type = false, $withLiveEdit = true)
    {
        $ar = [
            self::BOT_MESSAGE_TYPE_GREETENG => yii::$app->l->t('hello my name is {name}, i\'am a system bot.', [
                'update' => $withLiveEdit,
                'name' => yii::$app->name
            ]),
        ];
        return $type === false ? $ar : $ar[$type];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%chat_members}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['user_id', 'name'], 'required'],
            [['chat_id', 'created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['chat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Chat::className(), 'targetAttribute' => ['chat_id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'id' => yii::$app->l->t('id'),
            'chat_id' => yii::$app->l->t('chat'),
            'user_id' => yii::$app->l->t('user'),
            'name' => yii::$app->l->t('name'),
            'additional_data' => yii::$app->l->t('additional data'),
            'created_at' => yii::$app->l->t('Date created'),
            'updated_at' => yii::$app->l->t('Date updated'),
            'order' => yii::$app->l->t('Order position'),
            'is_deleted' => yii::$app->l->t('Is deleted'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRelation()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return User
     */
    public function getUser()
    {
        $user = $this->userRelation;
        if (!$user) {
            $user = new User();
            $user->id = $this->user_id;
            $user->username = $this->name;
        }
        return $user;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['id' => 'chat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(ChatMessages::className(), ['sender_id' => 'id']);
    }

    /**
     * get member name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

}
