<?php

namespace app\models;

use app\components\extend\yii;
use app\components\extend\Url;
use app\components\extend\Model;
use app\models\behaviors\PaymentBehavior;
use Codeception\Exception\ConfigurationException;

/**
 * This is the model class for table "{{%transactions}}".
 *
 * @property integer $id
 * @property integer $payment_system
 * @property integer $user_id
 * @property string $item_id
 * @property string $item_model
 * @property integer $status
 * @property string $additional_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order
 * @property integer $is_deleted
 * @property integer $quantity quantity
 * @property string $currency
 * @property string $amount
 * @property \app\components\helper\LogHelper $log
 *
 * @property User $user
 * @property behaviors\PaymentBehavior $payment
 */
class Transactions extends \app\components\extend\Model
{

    const ADDITIONAL_DATA_URL = 'additional_data[url]';
    const ADDITIONAL_DATA_CART_INFO = 'additional_data[shopping_cart_info]';
    const ADDITIONAL_DATA_HISTORY = 'additional_data[history]';
    const ADDITIONAL_DATA_REQUESTED_ITEM = 'additional_data[requested_item]';

    /**
     * currencies
     */
    const CURRENCY_EUR = 'EUR';
    const CURRENCY_USD = 'USD';
    const CURRENCY_RUB = 'RUB';

    /**
     * statuses
     */
    const STATUS_NEW = 1;
    const STATUS_PENDING = 2;
    const STATUS_PAID = 3;
    const STATUS_CLOSED = 4;

    /**
     * models
     */
    const ITEM_MODEL_PRODUCTS = 'Products';

    /**
     * get currencies labels
     * @param string|null $currency
     * @return array|string
     */
    public function getCurrenciesLabels($currency = null, $details = false)
    {
        $ar = [
            self::CURRENCY_EUR => '€' . ($details ? ' (' . self::CURRENCY_EUR . ')' : ''),
            self::CURRENCY_USD => '$' . ($details ? ' (' . self::CURRENCY_USD . ')' : ''),
            self::CURRENCY_RUB => 'р.' . ($details ? ' (' . self::CURRENCY_RUB . ')' : ''),
        ];
        return $currency ? $ar[$currency] : $ar;
    }

    /**
     * get status labels
     * @param string|null $status
     * @return array|string
     */
    public function getStatusLabels($status = null)
    {
        $ar = [
            self::STATUS_NEW => yii::$app->l->t('new'),
            self::STATUS_PENDING => yii::$app->l->t('pending'),
            self::STATUS_PAID => yii::$app->l->t('paid'),
            self::STATUS_CLOSED => yii::$app->l->t('closed'),
        ];
        return $status ? $ar[$status] : $ar;
    }

    /**
     *
     * @return PaymentBehavior
     */
    public function getPayment()
    {
        return new PaymentBehavior;
    }

    /**
     * get models that can be buyed
     * @param type $item
     * @return array|string
     */
    public function getItemModelClasses($item)
    {
        $ar = [
            self::ITEM_MODEL_PRODUCTS => Products::className(),
        ];
        return $item ? $ar[$item] : $ar;
    }

    /**
     * get labesl of models that can be buyed
     * @param type $item
     * @return array|string
     */
    public function getItemModelClassLabels($item)
    {
        $ar = [
            self::ITEM_MODEL_PRODUCTS => yii::$app->l->t('Product'),
        ];
        return $item ? $ar[$item] : $ar;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transactions}}';
    }

    /**
     * transactions logs
     * @return type
     */
    public static function getLog()
    {
        return yii::$app->helper->log([
            'initiator' => yii::$app->l->t('transactions')
        ])->setDetails(true);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'settings' => [
                'class' => settings\TransactionSettings::className(),
            ],
        ] + parent::behaviors();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['payment_system', 'status', 'created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
            [['item_id', 'item_model'], 'required'],
            [['user_id', 'item_id'], 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'id' => yii::$app->l->t('id'),
            'payment_system' => yii::$app->l->t('payment system'),
            'user_id' => yii::$app->l->t('buyer'),
            'item_id' => yii::$app->l->t('item id'),
            'item_model' => yii::$app->l->t('item model'),
            'status' => yii::$app->l->t('status'),
            'additional_data' => yii::$app->l->t('additional data'),
            'created_at' => yii::$app->l->t('Date created'),
            'updated_at' => yii::$app->l->t('Date updated'),
            'order' => yii::$app->l->t('Order position'),
            'is_deleted' => yii::$app->l->t('Is deleted'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * add new transaction
     * @param array $data
     * @return Transactions
     */
    public static function add($data)
    {
        $transaction = new Transactions();
        $params = self::getItemFromData($data);
        $amount = @$params['amount'];
        $class = @$params['class'];
        $paymentSystem = @$params['system'];
        $email = @$params['email'];
        $quantity = @$params['quantity'] ? @$params['quantity'] : 1;
        $userId = @$params['userId'];
        $primaryKey = @$params['primaryKey'];
        $cartInfo = @$params['shopping_cart_info'];
        if (!$class || !class_exists($class) || !$primaryKey) {
            //self::getLog()->error(yii::$app->l->t('wrong item class or primary key'));
            return $transaction;
        }
        $model = new $class();
        if (!($model instanceof Model)) {
            self::getLog()->error(yii::$app->l->t('wrong item instance'));
            return $transaction;
        }
        $model = $class::findOne($params['primaryKey']);
        if (!$model) {
            self::getLog()->error(yii::$app->l->t('item not found'));
            return $transaction;
        }
        if ($existingTransaction = $transaction::find()->where(['amount' => $amount, 'user_id' => yii::$app->user->id, 'item_id' => $primaryKey, 'item_model' => $class])->one()) {
            $transaction = $existingTransaction;
        } else {
            $transaction->currency = $transaction->getSetting('systemCurrency', self::CURRENCY_USD);
            $transaction->amount = $amount;
        }
        $transaction->quantity = $quantity;
        $transaction->item_model = $class;
        $transaction->item_id = $primaryKey;
        $transaction->user_id = self::checkUser(($userId ? $userId : yii::$app->user->id), $email);
        $transaction->status = !$transaction->status ? $transaction::STATUS_NEW : $transaction->status;
        $transaction->payment_system = $paymentSystem;
        if ($params) {
            $transaction->setData($transaction::ADDITIONAL_DATA_REQUESTED_ITEM, yii::$app->helper->str()->encrypt($params));
            $transaction->setData($transaction::ADDITIONAL_DATA_CART_INFO, $cartInfo);

        }
        if ($transaction->validate() && $transaction->save()) {
            return $transaction;
        }
        return $transaction;
    }

    /**
     *
     * @param type $deleteState
     * @return \app\components\extend\ActiveQuery
     */
    public static function findMy($deleteState = self::DELETED_FALSE)
    {
        $query = parent::find($deleteState);
        $query->where(['user_id' => yii::$app->user->id]);
        return $query;
    }

    /**
     *
     * @param array $data
     * @return string
     * @throws ConfigurationException
     */
    public static function getItemFromData($data)
    {
        $item = @$data['item'];
        if (!$item) {
            return null;
        }
        $item = yii::$app->helper->str()->decrypt($item);
        $item['quantity'] = @$data['quantity'];
        $item['amount'] = @$data['amount'];
        $item['system'] = @$data['system'];
        $item['email'] = @$data['email'];
        $item['shopping_cart_info'] = @$data['shopping_cart_info'];
        $item['userId'] = yii::$app->user->id;
        return $item;
    }

    /**
     *
     * @param type $userId
     * @param type $email
     * @return type
     */
    public static function checkUser($userId, $email)
    {
        if (!yii::$app->user->isGuest) {
            return $userId;
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $userId;
        }
        yii::$app->user->setReturnUrl(Url::to(['/payment/index']));
        if ($user = User::findByEmail($email)) {
            yii::$app->getResponse()->redirect(yii::$app->getUser()->loginUrl);
            return yii::$app->end();
        } else {
            $signUp = new forms\SignupForm();
            $signUp->username = $email;
            $signUp->email = $email;
            $signUp->password = uniqid();
            $signUp->passwordRepeat = $signUp->password;
            if (!$user = $signUp->signup()) {
                yii::$app->helper->log()->error(['cannot create user for payment system', 'details' => $signUp->errors]);
                return false;
            }
        }
        return $user->primarykey;
    }

    /**
     * @param boolean $asString
     * @param boolean $formated
     * @return type
     */
    public function getAmount($asString = true, $formated = true)
    {
        $model = $this->model;
        //$model->price = $this->amount;
        /* @var $model Products */
        return $model->getPrice($asString, $formated);
    }

    /**
     * get item model
     * @return \app\components\extend\Model
     * @return PaymentBehavior
     * @throws ConfigurationException
     */
    public function getModel()
    {
        $primaryKey = $this->item_id;
        $class = $this->item_model;
        if (!$class || !class_exists($class) || !$primaryKey) {
            throw new ConfigurationException(yii::$app->l->t('wrong item class or primary key'));
        }
        $model = new $class();
        /* @var $model \app\components\extend\Model */
        if (!($model instanceof Model)) {
            throw new ConfigurationException(yii::$app->l->t('wrong model instance'));
        }
        return $model::findOne($primaryKey);
    }

    /**
     * get transaction title
     * @return type
     */
    public function getTitle()
    {
        return $this->model->title;
    }

    /**
     * go to pay
     */
    public function goToPay()
    {
        if ($this->payment_system && $model = $this->model) {
            /* @var $model PaymentBehavior */
            $model->goToPay($this);
        }
    }

    /**
     * process payment
     */
    public function processPayment()
    {
        if ($this->payment_system && $model = $this->model) {
            /* @var $model PaymentBehavior */
            return $model->processPayment($this);
        }
    }

    /**
     * process payment
     */
    public function cancelPayment()
    {
        if ($model = $this->model) {
            /* @var $model PaymentBehavior */
            $model->cancelPayment($this);
        }
    }

    /**
     * render transactions model image
     * @param type $params
     */
    public function renderImage($params = [])
    {
        if ($model = $this->model) {
            return method_exists($model, 'renderImage') ? $model->renderImage($params) : '';
        }
    }

}
