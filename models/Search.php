<?php

namespace app\models;

use app\components\extend\yii;
use app\models\behaviors\JsonFields;

/**
 * This is the model class for table "{{%search}}".
 *
 * @property string $id
 * @property string $link
 * @property string $params
 * @property string $model
 * @property string $language_id
 */
class Search extends \app\components\extend\Model
{

    const MODEL_CLASS = 'params.className';
    const MODEL_PK = 'params.primaryKey';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%search}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['link', 'params', 'model', 'language_id'], 'required'],
            [['model'], 'string', 'max' => 200],
            [['language_id'], 'string', 'max' => 2]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'link' => yii::$app->l->t('mink'),
            'model' => yii::$app->l->t('model'),
            'language_id' => yii::$app->l->t('language'),
        ]);
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValues()
    {
        return $this->hasMany(SearchValues::className(), ['search_id' => 'id']);
    }

    /**
     * get searched model instance
     * @return \app\components\extend\Model
     */
    public function getSearchedModel()
    {
        $sModel = $this->getData(Search::MODEL_CLASS);
        $sModelPk = $this->getData(self::MODEL_PK);
        if (!is_object($sModel) || !is_string($sModel) || $sModelPk) {
            return null;
        }
        /* @var $sModel \app\components\extend\Model */
        $model = (new $sModel)->findOne($sModelPk);
        if ($model) {
            return $model;
        }
        return null;
    }

}
