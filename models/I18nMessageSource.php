<?php

namespace app\models;

use app\components\extend\yii;
use app\components\extend\ArrayHelper;

/**
 * This is the model class for table "{{%i18n_message_source}}".
 *
 * @property integer $id
 * @property string $message
 * @property string $category
 * @property integer $is_new
 *
 * @property I18nMessage[] $i18nMessages
 */
class I18nMessageSource extends \app\components\extend\Model
{

    public $language;
    public $CDeleteAllTrigger = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%i18n_message_source}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['category', 'language'], 'string', 'max' => 32]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasMany(I18nMessage::className(), ['id' => 'id']);
    }

    /**
     * array
     * @return type
     */
    public function getCategories()
    {
        return ArrayHelper::map(self::find()->groupBy('category')->all(), 'category', 'translateCategory');
    }

    public function getTranslateCategory()
    {
        return yii::$app->l->t($this->category, ['update' => 'false']);
    }

    public function getTranslationsForEachLanguage()
    {
        $tmp = '';
        yii::$app->l->liveEditT = true;
        if (yii::$app->l->multi) {
            foreach (yii::$app->l->languages as $l => $name) {
                $tmp .= ($tmp == '' ? '' : '  ,  ') . $name . ' - ' . yii::$app->l->t($this->message, ['category' => $this->category], $l);
            }
        } else {
            $tmp .= yii::$app->l->t($this->message, ['category' => $this->category], yii::$app->language);
        }
        yii::$app->l->liveEditT = false;
        return $tmp;
    }

}
