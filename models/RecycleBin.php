<?php

namespace app\models;

use app\components\extend\yii;
use app\components\extend\Model;

/**
 * This is the model class for table "{{%recycle_bin}}".
 *
 * @property string $model
 * @property string $model_pk
 */
class RecycleBin extends Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%recycle_bin}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model', 'model_pk'], 'required'],
            [['model', 'model_pk'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model' => yii::$app->l->t('Related model'),
            'model_pk' => yii::$app->l->t('Related model primary key'),
        ];
    }

    /**
     * empty recycle bin (clear all deleted records)
     */
    public static function emptyRecycleBin()
    {
        $trash = self::find()->where('created_at < :t', ['t' => date('U', strtotime('-1 minutes'))])->all();
        if ($trash) {
            $deleted = false;
            foreach ($trash as $item) {
                $modelClass = unserialize($item->model);
                $item->model = class_exists($modelClass) ? (new $modelClass) : null;
                if ($item->model instanceof Model) {
                    if ($m = $item->model->findOne(unserialize($item->model_pk), Model::DELETED_TRUE)) {
                        /* @var $m Model */
                        /* @var $m behaviors\JsonFields */
                        $m->is_deleted = Model::DELETED_IRREVERSIBLE;
                        $deleted = $m->delete();
                    }
                }
            }
        }
        return null;
    }

}
