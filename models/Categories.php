<?php

namespace app\models;

use app\models\LotTypes;
use app\components\extend\yii;
use app\models\base\CategoriesBase;
use app\models\behaviors\SeoBehavior;
use app\components\extend\ArrayHelper;
use app\models\settings\CategoriesSettings;
use app\models\behaviors\GameServersBehavior;

class Categories extends CategoriesBase
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $b = parent::behaviors();
        $b['seo'] = [
            'class' => SeoBehavior::className(),
        ];
        $b['settings'] = [
            'class' => CategoriesSettings::className(),
        ];
        $b['servers'] = [
            'class' => GameServersBehavior::className(),
        ];
        return $b;
    }

    /**
     * view action url
     * @return string
     */
    public function getActionUrl()
    {
        if ($this->type == self::TYPE_GAME) {
            return '/game/view?id={id}';
        }
        return '/genre/view?id={id}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['type', 'required'],
            ['servers_list', 'safe'],
            ['parent', 'checkGameGenre']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function checkGameGenre($attribute)
    {
        if ($this->type == self::TYPE_GAME) {
            if ($this->parent <= 0) {
                $this->addError($attribute, yii::$app->l->t('please select genre'));
            }
        }
    }

    /**
     * constant attributes
     */
    const ADDITIONAL_DATA_LOT_TYPES = 'additional_data[lot_types]';
    const ADDITIONAL_DATA_LOT_LIMIT = 'additional_data[lot_limits]';
    const ADDITIONAL_DATA_SHOW_IN_TOP = 'additional_data[in_top]';
    const ADDITIONAL_DATA_SERVERS = 'additional_data[servers]';

    /**
     * constant types
     */
    const TYPE_GAME = 2;

    /**
     * @inheritdoc
     */
    public function getTypeLabels($type = false, $withLiveEdit = true)
    {
        $ar = parent::getTypeLabels(false, $withLiveEdit);
        $ar[self::TYPE_MAIN] = yii::$app->l->t('genres', ['update' => $withLiveEdit]);
        $ar[self::TYPE_GAME] = yii::$app->l->t('games', ['update' => $withLiveEdit]);
        return $type !== false ? $ar[$type] : $ar;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $ar = [];
        if ($this->type == self::TYPE_GAME) {
            $ar[self::ADDITIONAL_DATA_SHOW_IN_TOP] = yii::$app->l->t('show in top');
            $ar[self::ADDITIONAL_DATA_SERVERS] = yii::$app->l->t('servers');
            $ar[self::ADDITIONAL_DATA_LOT_TYPES] = yii::$app->l->t('lot types');
            $ar[self::ADDITIONAL_DATA_LOT_LIMIT] = yii::$app->l->t('number of allowed lots for this game');
        }
        return array_merge(parent::attributeLabels(), $ar);
    }

    /**
     * get game servers
     * @return type
     */
    public function getGameServers()
    {
        return $this->getData(self::ADDITIONAL_DATA_SERVERS);
    }

    /**
     * get active lot types
     * @return LotTypes[]
     */
    public function getActiveLottypes($map = false)
    {
        $lotTypes = LotTypes::find()->where(['status' => LotTypes::STATUS_ACTIVE])->all();
        return $map ? ArrayHelper::map($lotTypes, 'id', 'title') : $lotTypes;
    }

    /**
     *
     * @param type $string
     * @return string
     */
    public function getLotTypesList($string = false)
    {
        $ids = [];
        $str = '';
        $types = $this->getData(self::ADDITIONAL_DATA_LOT_TYPES);
        $typesLimit = $this->getData(self::ADDITIONAL_DATA_LOT_LIMIT);
        if (!$types) {
            return $string ? yii::$app->l->t('no results') : null;
        }
        foreach ($types as $id => $title) {
            $ids[$id] = $id;
            $str .= ($str != '' ? ', ' : '') . $title . ' (' . $typesLimit[$id] . ')';
        }
        if ($string) {
            return $str;
        }
        return LotTypes::find()->where(['in', 'id', $ids])->all();
    }

    public function getAvailableServers($ids = [])
    {
        $q = Servers::find();
        $q->where(['category_id' => $this->primaryKey]);
        if (count($ids) > 0) {
            $q->andWhere(['in', 'id', $ids]);
        }
        return ArrayHelper::map($q->all(), 'id', 'title');
    }

    public function getProducts()
    {
        $q = Products::find();
        $q->where(['category_id' => $this->primaryKey]);
        $q->andWhere(['status' => Products::STATUS_ACTIVE]);
        return new \app\components\extend\ActiveDataProvider([
            'query' => $q
        ]);
    }

    public function getCountProducts()
    {
        $q = Products::find();
        $q->where(['category_id' => $this->primaryKey]);
        $q->andWhere(['status' => Products::STATUS_ACTIVE]);
        return $q->count();
    }

}
