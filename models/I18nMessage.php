<?php

namespace app\models;

use app\components\extend\yii;

/**
 * This is the model class for table "{{%i18n_message}}".
 *
 * @property integer $id
 * @property string $language
 * @property string $translation
 *
 * @property I18nMessageSource $id0
 */
class I18nMessage extends \app\components\extend\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%i18n_message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id', 'is_new'], 'integer'],
            [['translation'], 'string'],
            [['language'], 'string', 'max' => 16]
        ];
    }

    public function attributeLabels()
    {
        return [];
    }

    public function attributeHints()
    {
        return [];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslation()
    {
        return $this->hasOne(I18nMessageSource::className(), ['id' => 'id']);
    }

}
