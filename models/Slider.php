<?php

namespace app\models;

use app\models\File;
use app\models\t\SliderT;
use app\components\extend\yii;
use app\models\behaviors\FileSaveBehavior;

/**
 * This is the model class for table "{{%slider}}".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $status
 * @property string $additional_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order
 * @property integer $is_deleted
 *
 * @property SliderT[] $sliderTs
 * @property SliderT $t
 */
class Slider extends \app\components\extend\Model
{

    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 2;
    const TYPE_MAIN = 1;
    const TYPE_ADS = 2;
    const TYPE_STORE = 3;

    /**
     * additional data
     */
    const ADDITIONAL_DATA_PRICE = 'additional_data[price]';
    const ADDITIONAL_DATA_CONTENT = 'additional_data[content]';
    const ADDITIONAL_DATA_LINK = 'additional_data[link]';
    const ADDITIONAL_DATA_IMAGE = 'additional_data[image]';

    public $title;

    /**
     * @param integer $type
     * @param boolean $withLiveEdit (return translated labels wrapped in html tag if TRUE)
     * @return array/string
     */
    public static function getTypeLabels($type = false, $withLiveEdit = true)
    {
        $ar = [
            static::TYPE_MAIN => yii::$app->l->t('main slider', ['update' => $withLiveEdit]),
            static::TYPE_ADS => yii::$app->l->t('ads slider', ['update' => $withLiveEdit]),
            static::TYPE_STORE => yii::$app->l->t('store slider', ['update' => $withLiveEdit]),
        ];

        return $type === false ? $ar : $ar[$type];
    }

    /**
     * @param integer/boolean $status
     * @param boolean $withLiveEdit (return translated labels wrapped in html tag if TRUE)
     * @return type
     */
    public function getStatusLabels($status = false, $withLiveEdit = true)
    {
        $ar = [
            self::STATUS_ACTIVE => yii::$app->l->t('active', ['update' => $withLiveEdit]),
            self::STATUS_DISABLED => yii::$app->l->t('disabled', ['update' => $withLiveEdit]),
        ];
        return $status !== false ? $ar[$status] : $ar;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%slider}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return parent::behaviors() + [
            'settings' => [
                'class' => settings\SliderSettings::className(),
            ],
            't' => [
                'class' => behaviors\TranslateModel::className(),
                't' => new SliderT(),
                'fk' => 'id',
            ],
            'fileSaveBehavior' => [
                'class' => FileSaveBehavior::className(),
                'fileAttributes' => [self::ADDITIONAL_DATA_IMAGE],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [['title'], 'required'],
            [[self::ADDITIONAL_DATA_IMAGE], 'file', 'maxFiles' => 1, 'extensions' => File::imageExtensions(true), 'skipOnEmpty' => true, 'on' => self::SCENARIO_UPLOAD_FILE],
            [[self::ADDITIONAL_DATA_LINK, self::ADDITIONAL_DATA_CONTENT, self::ADDITIONAL_DATA_PRICE], 'safe'],
            [[self::ADDITIONAL_DATA_LINK], 'url'],
            [[self::ADDITIONAL_DATA_PRICE], 'integer'],
            [['type', 'status'], 'required'],
            [['type'], 'default', 'value' => self::TYPE_MAIN],
            [['status'], 'default', 'value' => self::STATUS_ACTIVE],
            [['type', 'status', 'created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
            [['additional_data', 'title'], 'safe'],
        ];
        return array_merge(parent::rules(), $rules);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = array_merge(parent::attributeLabels(), [
            'id' => yii::$app->l->t('id'),
            'type' => yii::$app->l->t('type'),
            'status' => yii::$app->l->t('status'),
            'created_at' => yii::$app->l->t('Date created'),
            'updated_at' => yii::$app->l->t('Date updated'),
            'order' => yii::$app->l->t('Order position'),
            'is_deleted' => yii::$app->l->t('Is deleted'),
            self::ADDITIONAL_DATA_IMAGE => yii::$app->l->t('image'),
            self::ADDITIONAL_DATA_LINK => yii::$app->l->t('link'),
            self::ADDITIONAL_DATA_CONTENT => yii::$app->l->t('content'),
            self::ADDITIONAL_DATA_PRICE => yii::$app->l->t('price'),
        ]);
        return array_merge($this->t->attributeLabels(), $labels);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSliderTs()
    {
        return $this->hasMany(SliderT::className(), ['id' => 'id']);
    }

    public function renderImage($options = [])
    {
        return $this->getFile(self::ADDITIONAL_DATA_IMAGE)->renderImage($options);
    }

}
