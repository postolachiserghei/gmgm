<?php

namespace app\models;

use app\components\extend\yii;

/**
 * This is the model class for table "{{%chat_contacts}}".
 *
 * @property integer $user_id
 * @property integer $contact_id
 * @property integer $status
 * @property string $additional_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order
 * @property integer $is_deleted
 *
 * @property User $contact
 * @property User $user
 */
class ChatContacts extends \app\components\extend\Model
{

    const STATUS_REQUESTED = 1;


    /**
     * @param integer/boolean $status
     * @param boolean $withLiveEdit (return translated labels wrapped in html tag if TRUE)
     * @return type
     */
    public function getStatusLabels($status = false, $withLiveEdit = true)
    {
        $ar = [
            self::STATUS_ACTIVE => yii::$app->l->t('user active', ['update' => $withLiveEdit]),
            self::STATUS_DISABLED => yii::$app->l->t('user disabled', ['update' => $withLiveEdit]),
            self::STATUS_DELETED => yii::$app->l->t('user deleted', ['update' => $withLiveEdit]),
        ];
        return $status !== false ? $ar[$status] : $ar;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%chat_contacts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['user_id', 'contact_id'], 'required'],
            [['user_id', 'contact_id', 'status', 'created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
            [['additional_data'], 'string'],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['contact_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'user_id' => yii::$app->l->t('user_id'),
            'contact_id' => yii::$app->l->t('contact user id'),
            'status' => yii::$app->l->t('status'),
            'additional_data' => yii::$app->l->t('additional data'),
            'created_at' => yii::$app->l->t('Date created'),
            'updated_at' => yii::$app->l->t('Date updated'),
            'order' => yii::$app->l->t('Order position'),
            'is_deleted' => yii::$app->l->t('Is deleted'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(User::className(), ['id' => 'contact_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}
