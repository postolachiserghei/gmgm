<?php

namespace app\models;

use yii\db\Query;
use app\components\extend\yii;
use app\components\extend\Migration;

/**
 * This is the model class for table "{{%updates}}".
 *
 * @property string $update
 * @property behaviors\JsonFields $this
 * @property array $tables
 * @property array $updates
 */
class Updates extends \app\components\extend\Model
{

    const FILE_EXT = 'txt';

    public $availableTables;
    public $rewrite;

    public function init()
    {
        $init = parent::init();
        $this->availableTables = $this->getTables();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%updates}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['update'], 'required'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'update' => yii::$app->l->t('update id'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['update'];
    }

    /**
     * filter from available tables only those that are listed in given array
     * @param array $tables ['{{%user}}','{{%languages}}', ...]
     * @return array ['{{%table_name}}'=>['label'=>'table label','model'=> (Model | null)]]
     */
    public function selectThisTables($tables)
    {
        $data = [];
        foreach ($this->availableTables as $table => $info) {
            if (in_array($table, $tables)) {
                $data[$table] = $info;
            }
        }
        return $data;
    }

    /**
     * get list of tables available for update
     * @return array
     */
    public function getTables()
    {
        if ($this->availableTables) {
            return $this->availableTables;
        }
        $tables = [
            User::tableName() => [
                'label' => 'users',
                'pk' => User::primaryKey(),
                'model' => User::className()
            ],
            '{{%auth_rule}}' => [
                'label' => 'rules (RBAC)',
                'pk' => 'name',
                'model' => null
            ],
            '{{%auth_item}}' => [
                'label' => 'role names (RBAC)',
                'pk' => 'name',
                'model' => null
            ],
            '{{%auth_assignment}}' => [
                'label' => 'rules (RBAC)',
                'pk' => ['item_name', 'user_id'],
                'model' => null
            ],
            '{{%auth_item_child}}' => [
                'pk' => ['parent', 'child'],
                'label' => 'permissions (RBAC)',
                'model' => null
            ],
            Languages::tableName() => [
                'label' => 'languages',
                'pk' => Languages::primaryKey(),
                'model' => Languages::className()
            ],
            File::tableName() => [
                'label' => 'file table',
                'pk' => File::primaryKey(),
                'model' => File::className()
            ],
            FileDestination::tableName() => [
                'label' => 'file destinations',
                'pk' => FileDestination::primaryKey(),
                'model' => FileDestination::className()
            ],
            I18nMessageSource::tableName() => [
                'label' => 'translation source messages',
                'pk' => I18nMessageSource::primaryKey(),
                'model' => I18nMessageSource::className()
            ],
            I18nMessage::tableName() => [
                'label' => 'translated messages',
                'pk' => I18nMessage::primaryKey(),
                'model' => I18nMessage::className()
            ],
            Menu::tableName() => [
                'label' => 'menu ',
                'pk' => Menu::primaryKey(),
                'model' => Menu::className(),
            ],
            t\MenuT::tableName() => [
                'label' => 'menu translations',
                'pk' => t\MenuT::primaryKey(),
                'model' => t\MenuT::className()
            ],
            Pages::tableName() => [
                'label' => 'pages',
                'pk' => Pages::primaryKey(),
                'model' => Pages::className(),
            ],
            t\PagesT::tableName() => [
                'label' => 'page translations',
                'pk' => t\PagesT::primaryKey(),
                'model' => t\PagesT::className()
            ],
            Search::tableName() => [
                'label' => 'search',
                'pk' => Search::primaryKey(),
                'model' => Search::className(),
            ],
            SearchValues::tableName() => [
                'label' => 'search values',
                'pk' => SearchValues::primaryKey(),
                'model' => SearchValues::className()
            ],
            Settings::tableName() => [
                'label' => 'Settings',
                'pk' => Settings::primaryKey(),
                'model' => Settings::className()
            ]
        ];
        return $tables;
    }

    /**
     * count new records available for update generation
     * @param string $table table name
     * @return integer
     */
    public function getCountNewItems($table)
    {
        return count($this->getItemsQuery($table)->queryAll());
    }

    /**
     * check if table new records to be included in updates
     * @param string $table ex: base_table_name
     * @return boolean
     */
    public function getTableHasNewRecords($table)
    {
        return $this->getCountNewItems($table);
    }

    /**
     * check if table is up to date
     * @param string $table {{%my_table}}
     */
    public function getTableIsUpToDate($table)
    {

    }

    /**
     *
     * @param string $table
     * @return string
     */
    public function getUpdatePrefix($table)
    {
        return str_replace(array('{{%', '}}'), '', $table);
    }

    /**
     *
     * @param string $prefix
     * @param string $table
     * @param string $condition
     * @return yii\db\Command
     */
    public function getItemsQuery($table, $condition = null, $prefix = null)
    {
        if (!$prefix) {
            $prefix = $this->getUpdatePrefix($table);
        }
        $lastUpdate = self::find()->filterWhere(['like', 'update', $prefix . '-'])->orderBy(['created_at' => SORT_DESC])->one();
        $lim = ' LIMIT 1000';
        if ($lastUpdate) {
            $sql = "SELECT * FROM " . $table . ' WHERE (created_at>:last_update) ' . ($condition ? 'AND ' . $condition : '') . $lim;
        } else {
            $sql = "SELECT * FROM " . $table . ($condition ? ' WHERE ' . $condition : '') . ' ' . $lim;
        }
        $query = yii::$app->db->createCommand($sql, ['last_update' => ($lastUpdate ? $lastUpdate->created_at : 0)]);
        return $query;
    }

    /**
     * get items to generate update
     * @param string $table {{%table_name}}
     * @param string $condition
     * @return array [
     *      'tb'=>'table_name', // without prefix and wrapper: table_name
     *      'items'=>['column_name'=>'value','column_name_1'=>'value_1', ...]  //items from table
     * ]
     */
    public function getUpdateItems($table, $condition = null)
    {
        $info = $this->selectThisTables([$table]);
        return [
            'tb' => $this->getUpdatePrefix($table),
            'info' => $info[$table],
            'items' => $this->getItemsQuery($table)->queryAll()
        ];
    }

    /**
     * save all generated updates
     * @param array $data
     * @return type
     */
    public function saveUpdates($data, $order)
    {
        $updateId = $data['tb'] . '-' . date('U');
        $data['order'] = $order;
        $path = 'updates/' . date('Y') . '/' . date('W', date('U'));
        $fileName = $updateId . '.' . self::FILE_EXT;
        if ($this->signUpdate($updateId, ['order' => $order, 'file' => $fileName, 'totalCount' => count($data['items'])])) {
            yii::$app->helper->file(['publicPath' => false, 'rootPath' => true])->writeToFile($path, $fileName, serialize($data));
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @return type
     */
    public function getUpdates()
    {
        $strH = yii::$app->helper->str();
        $fileHelper = yii::$app->helper->file(['publicPath' => false, 'rootPath' => true]);
        $path = $fileHelper->getPath('updates');
        if (!is_dir($path)) {
            return [];
        }
        $updates = $fileHelper->findFiles($path);
        $data = [];
        foreach ($updates as $file) {
            if (!yii::$app->helper->str()->strigContains($file, '.' . self::FILE_EXT) ||
            (basename($file, '.' . self::FILE_EXT) == 'readme') ||
            self::find()->where(['update' => basename($file, '.' . self::FILE_EXT)])->one()) {
                continue;
            }
            $f = str_replace(yii::$app->helper->settings()->getValue('tld'), '', $file);
            $createdAt = $strH->getStringBetween($f, '-', '.txt');
            $ar = unserialize(file_get_contents($file));
            $data[$ar['order']] = [
                'file' => $file,
                'tb' => $ar['tb'],
                'totalCount' => count($ar['items']),
                'created_at' => yii::$app->formatter->asDatetime($createdAt) . ' - ' . $createdAt,
                'info' => $ar['info'],
            ];
        }
        ksort($data);
        return $data;
    }

    /**
     *
     * @param string $file path to the file
     * @return boolean
     * @example '/my/folder/my_file.txt'
     */
    public function applyUpdates($file)
    {
        $migrateion = new Migration();
        if (is_file($file)) {
            $data = @unserialize(file_get_contents($file));
            if ($data && isset($data['tb']) && isset($data['items']) && is_array($data['items'])) {
                $pk = $data['info']['pk'];
                $table = '{{%' . $data['tb'] . '}}';
                $inserted = false;
                $i = 0;
                foreach ($data['items'] as $k => $v) {
                    $condition = [];
                    foreach ($v as $column => $value) {
                        if ($pk == $column || (is_array($pk) && in_array($column, $pk))) {
                            $condition[$column] = $value;
                        }
                    }
                    if (count($condition) > 0) {
                        $query = new Query();
                        if ($query->from($table)->where($condition)->one()) {
                            if ($this->rewrite == 1) {
                                $inserted = yii::$app->db->createCommand()->update($table, $v, implode('=:', $condition), $condition)->execute();
                                $i++;
                            }
                        } else {
                            $inserted = $this->db->createCommand()->insert($table, $v)->execute();
                        }
                    } else {
                        $inserted = $this->db->createCommand()->insert($table, $v)->execute();
                        $i++;
                    }
                }
//                $this->signUpdate(basename($file, '.' . self::FILE_EXT), ['order' => $data['order'], 'file' => basename($file), 'inserted' => ($i), 'totalCount' => count($data['items'])]);
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param string $id
     * @param array $data
     * @return boolean
     */
    public function signUpdate($id, $data)
    {
        $model = new Updates();
        $model->update = $id;
        $model->additional_data = $data;
        if ($model->validate()) {
            return $model->save();
        }
    }

}
