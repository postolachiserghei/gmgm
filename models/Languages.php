<?php

namespace app\models;

use app\components\extend\yii;
use app\components\extend\Url;
use app\components\extend\Html;
use app\components\extend\ArrayHelper;

/**
 * This is the model class for table "pro_languages".
 *
 * @property string $language_id
 * @property string $language_name
 * @property integer $language_active
 * @property integer $language_is_default
 */
class Languages extends \app\components\extend\Model
{

    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 0;
    const IS_DEFAULT = 1;
    const IS_NOT_DEFAULT = 0;

    public $multi;
    public $default;
    public $liveEditT = false;
    public $allLanguages;
    public $languages;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $init = parent::init();
        if ($this->isNewRecord) {
            $this->language_active = self::STATUS_ACTIVE;
        }
        return $init;
    }

    /**
     * get current language from url
     * @param mixed $alternative
     * @return string
     */
    public function getLanguageFromUrl($alternative = null)
    {
        if (!$this->allLanguages) {
            return $alternative;
        }
        foreach (ArrayHelper::map($this->allLanguages, 'language_id', 'language_id') as $lang) {
            if (yii::$app->helper->str()->strigContains(yii::$app->request->url, '/' . $lang)) {
                $this->changeLanguage($lang);
                return $lang;
            }
        }
        return $alternative;
    }

    /**
     *
     * @param \app\components\extend\UrlManager $urlManager
     */
    public function setLanguageOptions($urlManager)
    {
        $this->getLanguages();
        $this->multi = count($this->languages) > 1 ? true : false;
        if (yii::$app->l->multi) {
            yii::$app->language = $this->getLanguageFromUrl($this->default);
            if ((yii::$app->request->url == '/' || yii::$app->request->url == '') && yii::$app->language != $this->default) {
                header('location: /' . yii::$app->language);
                exit();
            }
            if (yii::$app->language != $this->default) {
                $urlManager->setBaseUrl('/' . yii::$app->language);
            }
        }
    }

    public function changeLanguage($id)
    {
        return yii::$app->helper->data()->setCookie('language', $id);
    }

    /**
     * gel all languages
     * @return Languages[]
     */
    public function getLanguages()
    {
        if ($this->allLanguages) {
            return $this->allLanguages;
        }
        if ($this->allLanguages = self::find()->where(['language_active' => self::STATUS_ACTIVE])->orderBy(['language_is_default' => SORT_DESC])->all()) {
            foreach ($this->allLanguages as $m) {
                $this->languages[$m->language_id] = $m->language_name;
                if (!$this->default && $m->language_is_default === self::IS_DEFAULT) {
                    $this->default = $m->language_id;
                }
            }
            if (count($this->languages) > 1) {
                $this->multi = true;
            } else {
                $this->default = $m->language_id;
                $this->languages[$this->default] = $this->language_name;
                $this->allLanguages = $this->languages;
            }
        } else {
            $this->default = yii::$app->language;
            $this->languages[$this->default] = $this->default;
            $this->allLanguages = $this->languages;
        }
        if (array_key_exists($this->default, $this->languages))
            return [$this->default => $this->languages[$this->default]] + $this->languages;
        return $this->languages;
    }

    /**
     * @param int $status
     * @param boolean $withLiveEdit (return translated labels wrapped in html tag if TRUE)
     * @return array/string
     */
    public function getStatusLabels($status = false, $withLiveEdit = true)
    {
        $ar = [
            self::STATUS_ACTIVE => yii::$app->l->t('language active', ['update' => $withLiveEdit]),
            self::STATUS_DISABLED => yii::$app->l->t('language disabled', ['update' => $withLiveEdit]),
        ];
        return $status !== false ? $ar[$status] : $ar;
    }

    /**
     * @param int $default
     * @param boolean $withLiveEdit (return translated labels wrapped in html tag if TRUE)
     * @return array/string
     */
    public function getDefaultLabels($default = null, $withLiveEdit = true)
    {
        $ar = [
            self::IS_DEFAULT => yii::$app->l->t('yes', ['update' => $withLiveEdit]),
            self::IS_NOT_DEFAULT => yii::$app->l->t('no', ['update' => $withLiveEdit])
        ];
        return $default !== null ? $ar[$default] : $ar;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%languages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['language_id', 'language_name'], 'required'],
            [['language_id', 'language_name'], 'unique'],
            [['language_active'], 'integer'],
            [['language_active'], 'default', 'value' => self::STATUS_ACTIVE],
            [['language_is_default'], 'integer'],
            [['language_id'], 'string', 'max' => 2],
            [['language_name'], 'string', 'max' => 64],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'language_id' => self::t('id'),
            'language_name' => self::t('name'),
            'language_active' => self::t('status'),
            'language_is_default' => self::t('language is default'),
        ]);
    }

    /**
     * @param string $message
     * @param array $params
     * @param string $language
     * @return string
     */
    public function t($message, $params = [], $language = null)
    {
        $language = ($language ? $language : yii::$app->language);
        if (!isset($params['update'])) {
            $params['update'] = true;
        }
        if (!isset($params['ucf'])) {
            $params['ucf'] = true;
        }
        if (array_key_exists('category', $params)) {
            $category = $params['category'];
        } else {
            if (yii::$app->controller && yii::$app->controller->module) {
                $category = yii::$app->controller->module->id;
            } else {
                $category = 'app';
            }
        }
        if (!isset(yii::$app->i18n->translations[$category])) {
            yii::$app->i18n->translations[$category] = yii::$app->i18n->translations['app'];
        }
        $t = yii::t($category, mb_strtolower($message, 'UTF-8'), $params, $language);

        if ((is_array($params) && (array_key_exists('ucf', $params)) || !array_key_exists('lcf', $params)) || $params == 'ucf')
            $t = self::mb_ucfirst($t);
        if ((is_array($params) && array_key_exists('lcf', $params)) || $params == 'lcf')
            $t = self::mb_lcfirst($t);
        if ($this->liveEditT && isset($params['update']) && $params['update'] === true && yii::$app->user->can('translations-update', ['module' => 'admin'])) {
            $edit = Html::tag('span', $t, [
                'data' => [
                    'message' => yii::$app->l->t('update message', ['update' => false])
                    . '<br/><span style="font-size:0.8em;">'
                    . yii::$app->l->t('message: "{message}"', ['message' => $t, 'update' => false,])
                    . '<br/>' . yii::$app->l->t('language: {language}', ['language' => (array_key_exists($language, $this->languages) ? $this->languages[$language] : $language), 'update' => false,])
                    . '</span>',
                    'source' => $message,
                    't' => $t,
                    'language' => $language,
                    'category' => $category,
                    'url' => Url::to('/' . yii::$app->controller->module->id . '/translations/update')
                ]
            ]);
            $t = Html::tag('span', $edit, [
                'class' => 'btn btn-xs', 'icon' => 'pencil',
                'onclick' => "Languages.editTranslation($('span',$(this)))",
            ]);
        }
        return $t;

//        $('*').bind('blur change click dblclick error focus focusin focusout hover keydown keypress keyup load mousedown mouseenter mouseleave mousemove mouseout mouseover mouseup resize scroll select submit', function(event){
//    event.preventDefault();
//});
        //@TODO: translation live edit
    }

    /**
     * if system does not found translation it will be inserted into the db
     * @param array $event contains info about the translation: message,category,language,cache..
     * @return boolean
     */
    public static function insertMissingTranslationIntoDb($event)
    {
        $m = $event->message;
        $c = $event->category;
        $l = $event->language;
        $source = I18nMessageSource::find()->where('message=:m AND category=:c', ['m' => mb_strtolower($m), 'c' => mb_strtolower($c),])->one();
        if (!$source) {
            $source = new I18nMessageSource();
            $source->category = $c;
            $source->message = $m;
            $source->save();
        }
    }

    /**
     * Uppercase first letter from word/text
     * @word - word/text (string)
     * @returns -  transformed text/word (string)
     */
    public static function mb_ucfirst($word)
    {
        return mb_strtoupper(mb_substr($word, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr($word, 1, mb_strlen($word), 'UTF-8');
    }

    /**
     * Lowercase first letter from word/text
     * @word - word/text (string)
     * @returns -  transformed text/word (string)
     */
    public static function mb_lcfirst($word)
    {
        return mb_strtolower(mb_substr($word, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr($word, 1, mb_strlen($word), 'UTF-8');
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->setDefault($insert);
        }
        return true;
    }

    /**
     * clear old defaults
     * @param type $insert
     */
    public function setDefault($insert)
    {
        $isDefault = $this->language_is_default == self::IS_DEFAULT;
        if (($insert && $isDefault) || ($isDefault && $this->oldAttributes['language_is_default'] == self::IS_NOT_DEFAULT)) {
            self::updateAll(['language_is_default' => self::IS_NOT_DEFAULT], ['language_is_default' => self::IS_DEFAULT]);
        }
    }

    /**
     * get active language items for nav
     * @return array
     */
    public function getMenuLanguages()
    {
        if (!yii::$app->l->multi)
            return [];
        $items = [];
        $s = yii::$app->helper->str();
        $appl = '/' . yii::$app->language;
        foreach ($this->allLanguages as $k => $v) {
            if (yii::$app->language == $v->language_id) {
                continue;
            }
            $pathInfo = yii::$app->request->pathInfo;
            $params = yii::$app->request->queryParams;
            unset($params['language']);
            if (yii::$app->request->isPjax) {
                unset($params['_'], $params['_pjax'], $params['pjax_page_container']);
            }
            $arUrl = [$pathInfo] + $params;
            $url = yii::$app->request->createUrl($arUrl);
            if ($url && ($s->startsWith($url, $appl, false) || $url == '/' . yii::$app->language)) {
                $url = mb_substr($url, ($s->startsWith($url, $appl . $appl, false) ? 6 : 3));
            }
            if ($v->language_id != yii::$app->l->default) {
                $url = '/' . $v->language_id . ($url == '/' ? '' : $url);
            } else {
                $url = $url == '' ? '/' : $url;
            }
            $items[] = [
                'label' => ($v->language_name && $v->language_name != '') ? $v->language_name : $v->language_id,
                'url' => $url,
                'linkOptions' => [
                    'data' => [
                        'host-info' => yii::$app->request->pathInfo,
                        'url' => $url,
                        'query_string' => yii::$app->request->queryString,
                        'query_params' => yii::$app->request->queryParams,
                    ]
                ],
                'active' => ($v->language_id === yii::$app->language),
            ];
        }
        return count($items) > 0 ? [
            [
                'label' => yii::$app->language,
                'items' => $items,
                'visible' => yii::$app->l->multi
            ]
        ] : [];
    }

    /**
     *
     * @param type $asArray
     * @return type
     */
    public function getPrimaryKey($asArray = false)
    {
        return $this->language_id;
    }

}
