<?php

namespace app\models;

use app\components\extend\yii;
use app\models\behaviors\JsonFields;

/**
 * This is the model class for table "{{%tasks}}".
 *
 * @property integer $id
 * @property string $object
 * @property string $method
 * @property string $params
 * @property integer $start_time
 * @property integer $priority
 * @property integer $status
 * @property string $additional_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order
 * @property integer $is_deleted
 */
class Tasks extends \app\components\extend\Model
{

    const PRIORITY_HIGH = 1;
    const PRIORITY_NORMAL = 2;
    const PRIORITY_LOW = 3;
    const STATUS_NEW = 1;
    const STATUS_DONE = 2;
    const STATUS_FAILED = 3;

    public static function getLog()
    {
        return yii::$app->helper->log()->setInitiator(self::className());
    }

    /**
     * set record status
     * @param type $status
     * @return type
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this->save();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'jsonFields' => [
                'class' => JsonFields::className(),
                'fields' => [
                    'additional_data',
                    'params',
                ]
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tasks}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['object'], 'string'],
            [['start_time', 'priority', 'status', 'created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
            [['method'], 'string', 'max' => 250],
            [['params', 'additional_data'], 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'id' => 'id',
            'object' => 'class object',
            'method' => 'oject method',
            'params' => 'method params',
            'start_time' => 'task start after time',
            'priority' => 'task priority',
            'status' => 'task status',
            'additional_data' => 'additional data',
            'created_at' => 'Date created',
            'updated_at' => 'Date updated',
            'order' => 'Order position',
            'is_deleted' => 'Is deleted',
        ]);
    }

    /**
     * add new task
     * @param object|string $object object or serialized object
     * @param string $method object method name
     * @param array $params
     * @param integer $startTime
     * @param integer $priority
     */
    public static function add($object, $method, $params = [], $startTime = null, $priority = null)
    {
        $log = self::getLog();
        $task = new Tasks();
        $task->object = (!is_object($object) ? $object : serialize($object));
        $task->method = $method;
        $task->params = $params;
        $task->start_time = ($startTime ? $startTime : time('U'));
        $task->priority = $priority ? $priority : self::PRIORITY_NORMAL;
        if ($task->validate()) {
            return $task->save();
        } else {
            $log->error($task->errors);
        }
    }

    /**
     * execute current task
     */
    public function exec()
    {
        $object = unserialize($this->object);
        $method = $this->method;
        $errors = [];
        $log = self::getLog();
        if (!is_object($object)) {
            $errors[] = yii::$app->l->t('wrong task oject given');
        } else {
            if (!method_exists($object, $method)) {
                $errors[] = yii::$app->l->t('wrong method given');
            }
        }
        if (count($errors) > 0) {
            $this->setStatus(self::STATUS_FAILED);
            return $log->error(yii::$app->l->t('task NR{id} execution failed!', [
                'id' => $this->id,
            ]) . "<br/>" . yii::$app->l->t('details') . ":<br/>-" . implode('<br/>-', $errors) . "<br/>");
        }
        return call_user_func_array(function($a = null, $b = null, $c = null, $d = null, $e = null, $f = null, $g = null) use ($object, $method, $log) {
            try {
                yii::$app->db->beginTransaction();
                if (@$object->$method($a, $b, $c, $d, $e, $f, $g)) {
                    yii::$app->db->transaction->commit();
                    $this->setStatus(self::STATUS_DONE);
                } else {
                    yii::$app->db->transaction->rollBack();
                    $log->error(yii::$app->l->t('task NR{id} execution failed!', ['id' => $this->id]));
                    return $this->setStatus(self::STATUS_FAILED);
                }
            } catch (Exception $exc) {
                yii::$app->db->transaction->rollBack();
                $log->error(yii::$app->l->t('{id} task execution error', [
                    'id' => $this->id . $exc,
                ]));
                return $this->setStatus(self::STATUS_FAILED);
            }
        }, $this->params);
    }

    /**
     * execute new tasks
     * @param boolean $console
     */
    public static function process($console = false)
    {
        $q = self::find();
        $q->where(['status' => self::STATUS_NEW])->limit(10)->orderBy(['priority' => SORT_ASC]);
        $q->andWhere(['<=', 'start_time', date('U')]);
        $tasks = $q->all();
        if ($tasks) {
            foreach ($tasks as $task) {
                /* @var $task Tasks */
                $task->exec();
            }
        }
    }

}
