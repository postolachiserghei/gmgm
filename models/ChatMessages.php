<?php

namespace app\models;

use app\components\extend\yii;

/**
 * This is the model class for table "{{%chat_messages}}".
 *
 * @property integer $chat_id
 * @property integer $sender_id
 * @property string $message
 * @property integer $status
 * @property string $additional_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order
 * @property integer $is_deleted
 *
 * @property ChatMembers $sender
 */
class ChatMessages extends \app\components\extend\Model
{

    const STATUS_NEW = 1;
    const STATUS_READ = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%chat_messages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['chat_id', 'sender_id', 'message'], 'required'],
            [['message'], 'string'],
            [['chat_id', 'sender_id', 'status', 'created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
            [['sender_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChatMembers::className(), 'targetAttribute' => ['sender_id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'chat_id' => yii::$app->l->t('chat'),
            'sender_id' => yii::$app->l->t('sender'),
            'message' => yii::$app->l->t('message'),
            'status' => yii::$app->l->t('status'),
            'additional_data' => yii::$app->l->t('additional data'),
            'created_at' => yii::$app->l->t('Date created'),
            'updated_at' => yii::$app->l->t('Date updated'),
            'order' => yii::$app->l->t('Order position'),
            'is_deleted' => yii::$app->l->t('Is deleted'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(ChatMembers::className(), ['id' => 'sender_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['id' => 'chat_id']);
    }

    public function setAsRead($userId = null)
    {
        if (!$userId) {
            $userId = yii::$app->user->id;
        }
        if ($this->sender->user_id == yii::$app->user->id) {
            return;
        }
        $isRead = $this->readAttribute($userId);
        $readBy = $this->getData($isRead);
        if (yii::$app->helper->str()->strigContains($readBy, "[$userId]") && $this->status == self::STATUS_READ) {
            return;
        }
        $data = [
            'read' => ($readBy ? $readBy : '') . "[$userId]"
        ];
        $this->status = self::STATUS_READ;
        $this->additional_data = is_array($this->additional_data) ? array_merge($this->additional_data, $data) : $data;
        $this->save();
    }

    public static function readAttribute($id)
    {
        return 'additional_data[read]';
    }

}
