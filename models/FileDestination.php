<?php

namespace app\models;

use app\components\extend\yii;

/**
 * This is the model class for table "{{%file_destination}}".
 *
 * @property    integer     $file_name
 * @property    string      $destination
 */
class FileDestination extends \app\components\extend\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%file_destination}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['file_name', 'destination'], 'required'],
            [['file_name'], 'unique', 'targetAttribute' => ['file_name', 'destination']]
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'file_name' => yii::$app->l->t('file'),
            'destination' => yii::$app->l->t('destination'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['file_name', 'destination'];
    }

}
