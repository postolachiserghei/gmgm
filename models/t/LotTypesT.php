<?php

namespace app\models\t;

use app\models\LotTypes;
use app\components\extend\yii;

/**
 * This is the model class for table "{{%lot_types_t}}".
 *
 * @property integer $lot_id
 * @property string $title
 * @property string $language_id
 *
 * @property LotTypes $lot
 */
class LotTypesT extends \app\components\extend\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%lot_types_t}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['lot_id'], 'exist', 'skipOnError' => true, 'targetClass' => LotTypes::className(), 'targetAttribute' => ['lot_id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'lot_id' => yii::$app->l->t('Lot id'),
            'title' => yii::$app->l->t('Lot title'),
            'language_id' => yii::$app->l->t('Lot language'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLot()
    {
        return $this->hasOne(LotTypes::className(), ['id' => 'lot_id']);
    }

}
