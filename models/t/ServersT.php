<?php

namespace app\models\t;

use app\models\Servers;
use app\components\extend\yii;

/**
 * This is the model class for table "{{%servers_t}}".
 *
 * @property integer $server_id
 * @property string $title
 * @property string $additional_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order
 * @property integer $is_deleted
 *
 * @property Servers $server
 */
class ServersT extends \app\components\extend\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%servers_t}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['server_id', 'created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
            [['title'], 'string'],
            [['server_id'], 'exist', 'skipOnError' => true, 'targetClass' => Servers::className(), 'targetAttribute' => ['server_id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(),[
            'server_id' => yii::$app->l->t('server id'),
            'title' => yii::$app->l->t('url'),
            'additional_data' => yii::$app->l->t('additional data'),
            'created_at' => yii::$app->l->t('Date created'),
            'updated_at' => yii::$app->l->t('Date updated'),
            'order' => yii::$app->l->t('Order position'),
            'is_deleted' => yii::$app->l->t('Is deleted'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServer()
    {
        return $this->hasOne(Servers::className(), ['id' => 'server_id']);
    }
}
