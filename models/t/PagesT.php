<?php

namespace app\models\t;

use app\components\extend\yii;

/**
 * This is the model class for table "{{%pages_t}}".
 *
 * @property integer $page_id
 * @property string $title
 * @property string $content
 * @property string $language_id
 */
class PagesT extends \app\components\extend\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pages_t}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['content'], 'required'],
            [['content'], 'string'],
            [['data', 'title'], 'safe'],
            [['title'], 'string', 'max' => 250],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $ar = [
            'page_id' => yii::$app->l->t('page'),
            'title' => yii::$app->l->t('title'),
            'content' => yii::$app->l->t('content'),
        ];

        $l = parent::LanguageNoteLabels($ar) + ['language_id' => yii::$app->l->t('language')];
        return array_merge(parent::attributeLabels(), $l);
    }

    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['page_id', 'language_id'];
    }

}
