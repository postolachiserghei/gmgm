<?php

namespace app\models\t;

use app\components\extend\yii;

/**
 * This is the model class for table "{{%menu_t}}".
 *
 * @property integer $menu_id
 * @property string $title
 * @property string $url
 * @property string $language_id
 *
 * @property Menu $menu
 */
class MenuT extends \app\components\extend\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%menu_t}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['url'], 'string'],
            [['title'], 'string', 'max' => 250],
            [['menu_id'], 'safe'],
            [['language_id'], 'string', 'max' => 2]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $language = yii::$app->language != $this->language_id ? $this->language_id : '';
        $ar = [
            'menu_id' => yii::$app->l->t('menu') . $language,
            'title' => yii::$app->l->t('title') . $language,
            'url' => yii::$app->l->t('menu url') . $language,
        ];
        $l = parent::LanguageNoteLabels($ar) + ['language_id' => yii::$app->l->t('language')];
        return array_merge(parent::attributeLabels(), $l);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * @return array
     */
    public static function primaryKey()
    {
        return ['menu_id', 'language_id'];
    }

}
