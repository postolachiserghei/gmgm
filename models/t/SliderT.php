<?php

namespace app\models\t;

use app\components\extend\yii;

/**
 * This is the model class for table "{{%slider_t}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $language_id
 * @property string $additional_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order
 * @property integer $is_deleted
 *
 * @property Slider $id0
 */
class SliderT extends \app\components\extend\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%slider_t}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'id' => yii::$app->l->t('slider'),
            'title' => yii::$app->l->t('title'),
            'language_id' => yii::$app->l->t('language'),
            'created_at' => yii::$app->l->t('Date created'),
            'updated_at' => yii::$app->l->t('Date updated'),
            'order' => yii::$app->l->t('Order position'),
            'is_deleted' => yii::$app->l->t('Is deleted'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Slider::className(), ['id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['id', 'language_id'];
    }

}
