<?php

namespace app\models\t;

use app\models\Seo;
use app\components\extend\yii;

/**
 * This is the model class for table "{{%seo_t}}".
 *
 * @property integer $seo_id
 * @property string $alias
 * @property string $title
 * @property string $h1
 * @property string $keywords
 * @property string $description
 * @property string $language_id
 *
 * @property Seo $seo
 */
class SeoT extends \app\components\extend\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%seo_t}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'updateSeoLinks' => [
                'class' => \app\models\behaviors\UpdateSeoLinksBehavior::className(),
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['alias'], 'required'],
            [['alias', 'h1', 'keywords', 'description'], 'string'],
            [['title'], 'string', 'max' => 100],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $ar = [
            'seo_id' => yii::$app->l->t('seo'),
            'alias' => yii::$app->l->t('alias'),
            'title' => yii::$app->l->t('title'),
            'h1' => yii::$app->l->t('h1'),
            'keywords' => yii::$app->l->t('keywords'),
            'description' => yii::$app->l->t('description'),
            'language_id' => yii::$app->l->t('language'),
        ];

        $l = parent::LanguageNoteLabels($ar);

        return array_merge(parent::attributeLabels(), $l);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeo()
    {
        return $this->hasOne(Seo::className(), ['id' => 'seo_id']);
    }

    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['seo_id', 'language_id'];
    }

    /**
     * set meta tags
     * @param type $controller
     */
    public function setMetaData($controller)
    {


        if (trim($this->title) != '') {
            yii::$app->helper->data()->setParam('pageTitle', $this->title);
        }
        if (trim($this->description) != '') {
            yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => $this->description,
            ]);
        }
        if (trim($this->keywords) != '') {
            yii::$app->view->registerMetaTag([
                'name' => 'keywords',
                'content' => $this->keywords,
            ]);
        }

        if (trim($this->h1) != '') {
            yii::$app->helper->data()->setParam('h1', $this->h1);
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $bs = parent::beforeSave($insert);
        if (!yii::$app->helper->str()->startsWith($this->alias, '/')) {
            $this->alias = '/' . $this->alias;
        }
        return $bs;
    }

}
