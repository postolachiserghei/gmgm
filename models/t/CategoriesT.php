<?php

namespace app\models\t;

use app\models\Categories;
use app\components\extend\yii;

/**
 * This is the model class for table "{{%categories_t}}".
 *
 * @property integer $category_id
 * @property string $title
 * @property string $language_id
 * @property string $additional_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order
 * @property integer $is_deleted
 *
 * @property Categories $category
 */
class CategoriesT extends \app\components\extend\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%categories_t}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['category_id', 'created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
            [['title'], 'string', 'max' => 250],
            [['language_id'], 'string', 'max' => 15],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'category_id' => yii::$app->l->t('Category pk'),
            'title' => yii::$app->l->t('Category item title'),
            'language_id' => yii::$app->l->t('Category item language'),
            'created_at' => yii::$app->l->t('Date created'),
            'updated_at' => yii::$app->l->t('Date updated'),
            'order' => yii::$app->l->t('Order position'),
            'is_deleted' => yii::$app->l->t('Is deleted'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

}
