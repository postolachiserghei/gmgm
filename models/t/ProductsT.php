<?php

namespace app\models\t;

use app\models\Products;
use app\components\extend\yii;

/**
 * This is the model class for table "{{%products_t}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $language_id
 * @property string $additional_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order
 * @property integer $is_deleted
 *
 * @property Products $id0
 */
class ProductsT extends \app\components\extend\Model
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products_t}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id', 'created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 250],
            [['language_id'], 'string', 'max' => 10],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'id' => yii::$app->l->t('slider'),
            'title' => yii::$app->l->t('title'),
            'description' => yii::$app->l->t('description'),
            'image' => yii::$app->l->t('image'),
            'language_id' => yii::$app->l->t('language'),
            'additional_data' => yii::$app->l->t('additional data'),
            'created_at' => yii::$app->l->t('Date created'),
            'updated_at' => yii::$app->l->t('Date updated'),
            'order' => yii::$app->l->t('Order position'),
            'is_deleted' => yii::$app->l->t('Is deleted'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Products::className(), ['id' => 'id']);
    }

}
