<?php

namespace app\models;

use app\components\extend\yii;
use app\components\extend\Url;
use app\components\extend\Html;
use app\components\extend\ArrayHelper;

/**
 * This is the model class for table "{{%file}}".
 *
 * @property    string      $scheme
 * @property    string      $host
 * @property    string      $path
 * @property    string      $name
 * @property    string      $title
 * @property    string      $extension
 * @property    integer     $size
 * @property    string      $mime
 * @property    integer     $created_at
 * @property    integer     $location
 * @property    boolean     $status
 * @property    boolean     $isImage
 * @property    string      $url
 * @property    string      $owner
 * @property    string      $destination
 * @method public renderImage(array $options)
 * @method public getDestinations()
 */
/* @var $ftp \app\components\helper\ftp\FtpClient */
class File extends \app\components\extend\Model
{

    protected $ftp;
    public $helper;
    public $destination;

    const STATUS_UPLOADED = 0;
    const STATUS_DELETED = 1;
    const STATUS_IN_SYNC = 2;
    const LOCATION_LOCAL = 1;
    const LOCATION_FTP = 2;
    const SIZE_ORIGINAL = 0;
    const SIZE_LG = 1;
    const SIZE_MD = 2;
    const SIZE_SM = 3;

    /**
     * @param integer $location
     * @param boolean $withLiveEdit (return translated labels wrapped in html tag if TRUE)
     * @return array/string
     */
    public function getLocationLabels($location = false, $withLiveEdit = true)
    {
        $ar = [
            self::LOCATION_LOCAL => yii::$app->l->t('local', ['update' => $withLiveEdit]),
            self::LOCATION_FTP => yii::$app->l->t('ftp', ['update' => $withLiveEdit]),
        ];
        return $location === false ? $ar : $ar[$location];
    }

    /**
     * @param integer $status
     * @param boolean $withLiveEdit (return translated labels wrapped in html tag if TRUE)
     * @return array/string
     */
    public function getStatusLabels($status = false, $withLiveEdit = true)
    {
        $ar = [
            self::STATUS_UPLOADED => yii::$app->l->t('uploaded', ['update' => $withLiveEdit]),
            self::STATUS_IN_SYNC => yii::$app->l->t('in sync', ['update' => $withLiveEdit]),
            self::STATUS_DELETED => yii::$app->l->t('deleted', ['update' => $withLiveEdit]),
        ];
        return $status === false ? $ar : $ar[$status];
    }

    /**
     * sizes for image thumbs
     * @return array
     */
    public static function getImageSizes()
    {
        return [
            self::SIZE_LG => 'large',
            self::SIZE_MD => 'medium',
            self::SIZE_SM => 'small',
        ];
    }

    /**
     * size values for image thumbs
     * @param integer $size
     * @return array
     */
    public static function getImageSizeValues($size = null)
    {
        $ar = [
            self::SIZE_LG => [1024, 850],
            self::SIZE_MD => [250, 200],
            self::SIZE_SM => [75, 50],
        ];

        return $size !== null ? $ar[$size] : $ar;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%file}}';
    }

    /* relations */

    public function getOwnerUser()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }

    /**
     * owner user name
     */
    public function getOwnerUserName()
    {
        if ($user = $this->ownerUser) {
            return $user->fullName;
        }
        return yii::$app->l->t('guest');
    }

    /**
     * get file destinations
     * @return FileDestination
     */
    public function getDestinations()
    {
        return $this->hasOne(FileDestination::className(), ['file_name' => 'name']);
    }

    /**
     * owner user name
     */
    public function getDestinationList($string = false)
    {
        $ar = [];
        if ($destinations = $this->getDestinations()->all()) {
            $ar = ArrayHelper::map($destinations, 'destination', 'destination');
        }
        if ($string) {
            return count($ar) > 0 ? implode(',', $ar) : implode(',', ['none']);
        }
        return count($ar) > 0 ? $ar : ['none'];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return parent::behaviors() + [
            'settings' => [
                'class' => settings\FileSettings::className(),
            ],
            'isImageBehavior' => [
                'class' => behaviors\FileImageBehavior::className(),
            ],
            'transferToFtp' => [
                'class' => behaviors\FileFtpBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['location'], 'default', 'value' => self::LOCATION_LOCAL],
            [['scheme'], 'default', 'value' => 'http://'],
            [['host'], 'default', 'value' => yii::$app->helper->settings()->getSetting('tld')],
            [['host', 'path', 'name', 'title', 'extension', 'size', 'mime'], 'required'],
            [['owner', 'created_at', 'status', 'location'], 'safe'],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'host' => yii::$app->l->t('host'),
            'path' => yii::$app->l->t('path'),
            'name' => yii::$app->l->t('name'),
            'title' => yii::$app->l->t('title'),
            'extension' => yii::$app->l->t('extension'),
            'size' => yii::$app->l->t('size'),
            'mime' => yii::$app->l->t('mime type'),
            'created_at' => yii::$app->l->t('created at'),
            'status' => yii::$app->l->t('status'),
            'location' => yii::$app->l->t('location'),
            'owner' => yii::$app->l->t('owner'),
            'destination' => yii::$app->l->t('destination'),
        ]);
    }

    /**
     *
     * @param string $name
     * @param object $info
     * @param string $path
     * @return mixed
     */
    public static function saveFileInfo($name, $info, $path)
    {
        $file = new File;
        $file->name = trim($name);
        $file->extension = preg_replace('/\s+/', '', pathinfo(yii::$app->helper->file()->getPath($path . $name), PATHINFO_EXTENSION));
        $file->title = preg_replace('/\s+/', '', $info->name);
        $file->size = preg_replace('/\s+/', '', $info->size);
        $file->path = preg_replace('/\s+/', '', $path);
        $file->status = self::STATUS_UPLOADED;
        $file->mime = $info->type;
        if ($file->validate() && $file->save()) {
            return $file;
        }
        return null;
    }

    /**
     * delete file info record
     * @param integer $name
     * @return boolean
     */
    public static function deleteFileByName($name, $params)
    {
        if ($f = self::find()->where(['name' => $name])->one()) {
            /* @var $f File */
            $f->is_deleted = self::DELETED_IRREVERSIBLE;
            if (is_array($params) && array_key_exists('beforeDelete', $params) && is_callable($params['beforeDelete'])) {
                if ($params['beforeDelete']($f)) {
                    $delete = $f->delete();
                }
            } else {
                $delete = $f->delete();
            }
            if (is_array($params) && array_key_exists('afterDelete', $params) && is_callable($params['afterDelete'])) {
                if (isset($delete) && $delete) {
                    $params['afterDelete']($f);
                }
            }
        }
        return true;
    }

    /**
     * set uploaded
     * @return type
     */
    public function setUploaded()
    {
        $this->status = self::STATUS_UPLOADED;
        return $this->validate() ? $this->save() : false;
    }

    /**
     * set in sync
     * @return type
     */
    public function setInSync()
    {
        $this->status = self::STATUS_IN_SYNC;
        return $this->validate() ? $this->save() : false;
    }

    /**
     * set as deleted record (change status)
     * @param $destination
     * @return boolean
     */
    public function setDeleted($destination = null)
    {
        if ($destination && $oneDestination = FileDestination::find()->where(['_file_name' => $this->name, 'destination' => $destination])->one()) {
            $oneDestination->is_deleted = self::DELETED_IRREVERSIBLE;
            if ($oneDestination->delete() && !FileDestination::find()->where(['file_name' => $this->name])->one()) {
                return $this->delete();
            }
            return false;
        } else {
            return $this->delete();
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if ($this->location == self::LOCATION_FTP && ($this->getSetting('transfer_to_ftp') != self::LOCATION_FTP)) {
            return false;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $bs = parent::beforeSave($insert);
        if (!yii::$app->request->isConsoleRequest && $insert && !yii::$app->user->isGuest) {
            $this->owner = yii::$app->user->id;
        }
        return $bs;
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        $ad = parent::afterDelete();
        if ($this->is_deleted === self::DELETED_IRREVERSIBLE) {
            $this->removeLocalFile();
            $this->removeFtpFile();
            $destinations = FileDestination::find()->where(['file_name' => $this->name])->all();
            if (!$destinations) {
                return $ad;
            }
            foreach ($destinations as $destination) {
                /* @var $destination FileDestination */
                $destination->is_deleted = $this->is_deleted;
                $destination->delete();
            }
        }
        return $ad;
    }

    /**
     * remove local files
     * @return boolean
     */
    public function removeLocalFile()
    {
        $fh = yii::$app->helper->file();
        if ($this->isImage) {
            foreach (self::getImageSizes() as $k => $v) {
                $thumb = $this->path . $k . $this->name;
                $fh->rm($thumb);
            }
        }
        return $fh->rm($this->path . $this->name);
    }

    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['name'];
    }

    /**
     *
     * @param string $fileNamePrefix
     * @return string
     */
    public function getUrl($fileNamePrefix = '', $alternative = null)
    {
        $url = $this->scheme . $this->host . $this->path . $fileNamePrefix . $this->name;
        $result = preg_replace('/\s+/', '', ($url == $fileNamePrefix ? null : $url));
        if ($result == '') {
            return $alternative;
        }
        if ($this->location === self::LOCATION_FTP) {
            $header_response = get_headers($result, 1);
            if (strpos($header_response[0], "404") !== false) {
                return $result;
            }
        } else {
            if ($this->getFileExists($fileNamePrefix)) {
                return $result;
            }
        }
        return $alternative;
    }

    /**
     *
     * @param string $fileNamePrefix
     * @return string
     */
    public function getSource($fileNamePrefix = '')
    {
        return preg_replace('/\s+/', '', yii::$app->helper->file()->getPath($this->path) . $fileNamePrefix . $this->name);
    }

    /**
     * check if file exists
     * @param string $fileNamePrefix
     */
    public function getFileExists($fileNamePrefix = '')
    {
        return is_file($this->getSource($fileNamePrefix));
    }

    /**
     * check if file is image
     * @param string | null $extensions null | 'jpg'
     * @return boolean
     */
    public function getIsImage($extension = null)
    {
        return in_array(($this->extension ? $this->extension : $extension), static::imageExtensions());
    }

    /**
     * render file
     * @param array $options
     * @return html
     */
    public function renderFile($options = [], $image = null)
    {
        return ($this->isImage || $image === true) ? $this->renderImage($options) : $this->getIcon($options);
    }

    /**
     * human readable file size
     * @return string
     */
    public function getFormattedSize()
    {
        return yii::$app->helper->file()->bytesToSize($this->size);
    }

    /**
     * sync files
     */
    public static function syncTableFiles()
    {
        self::updateAll(['status' => self::STATUS_DELETED], 'created_at<:d AND (SELECT COUNT(*) FROM ' . FileDestination::tableName() . ' WHERE file_name=name)=0', [
            'd' => strtotime('yesterday midnight')
        ]);
        if ($files = self::find()->where(['location' => self::LOCATION_LOCAL, 'status' => self::STATUS_UPLOADED])->limit(20)->all()) {
            foreach ($files as $f) {
                /* @var $f File */
                /* @var $f behaviors\FileFtpBehavior */
                if ($f->setInSync()) {
                    $f->uploadToFtp();
                    $f->setUploaded();
                }
            }
        }
        if ($dfiles = self::find(['status' => self::STATUS_DELETED])->limit(30)->all()) {
            foreach ($dfiles as $f) {
                if ($f->status == self::STATUS_DELETED) {
                    $f->delete();
                }
            }
        }
    }

    /**
     * retiurns url do downoad file
     * @return string
     */
    public function getDownloadUrl()
    {
        return Url::to(['/site/download', 'file' => $this->name]);
    }

    /**
     * retiurns link do downoad file
     * @return html
     */
    public function getDownloadLink($options = [])
    {
        $htmlOptions = array_merge(['icon' => 'download', 'data-pjax' => 0, 'target' => '_blank',], $options);
        return Html::a('', $this->downloadUrl, $htmlOptions);
    }

    /**
     * get default image extensions
     * @param boolean $asString
     * @return mixed
     */
    public static function imageExtensions($asString = false)
    {
        $ext = ['png', 'jpg', 'jpeg', 'gif', 'wbmp', 'xbm'];
        return $asString ? implode(',', $ext) : $ext;
    }

    /**
     *
     * @param array $options
     * @return type
     */
    public static function getDefaultNoImage($options = [])
    {
        $size = array_key_exists('size', $options) ? $options['size'] : File::SIZE_LG;
        if ($size == self::SIZE_ORIGINAL) {
            $size = '';
        }
        return Html::img(self::getDefaultNoImageUrl(), $options);
    }

    /**
     *
     * @param array $options
     * @return type
     */
    public static function getDefaultNoImageUrl($options = [])
    {
        $size = array_key_exists('size', $options) ? $options['size'] : File::SIZE_LG;
        if ($size == self::SIZE_ORIGINAL) {
            $size = '';
        }
        return '/public/img/' . $size . 'no-image.png';
    }

    /**
     * set destination
     * @param string $destination
     * @return boolean
     */
    public function addDestination($destination)
    {
        $model = new FileDestination();
        $model->destination = $destination;
        $model->file_name = $this->name;
        if ($model->validate()) {
            return $model->save();
        }
        return null;
    }

    /**
     * get file created date
     * @return type
     */
    public function getDate()
    {
        return yii::$app->formatter->asDatetime($this->created_at);
    }

    /**
     * get file sizes
     * @param array $condition
     * @return integer
     */
    public function getFilesSize($condition = [])
    {
        $s = self::find()->where($condition)->select('sum(size) as size')->one()->size;
        return (int) $s > 0 ? yii::$app->helper->file()->bytesToSize($s) : 0;
    }

    /**
     * download file
     * @return type
     */
    public function download()
    {
        if ($this->location == self::LOCATION_FTP) {
            header('Content-disposition: attachment; filename=' . $this->title);
            header('Content-type: ' . $this->mime);
            return readfile($this->url);
        }
        return yii::$app->response->sendFile($this->source, $this->title);
    }

}
