<?php

namespace app\models\base;

use yii\rbac\Item;
use app\models\File;
use yii\db\Expression;
use app\models\ChatContacts;
use yii\web\IdentityInterface;
use app\components\extend\yii;
use app\components\extend\Html;
use yii\base\NotSupportedException;
use app\models\settings\UserSettings;
use app\models\behaviors\FileSaveBehavior;
use app\modules\admin\components\rbac\ItemHelper;
use app\models\behaviors\user\UserPaymentSystemsBehavior;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string $avatar
 * @property UserPaymentSystemsBehavior $paymentStstem
 */
class BaseUser extends \app\components\extend\Model implements IdentityInterface
{

    /**
     * statuses
     */
    const STATUS_DELETED = 0;
    const STATUS_DISABLED = 1;
    const STATUS_ACTIVE = 10;

    /**
     * roles
     */
    const ROLE_USER = 1;
    const ROLE_ADMIN = 99;

    /**
     * scenarios
     */
    const SCENARIO_SIGN_UP = 'signup';
    const SCENARIO_PROFILE = 'profile';
    const SCENARIO_RESET_PASSWORD = 'resetPassword';

    /**
     * additional data attributes
     */
    const ADDTIONAL_DATA_SUPPORT_MANAGER = 'additional_data[support_manager]';
    const ADDTIONAL_DATA_USER_PAYMENT_SYSTEM = 'additional_data[payment_system]';

    public $rbacRole;

    /**
     * @param integer/boolean $status
     * @param boolean $withLiveEdit (return translated labels wrapped in html tag if TRUE)
     * @return type
     */
    public function getStatusLabels($status = false, $withLiveEdit = true)
    {
        $ar = [
            self::STATUS_ACTIVE => yii::$app->l->t('user active', ['update' => $withLiveEdit]),
            self::STATUS_DISABLED => yii::$app->l->t('user disabled', ['update' => $withLiveEdit]),
            self::STATUS_DELETED => yii::$app->l->t('user deleted', ['update' => $withLiveEdit]),
        ];
        return $status !== false ? $ar[$status] : $ar;
    }

    public $password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'settings' => [
                'class' => UserSettings::className(),
            ],
            'saveFiles' => [
                'class' => FileSaveBehavior::className(),
                'fileAttributes' => ['avatar']
            ],
            'paymentSystem' => [
                'class' => UserPaymentSystemsBehavior::className(),
            ],
        ]);
    }

    /**
     * user payment system behavior
     * @return UserPaymentSystemsBehavior
     */
    public function getPaymentStstem()
    {
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['avatar'], 'file', 'extensions' => File::imageExtensions(true), 'skipOnEmpty' => true, 'on' => self::SCENARIO_UPLOAD_FILE],
            [['username', 'email'], 'required'],
            [['password'], 'checkEmptyPassword', 'skipOnEmpty' => false],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key', 'password'], 'string', 'max' => 32],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['role', 'default', 'value' => self::ROLE_USER],
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'unique', 'targetClass' => 'app\models\User'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => 'app\models\User'],
            ['password', 'string', 'min' => 6],
            ['rbacRole', 'fakeRule'],
            [['avatar'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_PROFILE] = array_keys($this->attributeLabels(['status', 'role', 'rbacRole']));
        $scenarios[self::SCENARIO_RESET_PASSWORD] = ['password_reset_token', 'password'];
        $scenarios[self::SCENARIO_SIGN_UP] = array_keys($this->attributeLabels(['status', 'role']));
        $scenarios[UserPaymentSystemsBehavior::SCENARION_VALIDATE_UPS] = array_keys($this->attributeLabels(['additional_data']));
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels($except = false)
    {
        $ar = [
            'username' => yii::$app->l->t('username'),
            'password' => yii::$app->l->t('password'),
            'email' => yii::$app->l->t('email'),
            'avatar' => yii::$app->l->t('avatar'),
            'role' => yii::$app->l->t('role'),
            'rbacRole' => yii::$app->l->t('role'),
            'status' => yii::$app->l->t('status'),
            self::ADDTIONAL_DATA_SUPPORT_MANAGER => yii::$app->l->t('is support manager'),
        ];
        if ($except !== false && is_array($except))
            foreach ($except as $k)
                unset($ar[$k]);

        return array_merge(parent::attributeLabels(), $ar);
    }

    /* relations */

    public function getAvatarFile()
    {
        return $this->hasOne(File::className(), ['name' => 'avatar']);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(ChatContacts::className(), ['user_id' => 'id']);
    }

    /* relations end */

    /**
     * check empty password
     * @param type $attribute
     * @param type $param
     */
    public function checkEmptyPassword($attribute, $param)
    {
        if (($this->isNewRecord || !$this->password_hash || !$this->auth_key) && trim($this->$attribute) == '') {
            $this->addError($attribute, yii::t('yii', '{attribute} cannot be blank.', ['attribute' => $this->getAttributeLabel($attribute)]));
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @param boolean $activeOnly
     * @return static|null
     */
    public static function findByEmail($email, $activeOnly = true)
    {
        $q = static::find()->where(['email' => $email]);
        if ($activeOnly) {
            $q->andWhere(['status' => self::STATUS_ACTIVE]);
        }
        return $q->one();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * encode password
     */
    public function encodePassword()
    {
        if ($this->password && trim($this->password) != '') {
            $this->setPassword($this->password);
            $this->generateAuthKey();
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if ($this->role == self::ROLE_ADMIN) {
            return false;
        }
        return parent::beforeDelete();
    }

    /**
     *
     * @param type $insert
     * @return type
     */
    public function beforeSave($insert)
    {
        $this->encodePassword();
        if ($this->role == self::ROLE_ADMIN) {
            if ($this->scenario != self::SCENARIO_PROFILE && $this->scenario != self::SCENARIO_RESET_PASSWORD) {
                return false;
            }
        } else {
            $this->role = self::ROLE_USER;
        }
        return parent::beforeSave($insert);
    }

    /**
     * button that opens role asignment modal
     * @param array $options html attributes (yii format)
     * @return string
     */
    public function getRolesButton($options = [])
    {
        $item = new ItemHelper();
        $assignments = [];
        foreach ($item->getAssignments($this->primaryKey) as $k => $v) {
            $assignments[] = $k;
        }
        $options['title'] = yii::$app->l->t('roles', ['update' => false]);
        if (!array_key_exists('class', $options)) {
            $options['class'] = 'user-assignments-modal-button';
        } else {
            $options['class'] .= ' user-assignments-modal-button';
        }
        $options['onclick'] = 'showUserRolesModal($(this));return false;';
        $options['data'] = [
            'user-name' => $this->username,
            'user-id' => $this->primaryKey,
            'assignments' => $assignments,
        ];
        return Html::a(Html::ico('key'), '#', $options);
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->rbacRole = $this->assignedRolesArray();
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveRbacRoles();
    }

    public function saveRbacRoles()
    {
        if ((yii::$app->user->can('rbac-assignment') || $this->scenario == self::SCENARIO_SIGN_UP) && $this->rbacRole) {
            (new ItemHelper())->revokeAll($this->primaryKey);
            $roles = is_array($this->rbacRole) ? $this->rbacRole : [$this->rbacRole];
            foreach ($roles as $k => $v) {
                $item = new ItemHelper(['name' => $v]);
                $item->assign($item, (int) $this->primaryKey);
            }
        }
    }

    /**
     * check if user has certain rol
     * @param mixed $role string | array  ex: $this->hasRole('user') | $this->hasRole(['user','moderator'])
     * @return boolean
     */
    public function hasRole($role)
    {
        $roles = [];
        $item = new ItemHelper();
        $item->getRolesByUser($this->primaryKey);
        if (count($item->items) > 0) {
            foreach ($item->items[Item::TYPE_ROLE] as $k) {
                $roles[$k->name] = $k->name;
            }
        }
        if (is_array($role)) {
            return (count(array_intersect_key($roles, array_flip($role))) > 0);
        } else {
            return array_key_exists($role, $roles);
        }
    }

    /**
     * get roles assigned to this user
     * @return array
     */
    public function assignedRolesArray()
    {
        $tmp = [];
        $item = new ItemHelper();
        $item->getRolesByUser($this->primaryKey);
        if (count($item->items) > 0) {
            foreach ($item->items[Item::TYPE_ROLE] as $k) {
                $tmp[] = $k->name;
            }
        }
        return $tmp;
    }

    /**
     * get all titles of roles assigned to this user
     * @param array $options html attributes (yii format)
     * @return string html tag
     */
    public function assignedRoles($options = [])
    {
        $tmp = '';
        $item = new ItemHelper();
        $item->getRolesByUser($this->primaryKey);
        $elementClass = 'text-success';
        array_key_exists('class', $options) ? $elementClass = $options['class'] : $options['class'] = $elementClass;
        if (count($item->items) > 0) {
            foreach ($item->items[Item::TYPE_ROLE] as $k) {
                $options['data'] = ['name' => $k->name];
                $tmp .= Html::tag('div', $k->getTitle(), $options);
            }
        }
        return Html::tag('div', ($tmp === '' ? Html::tag('div', '', ['class' => 'text-danger']) : $tmp), [
            'class' => 'user-assigned-roles',
            'data' => [
                'user-id' => $this->primaryKey,
                'element' => 'div',
                'element-class' => $elementClass,
            ]
        ]);
    }

    /**
     * return available roles
     * @return array
     */
    public function getAvailableRoles()
    {
        $item = new ItemHelper();
        $item->getRoles();
        $ar = [];
        if (count($item->items) > 0) {
            foreach ($item->items[Item::TYPE_ROLE] as $k) {
                $ar[$k->name] = $k->getTitle();
            }
        }
        return $ar;
    }

    /**
     * get user full name
     * @return string
     */
    public function getFullName($alternative = 'guest')
    {
        $name = $this->username;
        if (!$name) {
            return $alternative;
        }
        return $name;
    }

    /**
     * render image
     * @param type $options
     * @return string
     */
    public function renderAvatar($options = [])
    {
        if (!array_key_exists('size', $options))
            $options['size'] = File::SIZE_SM;
        if (!array_key_exists('title', $options))
            $options['title'] = $this->username;
        if (!array_key_exists('alt', $options))
            $options['alt'] = $this->username;
        if ($image = $this->getFile('avatar'))
            return $image->renderImage($options);
    }

    /**
     *
     * @return \app\components\extend\ActiveQuery
     */
    public function getSupportManagers()
    {
        $q = self::find();
        $nj = $q->normalizeJsonConstants(self::tableName() . '.' . self::ADDTIONAL_DATA_SUPPORT_MANAGER);
        $q->where(['status' => self::STATUS_ACTIVE]);
        $q->andWhere($nj . '="1"');
        return $q;
    }

}
