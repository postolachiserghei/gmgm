<?php

namespace app\models\base;

use app\models\Seo;
use app\models\File;
use app\models\t\CategoriesT;
use app\components\extend\yii;
use app\models\behaviors\SeoBehavior;
use app\models\behaviors\TranslateModel;
use app\models\behaviors\SearchBehavior;
use app\models\behaviors\FileSaveBehavior;

/**
 * This is the model class for table "{{%categories}}".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $parent
 * @property integer $status
 * @property string $additional_data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order
 * @property integer $is_deleted
 *
 * @property CategoriesT[] $categoriesTs
 * @property Seo $seo
 */
class CategoriesBase extends \app\components\extend\Model
{

    /**
     * additional data constants
     */
    const ADDITIONAL_DATA_IMAGE = 'additional_data[image]';
    const ADDITIONAL_DATA_DESCRIPTION = 'additional_data[description]';

    /**
     * type constants
     */
    const TYPE_MAIN = 1;

    /**
     * status constants
     */
    const STATUS_ACTIVE = 1;
    const STATUS_ACTIVE_FALSE = 2;

    public $title;

    /**
     * @param integer $status
     * @param boolean $withLiveEdit (return translated labels wrapped in html tag if TRUE)
     * @return array/string
     */
    public function getStatusLabels($status = false, $withLiveEdit = true)
    {
        $ar = [
            self::STATUS_ACTIVE => yii::$app->l->t('item is active', ['update' => $withLiveEdit]),
            self::STATUS_ACTIVE_FALSE => yii::$app->l->t('item is not active', ['update' => $withLiveEdit]),
        ];
        return $status === false ? $ar : $ar[$status];
    }

    /**
     * @param integer $type
     * @param boolean $withLiveEdit (return translated labels wrapped in html tag if TRUE)
     * @return array/string
     */
    public function getTypeLabels($type = false, $withLiveEdit = true)
    {
        $ar = [
            static::TYPE_MAIN => yii::$app->l->t('main category', ['update' => $withLiveEdit]),
        ];

        return $type === false ? $ar : $ar[$type];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%categories}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return parent::behaviors() + [
            't' => [
                'class' => TranslateModel::className(),
                't' => new CategoriesT(),
                'fk' => 'category_id',
            ],
            'saveFiles' => [
                'class' => FileSaveBehavior::className(),
                'fileAttributes' => [self::ADDITIONAL_DATA_IMAGE]
            ],
            'search' => [
                'class' => SearchBehavior::className(),
                'writeSearchDataIf' => ['status' => self::STATUS_ACTIVE],
                'searchedAttributes' => [
                    'title', self::ADDITIONAL_DATA_DESCRIPTION
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['title'], 'required'],
            [[self::ADDITIONAL_DATA_IMAGE], 'file', 'extensions' => File::imageExtensions(true)],
            [['type', 'parent', 'status', 'created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'title' => yii::$app->l->t('title'),
            'type' => yii::$app->l->t('Category type'),
            'parent' => yii::$app->l->t('Category parent'),
            'status' => yii::$app->l->t('Category status'),
            'created_at' => yii::$app->l->t('Date created'),
            'updated_at' => yii::$app->l->t('Date updated'),
            'order' => yii::$app->l->t('Order position'),
            'is_deleted' => yii::$app->l->t('Is deleted'),
            'url' => yii::$app->l->t('url'),
            self::ADDITIONAL_DATA_IMAGE => yii::$app->l->t('image'),
            self::ADDITIONAL_DATA_DESCRIPTION => yii::$app->l->t('description'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriesTs()
    {
        return $this->hasMany(CategoriesT::className(), ['category_id' => 'id']);
    }

    /**
     * get menu item parent
     * @param mixed $noParent (returns alternate value in case of no parent)
     * @return mixed
     */
    public function getParent($noParent = null)
    {
        $parent = self::find()->where([
                    'id' => $this->parent
                ])->one();
        return (!$parent && $noParent) ? $noParent : $parent;
    }

    /**
     * return parent name
     */
    public function getParentName()
    {
        $parent = $this->getParent(yii::$app->l->t('root'));
        return is_string($parent) ? $parent : $parent->title;
    }

    /**
     * return children of the itemm
     * @return mixed
     */
    public function getChildren()
    {
        return self::find()->where([
                    'parent' => $this->primaryKey
                ])->all();
    }

    /**
     * delete item children
     */
    public function deleteChildren()
    {
        $children = $this->getChildren();
        if ($children) {
            foreach ($children as $c)
                $c->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        $this->deleteChildren();
    }

    /**
     * 
     * @param array $options
     * @param string $label
     * @return string
     */
    public function getLink($options = [], $label = null)
    {
        return $this->seo->getAliasLink($options, $label);
    }

    /**
     * render image
     * @param type $options
     * @return type
     */
    public function renderImage($options = [])
    {
        return $this->getFile(self::ADDITIONAL_DATA_IMAGE)->renderImage($options);
    }

}
