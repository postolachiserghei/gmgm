<?php

namespace app\models\base;

use app\models\File;
use app\models\User;
use app\models\Categories;
use app\models\t\ProductsT;
use app\components\extend\yii;
use app\models\behaviors\SeoBehavior;
use app\models\behaviors\TranslateModel;
use app\models\behaviors\SearchBehavior;
use app\models\behaviors\PaymentBehavior;
use app\models\settings\ProductsSettings;
use app\models\behaviors\FileSaveBehavior;

/**
 * This is the model class for table "{{%products}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $type
 * @property integer $available
 * @property string $price
 * @property integer $status
 * @property string $additional_data
 * @property string $currencySign
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $order
 * @property integer $is_deleted
 * @property integer $owner_id
 * @property string $encodedData
 *
 * @property Categories $category
 * @property User[] $user
 * @property ProductsT[] $productsTs
 */
class BaseProducts extends \app\components\extend\Model
{

    public $image;
    public $title;
    public $description;

    /**
     * status constants
     */
    const STATUS_ACTIVE = 1;
    const STATUS_ACTIVE_FALSE = 0;

    /**
     * @param integer $status
     * @param boolean $withLiveEdit (return translated labels wrapped in html tag if TRUE)
     * @return array/string
     */
    public function getStatusLabels($status = false, $withLiveEdit = true)
    {
        $ar = [
            self::STATUS_ACTIVE => yii::$app->l->t('item is active', ['update' => $withLiveEdit]),
            self::STATUS_ACTIVE_FALSE => yii::$app->l->t('item is not active', ['update' => $withLiveEdit]),
        ];
        return $status === false ? $ar : $ar[$status];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            't' => [
                'class' => TranslateModel::className(),
                't' => new ProductsT(),
                'fk' => 'id',
            ],
            'seo' => [
                'class' => SeoBehavior::className(),
                'actionUrl' => '/products/view?id={id}'
            ],
            'saveFiles' => [
                'class' => FileSaveBehavior::className(),
                'fileAttributes' => ['image']
            ],
            'settings' => [
                'class' => ProductsSettings::className(),
            ],
            'payment' => [
                'class' => PaymentBehavior::className(),
            ],
            'search' => [
                'class' => SearchBehavior::className(),
                'searchedAttributes' => [
                    'title', 'description'
                ],
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['image'], 'file', 'extensions' => File::imageExtensions(true)],
            [['category_id', 'type', 'available', 'status', 'created_at', 'updated_at', 'order', 'is_deleted'], 'integer'],
            [['price', 'available'], 'required'],
            [['available'], 'number', 'min' => 1],
            [['price'], 'number', 'min' => 0.01],
            [['image', 'title', 'description'], 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'id' => yii::$app->l->t('id'),
            'category_id' => yii::$app->l->t('category'),
            'type' => yii::$app->l->t('type'),
            'available' => yii::$app->l->t('available items'),
            'price' => yii::$app->l->t('price'),
            'status' => yii::$app->l->t('status'),
            'created_at' => yii::$app->l->t('Date created'),
            'updated_at' => yii::$app->l->t('Date updated'),
            'order' => yii::$app->l->t('Order position'),
            'is_deleted' => yii::$app->l->t('Is deleted'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductsT::className(), ['id' => 'id']);
    }

    public function getAvailableCategories()
    {

    }

    /**
     * get price per unit
     * @param type $asString
     * @return type
     */
    public function getPrice($asString = true)
    {
        $price = $this->price;
        if ($asString !== true) {
            return ($this->price + 0);
        }
        return yii::$app->formatter->asCurrency($price, $this->currency);
    }

    public function getCurrency()
    {
        $transaction = $this->transaction;
        /* @var $transaction \app\models\Transactions */
        return $transaction->getSetting('systemCurrency', $transaction::CURRENCY_USD);
    }

    /**
     * currency sign
     * @return string
     */
    public function getCurrencySign($currency = null)
    {
        if (!$currency) {
            $currency = $this->getCurrency();
        }
        $transaction = $this->transaction;
        /* @var $transaction \app\models\Transactions */
        return $transaction->getCurrenciesLabels($currency);
    }

    /**
     * get available items
     * @return type
     */
    public function getAvailable()
    {
        return $this->available;
    }

    /**
     * render image
     * @param array $options
     * @return string
     */
    public function renderImage($options = [])
    {
        if (!isset($options['size'])) {
            $options['size'] = File::SIZE_SM;
        }
        return $this->getFile('image')->renderImage($options);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $bs = parent::beforeSave($insert);
        if ($this->owner_id === null) {
            $this->owner_id = yii::$app->user->id;
        }
        return $bs;
    }

}
