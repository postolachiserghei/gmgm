<?php

namespace app\models;

use app\models\File;
use app\models\base\BaseUser;
use app\components\extend\yii;
use app\models\behaviors\FileSaveBehavior;

/**
 * User model
 */
class User extends BaseUser
{

    const ROLE_SITE_USER = 'user';
    const ROLE_SITE_STORE = 'store';
    const ADDITIONAL_DATA_PHONE = 'additional_data[phone]';
    const ADDITIONAL_DATA_FIO = 'additional_data[fio]';
    const ADDITIONAL_DATA_COMPANY_LOGO = 'additional_data[company_logo]';
    const ADDITIONAL_DATA_COMPANY_INFO = 'additional_data[company_info]';

    public $rbacRole;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $b = parent::behaviors();
        $b['saveFiles'] = [
            'class' => FileSaveBehavior::className(),
            'fileAttributes' => ['avatar', self::ADDITIONAL_DATA_COMPANY_LOGO]
        ];
        return $b;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $r = parent::rules();
        $r[] = [[self::ADDITIONAL_DATA_COMPANY_LOGO], 'file', 'extensions' => File::imageExtensions(true), 'skipOnEmpty' => true, 'on' => self::SCENARIO_UPLOAD_FILE];
        return $r;
    }

    /**
     * get user full name
     * @return string
     */
    public function getFullName($alternative = null)
    {
        $n = $this->getData(self::ADDITIONAL_DATA_FIO);
        if (trim($n) != '') {
            return $n;
        }
        return $this->username;
    }

    /**
     *
     * @param type $except
     * @return type
     */
    public function attributeLabels($except = false)
    {
        $ar = parent::attributeLabels($except);
        $ar[self::ADDITIONAL_DATA_FIO] = yii::$app->l->t('fio');
        $ar[self::ADDITIONAL_DATA_PHONE] = yii::$app->l->t('phone');
        $ar[self::ADDITIONAL_DATA_COMPANY_LOGO] = yii::$app->l->t('company logo');
        $ar[self::ADDITIONAL_DATA_COMPANY_INFO] = yii::$app->l->t('company info');
        return $ar;
    }

    /**
     * render image
     * @param type $options
     * @return string
     */
    public function renderLogo($options = [])
    {
        if (!array_key_exists('size', $options))
            $options['size'] = File::SIZE_SM;
        if (!array_key_exists('title', $options))
            $options['title'] = $this->username;
        if (!array_key_exists('alt', $options))
            $options['alt'] = $this->getData(self::ADDITIONAL_DATA_COMPANY_INFO);
        if ($image = $this->getFile(self::ADDITIONAL_DATA_COMPANY_LOGO))
            return $image->renderImage($options);
    }

}
