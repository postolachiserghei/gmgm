<?php
/* @var $this yii\web\View */
/* @var $helper app\components\helper\Helper */
$this->title = 'TEST';

use app\components\extend\Html;

$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('Hello and wellcome to {app}', [
                    'app' => yii::$app->name
        ]));
?>

<?= YII_ENV_DEV ? Html::tag('div', 'THIS is themed view file, see location : ' . $this->viewFile, ['class' => 'alert alert-warning']) : ''; ?>

<div class="panel panel-info">
    <div class="panel-heading">
        Lorem ipsum dolor sit amet
    </div>
    <div class="panel-body">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </div>
</div>

<div class="panel panel-success">
    <div class="panel-heading">
        Lorem ipsum dolor sit amet
    </div>
    <div class="panel-body">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </div>
</div>

<div class="panel panel-warning">
    <div class="panel-heading">
        Lorem ipsum dolor sit amet
    </div>
    <div class="panel-body">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </div>
</div>

<div class="panel panel-danger">
    <div class="panel-heading">
        Lorem ipsum dolor sit amet
    </div>
    <div class="panel-body">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </div>
</div>