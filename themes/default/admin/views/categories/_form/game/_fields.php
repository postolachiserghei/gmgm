<?php

use app\components\extend\Url;
use app\components\extend\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */
/* @var $form app\components\extend\ActiveForm */
/* @var $servers[] */
?>

<?= $form->field($model, $model::ADDITIONAL_DATA_SHOW_IN_TOP)->checkbox(['label' => false])->label($model->getAttributeLabel($model::ADDITIONAL_DATA_SHOW_IN_TOP)); ?>
<?= $form->field($model, 'order')->textInput(['type' => 'number']); ?>
<?= $form->field($model, 'servers_list')->checkboxList($servers)->label(yii::$app->l->t('servers')); ?>
<?=
count($servers) === 0 ? Html::tag('div', yii::$app->l->t('you need to {create_link} some servers before attaching them to the game', [
    'create_link' => Html::a(yii::$app->l->t('add', ['lcf' => true]), ['/admin/servers/create'])
]), ['class' => 'alert alert-warning']) : ''
?>
<div class="panel panel-info">
    <div class="panel-heading">
        <?= $model->getAttributeLabel($model::ADDITIONAL_DATA_LOT_TYPES); ?>
    </div>
    <div class="panel-body">
        <?php
        if ($lotTypes = $model->getActiveLottypes(false)) {

            foreach ($lotTypes as $k => $lotType) {
                ?>
                <div class="row">
                    <div class="col-md-6">
                        <?=
                        $form->field($model->t, $model::ADDITIONAL_DATA_LOT_TYPES . "[$k]")->checkbox([
                            'label' => false,
                            'value' => ($model->getData($model::ADDITIONAL_DATA_LOT_TYPES . "[$k]") == 1),
                        ])->label($lotType->title);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?=
                        $form->field($model->t, $model::ADDITIONAL_DATA_LOT_LIMIT . "[$k]")->input('number', [
                            'placeholder' => $model->getAttributeLabel($model::ADDITIONAL_DATA_LOT_LIMIT),
                            'value' => ($model->isNewRecord ? 10 : $model->getData($model::ADDITIONAL_DATA_LOT_LIMIT . "[$k]"))
                        ]);
                        ?>
                    </div>
                </div>
                <hr/>
                <?php
            }
        } else {
            echo Html::tag('div', yii::$app->l->t('you have to create some lot types or enable them {lot-types-link}', [
                'lot-types-link' => Html::a(yii::$app->l->t('lot types'), Url::to([
                    '/admin/lottypes/index'
                ]))
            ]));
        }
        ?>
    </div>
</div>
