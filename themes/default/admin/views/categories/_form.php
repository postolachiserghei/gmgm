<?php

use app\components\extend\Url;
use app\components\extend\Html;
use app\components\extend\ActiveForm;
use app\components\widgets\redactor\RedactorWidget;
use app\components\widgets\uploader\UploaderWidget;
use app\modules\admin\components\widgets\categories\CategoriesTreeWidget;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\models\Categories */
/* @var $model \app\models\behaviors\TranslateModel */
?>

<div class="Categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= Html::tag('div', $model->getTButtons(), ['class' => 'text-right']); ?>

    <div class="row">
        <div class="col-lg-<?= $model->type == $model::TYPE_GAME ? '8' : '12' ?>">
            <?=
            $form->field($model, 'type')->dropDownList($model->getTypeLabels(false, false), [
                'class' => 'form-control hidden ',
                'onchange' => 'CategoriesTreeWidget.reloadJsTreeByType($(this),$(this).data());return false;',
                'data' => [
                    'url' => Url::to([yii::$app->controller->action->id, 'id' => $model->primaryKey]),
                    'id' => $model->primaryKey,
                ]
            ]);
            ?>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, $model::ADDITIONAL_DATA_IMAGE)->widget(UploaderWidget::className()); ?>
                </div>
                <div class="col-md-8">
                    <?= $form->field($model, 'title')->textInput(); ?>
                    <div class="row">
                        <?=
                        $form->field($model->seo, 'alias', [
                            'template' => '<div class="col-lg-6">'
                            . '<div class="input-group">
                                <span class="input-group-btn" style="font-size:1em">
                                  <button class="btn btn-default" type="button">/' . $model->seo->l . '</button>
                                </span>
                                {input}
                            </div><!-- /input-group -->'
                            . '</div>' . '<div class="col-lg-6">
                            ' . $form->field($model->seo, 'title')->textInput() . '
                        </div>'
                        ])->textInput([
                            'value' => (!$model->seo->alias ? '/' : $model->seo->alias)
                        ])
                        ?>

                        <div class="col-lg-6">
                            <?= $form->field($model->seo, 'keywords')->textarea(['rows' => 5]) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model->seo, 'description')->textarea(['rows' => 5]) ?>
                        </div>
                    </div>
                </div>
            </div>

            <?=
            $model->type == $model::TYPE_GAME ? $this->render('_form/game/_fields', [
                'model' => $model,
                'servers' => $servers,
                'form' => $form,
            ]) : ''
            ?>
            <?= $form->field($model, $model::ADDITIONAL_DATA_DESCRIPTION)->widget(RedactorWidget::className()); ?>
        </div>
        <div class="col-lg-4 <?= ($model->type == $model::TYPE_MAIN ? 'hidden' : '') ?>  ">
            <?=
            $form->field($model, 'parent')->hiddenInput([
                'value' => 0
            ]);
            ?>
            <?= Html::label($model->getTypeLabels($parentType)); ?>
            <?= $model->type == $model::TYPE_GAME ? CategoriesTreeWidget::widget(['view' => 'form', 'type' => $parentType, 'model' => $model]) : '' ?>
        </div>
    </div>
    <?= $form->field($model, 'status')->radioList($model->getStatusLabels()); ?>
    <hr/>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? yii::$app->l->t('Create') : yii::$app->l->t('Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
