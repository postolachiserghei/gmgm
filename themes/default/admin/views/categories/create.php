<?php

use app\components\extend\Html;
use app\components\extend\Nav;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */


$this->title = yii::$app->l->t('create', ['update' => false]);
$this->params['breadcrumbs'][] = ['label' => $model->getTypeLabels($model->type), 'url' => ['index', 'type' => $model->type]];
$this->params['breadcrumbs'][] = yii::$app->l->t('create');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('create '.$model->getTypeLabels($model->type)));
$this->params['menu'] = Nav::CrudActions($model);
?>
<div class="Categories-create">
    <?=
    $this->render('_form', [
        'model' => $model,
        'servers' => $servers,
        'parentType' => $parentType
    ])
    ?>

</div>
