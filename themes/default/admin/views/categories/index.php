<?php

use app\models\File;
use app\components\extend\Nav;
use app\components\extend\Url;
use app\components\extend\Html;
use app\components\extend\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CategoriesSearch */
/* @var $dataProvider app\components\extend\ActiveDataProvider */
/* @var $model \app\models\Categories */



$this->title = yii::$app->l->t('Manage ' . $model->getTypeLabels(($model->type ? $model->type : $model::TYPE_MAIN), false), ['update' => false]);
$this->params['breadcrumbs'][] = $model->getTypeLabels(($model->type ? $model->type : $model::TYPE_MAIN));
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('Manage ' . $model->getTypeLabels(($model->type ? $model->type : $model::TYPE_MAIN))));
$this->params['menu'] = Nav::CrudActions($model, [
    'create' => [
        'label' => Html::ico(Nav::$icons['create']),
        'url' => Url::to(['create', 'type' => yii::$app->request->get('type')])
    ]
], ['create']);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="btn-group">
            <?php foreach ($model->getTypeLabels() as $t => $l): ?>
                <?=
                Html::a($l, Url::to(['index', 'type' => $t]), [
                    'class' => 'btn btn-default btn-default ' . ($t == $type ? 'active' : '')
                ]);
                ?>
            <?php endforeach; ?>
        </div>
        <br/>
        <br/>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                GridView::checkboxColumn(),
                [
                    'attribute' => $model::ADDITIONAL_DATA_IMAGE,
                    'filter' => false,
                    'value' => function($model) {
                        return $model->renderImage(['width' => '75']);
                    },
                ],
                [
                    'attribute' => 'title',
                ],
                [
                    'attribute' => 'order',
                ],
                [
                    'attribute' => 'status',
                    'value' => function($model) {
                        return $model->getStatusLabels($model->status);
                    },
                    'filter' => $model->getStatusLabels(),
                    'filterInputOptions' => GridView::defaultOptionsForFilterDropdown(),
                ],
                [
                    'class' => 'app\components\extend\ActionColumn',
                    'template' => Html::tag('div', '{view} ' . (yii::$app->l->multi ? '{TButtons}' : '{update}') . '  {delete}', ['class' => 'grid-view-actions']),
                    'buttons' => GridView::DefaultActions($model, [
                        'TButtons' => function($url, $model) {
                            return $model->getTButtons();
                        }
                    ]),
                ],
            ]
        ]);
        ?>
    </div>
</div>
