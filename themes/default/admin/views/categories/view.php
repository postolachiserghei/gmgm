<?php

use app\models\File;
use app\components\extend\Url;
use app\components\extend\Nav;
use app\components\extend\Html;
use app\components\extend\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */


$this->title = yii::$app->l->t('view', ['update' => false]);
$this->params['breadcrumbs'][] = ['label' => $model->getTypeLabels($model->type), 'url' => ['index', 'type' => $model->type]];
$this->params['breadcrumbs'][] = yii::$app->l->t('view');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('View ' . $model->getTypeLabels($model->type)));
$this->params['menu'] = Nav::CrudActions($model, [
    'create' => [
        'label' => Html::ico(Nav::$icons['create']),
        'url' => Url::to(['create', 'type' => $model->type])
    ]
], ['create']);
?>
<div class="Categories-view">

    <?php
    $attributes = [
        'type' => [
            'attribute' => 'type',
            'value' => $model->getTypeLabels($model->type)
        ],
        'labels' => [
            [
                'label' => yii::$app->l->t('translations'),
                'format' => 'raw',
                'value' => $model->getTButtons(),
                'visible' => yii::$app->l->multi
            ]
        ],
        'parent' => [
            'attribute' => 'parent',
            'value' => $model->getParentName(),
        ],
        $model::ADDITIONAL_DATA_IMAGE => [
            'format' => 'html',
            'attribute' => $model::ADDITIONAL_DATA_IMAGE,
            'value' => $model->renderImage(['size' => File::SIZE_MD]),
        ],
        $model::ADDITIONAL_DATA_DESCRIPTION => [
            'format' => 'html',
            'attribute' => $model::ADDITIONAL_DATA_DESCRIPTION,
        ],
        'status' => [
            'attribute' => 'status',
            'value' => $model->getStatusLabels($model->status),
        ],
        'url' => [
            'format' => 'raw',
            'attribute' => 'url',
            'value' => $model->getLink(),
        ]
    ];

    if ($model->type == $model::TYPE_GAME) {
        $attributes = array_merge($attributes, [
            $model::ADDITIONAL_DATA_LOT_TYPES => [
                'attribute' => $model::ADDITIONAL_DATA_LOT_TYPES,
                'value' => $model->getLotTypesList(true)
            ],
            $model::ADDITIONAL_DATA_SERVERS => [
                'attribute' => $model::ADDITIONAL_DATA_SERVERS,
                'value' => implode(', ', $model->getAvailableServers($model->getData($model::ADDITIONAL_DATA_SERVERS)))
            ]
        ]);
    }
    echo DetailView::widget([
        'model' => $model,
        'attributes' => DetailView::DefaultAttributes($model, $attributes, [$model::ADDITIONAL_DATA_LOT_LIMIT]),
    ])
    ?>

</div>
