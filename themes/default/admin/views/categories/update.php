<?php

use app\components\extend\Url;
use app\components\extend\Html;
use app\components\extend\Nav;
/* @var $this yii\web\View */
/* @var $model app\models\Categories */


$this->title = yii::$app->l->t('update', ['update' => false]);
$this->params['breadcrumbs'][] = ['label' => $model->getTypeLabels($model->type), 'url' => ['index', 'type' => $model->type]];
$this->params['breadcrumbs'][] = yii::$app->l->t('update');
$this->params['pageHeader'] = Html::tag('h1', yii::$app->l->t('Update '.$model->getTypeLabels($model->type)));
$this->params['menu'] = Nav::CrudActions($model, [
            'create' => [
                'label' => Html::ico(Nav::$icons['create']),
                'url' => Url::to(['create', 'type' => $model->type])
            ]
                ], ['delete_all', 'create'], true);
?>
<div class="Categories-update">
    <?=
    $this->render('_form', [
        'model' => $model,
        'servers' => $servers,
        'parentType' => $parentType
    ])
    ?>
</div>
