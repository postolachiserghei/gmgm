<?php
/* @var $this yii\web\View */
/* @var $model app\models\behaviors\FileSaveBehavior */
/* @var $model app\models\User */
$this->title = 'TEST';
$this->params['breadcrumbs'][] = $model->getFullName();
?>


<div class="col-xs-12 col-md-3">
    <div class="sp-buyerinfo-wrap">
        <div class="sp-buyerinfo-top-box">
            <div class="sp-buyerinfo-top-avatar">
                <div class="user-avatar" style="background-image: url(<?= $model->getFile('avatar')->getUrl(null, '/public/gmgm/img/user.png'); ?>);"></div>
            </div>
            <div class="sp-buyerinfo-top-info">
                <div class="sp-buyerinfo-top-infoline">
                    <strong>
                        <?= $model->getFullName(); ?>
                    </strong>
                </div>
                <div class="sp-buyerinfo-top-infoline">
                    <span>
                        Сделок: 14
                    </span>
                </div>
            </div>
        </div>
        <div class="sp-buyerinfo-bottom-info">
            <?= yii::$app->l->t('Зарегестрирован') ?> <?= yii::$app->formatter->asDate($model->created_at); ?>
        </div>
    </div>
</div>
<div class="col-xs-12 col-md-9">
    <div class="sp-review-wrap">
        <div class="row">
            <div class="col-xs-12">
                <div class="mp-sect-title">
                    <h2>
                        Отзывы о покупателе
                    </h2>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="mp-substore-review-item">
                    <div class="mp-substore-review-text __noava">
                        <div class="mp-substore-review-title">
                            <span>
                                Игра:
                            </span>
                            <a href="#">
                                Линейка
                            </a>
                            <span>
                                Куплено:
                            </span>
                            <strong>
                                Fuse Boost
                            </strong>
                        </div>
                        <div class="mp-substore-review-info">
                            <span>
                                Продавец:
                            </span>
                            <a href="#">
                                Nickman
                            </a>
                            <span class="saler-rates-wrap">
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-empty"></i>
                                <i class="icn icn-star-empty"></i>
                            </span>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="mp-substore-review-item">
                    <div class="mp-substore-review-text __noava">
                        <div class="mp-substore-review-title">
                            <span>
                                Игра:
                            </span>
                            <a href="#">
                                Линейка
                            </a>
                            <span>
                                Куплено:
                            </span>
                            <strong>
                                Fuse Boost
                            </strong>
                        </div>
                        <div class="mp-substore-review-info">
                            <span>
                                Продавец:
                            </span>
                            <a href="#">
                                Nickman
                            </a>
                            <span class="saler-rates-wrap">
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-empty"></i>
                                <i class="icn icn-star-empty"></i>
                            </span>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="mp-substore-review-item">
                    <div class="mp-substore-review-text __noava">
                        <div class="mp-substore-review-title">
                            <span>
                                Игра:
                            </span>
                            <a href="#">
                                Линейка
                            </a>
                            <span>
                                Куплено:
                            </span>
                            <strong>
                                Fuse Boost
                            </strong>
                        </div>
                        <div class="mp-substore-review-info">
                            <span>
                                Продавец:
                            </span>
                            <a href="#">
                                Nickman
                            </a>
                            <span class="saler-rates-wrap">
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-empty"></i>
                                <i class="icn icn-star-empty"></i>
                            </span>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="mp-substore-review-item">
                    <div class="mp-substore-review-text __noava">
                        <div class="mp-substore-review-title">
                            <span>
                                Игра:
                            </span>
                            <a href="#">
                                Линейка
                            </a>
                            <span>
                                Куплено:
                            </span>
                            <strong>
                                Fuse Boost
                            </strong>
                        </div>
                        <div class="mp-substore-review-info">
                            <span>
                                Продавец:
                            </span>
                            <a href="#">
                                Nickman
                            </a>
                            <span class="saler-rates-wrap">
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-empty"></i>
                                <i class="icn icn-star-empty"></i>
                            </span>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="mp-substore-review-item">
                    <div class="mp-substore-review-text __noava">
                        <div class="mp-substore-review-title">
                            <span>
                                Игра:
                            </span>
                            <a href="#">
                                Линейка
                            </a>
                            <span>
                                Куплено:
                            </span>
                            <strong>
                                Fuse Boost
                            </strong>
                        </div>
                        <div class="mp-substore-review-info">
                            <span>
                                Продавец:
                            </span>
                            <a href="#">
                                Nickman
                            </a>
                            <span class="saler-rates-wrap">
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-empty"></i>
                                <i class="icn icn-star-empty"></i>
                            </span>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="mp-substore-review-item">
                    <div class="mp-substore-review-text __noava">
                        <div class="mp-substore-review-title">
                            <span>
                                Игра:
                            </span>
                            <a href="#">
                                Линейка
                            </a>
                            <span>
                                Куплено:
                            </span>
                            <strong>
                                Fuse Boost
                            </strong>
                        </div>
                        <div class="mp-substore-review-info">
                            <span>
                                Продавец:
                            </span>
                            <a href="#">
                                Nickman
                            </a>
                            <span class="saler-rates-wrap">
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-empty"></i>
                                <i class="icn icn-star-empty"></i>
                            </span>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="mp-substore-review-item">
                    <div class="mp-substore-review-text __noava">
                        <div class="mp-substore-review-title">
                            <span>
                                Игра:
                            </span>
                            <a href="#">
                                Линейка
                            </a>
                            <span>
                                Куплено:
                            </span>
                            <strong>
                                Fuse Boost
                            </strong>
                        </div>
                        <div class="mp-substore-review-info">
                            <span>
                                Продавец:
                            </span>
                            <a href="#">
                                Nickman
                            </a>
                            <span class="saler-rates-wrap">
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-empty"></i>
                                <i class="icn icn-star-empty"></i>
                            </span>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="mp-substore-review-item">
                    <div class="mp-substore-review-text __noava">
                        <div class="mp-substore-review-title">
                            <span>
                                Игра:
                            </span>
                            <a href="#">
                                Линейка
                            </a>
                            <span>
                                Куплено:
                            </span>
                            <strong>
                                Fuse Boost
                            </strong>
                        </div>
                        <div class="mp-substore-review-info">
                            <span>
                                Продавец:
                            </span>
                            <a href="#">
                                Nickman
                            </a>
                            <span class="saler-rates-wrap">
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-empty"></i>
                                <i class="icn icn-star-empty"></i>
                            </span>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="mp-substore-review-item">
                    <div class="mp-substore-review-text __noava">
                        <div class="mp-substore-review-title">
                            <span>
                                Игра:
                            </span>
                            <a href="#">
                                Линейка
                            </a>
                            <span>
                                Куплено:
                            </span>
                            <strong>
                                Fuse Boost
                            </strong>
                        </div>
                        <div class="mp-substore-review-info">
                            <span>
                                Продавец:
                            </span>
                            <a href="#">
                                Nickman
                            </a>
                            <span class="saler-rates-wrap">
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-empty"></i>
                                <i class="icn icn-star-empty"></i>
                            </span>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="mp-substore-review-item">
                    <div class="mp-substore-review-text __noava">
                        <div class="mp-substore-review-title">
                            <span>
                                Игра:
                            </span>
                            <a href="#">
                                Линейка
                            </a>
                            <span>
                                Куплено:
                            </span>
                            <strong>
                                Fuse Boost
                            </strong>
                        </div>
                        <div class="mp-substore-review-info">
                            <span>
                                Продавец:
                            </span>
                            <a href="#">
                                Nickman
                            </a>
                            <span class="saler-rates-wrap">
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-full"></i>
                                <i class="icn icn-star-empty"></i>
                                <i class="icn icn-star-empty"></i>
                            </span>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="pagination-wrap">
            <ul>
                <li>
                    <a href="#">
                        <span class="icn icn-arrow-left"></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        1
                    </a>
                </li>
                <li>
                    <a href="#">
                        2
                    </a>
                </li>
                <li>
                    <a href="#">
                        3
                    </a>
                </li>
                <li>
                    <a href="#">
                        4
                    </a>
                </li>
                <li>
                    <a href="#">
                        5
                    </a>
                </li>
                <li>
                    <a href="#" class="active">
                        6
                    </a>
                </li>
                <li>
                    <a href="#">
                        7
                    </a>
                </li>
                <li>
                    <a href="#">
                        8
                    </a>
                </li>
                <li>
                    <a href="#">
                        9
                    </a>
                </li>
                <li>
                    <a href="#">
                        10
                    </a>
                </li>
                <li>
                    ...
                </li>
                <li>
                    <a href="#">
                        320
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="icn icn-arrow-right"></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
