<?php

use app\components\extend\yii;

/* @var $model app\models\User */
/* @var $lot \app\models\search\ProductsSearch */

$this->title = yii::$app->l->t('profile', ['update' => false]);
$this->params['breadcrumbs'][] = yii::$app->l->t('profile');
$view = $model->hasRole($model::ROLE_SITE_STORE) ? $model::ROLE_SITE_STORE : $model::ROLE_SITE_USER;
?>


<?=

$this->render('index/' . $view, [
    'model' => $model,
    'lot' => $lot,
    'tab' => $tab
]);
?>