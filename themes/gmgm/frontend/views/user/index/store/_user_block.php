<?php

use app\components\extend\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
?>

<div class="sp-salerinfo-wrap">
    <div class="sp-salerinfo-top-box">
        <div class="sp-salerinfo-top-avatar">
            <?= $model->renderAvatar(); ?>
        </div>
        <div class="sp-salerinfo-top-info">
            <div class="sp-salerinfo-top-infoline">
                <strong>
                    <?= $model->fullName; ?>
                </strong>
            </div>
            <div class="sp-salerinfo-top-infoline">
                <span>
                    Уровень: 2 <i class="icn icn-level-2"></i>
                </span>
            </div>
            <div class="sp-salerinfo-top-infoline">
                <span>
                    Сделок: 14
                </span>
            </div>
            <div class="sp-salerinfo-top-infoline">
                <span>
                    Лотов: 18
                </span>
            </div>
            <div class="sp-salerinfo-top-infoline">
                <span class="__alert">
                    Жалоб: 5
                </span>
            </div>
        </div>
    </div>
    <?php
    if ($model->primaryKey != yii::$app->user->id) {
        ?>
        <div class="sp-salerinfo-bottom-info">
            <a href="#" class="sp-salerinfo-top-fav">
                <?= yii::$app->l->t('В избранное') ?> <i class="icn icn-star-full"></i>
            </a>
        </div>
        <?php
    }
    ?>
</div>