<?php

use app\components\extend\Html;

/* @var $model \app\models\Products */
?>
<div class="row">
    <div class="col-md-3">
        <div class="gamepage-lots-info">
            <div class="gamepage-lots-info-title">
                <span>
                    <?= $model->title; ?>
                </span>
            </div>
            <div class="gamepage-lots-info-box" style="width: 100px">
                <?= $model->renderImage(); ?>
                <div class="gamepage-lots-info-text">
                    <span>
                        <?= $model->category->title ?>
                    </span>
                    <br>
                    <span>
                        <?= $model->getLotType()->title ?>
                    </span>
                </div>
            </div>
            <a class="gamepage-lots-info-showmore" onclick="$('.product-description_<?= $model->id ?>').slideToggle()">
                <i class="icn icn-arrow-down"></i><?= yii::$app->l->t('Show description') ?>
            </a>
        </div>
    </div>
    <div class="col-md-3">
        <div class="gamepage-lots-delivery-box">
            <?= $model->getData($model::ADDITIONAL_DATA_DELIVERY_IN_DAYS); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="gamepage-lots-value-box">
            <small>
                <?= yii::$app->l->t('items available') ?>:
            </small>
            <br>
            <strong>
                <?= $model->getAvailable(); ?>
            </strong>
        </div>
    </div>
    <div class="col-md-3">
        <div class="gamepage-lots-price-box">
            <span class="gamepage-lots-price-value">
                <?= $model->getPrice(true) ?>
            </span>
            <br>
            <button>
                <?=
                Html::a(yii::$app->l->t('update'), ['user/index', 'tab' => 'lot', 'update' => $model->id], [
                    'class' => 'text-white'
                ])
                ?>
            </button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div style="display: none" class="text-center product-description_<?= $model->id ?>">
            <?= $model->description ?>
        </div>
    </div>
</div>
