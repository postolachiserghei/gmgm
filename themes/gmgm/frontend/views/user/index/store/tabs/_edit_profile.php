<?php

use app\components\extend\Html;
use app\components\extend\ActiveForm;
use app\components\widgets\uploader\UploaderWidget;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $model app\models\behaviors\user\UserPaymentSystemsBehavior */
/* @var $form yii\widgets\ActiveForm */
//$pp = new \app\models\behaviors\payment\PayPal();
//$pp->validateAccount('postolachiserghei-facilitator@yandex.com');
//die();
?>
<div class="spv-edittab-wrap">
    <div class="mp-sect-title">
        <?= Html::tag('h2', yii::$app->l->t('Редактирование профиля')) ?>
    </div>
    <div class="row">
        <div class="colx-xs-12 col-sm-6">
            <?php $form = ActiveForm::begin(); ?>
            <div class="spv-edittab-box">
                <div class="form-group __files">
                    <?=
                    $form->field($model, $model::ADDITIONAL_DATA_COMPANY_LOGO)->widget(UploaderWidget::className(), [
                        'template' => UploaderWidget::TEMPLATE_COMPANY_LOGO
                    ])
                    ?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, $model::ADDITIONAL_DATA_FIO)->textInput(); ?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, $model::ADDITIONAL_DATA_PHONE)->textInput(); ?>
                </div>
                <div class="form-group">
                    <?= $model->hasRole([$model::ROLE_SITE_STORE]) ? $form->field($model, $model::ADDITIONAL_DATA_COMPANY_INFO)->textarea() : ''; ?>
                </div>
                <div class="form-group __ctrl">
                    <?= Html::submitButton(yii::$app->l->t('save'), ['class' => 'button __small']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="colx-xs-12 col-sm-6">
            <?php $form = ActiveForm::begin(); ?>
            <div class="spv-edittab-box text-center">
                <div class="spv-edittab-label">
                    <?= yii::$app->l->t('Изменить счет') ?>
                </div>

                <?php foreach ($model->paymentSystems as $key => $paymentSystemObject) : ?>
                    <div class="form-group __noicon input-group">
                        <?php
                        /* @var $paymentSystemObject \app\models\behaviors\payment\PaymentBase */
                        /* @var $paymentSystemObject \app\models\behaviors\payment\BitCoin */
                        /* @var $paymentSystemObject \app\models\behaviors\payment\PayPal */
                        echo $form->field($model, $model->getPSAttribuet($model->ups_attr_account, $key))->textInput([
                            'placeholder' => yii::$app->l->t('Введите счет {payment_system}', [
                                'payment_system' => $paymentSystemObject->title,
                                'update' => false,
                            ]),
                            'value' => $model->paymentStstem->getPaymentSystemAccount($key)
                        ])
                        ?>
                    </div>
                <?php endforeach; ?>
                <div class="form-group __ctrl">
                    <a href="#" onclick="yii.mes('todo')" class="">
                        <?= yii::$app->l->t('Продавать через аккаунт портала') ?>
                    </a>
                </div>
                <div class="form-group __ctrl">
                    <button class="button __small"><?= yii::$app->l->t('save') ?></button>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
</div>
