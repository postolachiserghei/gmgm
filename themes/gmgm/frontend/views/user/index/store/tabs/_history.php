<?php

use app\models\Transactions;
use app\components\extend\Html;
use app\components\extend\ActiveForm;
use app\components\widgets\uploader\UploaderWidget;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gamepage-lots-tablebox">
    <?php
    $transactions = Transactions::find()->where(['user_id' => yii::$app->user->id])->all();
    if ($transactions) {
        ?>
        <table>
            <tbody><tr>
                    <th>
                        <?= yii::$app->l->t('Информация о лоте') ?>
                    </th>
                    <th>
                        <?= yii::$app->l->t('Количество') ?>
                    </th>
                    <th>
                        <?= yii::$app->l->t('Цена') ?>
                    </th>
                    <th>
                        <?= yii::$app->l->t('Статус заказа') ?>
                    </th>
                </tr>
                <?php
                foreach ($transactions as $transaction) :
                    $model = $transaction->model;
                    /* @var $model \app\components\extend\Model */
                    /* @var $transaction \app\models\Transactions */
                    ?>
                    <tr>
                        <td>
                            <div class="gamepage-lots-info">
                                <div class="gamepage-lots-info-title">
                                    <span>
                                        <?= $model->title ?>
                                    </span>
                                </div>
                                <div class="gamepage-lots-info-box">
                                    <?= $model->renderImage(['width' => 105, 'height' => 80]); ?>
                                    <div class="gamepage-lots-info-text">
                                        <span>
                                            <?= $model->category->title ?>
                                        </span>
                                        <br>
                                        <span>
                                            <?= $model->type ?>
                                        </span>
                                    </div>
                                </div>
                                <a class="gamepage-lots-info-showmore">
                                    <i class="icn icn-arrow-down"></i><?= yii::$app->l->t('Show description') ?>
                                </a>
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-value-box">
                                <small>
                                    <?= yii::$app->l->t('куплено') ?>:
                                </small>
                                <br>
                                <strong>
                                    <?= $transaction->quantity ?>
                                </strong>
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-status-box">
                                <span class="gamepage-lots-price-value">
                                    <?= $transaction->getAmount() ?>
                                </span>
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-value-box">
                                <small>
                                    <?php
                                    if ($transaction->status === $transaction::STATUS_NEW) {
                                        echo Html::a(yii::$app->l->t('оплатить'), ['/payment/index'],['class'=>'button __small']);
                                    } else {
                                        echo $transaction->getStatusLabels($transaction->status);
                                    }
                                    ?>
                                </small>
                                <br>
                                <strong>
                                    <?= yii::$app->formatter->asDate($transaction->created_at) ?>
                                </strong>
                            </div>
                        </td>
                    </tr>
                    <tr class="gamepage-lots-table-moreinfo">
                        <td colspan="4">
                            <?= $model->description ?>
                        </td>
                    </tr>
                    <tr class="gamepage-lots-table-ctrl">
                        <td colspan="4">
                            <a onclick="yii.mes('todo')" href="#" class="button __small"><i class="icn icn-chat-white" ></i>Открыть чат</a>
                            <a onclick="yii.mes('todo')" href="#" class="button __small __orange"><i class="icn icn-comment-white"></i>Оставить отзыв</a>
                        </td>
                    </tr>

                    <?php
                endforeach;
                ?>
            </tbody>
        </table>
        <?php
    } else {
        ?>
        <div class="alert alert-warning">
            <?= yii::$app->l->t('no results') ?>
        </div>
        <?php
    }
    ?>
</div>
