<?php

use app\components\extend\Html;
use app\components\extend\ListView;
use app\components\extend\ActiveForm;
use app\components\widgets\uploader\UploaderWidget;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
/* @var $lot \app\models\search\ProductsSearch */
?>
<div class="spv-edittab-wrap">
    <div class="mp-sect-title">
        <?= Html::tag('h2', yii::$app->l->t('Подать обьявление')) ?>
    </div>
    <div class="spv-addlot-box">
        <div class="row">
            <?php $form = ActiveForm::begin(); ?>

            <div class="colx-xs-12 col-sm-6">
                <div class="form-group">
                    <?= $form->field($lot, 'category_id')->dropDownList($lot->getAvailableCategories()); ?>
                </div>
                <div class="form-group">
                    <?= $form->field($lot, 'type')->dropDownList($lot->getAvailableTypes()); ?>
                </div>
                <div class="form-group">
                    <?= $form->field($lot, 'title')->textInput(); ?>
                </div>
            </div>

            <div class="colx-xs-12 col-sm-6">
                <div class="form-group">
                    <?= $form->field($lot, 'price')->textInput(['type' => 'number']); ?>
                </div>
                <div class="form-group">
                    <?= $form->field($lot, 'available')->textInput(['type' => 'number']); ?>
                </div>
                <div class="form-group">
                    <?= $form->field($lot, $lot::ADDITIONAL_DATA_DELIVERY_IN_DAYS)->textInput(['type' => 'number']); ?>
                </div>
            </div>


            <div class="col-xs-12 __width">
                <?= $form->field($lot, 'description')->textarea(['class' => '__width']); ?>
            </div>

            <div class="colx-xs-12 col-sm-4">
                <div class="form-group __files __nomargin">
                    <?=
                    $form->field($lot, 'image')->widget(UploaderWidget::className(), [
                        'template' => UploaderWidget::TEMPLATE_COMPANY_LOGO
                    ]);
                    ?>
                </div>
            </div>
            <div class="colx-xs-12 col-sm-6">
                <div class="form-group __tar __checkbox __nomargin">
                    <?= $form->field($lot, 'status')->checkbox(['label' => false])->label(yii::$app->l->t('Подтверджение обьявления')); ?>
                </div>
            </div>


            <div class="colx-xs-12 col-sm-6">
                <div class="form-group __ctrl">
                    <?= Html::submitButton(yii::$app->l->t('save'), ['class' => 'button __small']) ?>
                </div>
            </div>


            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>


<div class="gamepage-lots-tablebox">

    <div>
        <div class="col-md-3 th">
            <?= yii::$app->l->t('Информация о лоте') ?>
        </div>
        <div class="col-md-3 th">
            <?= yii::$app->l->t('Доставка') ?>
        </div>
        <div class="col-md-3 th">
            <?= yii::$app->l->t('Количество') ?>
        </div>
        <div class="col-md-3 th">
            <?= yii::$app->l->t('Цена') ?>
        </div>

    </div>
    <?=
    ListView::widget([
        'dataProvider' => $lot->getUserProducts($model->id),
        'itemView' => '_lots/_item',
        'layout' => '{items}{pager}'
    ])
    ?>

</div>