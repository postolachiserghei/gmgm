<?php

use app\components\extend\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $lot \app\models\search\ProductsSearch */

$tabs = [
    'profile' => [
        'label' => yii::$app->l->t('Редактирование профиля'),
        'content' => 'tabs/_edit_profile'
    ],
    'lot' => [
        'label' => yii::$app->l->t('Добавить лот'),
        'content' => 'tabs/_lots'
    ],
    'history' => [
        'label' => yii::$app->l->t('История заказов'),
        'content' => 'tabs/_history'
    ],
    'finance' => [
        'label' => yii::$app->l->t('Финотчетность'),
        'content' => 'tabs/_finance'
    ],
    'orders' => [
        'label' => yii::$app->l->t('Заказ услуг'),
        'content' => 'tabs/_orders'
    ]
];
?>
<div class="col-xs-12 col-md-9">
    <div class="spv-tabs-wrap">
        <ul>
            <?php
            foreach ($tabs as $t => $tabinfo) {
                $link = Html::a(Html::tag('span', $tabinfo['label']), ['/user/index', 'tab' => $t]);
                echo Html::tag('li', $link, ['class' => (($tab) == $t ? 'active' : '')]);
            }
            ?>
        </ul>
    </div>
    <?=
    $this->render($tabs[$tab]['content'], [
        'model' => $model,
        'lot' => $lot,
        'tab' => $tab
    ]);
    ?>
</div>