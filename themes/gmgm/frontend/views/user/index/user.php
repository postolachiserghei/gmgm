<?php

use app\models\Menu;
use app\components\widgets\menu\MenuWidget;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
/* @var $lot \app\models\search\ProductsSearch */
?>


<div class="row">
    <div class="col-xs-12 col-md-3">
        <div class="bpv-privacy-wrap">
            <p>
                <?= yii::$app->l->t('Это приватная информация, её видите только вы и администратор портала') ?>
            </p>
        </div>

        <?=
        $this->render('user/_edit_profile', [
            'model' => $model
        ]);
        ?>
        <?= MenuWidget::widget(['type' => Menu::TYPE_HOW_TO, 'page' => MenuWidget::PAGE_GAME]); ?>
    </div>
    <div class="col-xs-12 col-md-9">
        <div class="bip-favstore-wrap">
            <div class="row">
                <div class="col-xs-12">
                    <div class="mp-sect-title">
                        <h2>
                            Избранные магазины
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-4">
                    <div class="bip-favstore-point">
                        <div class="bip-favstore-top-box">
                            <a href="#" class="delete-item">
                                <i class="icn icn-delete"></i>
                            </a>
                            <div class="bip-favstore-top-avatar">
                                <div class="user-avatar" style="background-image: url(/public/gmgm/img/user.png);"></div>
                            </div>
                            <div class="bip-favstore-top-info">
                                <div class="bip-favstore-top-infoline">
                                    <strong>
                                        %Username%
                                    </strong>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Уровень: 2 <i class="icn icn-level-2"></i>
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Сделок: 14
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Лотов: 18
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span class="__alert">
                                        Жалоб: 5
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-4">
                    <div class="bip-favstore-point">
                        <div class="bip-favstore-top-box">
                            <a href="#" class="delete-item">
                                <i class="icn icn-delete"></i>
                            </a>
                            <div class="bip-favstore-top-avatar">
                                <div class="user-avatar" style="background-image: url(/public/gmgm/img/user.png);"></div>
                            </div>
                            <div class="bip-favstore-top-info">
                                <div class="bip-favstore-top-infoline">
                                    <strong>
                                        %Username%
                                    </strong>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Уровень: 2 <i class="icn icn-level-2"></i>
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Сделок: 14
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Лотов: 18
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span class="__alert">
                                        Жалоб: 5
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-4">
                    <div class="bip-favstore-point">
                        <div class="bip-favstore-top-box">
                            <a href="#" class="delete-item">
                                <i class="icn icn-delete"></i>
                            </a>
                            <div class="bip-favstore-top-avatar">
                                <div class="user-avatar" style="background-image: url(/public/gmgm/img/user.png);"></div>
                            </div>
                            <div class="bip-favstore-top-info">
                                <div class="bip-favstore-top-infoline">
                                    <strong>
                                        %Username%
                                    </strong>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Уровень: 2 <i class="icn icn-level-2"></i>
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Сделок: 14
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Лотов: 18
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span class="__alert">
                                        Жалоб: 5
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-4">
                    <div class="bip-favstore-point">
                        <div class="bip-favstore-top-box">
                            <a href="#" class="delete-item">
                                <i class="icn icn-delete"></i>
                            </a>
                            <div class="bip-favstore-top-avatar">
                                <div class="user-avatar" style="background-image: url(/public/gmgm/img/user.png);"></div>
                            </div>
                            <div class="bip-favstore-top-info">
                                <div class="bip-favstore-top-infoline">
                                    <strong>
                                        %Username%
                                    </strong>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Уровень: 2 <i class="icn icn-level-2"></i>
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Сделок: 14
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Лотов: 18
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span class="__alert">
                                        Жалоб: 5
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-4">
                    <div class="bip-favstore-point">
                        <div class="bip-favstore-top-box">
                            <a href="#" class="delete-item">
                                <i class="icn icn-delete"></i>
                            </a>
                            <div class="bip-favstore-top-avatar">
                                <div class="user-avatar" style="background-image: url(/public/gmgm/img/user.png);"></div>
                            </div>
                            <div class="bip-favstore-top-info">
                                <div class="bip-favstore-top-infoline">
                                    <strong>
                                        %Username%
                                    </strong>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Уровень: 2 <i class="icn icn-level-2"></i>
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Сделок: 14
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Лотов: 18
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span class="__alert">
                                        Жалоб: 5
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-4">
                    <div class="bip-favstore-point">
                        <div class="bip-favstore-top-box">
                            <a href="#" class="delete-item">
                                <i class="icn icn-delete"></i>
                            </a>
                            <div class="bip-favstore-top-avatar">
                                <div class="user-avatar" style="background-image: url(/public/gmgm/img/user.png);"></div>
                            </div>
                            <div class="bip-favstore-top-info">
                                <div class="bip-favstore-top-infoline">
                                    <strong>
                                        %Username%
                                    </strong>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Уровень: 2 <i class="icn icn-level-2"></i>
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Сделок: 14
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span>
                                        Лотов: 18
                                    </span>
                                </div>
                                <div class="bip-favstore-top-infoline">
                                    <span class="__alert">
                                        Жалоб: 5
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bip-history-wrap">
            <div class="row">
                <div class="col-xs-12">
                    <div class="mp-sect-title">
                        <h2>
                            История покупок
                        </h2>
                    </div>
                </div>
            </div>
            <table>
                <tbody><tr>
                        <th>
                            Продавец
                        </th>
                        <th>
                            Информация о лоте
                        </th>
                        <th>
                            Доставка
                        </th>
                        <th>
                            Количество
                        </th>
                        <th>
                            Цена
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <div class="gamepage-lots-saller-info">
                                <div class="gamepage-lots-saller-img">
                                    <div class="user-avatar" style="background-image: url(/public/gmgm/img/user.png)"></div>
                                </div>
                                <div class="gamepage-lots-saller-text">
                                    <strong>
                                        Alex3366
                                    </strong>
                                    <br>
                                    <span>
                                        Уровень: 2 <i class="icn icn-level-2"></i>
                                    </span>
                                    <br>
                                    <span>
                                        Сделок: 15
                                    </span>
                                    <br>
                                    <span>
                                        Отзывов: 15
                                    </span>
                                    <br>
                                    <span>
                                        <i class="icn icn-star-full"></i>
                                        <i class="icn icn-star-full"></i>
                                        <i class="icn icn-star-half"></i>
                                        <i class="icn icn-star-empty"></i>
                                        <i class="icn icn-star-empty"></i>
                                    </span>
                                    <br>
                                    <span>
                                        Жалоб: 5
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-info">
                                <div class="gamepage-lots-info-title">
                                    <span>
                                        Fure Boost
                                    </span>
                                </div>
                                <div class="gamepage-lots-info-box">
                                    <div class="gamepage-lots-info-img" style="background-image: url(/public/gmgm/img/tamp.jpeg)"></div>
                                    <div class="gamepage-lots-info-text">
                                        <span>
                                            Dota 2
                                        </span>
                                        <br>
                                        <span>
                                            тип лота
                                        </span>
                                        <br>
                                        <span>
                                            подтип лота
                                        </span>
                                    </div>
                                </div>
                                <a class="gamepage-lots-info-showmore">
                                    <i class="icn icn-arrow-down"></i>Show description
                                </a>
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-delivery-box">
                                1 час
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-value-box">
                                <div class="gamepage-lots-setvalue-wrap">
                                    <span class="gamepage-lots-setvalue __minus">-</span>
                                    <input type="text" value="0">
                                    <span class="gamepage-lots-setvalue __plus">+</span>
                                </div>
                                <br>
                                <small>
                                    Доступно:
                                </small>
                                <br>
                                <strong>
                                    160 000 00
                                </strong>
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-price-box">
                                <span class="gamepage-lots-price-value">
                                    <strong>350.26</strong>$
                                </span>
                                <br>
                                <button>
                                    купить
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr class="gamepage-lots-table-moreinfo">
                        <td colspan="5">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus, reprehenderit id cupiditate, ipsam porro omnis! Maxime sit ipsum omnis, officia mollitia! Eaque hic repudiandae dignissimos distinctio veritatis. Harum voluptates atque debitis corporis maiores, incidunt doloribus ab vel iure adipisci, deleniti dicta, soluta? Doloremque odit sequi excepturi voluptates nostrum beatae repellat numquam, ipsum explicabo dolorem quas rerum reiciendis aut vel tempore non. Eveniet id ad pariatur eum ipsum nesciunt, assumenda distinctio ea voluptates et deserunt ut, dolorem, maxime eos excepturi quaerat incidunt quisquam neque! Perferendis harum, magni! Nulla vero impedit eius dolor totam, facere, voluptate earum officiis accusamus cumque quidem cum tempore, doloribus! Inventore impedit nulla nobis fugiat quasi voluptas possimus, iste incidunt deserunt repellat quibusdam, qui veniam illo saepe doloribus expedita ducimus corporis similique aut magni placeat nemo architecto. Illum, voluptatum, sapiente veniam nostrum atque quasi deleniti, quia unde saepe, quo nulla. Quas, aut. Nisi inventore dicta in illum molestiae.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="gamepage-lots-saller-info">
                                <div class="gamepage-lots-saller-img">
                                    <div class="user-avatar" style="background-image: url(/public/gmgm/img/user.png)"></div>
                                </div>
                                <div class="gamepage-lots-saller-text">
                                    <strong>
                                        Alex3366
                                    </strong>
                                    <br>
                                    <span>
                                        Уровень: 2 <i class="icn icn-level-2"></i>
                                    </span>
                                    <br>
                                    <span>
                                        Сделок: 15
                                    </span>
                                    <br>
                                    <span>
                                        Отзывов: 15
                                    </span>
                                    <br>
                                    <span>
                                        <i class="icn icn-star-full"></i>
                                        <i class="icn icn-star-full"></i>
                                        <i class="icn icn-star-half"></i>
                                        <i class="icn icn-star-empty"></i>
                                        <i class="icn icn-star-empty"></i>
                                    </span>
                                    <br>
                                    <span>
                                        Жалоб: 5
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-info">
                                <div class="gamepage-lots-info-title">
                                    <span>
                                        Fure Boost
                                    </span>
                                </div>
                                <div class="gamepage-lots-info-box">
                                    <div class="gamepage-lots-info-img" style="background-image: url(/public/gmgm/img/tamp.jpeg)"></div>
                                    <div class="gamepage-lots-info-text">
                                        <span>
                                            Dota 2
                                        </span>
                                        <br>
                                        <span>
                                            тип лота
                                        </span>
                                        <br>
                                        <span>
                                            подтип лота
                                        </span>
                                    </div>
                                </div>
                                <a class="gamepage-lots-info-showmore">
                                    <i class="icn icn-arrow-down"></i>Show description
                                </a>
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-delivery-box">
                                1 час
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-value-box">
                                <div class="gamepage-lots-setvalue-wrap">
                                    <span class="gamepage-lots-setvalue __minus">-</span>
                                    <input type="text" value="0">
                                    <span class="gamepage-lots-setvalue __plus">+</span>
                                </div>
                                <br>
                                <small>
                                    Доступно:
                                </small>
                                <br>
                                <strong>
                                    160 000 00
                                </strong>
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-price-box">
                                <span class="gamepage-lots-price-value">
                                    <strong>350.26</strong>$
                                </span>
                                <br>
                                <button>
                                    купить
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr class="gamepage-lots-table-moreinfo">
                        <td colspan="5">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus, reprehenderit id cupiditate, ipsam porro omnis! Maxime sit ipsum omnis, officia mollitia! Eaque hic repudiandae dignissimos distinctio veritatis. Harum voluptates atque debitis corporis maiores, incidunt doloribus ab vel iure adipisci, deleniti dicta, soluta? Doloremque odit sequi excepturi voluptates nostrum beatae repellat numquam, ipsum explicabo dolorem quas rerum reiciendis aut vel tempore non. Eveniet id ad pariatur eum ipsum nesciunt, assumenda distinctio ea voluptates et deserunt ut, dolorem, maxime eos excepturi quaerat incidunt quisquam neque! Perferendis harum, magni! Nulla vero impedit eius dolor totam, facere, voluptate earum officiis accusamus cumque quidem cum tempore, doloribus! Inventore impedit nulla nobis fugiat quasi voluptas possimus, iste incidunt deserunt repellat quibusdam, qui veniam illo saepe doloribus expedita ducimus corporis similique aut magni placeat nemo architecto. Illum, voluptatum, sapiente veniam nostrum atque quasi deleniti, quia unde saepe, quo nulla. Quas, aut. Nisi inventore dicta in illum molestiae.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="gamepage-lots-saller-info">
                                <div class="gamepage-lots-saller-img">
                                    <div class="user-avatar" style="background-image: url(/public/gmgm/img/user.png)"></div>
                                </div>
                                <div class="gamepage-lots-saller-text">
                                    <strong>
                                        Alex3366
                                    </strong>
                                    <br>
                                    <span>
                                        Уровень: 2 <i class="icn icn-level-2"></i>
                                    </span>
                                    <br>
                                    <span>
                                        Сделок: 15
                                    </span>
                                    <br>
                                    <span>
                                        Отзывов: 15
                                    </span>
                                    <br>
                                    <span>
                                        <i class="icn icn-star-full"></i>
                                        <i class="icn icn-star-full"></i>
                                        <i class="icn icn-star-half"></i>
                                        <i class="icn icn-star-empty"></i>
                                        <i class="icn icn-star-empty"></i>
                                    </span>
                                    <br>
                                    <span>
                                        Жалоб: 5
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-info">
                                <div class="gamepage-lots-info-title">
                                    <span>
                                        Fure Boost
                                    </span>
                                </div>
                                <div class="gamepage-lots-info-box">
                                    <div class="gamepage-lots-info-img" style="background-image: url(/public/gmgm/img/tamp.jpeg)"></div>
                                    <div class="gamepage-lots-info-text">
                                        <span>
                                            Dota 2
                                        </span>
                                        <br>
                                        <span>
                                            тип лота
                                        </span>
                                        <br>
                                        <span>
                                            подтип лота
                                        </span>
                                    </div>
                                </div>
                                <a class="gamepage-lots-info-showmore">
                                    <i class="icn icn-arrow-down"></i>Show description
                                </a>
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-delivery-box">
                                1 час
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-value-box">
                                <div class="gamepage-lots-setvalue-wrap">
                                    <span class="gamepage-lots-setvalue __minus">-</span>
                                    <input type="text" value="0">
                                    <span class="gamepage-lots-setvalue __plus">+</span>
                                </div>
                                <br>
                                <small>
                                    Доступно:
                                </small>
                                <br>
                                <strong>
                                    160 000 00
                                </strong>
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-price-box">
                                <span class="gamepage-lots-price-value">
                                    <strong>350.26</strong>$
                                </span>
                                <br>
                                <button>
                                    купить
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr class="gamepage-lots-table-moreinfo">
                        <td colspan="5">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus, reprehenderit id cupiditate, ipsam porro omnis! Maxime sit ipsum omnis, officia mollitia! Eaque hic repudiandae dignissimos distinctio veritatis. Harum voluptates atque debitis corporis maiores, incidunt doloribus ab vel iure adipisci, deleniti dicta, soluta? Doloremque odit sequi excepturi voluptates nostrum beatae repellat numquam, ipsum explicabo dolorem quas rerum reiciendis aut vel tempore non. Eveniet id ad pariatur eum ipsum nesciunt, assumenda distinctio ea voluptates et deserunt ut, dolorem, maxime eos excepturi quaerat incidunt quisquam neque! Perferendis harum, magni! Nulla vero impedit eius dolor totam, facere, voluptate earum officiis accusamus cumque quidem cum tempore, doloribus! Inventore impedit nulla nobis fugiat quasi voluptas possimus, iste incidunt deserunt repellat quibusdam, qui veniam illo saepe doloribus expedita ducimus corporis similique aut magni placeat nemo architecto. Illum, voluptatum, sapiente veniam nostrum atque quasi deleniti, quia unde saepe, quo nulla. Quas, aut. Nisi inventore dicta in illum molestiae.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="gamepage-lots-saller-info">
                                <div class="gamepage-lots-saller-img">
                                    <div class="user-avatar" style="background-image: url(/public/gmgm/img/user.png)"></div>
                                </div>
                                <div class="gamepage-lots-saller-text">
                                    <strong>
                                        Alex3366
                                    </strong>
                                    <br>
                                    <span>
                                        Уровень: 2 <i class="icn icn-level-2"></i>
                                    </span>
                                    <br>
                                    <span>
                                        Сделок: 15
                                    </span>
                                    <br>
                                    <span>
                                        Отзывов: 15
                                    </span>
                                    <br>
                                    <span>
                                        <i class="icn icn-star-full"></i>
                                        <i class="icn icn-star-full"></i>
                                        <i class="icn icn-star-half"></i>
                                        <i class="icn icn-star-empty"></i>
                                        <i class="icn icn-star-empty"></i>
                                    </span>
                                    <br>
                                    <span>
                                        Жалоб: 5
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-info">
                                <div class="gamepage-lots-info-title">
                                    <span>
                                        Fure Boost
                                    </span>
                                </div>
                                <div class="gamepage-lots-info-box">
                                    <div class="gamepage-lots-info-img" style="background-image: url(/public/gmgm/img/tamp.jpeg)"></div>
                                    <div class="gamepage-lots-info-text">
                                        <span>
                                            Dota 2
                                        </span>
                                        <br>
                                        <span>
                                            тип лота
                                        </span>
                                        <br>
                                        <span>
                                            подтип лота
                                        </span>
                                    </div>
                                </div>
                                <a class="gamepage-lots-info-showmore">
                                    <i class="icn icn-arrow-down"></i>Show description
                                </a>
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-delivery-box">
                                1 час
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-value-box">
                                <div class="gamepage-lots-setvalue-wrap">
                                    <span class="gamepage-lots-setvalue __minus">-</span>
                                    <input type="text" value="0">
                                    <span class="gamepage-lots-setvalue __plus">+</span>
                                </div>
                                <br>
                                <small>
                                    Доступно:
                                </small>
                                <br>
                                <strong>
                                    160 000 00
                                </strong>
                            </div>
                        </td>
                        <td>
                            <div class="gamepage-lots-price-box">
                                <span class="gamepage-lots-price-value">
                                    <strong>350.26</strong>$
                                </span>
                                <br>
                                <button>
                                    купить
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr class="gamepage-lots-table-moreinfo">
                        <td colspan="5">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus, reprehenderit id cupiditate, ipsam porro omnis! Maxime sit ipsum omnis, officia mollitia! Eaque hic repudiandae dignissimos distinctio veritatis. Harum voluptates atque debitis corporis maiores, incidunt doloribus ab vel iure adipisci, deleniti dicta, soluta? Doloremque odit sequi excepturi voluptates nostrum beatae repellat numquam, ipsum explicabo dolorem quas rerum reiciendis aut vel tempore non. Eveniet id ad pariatur eum ipsum nesciunt, assumenda distinctio ea voluptates et deserunt ut, dolorem, maxime eos excepturi quaerat incidunt quisquam neque! Perferendis harum, magni! Nulla vero impedit eius dolor totam, facere, voluptate earum officiis accusamus cumque quidem cum tempore, doloribus! Inventore impedit nulla nobis fugiat quasi voluptas possimus, iste incidunt deserunt repellat quibusdam, qui veniam illo saepe doloribus expedita ducimus corporis similique aut magni placeat nemo architecto. Illum, voluptatum, sapiente veniam nostrum atque quasi deleniti, quia unde saepe, quo nulla. Quas, aut. Nisi inventore dicta in illum molestiae.
                        </td>
                    </tr>
                </tbody></table>
        </div>
        <div class="sp-review-wrap">
            <div class="row">
                <div class="col-xs-12">
                    <div class="mp-sect-title">
                        <h2>
                            Отзывы о покупателе
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="mp-substore-review-item">
                        <div class="mp-substore-review-text __noava">
                            <div class="mp-substore-review-title">
                                <span>
                                    Игра:
                                </span>
                                <a href="#">
                                    Линейка
                                </a>
                                <span>
                                    Куплено:
                                </span>
                                <strong>
                                    Fuse Boost
                                </strong>
                            </div>
                            <div class="mp-substore-review-info">
                                <span>
                                    Продавец:
                                </span>
                                <a href="#">
                                    Nickman
                                </a>
                                <span class="saler-rates-wrap">
                                    <i class="icn icn-star-full"></i>
                                    <i class="icn icn-star-full"></i>
                                    <i class="icn icn-star-full"></i>
                                    <i class="icn icn-star-empty"></i>
                                    <i class="icn icn-star-empty"></i>
                                </span>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="mp-substore-review-item">
                        <div class="mp-substore-review-text __noava">
                            <div class="mp-substore-review-title">
                                <span>
                                    Игра:
                                </span>
                                <a href="#">
                                    Линейка
                                </a>
                                <span>
                                    Куплено:
                                </span>
                                <strong>
                                    Fuse Boost
                                </strong>
                            </div>
                            <div class="mp-substore-review-info">
                                <span>
                                    Продавец:
                                </span>
                                <a href="#">
                                    Nickman
                                </a>
                                <span class="saler-rates-wrap">
                                    <i class="icn icn-star-full"></i>
                                    <i class="icn icn-star-full"></i>
                                    <i class="icn icn-star-full"></i>
                                    <i class="icn icn-star-empty"></i>
                                    <i class="icn icn-star-empty"></i>
                                </span>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="mp-substore-review-item">
                        <div class="mp-substore-review-text __noava">
                            <div class="mp-substore-review-title">
                                <span>
                                    Игра:
                                </span>
                                <a href="#">
                                    Линейка
                                </a>
                                <span>
                                    Куплено:
                                </span>
                                <strong>
                                    Fuse Boost
                                </strong>
                            </div>
                            <div class="mp-substore-review-info">
                                <span>
                                    Продавец:
                                </span>
                                <a href="#">
                                    Nickman
                                </a>
                                <span class="saler-rates-wrap">
                                    <i class="icn icn-star-full"></i>
                                    <i class="icn icn-star-full"></i>
                                    <i class="icn icn-star-full"></i>
                                    <i class="icn icn-star-empty"></i>
                                    <i class="icn icn-star-empty"></i>
                                </span>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="mp-substore-review-item">
                        <div class="mp-substore-review-text __noava">
                            <div class="mp-substore-review-title">
                                <span>
                                    Игра:
                                </span>
                                <a href="#">
                                    Линейка
                                </a>
                                <span>
                                    Куплено:
                                </span>
                                <strong>
                                    Fuse Boost
                                </strong>
                            </div>
                            <div class="mp-substore-review-info">
                                <span>
                                    Продавец:
                                </span>
                                <a href="#">
                                    Nickman
                                </a>
                                <span class="saler-rates-wrap">
                                    <i class="icn icn-star-full"></i>
                                    <i class="icn icn-star-full"></i>
                                    <i class="icn icn-star-full"></i>
                                    <i class="icn icn-star-empty"></i>
                                    <i class="icn icn-star-empty"></i>
                                </span>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagination-wrap">
                <ul>
                    <li>
                        <a href="#">
                            <span class="icn icn-arrow-left"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            1
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            2
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            3
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            4
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            5
                        </a>
                    </li>
                    <li>
                        <a href="#" class="active">
                            6
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            7
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            8
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            9
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            10
                        </a>
                    </li>
                    <li>
                        ...
                    </li>
                    <li>
                        <a href="#">
                            320
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="icn icn-arrow-right"></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>