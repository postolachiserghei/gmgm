<?php

use app\models\User;
use app\components\extend\Html;
use app\components\extend\ActiveForm;
use app\components\widgets\uploader\UploaderWidget;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(); ?>
<div class="bpv-infoform-wrap">
    <div class="bpv-infoform-box">
        <div class="form-group __files">
            <?=
            $form->field($model, 'avatar')->widget(UploaderWidget::className(), [
                'template' => UploaderWidget::TEMPLATE_CHANGE_AVATAR,
            ])
            ?>
        </div>

        <div class="form-group">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="form-group">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="form-group">
            <?= $form->field($model, $model::ADDITIONAL_DATA_FIO)->textInput(); ?>
        </div>
        <div class="form-group">
            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
        </div>
        <div class="form-group">
            <?= $form->field($model, $model::ADDITIONAL_DATA_PHONE)->textInput(); ?>
        </div>
        <div class="form-group __ctrl">
            <?= Html::submitButton(yii::$app->l->t('save'), ['class' => 'button __small']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>