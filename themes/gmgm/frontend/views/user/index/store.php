<?php

use app\models\Menu;
use app\models\File;
use app\components\extend\Html;
use app\components\widgets\menu\MenuWidget;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $lot \app\models\search\ProductsSearch */
?>

<section class="gamepage-top-wrap">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="gamepage-top-box">
                    <div class="gamepage-top-title">
                        <?= Html::tag('h3', $model->getFullName()); ?>
                    </div>
                    <div class="gamepage-top-info">
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <?=
                                $model->renderLogo([
                                    'class' => 'gamepage-top-info-img_',
                                    'size' => File::SIZE_LG,
                                    'bg-size' => 'contain',
                                ])
                                ?>
                            </div>
                            <div class="col-xs-12 col-md-8">
                                <div class="gamepage-top-info-text">
                                    <?= $model->getData($model::ADDITIONAL_DATA_COMPANY_INFO) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <?= $this->render('store/_user_block', ['model' => $model]); ?>
                <?= MenuWidget::widget(['type' => Menu::TYPE_HOW_TO, 'page' => MenuWidget::PAGE_GAME]); ?>
            </div>
            <?= $this->render('store/tabs', ['model' => $model, 'lot' => $lot, 'tab' => $tab]) ?>
        </div>
    </div>
</section>