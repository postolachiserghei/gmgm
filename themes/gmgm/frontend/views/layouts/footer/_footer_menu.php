<?php

use app\components\extend\Nav;
use app\models\Menu;
use app\components\widgets\menu\MenuWidget;
?>
<footer>
    <div class="footer-link-wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="footer-link-box">
                        <?= MenuWidget::widget(['type' => Menu::TYPE_FOOTER, 'params' => ['liOptions' => ['class' => 'footer-link-item']]]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-menu-wrap">
        <div class="container">
            <div class="row">
                <?= MenuWidget::widget(['type' => Menu::TYPE_FOOTER_LVL_2]); ?>
            </div>
        </div>
    </div>
    <div class="footer-copy-wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="footer-copy-point">
                        © <?= yii::$app->l->t('все права защищены {year}', ['year' => date('Y')]) ?> 
                    </div>
                    <div class="footer-copy-point">
                        <?= yii::$app->l->t('Подписаться наv новости'); ?> 
                    </div>
                    <div class="footer-copy-point">
                        <?= yii::$app->l->t('Registered Names And Trademarks Are The Copyright And Property Of Their Respective Owners') ?> 
                    </div>
                    <div class="footer-copy-point">
                        <?=
                        yii::$app->l->t('Designed By : {name}', [
                            'name' => 'Svitlyi'
                        ])
                        ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>