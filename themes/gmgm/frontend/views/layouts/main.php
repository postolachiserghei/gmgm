<?php

use app\components\extend\Html;
use app\components\extend\Pjax;
use app\assets\GmgmarketAssets;
use app\components\helper\Helper;

GmgmarketAssets::register($this);
/* @var $this app\components\extend\View */
/* @var $content string */
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode(Helper::data()->getParam('pageTitle', $this->title)) ?></title>
        <?php $this->head() ?>
    </head>
    <body data-notification-top-margin="0">
        <?php
        $this->beginBody();
        if (yii::$app->controller->isPjaxAction) {
            echo $this->render('_loading');
            Pjax::begin();
        }
        ?>
        <?= $this->render('header/_top_menu'); ?>
        <?= $this->render('header/_main_menu'); ?>
        <?= $this->render('header/_page_header'); ?>
        <?= Html::tag('div', $content, ['class' => 'container main_container']); ?>
        <?= $this->render('footer/_footer_menu'); ?>
        <?php
        if (yii::$app->controller->isPjaxAction) {
            Pjax::end();
        }
        ?>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage(); ?>
