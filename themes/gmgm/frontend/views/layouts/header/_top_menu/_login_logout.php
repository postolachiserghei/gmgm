<?php

use app\components\extend\Html;
use app\components\extend\Url;

/* @var $user \app\models\User */
/* @var $user \app\models\behaviors\FileSaveBehavior */
?>

<?php
if (yii::$app->user->isGuest) {
    ?>
    <?= Html::a('<i class="icn icn-login"></i><span>' . yii::$app->l->t('Вход') . '</span>', Url::to(['/site/login'])) ?>
    <?= Html::a('<i class="icn icn-register"></i><span>' . yii::$app->l->t('Регистрация') . '</span>', Url::to(['/site/signup'])) ?>

    <?php
} else {
    ?>
    <a href="<?= Url::to(['/user/index']) ?>">
        <div class="header-login-userimg">
            <div class="user-avatar" style="background-image: url(<?= $user->getFile('avatar')->getUrl(null, '/public/gmgm/img/user.png') ?>)"></div>
        </div>
        <span><?= $user->getFullName(); ?></span>
    </a>
    <a href="<?= Url::to(['/site/logout']) ?>">
        <i class="icn icn-logout"></i>
        <span><?= yii::$app->l->t('выход') ?></span>
    </a>
<?php }
?>