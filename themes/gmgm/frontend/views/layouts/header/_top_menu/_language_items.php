<?php

use app\components\extend\Html;

$languages = yii::$app->l->getMenuLanguages();

if (is_array($languages) && array_key_exists(0, $languages) && count($languages[0]) > 1) {
    foreach ($languages[0]['items'] as $l) {
        $a = '<a ' . (isset($l['linkOptions']) ? Html::renderTagAttributes($l['linkOptions']) : '') . ' href="' . $l['url'] . '">' . $l['label'] . '</a>';
        if (isset($getLinks)) {
            echo $a;
            continue;
        }
        echo Html::tag('li', $a);
    }
}
