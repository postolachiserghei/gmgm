<?php

use yii\widgets\Breadcrumbs;

$menuItems = isset($this->params['menu']) ? $this->params['menu'] : [];
?>
<div class="container">
    <div class="bread-wrap">
        <?php
        echo Breadcrumbs::widget([
            'options' => [
                'class' => 'bread-box'
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
        ]);
        ?>
    </div>
    <?= (isset($this->params['pageHeader']) ? $this->params['pageHeader'] : '') ?>
</div>