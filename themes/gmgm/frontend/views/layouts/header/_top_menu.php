<?php

use app\models\Menu;
use app\components\helper\Helper;
use app\components\widgets\menu\MenuWidget;
use app\components\widgets\chat\ChatWidget;
use app\components\widgets\shopping_cart\ShoppingCartWidget;

/* @var $user \app\models\User */
/* @var $user \app\models\behaviors\FileSaveBehavior */
$user = Helper::user()->identity();
$loginLogout = $this->render('_top_menu/_login_logout', ['user' => $user]);
?>

<section class="header-login-wrap">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="header-login-left hidden-sm hidden-xs">
                    <?= $loginLogout; ?>
                </div>
                <div class="header-login-right">
                    <a href="#" onclick="Chat.drawConversations(null,true);">
                        <i class="icn icn-chat"></i>
                        <span class="hidden-xs">Чат</span>
                        <span class="chat-counter notif-count hidden">
                            0
                        </span>
                    </a>
                    <a href="#">
                        <i class="icn icn-favorite"></i>
                        <span class="hidden-xs">избранное</span>
                        <span class="notif-count">
                            5
                        </span>
                    </a>
                    <?php
                    if (yii::$app->l->multi) {
                        ?>
                        <div class="dropdown hidden-xs hidden-sm">
                            <button class="dropdown-toggle" type="button" data-toggle="dropdown"><?= yii::$app->l->languages[yii::$app->language] ?>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <?= $this->render('_top_menu/_language_items', ['user' => $user]) ?>
                            </ul>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="mobile-hamburger-toggle">
                        <div class="mobile-hamburger-bar-wrap">
                            <div class="mobile-hamburger-bar bar-1"></div>
                            <div class="mobile-hamburger-bar bar-2"></div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>
<?= ChatWidget::widget(); ?>
<?= ShoppingCartWidget::widget(); ?>


<div class="mobile-hamburger-menu">
    <ul>
        <li>
            <?= $this->render('_top_menu/_social_links', ['user' => $user, 'getLinks' => true]) ?>
        </li>
        <li>
            <?= $loginLogout; ?>
        </li>
        <?= MenuWidget::widget(['type' => Menu::TYPE_ASIDE]); ?>
        <li>
            <?= $this->render('_top_menu/_language_items', ['user' => $user, 'getLinks' => true]) ?>
        </li>
    </ul>
</div>