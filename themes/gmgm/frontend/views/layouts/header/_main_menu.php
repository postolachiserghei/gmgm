<?php

use app\models\Menu;
use app\components\extend\Url;
use app\components\extend\Html;
use app\components\widgets\menu\MenuWidget;
use app\components\widgets\games\GamesWidget;
?>

<header>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-3">
                <?=
                Html::a(Html::img('/public/gmgm/img/logo.png', [
                    'alt="logo"', 'class' => 'header-logo-img', 'width' => '262', 'height' => '140', 'bg-size' => 'inherit'
                ]), (yii::$app->l->default == yii::$app->language ? '/' : '/' . yii::$app->language));
                ?>
            </div>
            <?= GamesWidget::widget(['type' => GamesWidget::TYPE_MAIN_FILTER]); ?>
            <div class="header-right-menu-wrap hidden-xs hidden-sm">
                <ul>
                    <?= MenuWidget::widget(['type' => Menu::TYPE_ASIDE]) ?>
                </ul>
            </div>
        </div>
    </div>
</header>