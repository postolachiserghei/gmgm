<?php

use app\components\extend\yii;
use app\components\extend\Html;

/* @var $this yii\web\View */
/* @var $transaction app\models\Transactions */
/* @var $model app\models\behaviors\PaymentBehavior */

$this->title = '';
$this->params['breadcrumbs'][] = $model->title;
?>



<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?= Html::tag('h1', yii::$app->l->t('payment transaction response')); ?>
            <?= $transaction->getData($transaction::ADDITIONAL_DATA_HISTORY); ?>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <?=
                    yii::$app->l->t('payment response for transaction Nr.{transaction_nr}', [
                        'transaction_nr' => $transaction->primarykey
                    ])
                    ?>
                </div>
                <div class="panel-body">
                    <div class="text-info">
                        <b><?= yii::$app->l->t('item') ?></b>: <?= $model->title ?>
                    </div>
                    <br/>
                    <div class="text-info">
                        <b><?= yii::$app->l->t('amount') ?></b>: <?= $transaction->getAmount() ?>
                    </div>
                    <br/>
                    <div class="text-info">
                        <b><?= yii::$app->l->t('quantity') ?></b>: <?= $transaction->quantity ?>
                    </div>
                    <br/>
                    <div class="text-info">
                        <b><?= yii::$app->l->t('payment system') ?></b>: <?= $model->getPaymentSystemFromTransaction($transaction)->title ?> <?= $model->getPaymentSystemFromTransaction($transaction)->icon ?>
                    </div>
                    <br/>
                    <div class="text-<?= $transaction->status == $transaction::STATUS_PAID ? 'success' : 'warning' ?>">
                        <b><?= yii::$app->l->t('status') ?></b>: <?= $transaction->getStatusLabels($transaction->status) ?>
                    </div>
                    <hr/>
                    <?= Html::a(yii::$app->l->t('payment history'), ['/user/index', 'tab' => 'history'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        </div>
    </div>
</div>