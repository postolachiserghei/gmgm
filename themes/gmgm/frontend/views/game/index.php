<?php

use app\models\Menu;
use app\components\extend\Html;
use app\components\extend\ListView;
use app\components\widgets\menu\MenuWidget;
use app\components\widgets\games\GamesWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */
/* @var $dataProvider \app\components\extend\ActiveDataProvider */
/* @var $searchModel \app\models\search\CategoriesSearch */

$this->title = yii::$app->l->t('Все игры', ['update' => false]);

$this->params['breadcrumbs'][] = yii::$app->helper->data()->getParam('h1', yii::$app->l->t('Все игры'));
?>



<section class="gamepage-top-wrap">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="gamepage-top-box">
                    <div class="gamepage-top-title">
                        <?= Html::tag('h3', yii::$app->helper->data()->getParam('h1', yii::$app->l->t('Все игры'))) ?>
                    </div>
                    <div class="gamepage-top-info">
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="gamepage-top-info-img" style="background-image: url(/public/gmgm/img/tamp.jpeg);"></div>
                            </div>
                            <div class="col-xs-12 col-md-8">
                                <div class="gamepage-top-info-text">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-3">
            <div class="gp-how-to-wrap">
                <?= MenuWidget::widget(['type' => Menu::TYPE_HOW_TO, 'page' => MenuWidget::PAGE_GAME]) ?>
            </div>
            <?= GamesWidget::widget(['type' => GamesWidget::TYPE_TOP_GAME_PAGE]); ?>
        </div>
        <div class="col-xs-12 col-md-9">
            <?=
            ListView::widget([
                'layout' => '{items} <div class="pagination-wrap">{pager}</div>',
                'dataProvider' => $dataProvider,
                'itemView' => 'index/_item',
                'itemOptions' => function($model) {
                    return [
                        'tag' => 'div',
                        'class' => 'gp-games-box',
                    ];
                },
                'options' => [
                    'tag' => 'div',
                    'class' => 'gp-games-wrap'
                ],
                'pager' => [
                    'firstPageLabel' => false,
                    'lastPageLabel' => false,
                    'prevPageLabel' => '<span class="icn icn-arrow-left"></span>',
                    'nextPageLabel' => '<span class="icn icn-arrow-right"></span>',
                    'maxButtonCount' => 3,
                    'options' => [
                        'tag' => 'ul',
                        'class' => '',
                    ]
                ],
            ]);
            ?>
        </div>
    </div>
</div>