<?php

use app\components\extend\Html;
use app\components\extend\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */
?>

<div class="gamepage-lots-tablebox">

    <div>
        <div class="col-md-3 th">
            <?= yii::$app->l->t('Продавец') ?>
        </div>
        <div class="col-md-4 th">
            <?= yii::$app->l->t('Информация о лоте') ?>
        </div>
        <div class="col-md-1 th">
            <?= yii::$app->l->t('Доставка') ?>
        </div>
        <div class="col-md-2 th">
            <?= yii::$app->l->t('Количество') ?>
        </div>
        <div class="col-md-2 th">
            <?= yii::$app->l->t('Цена') ?>
        </div>

    </div>

    <?=
    ListView::widget([
        'dataProvider' => $model->getProducts(),
        'itemView' => '_lots/_item',
        'layout' => '{items}{pager}'
    ])
    ?>
</div>
