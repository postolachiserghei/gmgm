<?php

use app\models\Transactions;
use app\components\extend\Html;

/* @var $model \app\models\Products */
/* @var $model \app\models\behaviors\PaymentBehavior */
?>
<div class="row">
    <div class="col-md-3">
        <?=
        $this->render('_user_block', [
            'model' => $model->user
        ]);
        ?>
    </div>
    <div class="col-md-4">
        <div class="gamepage-lots-info">
            <div class="gamepage-lots-info-box">

                <div class="col-md-8">
                    <div class="gamepage-lots-info-title">
                        <span>
                            <?= $model->title; ?>
                        </span>
                    </div>
                    <?= $model->renderImage(); ?>
                </div>
                <div class="col-md-4">
                    <div class="gamepage-lots-info-text_">
                        <span>
                            <?= $model->category->title ?>
                        </span>
                        <br>
                        <span>
                            <?= $model->getLotType()->title ?>
                        </span>
                    </div>
                </div>
            </div>
            <a class="gamepage-lots-info-showmore" onclick="$('.product-description_<?= $model->id ?>').slideToggle()">
                <i class="icn icn-arrow-down"></i><?= yii::$app->l->t('Show description') ?>
            </a>
        </div>
    </div>
    <div class="col-md-1">
        <div class="gamepage-lots-delivery-box">
            <br/>
            <?= $model->getData($model::ADDITIONAL_DATA_DELIVERY_IN_DAYS); ?>
        </div>
    </div>
    <div class="col-md-2">
        <div class="gamepage-lots-value-box">
            <br/>
            <div class="gamepage-lots-setvalue-wrap" data-item='<?= $model->primaryKey ?>' data-max='<?= $model->getAvailable(); ?>'>
                <span class="gamepage-lots-setvalue __minus">-</span>
                <input onchange="ShoppingCart.setEl($(this)).changeQuantitty(<?= $model->primaryKey ?>, $(this).val())" type="text" value="1">
                <span class="gamepage-lots-setvalue __plus">+</span>
            </div>
            <br>
            <small>
                <?= yii::$app->l->t('items available') ?>:
            </small>
            <br>
            <strong>
                <?= $model->getAvailable(); ?>
            </strong>
        </div>
    </div>
    <div class="col-md-2">
        <div class="gamepage-lots-price-box">
            <br/>
            <span class="gamepage-lots-price-value">
                <?= $model->getPrice(true) ?>
            </span>
            <br>
            <?=
            Html::submitButton(yii::$app->l->t('купить'), [
                'class' => 'text-white',
                'onclick' => "ShoppingCart.setEl($(this)).add($model->primaryKey,'$model->encodedData');return false;",
            ]);
            ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div style="display: none" class="text-center product-description_<?= $model->id ?>">
            <?= $model->description ?>
        </div>
    </div>
</div>
