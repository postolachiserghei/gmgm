<?php

use app\models\Menu;
use app\components\extend\Html;
use app\components\widgets\menu\MenuWidget;
use app\components\widgets\games\GamesWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */

$this->title = $model->title;

$this->params['breadcrumbs'][] = yii::$app->helper->data()->getParam('h1', $model->title);
?>

<section class="gamepage-top-wrap">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="gamepage-top-box">
                    <div class="gamepage-top-title">
                        <h3>
                            <?= Html::tag('h1', yii::$app->helper->data()->getParam('h1', $model->title)); ?>
                        </h3>
                    </div>
                    <div class="gamepage-top-info">
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <?=
                                $model->renderImage([
                                    'width' => '359',
                                    'height' => '215',
                                ]);
                                ?>
                            </div>
                            <div class="col-xs-12 col-md-8">
                                <div class="gamepage-top-info-text">
                                    <?= $model->getData($model::ADDITIONAL_DATA_DESCRIPTION); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <div class="gp-how-to-wrap">
                    <?= MenuWidget::widget(['type' => Menu::TYPE_HOW_TO, 'page' => MenuWidget::PAGE_GAME]) ?>
                </div>
                <?= GamesWidget::widget(['type' => GamesWidget::TYPE_TOP_GAME_PAGE]) ?>
            </div>
            <div class="col-xs-12 col-md-9">
                <?= $this->render('view/_filter', ['model' => $model]); ?>
                <?= $this->render('view/_lots', ['model' => $model]); ?>
            </div>
        </div>
    </div>
</section>