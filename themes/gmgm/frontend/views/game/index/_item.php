<?php

use app\components\extend\Html;

/* @var $model \app\models\Categories */
?>

<?=
$model->renderImage([
    'width' => '230',
    'height' => '140',
    'class' => 'gp-games-box-img'
])
?>
<div class="gp-games-box-name">
    <?= $model->title; ?>
</div>
<div class="gp-games-box-info">
    <div class="gp-games-box-info-inner">
        <span>
            <?= $model->getCountProducts(); ?>
        </span>
        <br>
        <span>
            <?= yii::$app->l->t('Offers') ?>
        </span>
        <br>
        <?= Html::a(yii::$app->l->t('view'), ['/game/view', 'id' => $model->primarykey], ['class' => 'button __orange __small']) ?>
    </div>
</div>